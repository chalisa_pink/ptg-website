
/* ----------------- Start Document ----------------- */
$(document).ready(function(){
	if(document.getElementById("map") !== null){
	
		// Touch Gestures
		if ( $('#map').attr('data-map-scroll') == 'true' || $(window).width() < 992 ) {
			var scrollEnabled = false;
		} else {
			var scrollEnabled = true;
		}
	
		var mapOptions = {
			gestureHandling: scrollEnabled,
		}
	
		// Map Init
		window.map = L.map('map',mapOptions);
		$('#scrollEnabling').hide();
	
	
		// ----------------------------------------------- //
		// Popup Output
		// ----------------------------------------------- //
		function locationData(id,locationURL,locationImg,locationTitle, locationAddress, locationRating, locationRatingCounter) {
			return('')
		  }
	  
	
	
		// Listing rating on popup (star-rating or numerical-rating)
		var infoBox_ratingType = 'star-rating';
	
		map.on('popupopen', function () {
			if (infoBox_ratingType = 'numerical-rating') {
				numericalRating('.leaflet-popup .'+infoBox_ratingType+'');
			}
			if (infoBox_ratingType = 'star-rating') {
				starRating('.leaflet-popup .'+infoBox_ratingType+'');
			}
		});
	
	
		// ----------------------------------------------- //
		// Locations
		// ----------------------------------------------- //
		var lat = sessionStorage.getItem('lat');
		var lng = sessionStorage.getItem('lng');
		var province = sessionStorage.getItem('province');
		var amphure = sessionStorage.getItem('amphure');
		var distance = sessionStorage.getItem('distance');
		var productPetrol2 = sessionStorage.getItem('productPetrol2');
		var productPetrol3 = sessionStorage.getItem('productPetrol3');
		var productPetrol4 = sessionStorage.getItem('productPetrol4');
		var productPetrol5 = sessionStorage.getItem('productPetrol5');
		var productPetrol6 = sessionStorage.getItem('productPetrol6');
		var productPetrol7 = sessionStorage.getItem('productPetrol7');
		var productPetrol9 = sessionStorage.getItem('productPetrol9');
		var productPetrol12 = sessionStorage.getItem('productPetrol12');
		var productOther1 = sessionStorage.getItem('productOther1');
		var productOther8 = sessionStorage.getItem('productOther8');
		var productOther10 = sessionStorage.getItem('productOther10');
		var productOther13 = sessionStorage.getItem('productOther13');
		var productOther14 = sessionStorage.getItem('productOther14');

		// if(productPetrol2 && productPetrol3 && productPetrol4 && productPetrol5 && productPetrol6 && productPetrol7 && productPetrol9 && productPetrol12 && productOther1 && productOther8 && productOther10 && productOther13 && productOther14 === ""){
		// 	var productlist = ""
		// } else {
		// 	var productlist = productPetrol2 + "," + productPetrol3 + "," + productPetrol4 + "," + productPetrol5 + "," + productPetrol6 + "," + productPetrol7 + "," + productPetrol9 + "," + productPetrol12 + "," + productOther1 + "," + productOther8 + "," + productOther10 + "," + productOther13 + "," + productOther14;
		// }

		if(productPetrol2 && productPetrol3 && productPetrol4 && productPetrol5 && productPetrol6 && productPetrol7 && productPetrol9 && productPetrol12 && productOther1 && productOther8 && productOther10 && productOther13 && productOther14 === ""){
			var productlist = ""
		} else {
			var productlist = productPetrol2 + "," + productPetrol3 + "," + productPetrol4 + "," + productPetrol5 + "," + productPetrol6 + "," + productPetrol7 + "," + productPetrol9 + "," + productPetrol12 + "," + productOther1 + "," + productOther8 + "," + productOther10 + "," + productOther13 + "," + productOther14;
		}

		var resp;
		var settings = {
			"async": false,
			"crossDomain": true,
			"url": "https://asv-mobileapp-dev.azurewebsites.net/api/Location/GetLocationList?pageNo=1&lang=th&textsearch=&distance=" + distance + "&latitude=$" + lat + "&longitude=" + lng + "&province=" + province + "&amphure=" + amphure + "&isMap=" + true ,
			"method": "GET"
		}
		$.ajax(settings).done(function (response) {
			resp = response.data;
		});


		// LOOP LOCATION // รูปใน local จะไม่ขึ้น แต่ใน www ขึ้น
			// console.log(resp)
			var locations = [];
			for(var i in resp){
				locations.push(
					[locationData(resp[i].id), parseFloat(resp[i].latitude), parseFloat(resp[i].longitude), 1, '<img src="./images/logo3.png" class="img-round" width="20"></img>']
				)


		L.tileLayer(
			'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> Contributors',
			maxZoom: 18,
		}).addTo(map);
	
	
			markers = L.markerClusterGroup({
				spiderfyOnMaxZoom: true,
				showCoverageOnHover: false,
			  });
		   
			for (var i = 0; i < locations.length; i++) {
	
			  var listeoIcon = L.divIcon({
				  iconAnchor: [20, 51], // point of the icon which will correspond to marker's location
				  popupAnchor: [0, -51],
				  className: 'listeo-marker-icon',
				html:'<a href="/location/detail/' + resp[i].id + '/' + lat + '/' + lng + '">'+
					'<div class="marker-container">'+
							'<div class="marker-card">'+
							'<div class="front face">' + locations[i][4] + '</div>'+
							'<div class="back face">' + locations[i][4] + '</div>'+
							'<div class="marker-arrow"></div>'+
							'</div>'+
							'</div>'+
						'</a>'
				}
			  );
	
				var popupOptions =
				  {
				  'maxWidth': '270',
				  'className' : 'leaflet-infoBox'
				  }
					var markerArray = [];
				marker = new L.marker([locations[i][1],locations[i][2]], {
					icon: listeoIcon,
					
				  })
				  .bindPopup(locations[i][0],popupOptions );
				  //.addTo(map);
				  marker.on('click', function(e){
					
				   // L.DomUtil.addClass(marker._icon, 'clicked');
				  });
				  map.on('popupopen', function (e) {
					L.DomUtil.addClass(e.popup._source._icon, 'clicked');
				
	
				  }).on('popupclose', function (e) {
					if(e.popup){
					  L.DomUtil.removeClass(e.popup._source._icon, 'clicked');  
					}
					
				  });
				  markers.addLayer(marker);
			}
			map.addLayer(markers);
	
		
			markerArray.push(markers);
			if(markerArray.length > 0 ){
			  map.fitBounds(L.featureGroup(markerArray).getBounds().pad(0.2)); 
			}
		}
	
		// Custom Zoom Control
		map.removeControl(map.zoomControl);
	
		var zoomOptions = {
			zoomInText: '<i class="fa fa-plus" aria-hidden="true"></i>',
			zoomOutText: '<i class="fa fa-minus" aria-hidden="true"></i>',
		};
	
		// Creating zoom control
		var zoom = L.control.zoom(zoomOptions);
		zoom.addTo(map);
	
	}
	
	
	// ----------------------------------------------- //
	// Single Listing Map
	// ----------------------------------------------- //
	function singleListingMap() {
	
		var lng = parseFloat($( '#singleListingMap' ).data('longitude'));
		var lat =  parseFloat($( '#singleListingMap' ).data('latitude'));
		var singleMapIco =  "<i class='"+$('#singleListingMap').data('map-icon')+"'></i>";
	
		var listeoIcon = L.divIcon({
			iconAnchor: [20, 51], // point of the icon which will correspond to marker's location
			popupAnchor: [0, -51],
			className: 'listeo-marker-icon',
			html:  '<div class="marker-container no-marker-icon ">'+
							 '<div class="marker-card">'+
								'<div class="front face">' + singleMapIco + '</div>'+
								'<div class="back face">' + singleMapIco + '</div>'+
								'<div class="marker-arrow"></div>'+
							 '</div>'+
						   '</div>'
			
		  }
		);
	
		var mapOptions = {
			center: [lat,lng],
			zoom: 13,
			zoomControl: false,
			gestureHandling: true
		}
	
		var map_single = L.map('singleListingMap',mapOptions).setView([51.505, -0.09] , 13);
		var zoomOptions = {
		   zoomInText: '<i class="fa fa-plus" aria-hidden="true"></i>',
		   zoomOutText: '<i class="fa fa-minus" aria-hidden="true"></i>',
		};
	
		// Zoom Control
		var zoom = L.control.zoom(zoomOptions);
		zoom.addTo(map_single);
	
		map_single.scrollWheelZoom.disable();
	
		marker = new L.marker([lat,lng], {
			  icon: listeoIcon,
		}).addTo(map_single);
	
		// Open Street Map 
		// -----------------------//
		L.tileLayer(
			'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> Contributors',
			maxZoom: 18,
		}).addTo(map_single);
	
	
		// MapBox (Requires API Key)
		// -----------------------//
		// L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}', {
		//     attribution: " &copy;  <a href='https://www.mapbox.com/about/maps/'>Mapbox</a> &copy;  <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a>",
		//     maxZoom: 18,
		//     id: 'mapbox.streets',
		//     accessToken: 'ACCESS_TOKEN'
		// }).addTo(map_single);
		
	
		// Street View Button URL
		$('a#streetView').attr({
			href: 'https://www.google.com/maps/search/?api=1&query='+lat+','+lng+'',
			target: '_blank'
		});
	}
	
	// Single Listing Map Init
	if(document.getElementById("singleListingMap") !== null){
		singleListingMap();
	}
	
	
	});