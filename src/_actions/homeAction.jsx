import axios from 'axios';
import { Base_API } from '../../src/_constants/matcher';

export const homeAction = {
    getBanner
};

function getBanner(lang,cardType){
    return axios.get(`${Base_API.apis}/Promotion/GetPromotionInHome?lang=${lang}&cardType=${cardType}&_web=2`).then(res => {
        return res;
    })
}