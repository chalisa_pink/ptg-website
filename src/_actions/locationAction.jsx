import { Base_API } from "../_constants/matcher";
import axios from "axios";

export const ShopAction = {
    getShopProduct,
    getPTLocation,
    GetShopById,
    GetProvince,
    GetAmphure,
    GetPinMAp,
    GetHome,
    GetShopList,
    _GetShopList,
    _GetPartnerShopList,
    _GetShopById,
    _GetPromotionPartnerList,
    getPartnerById,
};

function getShopProduct() {
    return axios.get(`${Base_API.maps}/shop/GetProduct`).then(res => {
        return res.data;
    });
}

function getPTLocation(pageNo, lang, textsearch, distance, lat, lng, province, amphure, isMap ,productlist) {
    return axios.get(`${Base_API.maps}/Location/GetLocationList?pageNo=${pageNo}&lang=${lang}&textsearch=${textsearch}&distance=${distance}&latitude=${lat}&longitude=${lng}&province=${province}&amphure=${amphure}&isMap=${isMap}&productlist=${productlist}`).then(res => {
        return res.data;
    });
}

function GetShopById(lang, shopId, lat, lng) {
    return axios.get(`${Base_API.maps}/Shop/_GetShopById?lang=${lang}&shopId=${shopId}&latitude=${lat}&longitude=${lng}`).then(res => {
        return res.data;
    });
}

function GetProvince() {
    return axios.get(`${Base_API.apis}/Promotion/GetProvince`).then(res => {
        return res.data;
    });
}

function GetAmphure(formData) {
    return axios.post(`${Base_API.apis}/WebView/GetDropDownAMPHURE`, formData).then(res => {
        return res.data;
    });
}

function GetPinMAp() {
    return axios.get(`${Base_API.maps}/shop/GetPinMAp`).then(res => {
        return res.data;
    });
}

function GetShopList(pageNo, lang, textsearch, distance, lat, lng, province, amphure, isMap ,productlist) {
    return axios.get(`${Base_API.maps}/Shop/GetShopList?pageNo=${pageNo}&lang=${lang}&textsearch=${textsearch}&distance=${distance}&latitude=${lat}&longitude=${lng}&province=${province}&amphure=${amphure}&isMap=${isMap}&productlist=${productlist}`).then(res => {
        return res.data;
    });
}

function _GetShopList(pageNo, lang, textsearch, distance, lat, lng, province, amphure, isMap ,productlist) {
    return axios.get(`${Base_API.maps}/Shop/_GetShopList?pageNo=${pageNo}&lang=${lang}&textsearch=${textsearch}&distance=${distance}&latitude=${lat}&longitude=${lng}&province=${province}&amphure=${amphure}&isMap=${isMap}&productlist=${productlist}`).then(res => {
        return res.data;
    });
}

function _GetPartnerShopList(pageNo, lang, textsearch, distance, lat, lng, province, amphure, isMap ,productlist) {
    return axios.get(`${Base_API.maps}/Shop/_GetPartnerShopList?pageNo=${pageNo}&lang=${lang}&textsearch=${textsearch}&distance=${distance}&latitude=${lat}&longitude=${lng}&province=${province}&amphure=${amphure}&isMap=${isMap}&productlist=${productlist}`).then(res => {
        return res.data;
    });
}

function _GetShopById(lang, shopId, lat, lng) {
    return axios.get(`${Base_API.maps}/shop/_GetShopById?lang=${lang}&shopId=${shopId}&latitude=${lat}&longitude=${lng}`).then(res => {
        return res.data;
    });
}

function _GetPromotionPartnerList(lang,cardtype,partnerid){
    return axios.get(`${Base_API.maps}/Promotion/GetPromotionPartnerList?lang=${lang}&cardType=${cardtype}&partnerId=${partnerid}`).then(res => {
        return res.data;
    })
}

function getPartnerById(lang,partnerId,lat,lng){
    return axios.get(`${Base_API.maps}/Shop/_GetPartnerShopById?lang=${lang}&partnerId=${partnerId}&latitude=${lat}&longitude=${lng}`).then(res => {
        return res.data;
    })
}

function GetHome(lang,cardtype,partnerId){
    return axios.get(`${Base_API.maps}/Shop/GetHome?cardType=${cardtype}&partnerId=${partnerId}&lang=${lang}`).then(res => {
        return res.data;
    })
}
