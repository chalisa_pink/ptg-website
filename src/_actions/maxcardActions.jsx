import axios from 'axios';
import { Base_API } from '../../src/_constants/matcher';

export const maxcardActions = {
    getCardList,
    CheckPhoneBeforeSyncPTCard,
    SyncPTMaxCard,
    GetTransactionByCard ,
    getCustomerId,
    GetCardAndPointInfo
};

function getCardList(userid){
    return axios.get(`${Base_API.apis}/PTMaxCard/CardList?userId=${userid}`).then(res => {
        return res;
    })
}

function CheckPhoneBeforeSyncPTCard(formData){
    return axios.post(`${Base_API.apis}/PTMaxCard/CheckPhoneBeforeSyncPTCard`, formData).then(res => {        
        return res;
    })
}

function SyncPTMaxCard(formData){
    return axios.post(`${Base_API.apis}/PTMaxCard/SyncPTMaxCard`, formData).then(res => {        
        return res;
    })
}

function GetTransactionByCard(formData){
    return axios.post(`${Base_API.apis}/PTMaxCard/GetTransactionByCard`, formData).then(res => {        
        return res;
    })
}

function getCustomerId(userid){
    return axios.get(`${Base_API.apis}/PTMaxCard/GetCustomerId?userId=${userid}`).then(res => {
        return res;
    })
}

function GetCardAndPointInfo(userId){
    return axios.get(`${Base_API.apis}/PTMaxCard/GetCardAndPointInfo?userId=${userId}&udId=&deviceOs=web`).then(res => {
        return res;
    })
}