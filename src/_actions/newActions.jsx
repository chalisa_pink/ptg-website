import axios from 'axios';
import { Base_API } from '../_constants/matcher';

export const newActions = {
    getNew,
    getNewByID,
    getPromotion,
    getPromotionByID

};

function getNew(){
    return axios.get(`${Base_API.apis}/WebView/WebNews`).then(res => {
        return res;
    })
}
function getNewByID(id){
    return axios.get(`${Base_API.apis}/WebView/WebNews_Id?id=${id}`).then(res => {
        return res;
    })
}
function getPromotion(){
    return axios.get(`${Base_API.apis}/WebView/WebPromotion`).then(res => {
        return res;
    })
}
function getPromotionByID(id){
    return axios.get(`${Base_API.apis}/WebView/WebPromotion_Id?id=${id}`).then(res => {
        return res;
    })
}