import axios from 'axios';
import { Base_API } from '../_constants/matcher';

export const promotionActions = {
    GetPromotionList,
    getCateList,
    GetPromotion_EStamp,
    getDetailPromotionByID,
    GetReceiverbyTransaction,
    GetRedeembyTransaction,
    GetQRbyTransaction,
    InsertRedeemPromotion,
    GetPTBranch,
    checkRedeemTransection,
    GetPromotionPartnerList
};

function getCateList(){
    return axios.get(`${Base_API.apis}/Promotion/GetPromotionCatalog`).then(res => {
        return res;
    })
}

function GetPromotionList(lang, cardType, categoryId, pageNo){
    return axios.get(`${Base_API.apis}/Promotion/GetPromotionList?lang=${lang}&cardType=${cardType}&categoryId=${categoryId}&pageNo=${pageNo}`).then(res => {
        return res;
    })
}

function GetPromotionPartnerList(lang, cardType, partnerId){
    return axios.get(`${Base_API.apis}/Promotion/GetPromotionPartnerList?lang=${lang}&cardType=${cardType}&partnerId=${partnerId}`).then(res => {
        return res;
    })
}

function GetPromotion_EStamp(lang, cardType, categoryId, pageNo){
    return axios.get(`${Base_API.apis}/Promotion/GetPromotion_EStamp?lang=${lang}&cardType=${cardType}&categoryId=${categoryId}&pageNo=${pageNo}`).then(res => {
        return res;
    })
}

function getDetailPromotionByID(promotionid){
    return axios.get(`${Base_API.apis}/Promotion/GetPromotionById?language=th&promotionID=${promotionid}`).then(res => {
        return res;
    })
}

function GetReceiverbyTransaction(formData){
    return axios.post(`${Base_API.apis}/Promotion/GetReceiverbyTransaction`, formData).then(res => {
        return res;
    })
}

function GetRedeembyTransaction(formData){
    return axios.post(`${Base_API.apis}/Promotion/GetRedeembyTransaction`, formData).then(res => {
        return res;
    })
}

function GetQRbyTransaction(formData){
    return axios.post(`${Base_API.apis}/Promotion/GetQRbyTransaction`, formData).then(res => {
        return res;
    })
}

function InsertRedeemPromotion(userId,dataRedeem){
    return axios.post(`${Base_API.apis}/Promotion/InsertRedeemPromotion?udId=&deviceOs=web&_userid=${userId}`, dataRedeem).then(res => {
        return res;
    })
}

function GetPTBranch(provinceCode){
    return axios.get(`${Base_API.apis}/Promotion/GetPTBranch?provinceCode=${provinceCode}`).then(res => {
        return res;
    })
}

function checkRedeemTransection(promotionid,cardType){
    return axios.get(`${Base_API.apis}/Promotion/checkRedeemTransaction?promotionid=${promotionid}&cardType=${cardType}`).then(res => {
        return res;
    })
}
