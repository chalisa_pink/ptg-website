import axios from 'axios';
import { Base_API } from '../../src/_constants/matcher';
export const registerAction = {
    checkEmailorPhone,
    sendOTP,
    verifyOTP,
    getDropdownlist,
    getProvinceNew,
    getAmphure,
    getTumbon,
    getPostcode,
    checkPhoneExist,
    checkEmailExist,
    checkexistingIdCard,
    loginWebsite,
    loginSocial,
    registerWebSite,
    chkBeforePassword,
    forgotPassword,
    chkPassword,
    changePassword,
    getUserById,
    updateUser,
    activateAndRegister,
    confirmAfterRegis,
    GetCustomerProfile,
    updateAdditionalProfile,
    vsmartcall,
    getLocationIP,
    insertCaseInsident,
    CheckCardByLaser
};

function checkEmailorPhone(type,value){
    return axios.get(`${Base_API.apis}/Signin/CheckEmailOrPhoneNo?type=${type}&value=${value}`).then(res => {
        return res;
    })
}

function getAmphure(province){
  let formData = new FormData();
  formData.append('Address_Type', "AMPHURE")
  formData.append("CODE_GUID", province)
  return axios.post(`${Base_API.apis}/WebView/GetDropDownAddress`, formData).then(res => {
        return res;
      });
}

function getTumbon(amphure){
  let formData = new FormData();
  formData.append('Address_Type' , "DISTRICT")
  formData.append("codE_GUID" , amphure)
  return axios.post(`${Base_API.apis}/WebView/GetDropDownAddress`, formData).then(res => {
    return res;
  });
}

function getPostcode(district){
  let formData = new FormData();
  formData.append('Address_Type' , "POSTAL_CODE")
  formData.append("codE_GUID" , district)
  return axios.post(`${Base_API.apis}/WebView/GetDropDownAddress`, formData).then(res => {
    return res;
  });
}

function getProvinceNew(){
  return axios.post(`${Base_API.apis}/WebView/GetDropDownPROVINCE`).then(res => {
    return res;
  })
}

function sendOTP(phone,type){
    return axios.get(`${Base_API.apis}/Signin/beforVerifyOTP ?_receiver=${phone}&_action=${type}`).then(res => {
        return res;
    })
}

function verifyOTP(data){
    return axios.post(`${Base_API.apis}/Signin/VerifyOTP_Phone` , data).then(res => {
      return res;
    })
}

function checkPhoneExist(phone) {
  var reqData = `{
      "PHONE_NO": '${phone}'
    }`;
  let formData = new FormData();
  formData.append("reqData", reqData);

  return axios
    .post(
      `${Base_API.apis}/WebView/CheckExistingPhoneNo`,
      formData
    )
    .then(res => {
      return res;
    });
}

function checkEmailExist(email) {
  var reqData = `{
      "USER_EMAIL": '${email}'
    }`;
  let formData = new FormData();
  formData.append("reqData", reqData);

  return axios
    .post(
      `${Base_API.apis}/WebView/CheckExistingEmail`,
      formData
    )
    .then(res => {
      return res;
    });
}

function checkexistingIdCard(id_typecard,idcard) {
  var reqData = `{
      "CARD_TYPE_ID": ${id_typecard},
      "CARD_ID": '${idcard}'
    }`;
  let formData = new FormData();
  formData.append("reqData", reqData);
  
  return axios
    .post(
      `${Base_API.apis}/WebView/CheckExistingIdCard`,
      formData
    )
    .then(res => {
      return res;
    });
}

function getDropdownlist(title) {
    var title_type = `{
        "DROPDOWN_NAME": "${title}"
      }`;
    let formData = new FormData();
    formData.append("DROPDOWN_NAME", title);
  
    return axios({
      method: "POST",
      url: `${Base_API.apis}/WebView/GetDropDown`,
      data: formData
    })
      .then(function(response) {
        return response;
      })
      .catch(function(xhr, ajaxOptions, thrownError) {
        // console.log(xhr.status);
        // console.log(ajaxOptions);
        // console.log(thrownError);
      });
}

  function loginWebsite(formData){
    return axios.post(`${Base_API.apis}/Signin/LoginApp` , formData).then(res => {
      return res;
    })
  }

  function loginSocial(formData){
    return axios.post(`${Base_API.apis}/Signin/LoginSocial` ,formData).then(res => {
      return res;
    })
  }

  function registerWebSite(formData){
    return axios.post(`${Base_API.apis}/Signin/RegisterUser` , formData).then(res => {
      return res;
    })
  }

  function chkBeforePassword(formData){
    return axios.post(`${Base_API.apis}/Signin/CheckBeforeForgotPassword` , formData).then(res => {
      return res;
    })
  }

  function forgotPassword(formData){
    return axios.post(`${Base_API.apis}/Signin/ForgotPassword` , formData).then( res => {
      return res;
    })
  }

  function chkPassword(formData){
    return axios.post(`${Base_API.apis}/ManageUser/CheckPassword` , formData).then(res => {
      return res;
    })
  }

  function changePassword(formData){
    return axios.post(`${Base_API.apis}/ManageUser/ChangePassword` , formData).then(res => {
      return res;
    })
  }

  function getUserById(uid,tkmb){
    return axios.get(`${Base_API.apis}/ManageUser/GetUserById?userId=${uid}&tokenMobileApp=${tkmb}`).then( res => {
      return res;
    })
  }

  function updateUser(formData){
    return axios.post(`${Base_API.apis}/ManageUser/UpdateUser` , formData).then(res => {
      return res;
    })
  }

  function activateAndRegister(formdata,userid){
    var preview1;
    var preview2;
    var preview3;
    
    if(localStorage.getItem('imgPreview1') != null || localStorage.getItem('imgPreview1') != '') {
      if(localStorage.getItem('imgPreview2') === null & localStorage.getItem('imgPreview3') === null) {
        preview1 = `{
          "CASE_IMG": "${(localStorage.getItem('imgPreview1') == null ? "[]" : localStorage.getItem('imgPreview1'))}"
        }` 
      } else {
        preview1 = `{
          "CUSTOMER_IMG": "${localStorage.getItem('imgPreview1')}"
        },` 
      }
    }
  
    if(localStorage.getItem('imgPreview2') != null) {
      if(localStorage.getItem('imgPreview3') === null) {
        preview2 = `{
          "CUSTOMER_IMG": "${localStorage.getItem('imgPreview2')}"
        }` 
      } else {
        preview2 = `{
          "CUSTOMER_IMG": "${localStorage.getItem('imgPreview2')}"
        },` 
      } 
    }
    if(localStorage.getItem('imgPreview3') != null) {
      preview3 = `{
        "CUSTOMER_IMG": "${localStorage.getItem('imgPreview3')}"
      }` 
    }
  
    var reqData = `{
      "CARD_TYPE_ID": ${parseInt(formdata.CARD_TYPE_ID)},
      "CARD_ID": "${formdata.CARD_ID}",
      "TITLE_NAME_ID": ${parseInt(formdata.TITLE_NAME_ID)},
      "TITLE_NAME_REMARK": "${(formdata.TITLE_NAME_REMARK) ? formdata.TITLE_NAME_REMARK : ''}",
      "GENDER_ID": ${(formdata.GENDER_ID) ? parseInt(formdata.GENDER_ID) : 1},
      "FNAME_TH": "${(formdata.FNAME_TH) ? formdata.FNAME_TH : ''}",
      "LNAME_TH": "${(formdata.LNAME_TH) ? formdata.LNAME_TH : ''}",
      "BIRTH_DATE": "${(formdata.BIRTH_DATE) ? formdata.BIRTH_DATE : ''}",
      "MARITAL_STATUS": ${(formdata.MARITAL_STATUS) ? parseInt(formdata.MARITAL_STATUS) : 1},
      "ADDR_ADDRESS": "${(formdata.ADDR_NO) ? formdata.ADDR_NO : ''}",
      "ADDR_MOO": "${(formdata.ADDR_MOO) ? formdata.ADDR_MOO : ''}",
      "ADDR_VILLEGE": "${(formdata.ADDR_VILLAGE) ? formdata.ADDR_VILLAGE : ''}",
      "ADDR_SOI": "${(formdata.ADDR_SOI) ? formdata.ADDR_SOI : ''}",
      "ADDR_STREET": "${(formdata.ADDR_STREET) ? formdata.ADDR_STREET : ''}",
      "ADDR_ORG_SUB_DISTRICT_ID": "${(formdata.ADDR_ORG_SUB_DISTRICT_ID) ? formdata.ADDR_ORG_SUB_DISTRICT_ID : ''}",
      "ADDR_ORG_AMPHURE_ID": "${(formdata.ADDR_ORG_AMPHURE_ID) ? formdata.ADDR_ORG_AMPHURE_ID : ''}",
      "ADDR_ORG_PROVINCE_ID": "${(formdata.ADDR_ORG_PROVINCE_ID) ? formdata.ADDR_ORG_PROVINCE_ID : ''}",
      "ADDR_ORG_POSTALCODE_ID": "${(formdata.ADDR_ORG_POSTALCODE_ID) ? formdata.ADDR_ORG_POSTALCODE_ID : ''}",
      "ADDR_OTHER": "${(formdata.ADDR_OTHER) ? formdata.ADDR_OTHER : ''}",
      "OCCUPATION_ID": ${(formdata.OCCUPATION_ID) ? formdata.OCCUPATION_ID : 0},
      "OCCAPATION_REMARK": "${(formdata.OCCAPATION_REMARK) ? formdata.OCCAPATION_REMARK : ''}",
      "EDUCATION_ID": ${(formdata.EDUCATION_ID) ? formdata.EDUCATION_ID : 0},
      "INCOME_ID": "${(formdata.INCOME_ID) ? formdata.INCOME_ID : 0}",
      "USER_EMAIL": "${(formdata.USER_EMAIL) ? formdata.USER_EMAIL : ''}",
      "PHONE_NO": "${(formdata.PHONE_NO) ? formdata.PHONE_NO : ''}",
      "PHONE_HOME": "${(formdata.PHONE_HOME) ? formdata.PHONE_HOME : ''}",
      "ABOUT_SOURCE": ${(formdata.ABOUT_SOURCE) ? formdata.ABOUT_SOURCE : 7},
      "REF_CARD_NO": "${(formdata.REF_CARD_NO) ? formdata.REF_CARD_NO : ''}",
      "PMT_FOOD": ${(formdata.PMT_FOOD === true) ? 1 : 0 },
      "PMT_TRAVEL": ${(formdata.PMT_TRAVEL === true) ? 1 : 0},
      "PMT_SHOPPING": ${(formdata.PMT_SHOPPING === true) ? 1 : 0},
      "PMT_BEAUTY": ${(formdata.PMT_BEAUTY === true) ? 1 : 0},
      "PMT_SPORT_AND_CAR": ${(formdata.PMT_SPORT_AND_CAR === true) ? 1 : 0},
      "PMT_OTHERS": ${(formdata.PMT_OTHERS === true) ? 1 : 0},
      "PMT_OTHERS_REMARK": "${(formdata.PMT_OTHERS_REMARK) ? formdata.PMT_OTHERS_REMARK : ''}",
      "ACT_EN_MUSIC": ${(formdata.ACT_EN_MUSIC === true) ? 1 : 0},
      "ACT_EN_CONCERT": ${(formdata.ACT_EN_CONCERT === true) ? 1 : 0},
      "ACT_EN_COUNTRY_CONCERT": ${(formdata.ACT_EN_COUNTRY_CONCERT === true) ? 1 : 0},
      "ACT_EN_MOVIES": ${(formdata.ACT_EN_MOVIES === true) ? 1 : 0},
      "ACT_EN_THEATER": ${(formdata.ACT_EN_THEATER === true) ? 1 : 0},
      "ACT_EN_OTHERS": ${(formdata.ACT_EN_OTHERS === true) ? 1 : 0},
      "ACT_EN_OTHERS_REMARK": "${(formdata.ACT_EN_OTHERS_REMARK) ? formdata.ACT_EN_OTHERS_REMARK : ''}",
      "ACT_SPT_FITNESS": ${(formdata.ACT_SPT_FITNESS === true) ? 1 : 0},
      "ACT_SPT_RUN": ${(formdata.ACT_SPT_RUN === true) ? 1 : 0},
      "ACT_SPT_CYCLING": ${(formdata.ACT_SPT_CYCLING === true) ? 1 : 0},
      "ACT_SPT_FOOTBALL": ${(formdata.ACT_SPT_FOOTBALL === true) ? 1 : 0},
      "ACT_SPT_OTHERS": ${(formdata.ACT_SPT_OTHERS === true) ? 1 : 0},
      "ACT_SPT_OTHERS_REMARK": "${(formdata.ACT_SPT_OTHERS_REMARK) ? formdata.ACT_SPT_OTHERS_REMARK : ''}",
      "ACT_OD_SHOPPING": ${(formdata.ACT_OD_SHOPPING === true) ? 1 : 0},
      "ACT_OD_BEAUTY": ${(formdata.ACT_OD_BEAUTY === true) ? 1 : 0},
      "ACT_OD_RESTAURANT": ${(formdata.ACT_OD_RESTAURANT === true) ? 1 : 0},
      "ACT_OD_CLUB": ${(formdata.ACT_OD_CLUB === true) ? 1 : 0},
      "ACT_OD_MOMCHILD": ${(formdata.ACT_OD_MOMCHILD === true) ? 1 : 0},
      "ACT_OD_RALLY": ${(formdata.ACT_OD_RALLY === true) ? 1 : 0},
      "ACT_OD_ADVANTURE": ${(formdata.ACT_OD_ADVANTURE === true) ? 1 : 0},
      "ACT_OD_PHOTO": ${(formdata.ACT_OD_PHOTO === true) ? 1 : 0},
      "ACT_OD_PHILANTHROPY": ${(formdata.ACT_OD_PHILANTHROPY === true) ? 1 : 0},
      "ACT_OD_HOROSCOPE": ${(formdata.ACT_OD_HOROSCOPE === true) ? 1 : 0},
      "ACT_OD_ABROAD": ${(formdata.ACT_OD_ABROAD === true) ? 1 : 0},
      "ACT_OD_UPCOUNTRY": ${(formdata.ACT_OD_UPCOUNTRY === true) ? 1 : 0},
      "ACT_OD_OTHERS": ${(formdata.ACT_OD_OTHERS === true) ? 1 : 0},
      "ACT_OD_OTHERS_REMARK": "${(formdata.ACT_OD_OTHERS_REMARK) ? formdata.ACT_OD_OTHERS_REMARK : ''}",
      "ACT_ID_COOKING": ${(formdata.ACT_ID_COOKING === true) ? 1 : 0},
      "ACT_ID_BAKING": ${(formdata.ACT_ID_BAKING === true) ? 1 : 0},
      "ACT_ID_DECORATE": ${(formdata.ACT_ID_DECORATE === true) ? 1 : 0},
      "ACT_ID_GARDENING": ${(formdata.ACT_ID_GARDENING === true) ? 1 : 0},
      "ACT_ID_READING": ${(formdata.ACT_ID_READING === true) ? 1 : 0},
      "ACT_ID_GAMING": ${(formdata.ACT_ID_GAMING === true) ? 1 : 0},
      "ACT_ID_INTERNET": ${(formdata.ACT_ID_INTERNET === true) ? 1 : 0}, 
      "ACT_ID_OTHERS": ${(formdata.ACT_ID_OTHERS === true) ? 1 : 0},
      "ACT_ID_OTHERS_REMARK": "${(formdata.ACT_ID_OTHERS_REMARK) ? formdata.ACT_ID_OTHERS_REMARK : ''}",
      "ACT_HNGOUT_FRIEND": ${(formdata.ACT_HNGOUT_FRIEND === true) ? 1 : 0},
      "ACT_HNGOUT_COUPLE": ${(formdata.ACT_HNGOUT_COUPLE === true) ? 1 : 0},
      "ACT_HNGOUT_FAMILY": ${(formdata.ACT_HNGOUT_FAMILY === true) ? 1 : 0},
      "ACT_HNGOUT_NO": ${(formdata.ACT_HNGOUT_NO === true) ? 1 : (formdata.ACT_HNGOUT_COUPLE === undefined && formdata.ACT_HNGOUT_FAMILY === undefined && formdata.ACT_HNGOUT_FRIEND === undefined ? 1 : (formdata.ACT_HNGOUT_COUPLE === false && formdata.ACT_HNGOUT_FAMILY === false && formdata.ACT_HNGOUT_FRIEND === false ? 1 : 0))},
      "CARD_CAR_TYPE_ID": ${(formdata.CAR_TYPE) ? formdata.CAR_TYPE : (formdata.CAR_TYPE === undefined ? 0 : (formdata.CAR_TYPE == 0 ? 0 : formdata.CAR_TYPE ))},
      "CAR_BRAND": ${(formdata.CAR_BRAND) ? formdata.CAR_BRAND : (formdata.CAR_TYPE === undefined ? 17 : (formdata.CAR_TYPE == 0 ? 17 : formdata.CAR_BRAND))},
      "CAR_BRAND_REMARK": "${(formdata.CAR_BRAND_REMARK) ? formdata.CAR_BRAND_REMARK : ''}",
      "CUSTOMER_IMG_INFO": [${(preview1) ? preview1 : ''} ${(preview2) ? preview2 : ''}${(preview3) ? preview3 : ''}],
      "SOURCE_DATA": 16
    }`
    // console.log("reqData", reqData)
    let formData = new FormData();
    formData.append('reqData' , reqData)
    formData.append('userId' , userid)
    return axios.post(`${Base_API.apis}/WebView/ActivateAndRegister`, formData).then(res => {
        return res;
    });
  }

  function confirmAfterRegis(userid,type){
    return axios.get(`${Base_API.apis}/PTMaxCard/ConfirmAfterRegisterCard?userId=${userid}&type=${type}`)
  }

  function GetCustomerProfile(formData){
    return axios.post(`${Base_API.apis}/WebView/GetCustomerProfile` , formData).then(res => {
      return res;
    })
  }

  function updateAdditionalProfile(formdata,token,customerid, userId){
    var requestData = `{
      "CUSTOMER_ID":"${customerid}",
      "CARD_TYPE_ID": ${formdata.CARD_TYPE_ID},
      "TITLE_NAME_ID": ${formdata.TITLE_NAME_ID},
      "TITLE_NAME_REMARK": "${formdata.TITLE_NAME_REMARK}",
      "GENDER_ID": ${formdata.GENDER_ID},
      "FNAME_TH": "${formdata.FNAME_TH}",
      "LNAME_TH": "${formdata.LNAME_TH}",
      "BIRTH_DATE": "${formdata.BIRTH_DATE}",
      "ADDR_ADDRESS": "${formdata.ADDR_ADDRESS}",
      "ADDR_MOO": "${formdata.ADDR_MOO}",
      "ADDR_VILLEGE": "${formdata.ADDR_VILLEGE}",
      "ADDR_SOI": "${formdata.ADDR_SOI}",
      "ADDR_STREET": "${formdata.ADDR_STREET}",
      "ADDR_ORG_SUB_DISTRICT_ID": "${(formdata.ADDR_ORG_SUB_DISTRICT_ID).toUpperCase()}",
      "ADDR_ORG_AMPHURE_ID": "${(formdata.ADDR_ORG_AMPHURE_ID).toUpperCase()}",
      "ADDR_ORG_PROVINCE_ID": "${(formdata.ADDR_ORG_PROVINCE_ID).toUpperCase()}",
      "ADDR_ORG_POSTALCODE_ID": "${(formdata.ADDR_ORG_POSTALCODE_ID).toUpperCase()}",
      "ADDR_OTHER": "${(formdata.ADDR_OTHER ?? '')}",
      "OCCUPATION_ID": ${formdata.OCCUPATION_ID},
      "OCCAPATION_REMARK": "${(formdata.OCCAPATION_REMARK ?? '')}",
      "USER_EMAIL": "${formdata.USER_EMAIL}",
      "PHONE_NO": "${formdata.PHONE_NO}",
      "PHONE_HOME": "${formdata.PHONE_HOME}",
      "MARITAL_STATUS": ${formdata.MARITAL_STATUS},
      "EDUCATION_ID": ${(formdata.EDUCATION_ID) ?? 0},
      "INCOME_ID": "${(formdata.INCOME_ID) ?? 0}",
      "PMT_FOOD": ${formdata.PMT_FOOD ? 1 : 0},
      "PMT_TRAVEL": ${formdata.PMT_TRAVEL ? 1 : 0},
      "PMT_SHOPPING": ${formdata.PMT_SHOPPING ? 1 : 0},
      "PMT_BEAUTY": ${formdata.PMT_BEAUTY ? 1 : 0},
      "PMT_SPORT_AND_CAR": ${formdata.PMT_SPORT_AND_CAR ? 1 : 0},
      "PMT_OTHERS": ${formdata.PMT_OTHERS ? 1 : 0},
      "PMT_OTHERS_REMARK": "${formdata.PMT_OTHERS_REMARK}",
      "ACT_EN_MUSIC": ${formdata.ACT_EN_MUSIC ? 1 : 0},
      "ACT_EN_CONCERT": ${formdata.ACT_EN_CONCERT ? 1 : 0},
      "ACT_EN_COUNTRY_CONCERT": ${formdata.ACT_EN_COUNTRY_CONCERT ? 1 : 0},
      "ACT_EN_MOVIES": ${formdata.ACT_EN_MOVIES ? 1 : 0},
      "ACT_EN_THEATER": ${formdata.ACT_EN_THEATER ? 1 : 0},
      "ACT_EN_OTHERS": ${formdata.ACT_EN_OTHERS ? 1 : 0},
      "ACT_EN_OTHERS_REMARK": "${formdata.ACT_EN_OTHERS_REMARK}",
      "ACT_SPT_FITNESS": ${formdata.ACT_SPT_FITNESS ? 1 : 0},
      "ACT_SPT_RUN": ${formdata.ACT_SPT_RUN ? 1 : 0},
      "ACT_SPT_CYCLING": ${formdata.ACT_SPT_CYCLING ? 1 : 0},
      "ACT_SPT_FOOTBALL": ${formdata.ACT_SPT_FOOTBALL ? 1 : 0},
      "ACT_SPT_OTHERS": ${formdata.ACT_SPT_OTHERS ? 1 : 0},
      "ACT_SPT_OTHERS_REMARK": "${formdata.ACT_SPT_OTHERS_REMARK}",
      "ACT_OD_SHOPPING": ${formdata.ACT_OD_SHOPPING ? 1 : 0},
      "ACT_OD_BEAUTY": ${formdata.ACT_OD_BEAUTY ? 1 : 0},
      "ACT_OD_RESTAURANT": ${formdata.ACT_OD_RESTAURANT ? 1 : 0},
      "ACT_OD_CLUB": ${formdata.ACT_OD_CLUB ? 1 : 0},
      "ACT_OD_MOMCHILD": ${formdata.ACT_OD_MOMCHILD ? 1 : 0},
      "ACT_OD_RALLY": ${formdata.ACT_OD_RALLY ? 1 : 0},
      "ACT_OD_ADVANTURE": ${formdata.ACT_OD_ADVANTURE ? 1 : 0},
      "ACT_OD_PHOTO": ${formdata.ACT_OD_PHOTO ? 1 : 0},
      "ACT_OD_PHILANTHROPY": ${formdata.ACT_OD_PHILANTHROPY ? 1 : 0},
      "ACT_OD_HOROSCOPE": ${formdata.ACT_OD_HOROSCOPE ? 1 : 0},
      "ACT_OD_ABROAD": ${formdata.ACT_OD_ABROAD ? 1 : 0},
      "ACT_OD_UPCOUNTRY": ${formdata.ACT_OD_UPCOUNTRY ? 1 : 0},
      "ACT_OD_OTHERS": ${formdata.ACT_OD_OTHERS ? 1 : 0},
      "ACT_OD_OTHERS_REMARK": "${formdata.ACT_OD_OTHERS_REMARK}",
      "ACT_ID_COOKING": ${formdata.ACT_ID_COOKING ? 1 : 0},
      "ACT_ID_BAKING": ${formdata.ACT_ID_BAKING ? 1 : 0},
      "ACT_ID_DECORATE": ${formdata.ACT_ID_DECORATE ? 1 : 0},
      "ACT_ID_GARDENING": ${formdata.ACT_ID_GARDENING ? 1 : 0},
      "ACT_ID_READING": ${formdata.ACT_ID_READING ? 1 : 0},
      "ACT_ID_GAMING": ${formdata.ACT_ID_GAMING ? 1 : 0},
      "ACT_ID_INTERNET": ${formdata.ACT_ID_INTERNET ? 1 : 0}, 
      "ACT_ID_OTHERS": ${formdata.ACT_ID_OTHERS ? 1 : 0},
      "ACT_ID_OTHERS_REMARK": "${formdata.ACT_ID_OTHERS_REMARK}",
      "ACT_HNGOUT_FRIEND": ${formdata.ACT_HNGOUT_FRIEND ? 1 : 0},
      "ACT_HNGOUT_COUPLE": ${formdata.ACT_HNGOUT_COUPLE ? 1 : 0},
      "ACT_HNGOUT_FAMILY": ${formdata.ACT_HNGOUT_FAMILY ? 1 : 0},
      "ACT_HNGOUT_NO": ${formdata.ACT_HNGOUT_NO ? 1 : 0},
      "SOURCE_DATA": 16
    }`
    let formData = new FormData();
    formData.append('TOKEN_ID', token);
    formData.append('reqData' , requestData);
    formData.append('userId' , userId);

    return axios.post(`${Base_API.apis}/WebView/UpdateProfileExtra` , formData).then(res => {
        return res;
    })
  }

  function vsmartcall(formData){
    return axios.post(`${Base_API.apis}/WebView/vsmartcall` , formData).then(res => {
      return res;
    })
  }


  function getLocationIP(){
    return axios.get(`https://api.ipify.org/?format=json`).then(res => {
      return res;
    })
  }

  function insertCaseInsident(title,description,contact){
    var preview1;
    var preview2;
    var preview3;
  
    if(localStorage.getItem('imgPreview1') != null || localStorage.getItem('imgPreview1') != '') {
      if(localStorage.getItem('imgPreview2') === null & localStorage.getItem('imgPreview3') === null) {
        preview1 = `{
          "CASE_IMG": "${(localStorage.getItem('imgPreview1') == null ? "" : localStorage.getItem('imgPreview1'))}"
        }` 
      } else {
        preview1 = `{
          "CASE_IMG": "${localStorage.getItem('imgPreview1')}" 
        },` 
      }
    }
  
    if(localStorage.getItem('imgPreview2') != null) {
      if(localStorage.getItem('imgPreview3') === null) {
        preview2 = `{
          "CASE_IMG": "${localStorage.getItem('imgPreview2')}" 
        }` 
      } else {
        preview2 = `{
          "CASE_IMG": "${localStorage.getItem('imgPreview2')}" 
        },` 
      } 
    }
  
    if(localStorage.getItem('imgPreview3') != null) {
      preview3 = `{
        "CASE_IMG": "${localStorage.getItem('imgPreview3')}"
      }` 
    }
  
    var getData = `{
      "CASE_TYPE":"06",
      "CASE_SUB_TYPE":"0606",
      "CASE_SUB_TYPE_DETAIL": "060601",
      "TITLE": "${title}",
      "DESCRIPTION": "${(description == undefined ? '' : description)} ${contact}",
      "CASE_PHONE_NO": "-",
      "CASE_IMG_INFO": ${(preview1) == undefined ? '' : `[${(preview1) ? preview1 : ''}${(preview2) ? preview2 : ''}${(preview3) ? preview3 : ''}]`}
    }`;
    // console.log(getData)
    let formData = new FormData()
    formData.append('reqData' , getData)
    return axios.post(`${Base_API.apis}/WebView/insertcaseincidentnotoken` , formData).then(res => {
      return res;
    })
  }



function CheckCardByLaser(data){
  var uname = 'MAXREWARDS';
  var pass = '5Dj0ASMW2EB8as9hQX67JmLekuOKYTCx';
  return axios.post(`${Base_API.dopa}/api/CheckCardByLaser`, data, {
  auth: {
      username: uname,
      password: pass
  }
  }).then(res => {
      return res.data;
  }).catch(e => {
      return "ErrorApi";
  });
}