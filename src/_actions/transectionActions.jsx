import axios from 'axios';
import { Base_API } from '../../src/_constants/matcher';

export const getTransections = {
    getHistoryList
}

function getHistoryList(formData){
    return axios.post(`${Base_API.apis}/PTMaxCard/GetTransaction` , formData).then( res => {
        return res;
    });
}