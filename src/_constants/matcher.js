let config;
const urlPath = window.location.hostname;

if(urlPath === 'localhost'){
    config = {
        apis : `https://asv-mobileapp-dev.azurewebsites.net/api`,
        // apis : `https://asv-mobileapp-prod.azurewebsites.net/api`, //prod
        maps : `https://asv-mobileapp-prod.azurewebsites.net/api`,
        dopa: 'https://crmpartner.pt.co.th/dopo',
        pathWeb : '/website'
    }
}else if(urlPath === 'prd-pt-maxcard-asv.azurewebsites.net' || urlPath === 'asv-mobile-pt-maxcard-prod.azurewebsites.net' || urlPath === 'maxcard1.pt.co.th' || urlPath === 'ptmaxcard.com' || urlPath === 'www.ptmaxcard.com'){
    config = {
        apis : `https://asv-mobileapp-prod.azurewebsites.net/api`,
        // prod : `https://asv-mobileapp-prod.azurewebsites.net/api`,
        maps : `https://asv-mobileapp-prod.azurewebsites.net/api`,
        dopa: 'https://crmpartner.pt.co.th/dopo',
        pathWeb : ''
    }
}else{
    config = {
        apis : `https://asv-mobileapp-dev.azurewebsites.net/api`,
        // prod : `https://asv-mobileapp-prod.azurewebsites.net/api`,
        maps : `https://asv-mobileapp-prod.azurewebsites.net/api`,
        dopa: 'https://ptnewcrm-qas.pt.co.th:40443/pt-uat-dopa',
        pathWeb : '/website'
    }
}

export const Base_API = config;