import React from "react";
import '../Footer/footer.css';
import SweetAlert from 'react-bootstrap-sweetalert';
import {TinyButton as ScrollUpButton} from "react-scroll-up-button"; 
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../translations/en.json";
import intlMessageTH from "../../translations/th.json";
import { Base_API } from "../../_constants/matcher";

const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

export default class Footer extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            modal:null,
            show:false,
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
        };
    }

    
    openModal = (part) => {
        let Profile = localStorage.getItem('Profile')
        if(Profile){
            window.location.href = Base_API.pathWeb + part
        }else{
            var msg = <FormattedMessage id="pleaselogin" />;
            var img = `${process.env.PUBLIC_URL}/images/cancel.png`;

            this.alertLogin(msg,img)
        }
    };

    alertLogin(res, img) {
        alert = (
        <SweetAlert
            custom
            // warning
            showCloseButton
            closeOnClickOutside={false}
            focusConfirmBtn={false}
            title=""
            customIcon={img}
            showConfirm={false}
            showCancelButton
            onCancel={() => this.handleChoice(false)}
            onConfirm={() => this.handleChoice(true)}
        >
            <div className="iconClose" onClick={() => this.handleChoice(false)}></div>
            <div className="fontSizeCase">{res}</div>
        </SweetAlert>
        );
        this.setState({ show: true, modal: alert });
    }    

    handleChoice(){
        this.setState({ modal : null , show : false })
    }
    
    render(){ 
        return (
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].Footer}
            >
                <div>
                    <div className="set-zindex t-20">
                        <div className="container-fluid footer font-footer">
                            <div className="row mx-5 flex-wrap">
                                <div className="col-12 col-xs-12 col-sm-6 col-md-3 text-left col-footer order-1 order-md-1">
                                    <div className="my-2">
                                        <div className="row">
                                            <div className="col-2 col-md-12 img-logo-footer">
                                                <img width="50" className="m-2" src={`${process.env.PUBLIC_URL}/images/icon.png`} />
                                            </div>
                                            <div className="col-10 col-md-12">
                                                <div className="">บริษัท แมกซ์ การ์ด จำกัด</div>
                                            </div>
                                        </div>
                                        
                                        <div className="t-20 mx-3">
                                            Max Card Company Limited <br />
                                            <FormattedMessage id="addresss1" /><br />
                                            <FormattedMessage id="addresss2" /> 10310
                                             
                                        </div>
                                        <div className="m-3 logo-dbd">
                                            <img style={{width:'30%'}} src={`${process.env.PUBLIC_URL}/images/logo001@3x.png`}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-xs-12 col-sm-6 col-md-3 text-left col-footer order-2 order-md-2">
                                    <div className="px-3">
                                        <div className="head-footer-font pt-2"><FormattedMessage id="ptmaxcard" /></div>
                                        <a href="#" onClick={() => this.openModal('/PTmaxcard')}><FormattedMessage id="aboutus" /></a> <br/>
                                        <a href="#" onClick={() => this.openModal('/PTmaxcard/register')}><FormattedMessage id="registerptmaxcard" /></a> <br/>
                                        <a href={`${Base_API.pathWeb}/PTmaxcard/condition`}><FormattedMessage id="stepregis" /></a> <br/>
                                        <a href={`${Base_API.pathWeb}/PTmaxcard/condition`}><FormattedMessage id="calcu" /></a> <br/>
                                        <a href={`${Base_API.pathWeb}/PTmaxcard/condition`}><FormattedMessage id="conditioncard" /></a>
                                    </div>
                                </div>
                                <div className="col-12 col-xs-12 col-sm-6 col-md-3 text-left col-footer order-4 order-md-3">
                                    <div className="px-3">
                                        <div className="head-footer-font"><FormattedMessage id="reward" /></div>
                                        <a href={`${Base_API.pathWeb}/news`}><FormattedMessage id="newsactivitie" /> </a><br/>
                                        <a href={`${Base_API.pathWeb}/privileges`}><FormattedMessage id="promotion" /></a> <br/>
                                        <a href={`${Base_API.pathWeb}/privileges`}><FormattedMessage id="provilled" /> </a><br/>
                                        <a href={`${Base_API.pathWeb}/privileges`}>E-Stamp </a><br/>
                                        <a href={`${Base_API.pathWeb}/privileges`}><FormattedMessage id="movepoint" /></a>
                                    </div>
                                </div>
                                <div className="col-12 col-xs-12 col-sm-6 col-md-3 text-left col-footer order-3 order-md-4">
                                    <div className="px-3">    
                                        <div className="head-footer-font"><FormattedMessage id="useservice" /> </div>
                                        <a href={`${Base_API.pathWeb}/location`}><FormattedMessage id="location" /></a> <br />
                                        <a href={`${Base_API.pathWeb}/PTmaxcard/question`}><FormattedMessage id="qanda" /></a> <br />
                                        <a href="#" onClick={() => this.openModal('/PTmaxcard/report')}><FormattedMessage id="reportproblem" /></a> <br />
                                        <a href={`${Base_API.pathWeb}/PTmaxcard/condition`}><FormattedMessage id="conditionservice" /></a>
                                    </div>
                                    
                                </div>
                            </div>
                            <div>
                                <div className="img_callcenter">
                                    <img src={`${process.env.PUBLIC_URL}/images/footer_callcenter.png`} />
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* <div className="set-zindex t-20">
                        <div className="footer font-footer">
                            <div className="text-left ml-3">
                                <img style={{width:'15%'}}src={`${process.env.PUBLIC_URL}/images/icon.png`} />
                                <span>
                                    <div className="ptg-energy-font setalign">PTG ENERGY</div>
                                    <div className="setalign-two">
                                        บริษัทพีทีจี เอ็นเนอยี จำกัด
                                    </div>
                                </span>
                            </div>
                            <div className="text-left mt-3" style={{marginLeft:'12%'}}>
                                PTG Energy Public Company Limited <br />
                                <FormattedMessage id="addresss1" /><br />
                                <FormattedMessage id="addresss2" /><br />
                                10310 
                                <div className="my-3">
                                    <img style={{width:'30%'}} src={`${process.env.PUBLIC_URL}/images/logo001@3x.png`}/>
                                </div>
                            </div>
                            <div className="row container-fluid text-left">
                                <div className="col-12 col-sm-12 col-md-6 col-xs-6">
                                    <div className="head-footer-font"><FormattedMessage id="ptmaxcard" /></div>
                                    <a href="/PTmaxcard"><FormattedMessage id="aboutus" /></a> <br/>
                                    <a href="/PTmaxcard/register"><FormattedMessage id="registerptmaxcard" /></a> <br/>
                                    <a href="/PTmaxcard/condition"><FormattedMessage id="stepregis" /></a> <br/>
                                    <a href="/PTmaxcard/condition"><FormattedMessage id="calcu" /></a> <br/>
                                    <a href="/PTmaxcard/condition"><FormattedMessage id="conditioncard" /></a>
                                    <div className="head-footer-font mt-3"> <FormattedMessage id="useservice" /></div>
                                    <a href="/location"><FormattedMessage id="location" /></a> <br />
                                    <a href="/PTmaxcard/question"><FormattedMessage id="qanda" /></a> <br />
                                    <a href="/PTmaxcard/report"><FormattedMessage id="reportproblem" /></a> <br />
                                    <a href="/PTmaxcard/condition"><FormattedMessage id="conditionservice" /></a>
                                </div>
                                <div className="col-12 col-sm-12 col-md-6 col-xs-6 pt-3">
                                    <div className="head-footer-font"><FormattedMessage id="reward" /></div>
                                    <a href="/news"> <FormattedMessage id="newsactivitie" /></a><br/>
                                    <a href="/privileges"><FormattedMessage id="promotion" /></a> <br/>
                                    <a href="/privileges"> <FormattedMessage id="provilled" /></a><br/>
                                    <a href="/privileges">E-Stamp </a><br/>
                                    <a href="/privileges"><FormattedMessage id="movepoint" /></a>
                                    <div className="img_callcenter">
                                        <img src={`${process.env.PUBLIC_URL}/images/footer_callcenter.png`} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> */}
                    <ScrollUpButton />
                    {this.state.modal}
                </div>
            </IntlProvider>
        );
    }
}
