import React, {useState} from "react";
import { Route , withRouter} from 'react-router-dom';
import '../Header/header.css';
import SweetAlert from 'react-bootstrap-sweetalert';
import { registerAction } from '../../_actions/registerActions'; 
import { maxcardActions } from '../../_actions/maxcardActions'; 
import firebase from 'firebase';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../translations/en.json";
import intlMessageTH from "../../translations/th.json";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye,faEyeSlash } from "@fortawesome/free-solid-svg-icons";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');

var config = {
    apiKey: "AIzaSyAS7sELcK0DVddlXy2LQFkFp5PwLfmMmhU",
    authDomain: "ptg-application.firebaseapp.com",
    databaseURL: "https://ptg-application.firebaseio.com",
    projectId: "ptg-application",
    storageBucket: "ptg-application.appspot.com",
    messagingSenderId: "691162632611",
    appId: "1:691162632611:web:c11fce40b6bf58150eca16"
}

firebase.initializeApp(config);
const auth = firebase.auth
const provider = new firebase.auth.FacebookAuthProvider();
const providergm = new firebase.auth.GoogleAuthProvider();
provider.addScope('email');
provider.addScope('user_friends');
providergm.addScope('https://www.googleapis.com/auth/contacts.readonly');
firebase.auth().languageCode = 'en';
providergm.setCustomParameters({
    'login_hint': 'user@example.com'
});

class Header extends  React.Component {
    constructor(props){
        
        super(props);
        this.state = {
            language: 'th',
            show:false,
            modal:null,
            errors: {},
            errorsFocus:{},
            fields:{},
            user:null,
            userid:'',
            selectedOption: {
                value: 'TH', label: <div><img src={`${process.env.PUBLIC_URL}/images/TH.png`} /> {' '} </div> 
            },
            ipaddress:null,
            showPassword: false,
        };
        this.toggleShow = this.toggleShow.bind(this);
    }

    componentDidMount(){
        const { history } = this.props;

        let location = window.location.pathname;
        this.setLanguage();
        this.setLocationpath(location);

        registerAction.getLocationIP().then(e => {
            let { fields } = this.state;
            fields['udId'] = e.data.ip;
            this.setState({fields});
        })
    }

    toggleShow() {
        this.showModalLogin('','',!this.state.showPassword )
        this.setState({ 
            showPassword: !this.state.showPassword 
        });
      }

    setLocationpath(location){
        let { activeBtn1,activeBtn2,activeBtn3,activeBtn4,activeBtn5 } = this.state;
        if(location === "/" || location === "" || location === "/home"){
            activeBtn1 = "btn-active";
            activeBtn2 = "";
            activeBtn3 = "";
            activeBtn4 = "";
            activeBtn5 = "";
        }else if(location.includes("/PTmaxcard")){
            activeBtn1 = "";
            activeBtn2 = "btn-active";
            activeBtn3 = "";
            activeBtn4 = "";
            activeBtn5 = "";
        }else if(location.includes("/privileges")){
            activeBtn2 = "";
            activeBtn1 = "";
            activeBtn3 = "btn-active";
            activeBtn4 = "";
            activeBtn5 = "";
        }else if(location.includes("/news")){
            activeBtn2 = "";
            activeBtn1 = "";
            activeBtn3 = "";
            activeBtn4 = "btn-active";
            activeBtn5 = "";
        }else if(location.includes("/location") || location.includes("/Mainlocation")){
            activeBtn2 = "";
            activeBtn1 = "";
            activeBtn3 = "";
            activeBtn4 = "";
            activeBtn5 = "btn-active";
        }
        this.setState({ activeBtn1,activeBtn2,activeBtn3,activeBtn4,activeBtn5 })
    }

    setLanguage(){
        if(localStorage.getItem('language') == null){
            localStorage.setItem('language', 'th');
            this.setState({ language : 'th' });
        } else {        
            var language = localStorage.getItem('language');
            this.setState({ language : language });
        }
    }

    handleChangeLang(e) {
        if(e == "en"){
            localStorage.setItem('language', 'en');
            this.setState({ language : 'en' });
            window.location.reload(); 
        } else {
            localStorage.setItem('language', 'th');
            this.setState({ language : 'th' });
            window.location.reload(); 
        }
    };

    logingm = () => {
        var that = this;
        firebase.auth().signInWithPopup(providergm).then(function(result) {
            var token = result.credential.accessToken;
            var user = result.user;
            that.setState({user: user});
            that.loginSocialGm(user)
          }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            var email = error.email;
            var credential = error.credential;
          });
    }
    loginSocialGm = (data) => {
        localStorage.setItem("CardItem", "");
        var phone_no = '';
        if(data.phoneNumber !== null){
            phone_no = data.phoneNumber;
        }
        
        let formData = new FormData();
        formData.append('socialId' , data.uid)
        formData.append('socialType' , 'gmail')
        formData.append('email',data.email)
        formData.append('phone',phone_no)
        formData.append('udId',this.state.fields['udId'])
        formData.append('tokenId','')
        formData.append('playerId','WEBSITE')
        registerAction.loginSocial(formData).then(e => {
            if(e.data.errCode === 10){
                this.props.history.push({
                    pathname: `${process.env.PUBLIC_URL}/register/web`,
                    state: {
                      socialid : data.uid,
                      socialType: 'gmail',
                      udId:this.state.fields['udId'],
                      playerId : 'WEBSITE',
                      email:data.email,
                      phone:phone_no,
                    }
                  });
                  this.setState({ show: false , modal: null })
            }else if(e.data.isSuccess === true){
                var Profile = [];
                Profile.push({
                    "userId" : e.data.data.userId,
                    "firstName" : e.data.data.firstName,
                    "lastName" : e.data.data.lastName,
                    "phoneNo" : e.data.data.phoneNo,
                    "ptCardNo" : e.data.data.ptCardNo,
                    "email" : e.data.data.email,
                    "birthDay" : e.data.data.birthDay,
                    "tkmb" : e.data.data.tokenMobileApp,
                    "tkvsm" : e.data.data.tokeN_ID,
                    "customer_id":e.data.data.customeR_ID,
                    "isMember":e.data.data.isMemberPTG,
                    "full_address":e.data.data.fulL_ADDRESS,
                    "titlE_NAME_ID" :e.data.data.titlE_NAME_ID,
                    "titlE_NAME_REMARK":e.data.data.titlE_NAME_REMARK,
                    "citizeN_TYPE_ID":e.data.data.citizeN_TYPE_ID,
                    "citizeN_ID":e.data.data.citizeN_ID,
                    "idCard":e.data.data.idCard,
                    "customeR_STATUS":e.data.data.customeR_STATUS,
                    "isActive":e.data.data.isActive,
                    "gender" : e.data.data.gender
                });
                var Profilestringify = JSON.stringify(Profile);
                var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
                var ProfileEncrypt = Cipher.encrypt(keyCipher, Profilestringify);
                localStorage.setItem("Profile", ProfileEncrypt);
                localStorage.setItem("2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu", "true")
                this.setState({ 
                    uid : e.data.data.userId,
                    phone : e.data.data.phoneNo,
                    email : e.data.data.email,
                    ptCardNo : e.data.data.ptCardNo,
                    firstName : e.data.data.firstName,
                    lastName : e.data.data.lastName,
                    birthDay : e.data.data.birthDay,
                    gender : e.data.data.gender,
                    facebookId : e.data.data.facebookId,
                    gmailId:e.data.data.gmailId,
                    idCard:e.data.data.idCard,
                    pinCode:e.data.data.pinCode,
                    isActive:e.data.data.isActive,
                    tokeN_ID:e.data.data.tokeN_ID,
                    customeR_ID:e.data.data.customeR_ID,
                    citizeN_TYPE_ID:e.data.data.citizeN_TYPE_ID,
                    citizeN_ID:e.data.data.citizeN_ID,
                    titlE_NAME_ID:e.data.data.titlE_NAME_ID,
                    titlE_NAME_REMARK:e.data.data.titlE_NAME_REMARK,
                    fulL_ADDRESS:e.data.data.fulL_ADDRESS,
                    customeR_STATUS:e.data.data.customeR_STATUS,
                    isMemberPTG:e.data.data.isMemberPTG,
                    tokenMobileApp:e.data.data.tokenMobileApp,
                    show: false, 
                    modal: null 
                })
                if(e.data.data.ptCardNo === null){
                    localStorage.setItem("CardItem", "");
                    window.location.reload(); 
                } else {
                    localStorage.setItem("CardItem", "");
                    this.getCard(e.data.data.userId);
                }
            }
        })
    
    }

    login = () => {
        var that = this;
        firebase.auth().signInWithPopup(provider).then(function(result) {
          var user = result.user;
          that.setState({user: user});
          that.loginSocialFb(user);
        //   console.log("signInWithPopup Facebook result:", result)
        }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            if(error.code == "auth/account-exists-with-different-credential"){
                var msg = errorMessage;
                var img = `${process.env.PUBLIC_URL}/images/cancel.png`;
                that.modalCheckSubmit(msg,img);
                // that.setState({ modal : null , show : false })
            } 
            // console.log("signInWithPopup Facebook error:", error)
        });
    }
    setupLogout(){
        localStorage.removeItem("CardItem");
        localStorage.removeItem("Profile");
        localStorage.removeItem("2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu");
        localStorage.removeItem("name");
        // localStorage.clear();
        window.location.href=`${process.env.PUBLIC_URL}/home`;
        // var that = this;
        // auth().signOut().then(function() {
        //   that.setState({user: null});
        // }).catch(function(error) {
        //     console.log(error)
        // });
    }
    loginSocialFb = (data) => {
        localStorage.setItem("CardItem", "");
            var phone_no = '';
            if(data.phoneNumber !== null){
                phone_no = data.phoneNumber;
            }
            let formData = new FormData();
            formData.append('socialId' , data.uid)
            formData.append('socialType' , 'facebook')
            formData.append('email',data.email)
            formData.append('phone',phone_no)
            formData.append('udId',this.state.fields['udId'])
            formData.append('tokenId','')
            formData.append('playerId','WEBSITE')
            registerAction.loginSocial(formData).then(e => {
                // console.log("loginSocial :", e)
                if(e.data.errCode === 10){
                    this.props.history.push({
                        pathname: `${process.env.PUBLIC_URL}/register/web`,
                        state: {
                          socialid : data.uid,
                          socialType: 'facebook',
                          udId:this.state.fields['udId'],
                          playerId : 'WEBSITE',
                          email:data.email,
                          phone:phone_no,
                        }
                    });
                    this.setState({ show: false , modal: null })
                }else if(e.data.isSuccess === true){
                    var Profile = [];
                    Profile.push({
                        "userId" : e.data.data.userId,
                        "firstName" : e.data.data.firstName,
                        "lastName" : e.data.data.lastName,
                        "phoneNo" : e.data.data.phoneNo,
                        "ptCardNo" : e.data.data.ptCardNo,
                        "email" : e.data.data.email,
                        "birthDay" : e.data.data.birthDay,
                        "tkmb" : e.data.data.tokenMobileApp,
                        "tkvsm" : e.data.data.tokeN_ID,
                        "customer_id":e.data.data.customeR_ID,
                        "isMember":e.data.data.isMemberPTG,
                        "full_address":e.data.data.fulL_ADDRESS,
                        "titlE_NAME_ID" :e.data.data.titlE_NAME_ID,
                        "titlE_NAME_REMARK":e.data.data.titlE_NAME_REMARK,
                        "citizeN_TYPE_ID":e.data.data.citizeN_TYPE_ID,
                        "citizeN_ID":e.data.data.citizeN_ID,
                        "idCard":e.data.data.idCard,
                        "customeR_STATUS":e.data.data.customeR_STATUS,
                        "isActive":e.data.data.isActive,
                        "gender" : e.data.data.gender
                    });
                    var Profilestringify = JSON.stringify(Profile)
                    var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
                    var ProfileEncrypt = Cipher.encrypt(keyCipher, Profilestringify);
                    localStorage.setItem("Profile", ProfileEncrypt);
                    localStorage.setItem("2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu", "true")
                    this.setState({ 
                        uid : e.data.data.userId,
                        phone : e.data.data.phoneNo,
                        email : e.data.data.email,
                        ptCardNo : e.data.data.ptCardNo,
                        firstName : e.data.data.firstName,
                        lastName : e.data.data.lastName,
                        birthDay : e.data.data.birthDay,
                        gender : e.data.data.gender,
                        facebookId : e.data.data.facebookId,
                        gmailId:e.data.data.gmailId,
                        idCard:e.data.data.idCard,
                        pinCode:e.data.data.pinCode,
                        isActive:e.data.data.isActive,
                        tokeN_ID:e.data.data.tokeN_ID,
                        customeR_ID:e.data.data.customeR_ID,
                        citizeN_TYPE_ID:e.data.data.citizeN_TYPE_ID,
                        citizeN_ID:e.data.data.citizeN_ID,
                        titlE_NAME_ID:e.data.data.titlE_NAME_ID,
                        titlE_NAME_REMARK:e.data.data.titlE_NAME_REMARK,
                        fulL_ADDRESS:e.data.data.fulL_ADDRESS,
                        customeR_STATUS:e.data.data.customeR_STATUS,
                        isMemberPTG:e.data.data.isMemberPTG,
                        tokenMobileApp:e.data.data.tokenMobileApp,
                        show: false, 
                        modal: null 
                    })
                    if(e.data.data.ptCardNo === null){
                        localStorage.setItem("CardItem", "")
                        window.location.reload(); 
                    } else {
                        localStorage.setItem("CardItem", "");
                        this.getCard(e.data.data.userId)
                    }
                }
            })
    }
    handleChange(e){
        let { fields , errors , errorsFocus } = this.state;
        if(e.target.name === 'USERNAME'){
            fields['USERNAME'] = e.target.value;
        }else if(e.target.name === 'PASSWORD'){
            fields['PASSWORD'] = e.target.value;
        }else{
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
        }

        this.setState({ 
            errorsFocus,
            errors, 
            fields
        })
    }
    submitForm(e){
        localStorage.setItem("CardItem", "");
        e.preventDefault();
        let { fields} = this.state; 
        fields[e.target.name] = e.target.value;
        if(this.validateForm()){
            (async () => {
                // var ipaddress = await publicIp.v4();
                let formData = new FormData();
                formData.append('userName' , fields['USERNAME']);
                formData.append('password' , fields['PASSWORD']);
                formData.append('udId',this.state.fields['udId']);
                formData.append('tokenId','');
                formData.append('playerId','WEBSITE');
                registerAction.loginWebsite(formData).then( e => {
                    if(e.data.isSuccess === true){
                        var Profile = [];
                        Profile.push({
                            "userId" : e.data.data.userId,
                            "firstName" : e.data.data.firstName,
                            "lastName" : e.data.data.lastName,
                            "phoneNo" : e.data.data.phoneNo,
                            "ptCardNo" : e.data.data.ptCardNo,
                            "email" : e.data.data.email,
                            "birthDay" : e.data.data.birthDay,
                            "tkmb" : e.data.data.tokenMobileApp,
                            "tkvsm" : e.data.data.tokeN_ID,
                            "customer_id":e.data.data.customeR_ID,
                            "isMember":e.data.data.isMemberPTG,
                            "full_address":e.data.data.fulL_ADDRESS,
                            "titlE_NAME_ID" :e.data.data.titlE_NAME_ID,
                            "titlE_NAME_REMARK":e.data.data.titlE_NAME_REMARK,
                            "citizeN_TYPE_ID":e.data.data.citizeN_TYPE_ID,
                            "citizeN_ID":e.data.data.citizeN_ID,
                            "idCard":e.data.data.idCard,
                            "customeR_STATUS":e.data.data.customeR_STATUS,
                            "isActive":e.data.data.isActive,
                            "gender" : e.data.data.gender
                        });
                        var Profilestringify = JSON.stringify(Profile)
                        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
                        var ProfileEncrypt = Cipher.encrypt(keyCipher, Profilestringify);
                        localStorage.setItem("Profile", ProfileEncrypt);
                        localStorage.setItem("2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu", "true")
                        localStorage.setItem("name", e.data.data.firstName)
                        this.setState({ 
                            uid : e.data.data.userId,
                            phone : e.data.data.phoneNo,
                            email : e.data.data.email,
                            ptCardNo : e.data.data.ptCardNo,
                            firstName : e.data.data.firstName,
                            lastName : e.data.data.lastName,
                            birthDay : e.data.data.birthDay,
                            gender : e.data.data.gender,
                            facebookId : e.data.data.facebookId,
                            gmailId:e.data.data.gmailId,
                            idCard:e.data.data.idCard,
                            pinCode:e.data.data.pinCode,
                            isActive:e.data.data.isActive,
                            tokeN_ID:e.data.data.tokeN_ID,
                            customeR_ID:e.data.data.customeR_ID,
                            citizeN_TYPE_ID:e.data.data.citizeN_TYPE_ID,
                            citizeN_ID:e.data.data.citizeN_ID,
                            titlE_NAME_ID:e.data.data.titlE_NAME_ID,
                            titlE_NAME_REMARK:e.data.data.titlE_NAME_REMARK,
                            fulL_ADDRESS:e.data.data.fulL_ADDRESS,
                            customeR_STATUS:e.data.data.customeR_STATUS,
                            isMemberPTG:e.data.data.isMemberPTG,
                            tokenMobileApp:e.data.data.tokenMobileApp,
                            show: false, 
                            modal: null 
                        })
                        if(e.data.data.ptCardNo === null){
                            localStorage.setItem("CardItem", "");
                            window.location.reload(); 
                        } else {
                            localStorage.setItem("CardItem", "");
                            this.getCard(e.data.data.userId);
                        }
                    }else{
                        var msg = e.data.errMsg;
                        var img = `${process.env.PUBLIC_URL}/images/cancel.png`;
                        this.modalCheckSubmit(msg,img);
                    }
                })
            })();
        }else{
            this.showModalLogin('errorFocus','กรุณากรอกข้อมูล')
        }
    }
    validateForm(){
        let fields = this.state.fields;
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;

        if(!fields['USERNAME']){
            formIsValid = false;
            errors["USERNAME"] = 'กรุณากรอกข้อมูล'
            errorsFocus["USERNAME"] = 'errorFocus'
        }
        if(!fields['PASSWORD']){
            formIsValid = false;
            errors["PASSWORD"] = 'กรุณากรอกข้อมูล'
            errorsFocus["PASSWORD"] = 'errorFocus'
        }

        this.setState({
            errors: errors,
            errorsFocus: errorsFocus
        });
      
        return formIsValid;
    }
    modalCheckSubmit(res, img) {
        alert = (
        <SweetAlert
            custom
            showCloseButton
            closeOnClickOutside={false}
            focusConfirmBtn={false}
            title=""
            customIcon={img}
            showConfirm={false}
            showCancelButton
            onCancel={() => this.handleChoice(false)}
            onConfirm={() => this.handleChoice(true)}
        >
            <div className="iconClose" onClick={() => this.handleChoice(false)}></div>
            <div className="fontSizeCase">{res}</div>
        </SweetAlert>
        );
        this.setState({ show: true, modal: alert });
    }

    

    handleChoice(choice) {
        if (choice === false) {
        if(this.state.disableAgreement == true){
            this.setState({ show: false , modal: null })
        }else{
            this.setState({ show: false , modal: null })
        }
        }
    }

    showModalLogin(errorsFocus,errors,showPassword){
        if(showPassword == undefined){
            var showPassword = false
            var eye = <FontAwesomeIcon icon={faEye} />
        }else{
            var eye = (showPassword ? <FontAwesomeIcon icon={faEyeSlash} /> : <FontAwesomeIcon icon={faEye} />)
        }
        alert = (
            <SweetAlert
                custom
                closeOnClickOutside = {true}
                showConfirm = {false}
                customClass='setpositionmodal'
            >
                <div className="row pb-3">
                    <div className="col-lg-6 col-12 banner-login">
                        <img className="img-fluid w-100" src={`${process.env.PUBLIC_URL}/images/Untitled-1-02.jpg`} />
                    </div>
                    <div className="col-lg-6 col-12">
                        <form onSubmit={(e) => this.submitForm(e)} className="form-login">
                            <div className="row">
                                <div className="col-9 header-sigin">ลงชื่อเข้าใช้</div>
                                <div className="col-3">
                                    <img className="img-fluid btn-close-login" src={`${process.env.PUBLIC_URL}/images/cancel.png`} width="30" onClick={(e) => this.handleChoice(e)}/>
                                </div>
                            </div>
                            <div className="form-group pt-3 text-left">
                                <label className="header-sigin-labelmail" style={{display: "block"}}>เบอร์โทรศัพท์ / อีเมล</label>
                                <input 
                                    type="text"
                                    className={`form-control-set login-input ${errorsFocus}`}
                                    name="USERNAME"
                                    onChange={(e) => this.handleChange(e)}
                                />
                            </div>
                            <div className="errorMsg USERNAME">{errors}</div>
                            <div className="form-group pt-2 text-left form-password">
                                <label className="header-sigin-labelpass">รหัสผ่าน</label>
                                <input 
                                    type={showPassword ? 'text' : 'password'}
                                    className={`form-control-set login-input ${errorsFocus}`}
                                    name="PASSWORD"
                                    onChange={(e) => this.handleChange(e)}
                                />
                                <i className="eye" onClick={this.toggleShow}>{eye}</i>
                            </div>
                            <div className="errorMsg PASSWORD">{errors}</div>
                            <div className="pt-2">
                                <a onClick={() => window.location.href = `${process.env.PUBLIC_URL}/forgotpassword`}>
                                    <div className="label-forgot">ลืมรหัสผ่าน ?</div>
                                </a>
                            </div>
                            <button className="btn-block btn-signin-mail mt-3 mb-2" type="sumbit">
                                <div style={{marginTop: '5px'}}>เข้าสู่ระบบ</div>
                            </button>
                        </form>
                        <button className="btn-block btn-signin-fb" onClick={() => this.login()}>
                            <div style={{marginTop: '5px'}}>เข้าสู่ระบบด้วย Facebook</div>
                        </button>
                        <button className="btn-block btn-signin-gm mb-4" onClick={() => this.logingm()}>
                            <div style={{marginTop: '5px'}}>เข้าสู่ระบบด้วย Gmail</div>
                        </button>
                        <button className="btn-block btn-signin-mail" onClick={() => window.location.href = `${process.env.PUBLIC_URL}/register`}>
                            <div style={{marginTop: '5px'}}>สมัครสมาชิกใหม่</div>
                        </button>
                    </div>
                </div>
                {/* <div className="text-right" onClick={(e) => this.handleChoice(e)}><i className="fas fa-times icon-setfontsize"></i></div>
                <div className="row">
                    <div className="col-sm-6 col-xs-6 col-md-6 col-lg-6">
                        <img style={{width:'100%'}} src={`${process.env.PUBLIC_URL}/images/Untitled-1-02.jpg`} />
                    </div>
                    <div className="col-sm-6 col-xs-6 col-md-6 col-lg-6 mt-2 col-6-lineh">
                        <form onSubmit={(e) => this.submitForm(e)}>
                            <div className="header-sigin">ลงชื่อเข้าใช้</div>
                            <div className="my-3 label-header">
                                <label className="label-text-pc">เบอร์โทรศัพท์ / อีเมล</label>
                                <input 
                                    type="text"
                                    className={`form-control-set ${errorsFocus}`}
                                    name="USERNAME"
                                    onChange={(e) => this.handleChange(e)}
                                />
                            </div>
                            <div className="errorMsg USERNAME">{errors}</div>
                            <div className="my-3 label-header">
                                <label className="label-text-pc">รหัสผ่าน</label>
                                <input 
                                    type="password"
                                    className={`form-control-set ${errorsFocus}`}
                                    name="PASSWORD"
                                    onChange={(e) => this.handleChange(e)}
                                />
                            </div>
                            <div className="errorMsg PASSWORD">{errors}</div>
                            <a onClick={() => window.location.href = `${process.env.PUBLIC_URL}/forgotpassword`}>
                                <div className="label-forgot">ลืมรหัสผ่าน ?</div>
                            </a>
                            <button className="loginandregis-btn" type="sumbit">
                                <div style={{marginTop: '5px'}}>เข้าสู่ระบบ</div>
                            </button>
                        </form>
                        <button className="loginregis-fb-btn" onClick={() => this.login()}>
                            <div style={{marginTop: '5px'}}>เข้าสู่ระบบด้วย Facebook</div>
                        </button>
                        <button className="loginregis-gm-btn" onClick={() => this.logingm()}>
                            <div style={{marginTop: '5px'}}>เข้าสู่ระบบด้วย Gmail</div>
                        </button>
                        <button className="loginandregis-btn mt-3" onClick={() => window.location.href = `${process.env.PUBLIC_URL}/register`}>
                            <div style={{marginTop: '5px'}}>สมัครสมาชิกใหม่</div>
                        </button>
                    </div>
                </div> */}
            </SweetAlert>
        );
        this.setState({ show: true, modal: alert });
    }

    handleChoice(){
        this.setState({ modal : null , show : false })
    }

    showBtn(showBtn){
        if(showBtn === 'none'){
            this.setState({ showDropDown : 'block' })
        }else{
            this.setState({ showDropDown : 'none' })
        }
    }

    getCard(userId){
        maxcardActions.getCardList(userId).then(e => {
            if(e.data.isSuccess === true){
                if(e.data.data.length === 0){
                    localStorage.setItem("CardItem", "");
                    window.location.reload();
                }else{
                    var CardItem = e.data.data[0]
                    var CardItemstringify = JSON.stringify(CardItem)
                    var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
                    var CardItemEncrypt = Cipher.encrypt(keyCipher, CardItemstringify);
                    localStorage.setItem("CardItem", CardItemEncrypt);
                    var resp = e.data.data[0].carD_TYPE_ID;
                    window.location.reload();
                }
            }else{
                localStorage.setItem("CardItem",false);
                window.location.reload();
            }
        });
    }

    PTmaxcard(){
        if(localStorage.getItem('2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu') === "true"){
            var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
            var ProfileData = localStorage.getItem('Profile')
            var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
            ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
            var ProfileArray = JSON.parse(ProfileDecrypt);
            var Profile = ProfileArray[0];
            if(Profile.ptCardNo === null){
                window.location.href=`${process.env.PUBLIC_URL}/PTmaxcard/register/nocard`;
            } else {
                window.location.href=`${process.env.PUBLIC_URL}/PTmaxcard`;
            }
        } else {
            this.showModalLogin();
        }
    }

    registerPTmaxcard(){
        if(localStorage.getItem('2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu') === "true"){
            var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
            var ProfileData = localStorage.getItem('Profile')
            var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
            ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
            var ProfileArray = JSON.parse(ProfileDecrypt);
            var Profile = ProfileArray[0];
            if(Profile.userId === 'login'){
                window.location.href=`${process.env.PUBLIC_URL}/PTmaxcard/register`
            } else {
                this.showModalLogin()
            }
        } else {
            this.showModalLogin()
        }
    }

    checkPrivileges(){
        // if(localStorage.getItem('2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu') === "true"){
        //     var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
        //     var ProfileData = localStorage.getItem('Profile')
        //     var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
        //     ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        //     var ProfileArray = JSON.parse(ProfileDecrypt);
        //     var Profile = ProfileArray[0];
        //     if(Profile.ptCardNo === "null"){
        //         window.location.href=`${process.env.PUBLIC_URL}/privileges`
        //     } else {
        //         window.location.href=`${process.env.PUBLIC_URL}/privileges`
        //     }
        // } else {
            window.location.href=`${process.env.PUBLIC_URL}/privileges`
        // }
    }

    openModal = (part) => {
        let Profile = localStorage.getItem('Profile')
        if(Profile){
            window.location.href = part
        }else{
            var msg = <FormattedMessage id="pleaselogin" />;
            var img = `${process.env.PUBLIC_URL}/images/cancel.png`;

            this.modalCheckSubmit(msg,img)
        }
    };
    
 

    render(){

        const { selectedOption } = this.state; 
        if(localStorage.getItem('2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu') != null){
            var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
            var ProfileData = localStorage.getItem('Profile')
            var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
            ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
            var ProfileArray = JSON.parse(ProfileDecrypt);
            var Profile = ProfileArray[0];
        }
        const options = [
            { value: 'TH', label: <div><img src={`${process.env.PUBLIC_URL}/images/TH.png`} /> {' '} </div> },
            { value: 'EN', label: <div><img src={`${process.env.PUBLIC_URL}/images/EN.png`} /> {' '} </div> },
        ];
        return (
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].Header}
            >
                <div className="container-center">
                    <div className="header-mobile">
                        <img className="img-fluid w-100" src={`${process.env.PUBLIC_URL}/images/PT_WEBSITE-01-HomePage-07.jpg`} />
                        <nav className="navbar navbar-expand-lg nav-headermobile">
                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <img className="" src={`${process.env.PUBLIC_URL}/images/menu-01@3x.png`} width="18"/>
                            </button>
                            <a className="p-2" href={`${process.env.PUBLIC_URL}/home`}><img className="img-fluid" src={`${process.env.PUBLIC_URL}/images/Logo_web.png`} width="80" /></a>
                            {localStorage.getItem('2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu') === "true" ?  
                            <a className="navbar-brand dropdown-toggle" data-toggle="dropdown">
                                <img className="img-fluid" width="30" src={`${process.env.PUBLIC_URL}/images/login.png`}/>
                            </a>
                            : 
                            <a className="navbar-brand" onClick={() => this.showModalLogin()}>
                                <img className="img-fluid" width="30" src={`${process.env.PUBLIC_URL}/images/login.png`}/>
                            </a>
                            }
                            <div className="dropdown-menu dropdown-user-mobile">
                                <a className="dropdown-item dropdown-useritem-mobile" href={`${process.env.PUBLIC_URL}/editprofile`} >
                                    <FormattedMessage id="editprofile" />
                                </a>
                                <a className="dropdown-item dropdown-useritem-mobile" href={`${process.env.PUBLIC_URL}/newpassword`}>
                                    <FormattedMessage id="editpassword" />
                                </a>
                                <a className="dropdown-item dropdown-useritemsignout-mobile" onClick={() => this.setupLogout()} >
                                    <FormattedMessage id="signout" />
                                </a>
                            </div>
                            <div className="collapse navbar-collapse bg-nav-headermobile" id="navbarSupportedContent">
                                <ul className="navbar-nav">
                                    <li className="nav-item active">
                                        <a className="nav-link font-nav-mobile border-nav-mobile" href={`${process.env.PUBLIC_URL}/home`}><FormattedMessage id="home" /> <span className="sr-only">(current)</span></a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link font-nav-mobile border-nav-mobile" href="#" onClick={() => this.openModal(`${process.env.PUBLIC_URL}/PTmaxcard`)}><FormattedMessage id="ptmaxcard" /></a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link font-nav-mobile border-nav-mobile" href={`${process.env.PUBLIC_URL}/privileges`}><FormattedMessage id="reward" /></a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link font-nav-mobile border-nav-mobile" href={`${process.env.PUBLIC_URL}/news`}><FormattedMessage id="newsactivity" /></a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link font-nav-mobile" href={`${process.env.PUBLIC_URL}/Mainlocation`}><FormattedMessage id="loaction" /></a>
                                    </li>
                                </ul>
                                {/* <button onClick={() => this.registerPTmaxcard()} className="btn btn-block btn-nav-headermobile">
                                    <FormattedMessage id="signin" />
                                </button> */}
                            </div>
                        </nav>
                    </div>

                    <div className="header-desktop">
                        <img style={{width:'100%'}} src={`${process.env.PUBLIC_URL}/images/PT_WEBSITE-01-HomePage-07.jpg`} />
                        <img className="centered logo-header" src={`${process.env.PUBLIC_URL}/images/Logo_web.png`} />
                        <div onClick={() => this.showBtn(this.state.showDropDown)}><img className="mobile-pt logo-header settitlemb" src={`${process.env.PUBLIC_URL}/images/login.png`}/></div>
                        <div className="nav-topmenu">
                            <div className="nav-inline"><button onClick={() => window.location.href=`${process.env.PUBLIC_URL}/home`} className={`btn-custom ${this.state.activeBtn1}`}><FormattedMessage id="home" /></button></div>
                            <div className="nav-inline"><button onClick={() => this.PTmaxcard()} className={`btn-custom ${this.state.activeBtn2}`}><div><FormattedMessage id="ptmaxcard" /></div></button></div>
                            <div className="nav-inline"><button onClick={() => this.checkPrivileges()} className={`btn-custom ${this.state.activeBtn3}`}><FormattedMessage id="reward" /></button></div>
                            <div className="nav-inline"><button onClick={() => window.location.href=`${process.env.PUBLIC_URL}/news`} className={`btn-custom ${this.state.activeBtn4}`}><FormattedMessage id="newsactivity" /></button></div>
                            <div className="nav-inline"><button onClick={() => window.location.href=`${process.env.PUBLIC_URL}/Mainlocation`} className={`btn-custom ${this.state.activeBtn5}`}><FormattedMessage id="loaction" /></button></div>
                            <div className={localStorage.getItem('2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu') === "true" ? "navmenu-displaynone" : "nav-inline"}>
                                <button onClick={() => this.registerPTmaxcard()} className="btn-custom border-signin">
                                    <div><FormattedMessage id="register" /></div>
                                </button>
                            </div>
                            {localStorage.getItem('2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu') === "true" ?  
                                <div className="dropdown nav-inline content-profile">
                                    <button className="btn-custom btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img className="icon-login-true pr-1" src={`${process.env.PUBLIC_URL}/images/Pericon.png`} />
                                        {Profile.firstName}
                                    </button>
                                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a className="dropdown-item" href={`${process.env.PUBLIC_URL}/editprofile`} >
                                            <FormattedMessage id="editprofile" />
                                        </a>
                                        <a className="dropdown-item" href={`${process.env.PUBLIC_URL}/newpassword`}>
                                            <FormattedMessage id="editpassword" />
                                        </a>
                                        <a className="dropdown-item dropdown-item-signout" onClick={() => this.setupLogout()} >
                                            <FormattedMessage id="signout" />
                                        </a>
                                    </div>
                                </div> 
                                : 
                                <div className="nav-inline">
                                    <button 
                                        className="btn-custom" 
                                        type="button"
                                        onClick={() => this.showModalLogin()}
                                    >
                                        <img className="icon-login" src={`${process.env.PUBLIC_URL}/images/login.png`} />
                                        <FormattedMessage id="signin" />
                                    </button>
                                </div>
                            }
                            <div className="nav-inline line-changelang">|</div>
                            <div className="nav-inline btn-changelang">
                                <a className="dropdown-toggle" data-toggle="dropdown">
                                    {this.state.language == "th" ? "TH" : "EN"}
                                    {/* <img className="img-fluid" width="30" src={`${process.env.PUBLIC_URL}/images/TH.png`} /> */}
                                </a>
                                <div className="dropdown-menu">
                                    <a className="dropdown-item" onClick={() => this.handleChangeLang('th')}>
                                        TH
                                        {/* <img className="img-fluid" width="30" src={`${process.env.PUBLIC_URL}/images/TH.png`} /> */}
                                    </a>
                                    <a className="dropdown-item" onClick={() => this.handleChangeLang('en')}>
                                        EN
                                        {/* <img className="img-fluid" width="30" src={`${process.env.PUBLIC_URL}/images/EN.png`} /> */}
                                    </a>
                                </div>
                                {/* <Select
                                    value={selectedOption}
                                    onChange={this.handleChangeLang}
                                    options={options}
                                    isSearchable={false}
                                    className="btn-custom"
                                /> */}
                            </div>
                        </div>
                    </div>
                {this.state.modal}
                </div>
            </IntlProvider>
        )
    }

}
export default withRouter(Header);