import React from 'react';
import './404.css';

export default () => (
  <div className="height-404">
    <div className="container-fluid">
      <div className="row">
        <div className="col-12 text-center"> 
            <div className="card card-body height-404">
              <h1 className="h1-404">404</h1>
              <h5 className="h5-404">Oops, an error has occurred. Page not found!</h5>
            </div>
        </div>
      </div>
    </div>
  </div>
)
