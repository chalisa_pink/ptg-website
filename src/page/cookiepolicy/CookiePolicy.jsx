import React from 'react';

import LazyLoad from 'react-lazy-load';
import moment from "moment-timezone";
import 'moment/locale/th';

import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../translations/en.json";
import intlMessageTH from "../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

export class CookiePolicy extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'none',
        };
    }

    componentDidMount(){
    }

    render(){
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].PrivacyPolicy}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="bg-ptmaxcard-content policy-content">
                            <div className="container">
                                <div className="row pb-4">
                                    <div className="col-12">
                                        <div className="policy-title">นโยบายคุกกี้</div>
                                    </div>
                                </div>
                                <div className="policy-card-body">
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="policy-subtitle">คุกกี้คืออะไร</div>
                                            <span className="policy-text">
                                                <span className="policy-text-space">คุกกี้ (Cookies) คือไฟล์ขนาดเล็ก ซึ่งอาจถูกติดตั้ง และ/หรือจัดเก็บไว้ในอุปกรณ์ของท่าน (เช่น คอมพิวเตอร์, โทรศัพท์มือถือ, แท็บเล็ต เป็นต้น) เมื่อท่านได้เข้าเยี่ยมชมเว็บไซต์หนึ่ง ๆ และคุกกี้นั้นจะถูกส่งกลับไปยังเว็บไซต์ต้นทางในแต่ละครั้งของการเข้าเยี่ยมชมในครั้งต่อๆ มา หรือไปยังเว็บไซต์อื่นๆ ที่จดจำคุกกี้นั้นๆ ได้</span>
                                                <br/>
                                                <span className="policy-text-space">คุกกี้มีหลายประเภท โดยอาจเป็นประเภทที่สามารถทำให้เว็บไซต์หนึ่งๆ ทำงานได้ หรือทำงานได้อย่างมีประสิทธิภาพ หรืออาจทำหน้าที่เก็บรวบรวมข้อมูลส่วนบุคคลของท่าน และติดตามกิจกรรมออนไลน์ของท่าน เพื่อนำมาใช้ในการพัฒนาเว็บไซต์ และแสดงบทความที่เกี่ยวข้องกับท่าน หรือนำไปใช้เพื่อวัตถุประสงค์ทางการตลาดได้</span>
                                            </span>
                                            <br/>
                                            <br/>
                                            <div className="policy-subtitle">ประเภทคุกกี้ที่เราใช้</div>
                                            <span className="policy-text">
                                                เราจะใช้คุกกี้เมื่อท่านได้เข้าเยี่ยมชมเว็บไซต์ของเรา โดยสามารถแบ่งประเภทคุกกี้ตามการใช้งานได้ ดังนี้
                                                <br/>
                                                (1) คุกกี้ประเภทจำเป็นถาวร (Strictly Necessary Cookies) คือ คุกกี้ที่มีความจำเป็นเพื่อให้เว็บไซต์สามารถทำงานได้ และให้ท่านสามารถใช้คุณสมบัติต่างๆ ในเว็บไซต์ของเราได้
                                                <br/>
                                                (2) คุกกี้เพื่อการวิเคราะห์/เพื่อประสิทธิภาพ (Analytical/Performance Cookies) คือ คุกกี้ที่สามารถเก็บรวบรวมพฤติกรรมในการเข้าเยี่ยมชมเว็บไซต์ รวมถึงรวบรวมข้อมูลทางสถิติเกี่ยวกับวิธีการเข้าถึง และพฤติกรรมการเข้าเยี่ยมชมเว็บไซต์ของท่าน ซึ่งจะช่วยให้ท่านสามารถค้นหาสิ่งที่ต้องการได้อย่างง่ายดาย และช่วยให้เราเข้าใจถึงความสนใจของท่าน อีกทั้งยังสามารถวัดความมีประสิทธิผลของโฆษณาของเราได้อีกด้วย
                                            </span>
                                            <br/>
                                            <br/>
                                            <div className="policy-subtitle">รายการคุกกี้ที่เราใช้</div>
                                            <div className="policycookie-text table-responsive">
                                                <table className="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <td>ประเภทของคุกกี้</td>
                                                        <td>ชื่อ</td>
                                                        <td>วัตถุประสงค์</td>
                                                        <td>ระยะเวลาในการจัดเก็บ</td>
                                                        <td>Cookies Provider</td>
                                                    </tr>
                                                    </thead>
                                                    <tr>
                                                        <td rowspan="2">คุกกี้ประเภทจำเป็นถาวร (Strictly necessary cookies)</td>
                                                        <td>PHPSE SSID</td>
                                                        <td>คุกกี้ประเภทนี้จะถูกนำมาใช้เพื่อการเก็บ และแยกรหัสเซสชั่น (Session ID) ที่ไม่ซ้ำกันของท่านในฐานะที่เป็นผู้เข้าเยี่ยมชมเว็บไซต์ เพื่อการจัดการเซสชั่น (Session) ของผู้เข้าเยี่ยมชมบนเว็บไซต์ในแต่ละราย</td>
                                                        <td>ลบเมื่อปิดเบราว์เซอร์</td>
                                                        <td>เว็บไซต์</td>
                                                    </tr>
                                                    <tr>
                                                        <td>ASP.NET Session Id</td>
                                                        <td>คุกกี้ประเภทนี้จะถูกนำมาใช้เพื่อการนำรหัสเซชชั่น (Session ID) ไปใช้ในส่วนของการทำงานที่เกี่ยวกับโปรแกรม</td>
                                                        <td>ลบเมื่อปิดเบราว์เซอร์</td>
                                                        <td>เว็บไซต์</td>
                                                    </tr>
                                                    <tr>
                                                        <td >คุกกี้เพื่อการวิเคราะห์/เพื่อประสิทธิภาพ (Analytical/Performance Cookies)</td>
                                                        <td>_ga<br/>_gali<br/>_gat<br/>_gid</td>
                                                        <td>คุกกี้เหล่านี้จะถูกนำมาใช้เพื่อเก็บรวบรวมข้อมูลเกี่ยวกับการที่ท่านในฐานะที่เป็นผู้เข้าเยี่ยมชม และใช้งานในเว็บไซต์ของเราโดยเราจะนำคุกกี้นี้ไปใช้ในการรวบรวมรายงานดังกล่าวสำหรับนำไปใช้เป็นข้อมูลในการปรับปรุงเว็บไซต์ต่อไป</td>
                                                        <td>สำหรับนำไปใช้เป็นข้อมูลในการปรับปรุงเว็บไซต์ต่อไป</td>
                                                        <td>Google Analytics</td>
                                                    </tr>
                                                </table>
                                            </div>

                                            <br/>
                                            <div className="policy-subtitle">การจัดการคุกกี้</div>
                                            <span className="policy-text">
                                                <span className="policy-text-space">ท่านสามารถจะจัดการการใช้งานคุกกี้ได้ตลอดเวลาโดยการคลิกไปที่ไอคอน ซึ่งท่านสามารถคลิกเพื่อเปลี่ยนการตั้งค่าได้จาก ‘ยอมรับ/ปฏิเสธ’ เป็น ‘ยอมรับ/ปฏิเสธ’ หรือ ‘ยอมรับ/ปฏิเสธ’ เป็น ‘ยอมรับ/ปฏิเสธ’ หลังจากนั้นให้ท่านคลิก ‘บันทึกและปิด’ และท่านอาจจะทำการกด ‘รีเฟรช (Refresh)’ เพื่อเป็นการบันทึกการเปลี่ยนแปลงการตั้งค่าดังกล่าวได้อีกด้วย นอกจากนี้ท่านยังสามารถจะจัดการการใช้งานคุกกี้ทางบราวเซอร์ หรือหมวดการตั้งค่าในอุปกรณ์มือถือ หรือแท็บเล็ตของท่านได้ ดังนี้</span>
                                                <br/>
                                                <a className="policy-text-space" href="https://support.google.com/accounts/answer/61416?co=GENIE.Platform%3DDesktop&hl=en" target="_blank">
                                                    (1) Google Chrome 
                                                </a>
                                                <br/>
                                                <a className="policy-text-space" href="https://privacy.microsoft.com/en-us/windows-10-microsoft-edge-and-privacy" target="_blank">
                                                    (2) Microsoft Edge
                                                </a>
                                                <br/>
                                                <a className="policy-text-space" href="https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences" target="_blank">
                                                    (3) Mozilla Firefox
                                                </a>
                                                <br/>
                                                <a className="policy-text-space" href="https://support.microsoft.com/en-gb/help/17442/windows-internet-explorer-delete-manage-cookies" target="_blank">
                                                    (4) Microsoft Internet Explorer
                                                </a>
                                                <br/>
                                                <a className="policy-text-space" href="https://www.opera.com/help/tutorials/security/privacy/" target="_blank">
                                                    (5) Opera
                                                </a>
                                                <br/>
                                                ทั้งนี้ หากท่านปฏิเสธการใช้งานคุกกี้ เว็บไซต์อาจจะไม่สามารถทำงานได้ตามวัตถุประสงค์ที่กำหนดไว้ได้ ในการนี้ ท่านสามารถจะจัดการการใช้ Google Analytics Cookies ในทุกเว็บไซต์ได้ใน <a href="http://tools.google.com/dlpage/gaoptout" target="_blank">http://tools.google.com/dlpage/gaoptout. </a> และสามารถจะศึกษารายละเอียดเพิ่มเติมเกี่ยวกับคุกกี้ได้ใน <a href="www.aboutcookies.org" target="_blank">www.aboutcookies.org</a> หรือ <a href="www.allaboutcookies.org" target="_blank">www.allaboutcookies.org</a>
                                            </span>
                                            
                                            <br/>
                                            <br/>
                                            <div className="policy-subtitle">การปรับปรุงนโยบายคุกกี้</div>
                                            <span className="policy-text">
                                                <span className="policy-text-space">นโยบายคุกกี้นี้อาจได้รับการแก้ไขปรับปรุงเพื่อให้เป็นไปตามแนวปฏิบัติ กฎหมาย ข้อกำหนด หรือข้อบังคับต่างๆ ที่เกี่ยวข้องได้ ดังนั้น เราจึงขอแนะนำให้ท่านเข้ามาตรวจสอบการแก้ไขปรับปรุงนโยบายดังกล่าวเป็นครั้งคราว โดยนโยบายฉบับนี้ได้มีการปรับปรุงในครั้งล่าสุดเมื่อ [14/02/2021]</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </IntlProvider>
        )
    }
}
