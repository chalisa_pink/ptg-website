import React from 'react';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import SweetAlert from "react-bootstrap-sweetalert";
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

export class BannerDetail extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading:'none',
            errorcheckThisProps: false,
        };
    }
    componentDidMount(){
        if(this.props.location.state == undefined){
            this.setState({ errorcheckThisProps: true });
        } else {
            var type = this.props.location.state.type
            var bannerurl = this.props.location.state.bannerurl
            var bannercover = this.props.location.state.bannercover
            var bannertitle = this.props.location.state.bannertitle
            var bannerdetailth = this.props.location.state.bannerdetailth
            var bannerdetailen = this.props.location.state.bannerdetailen
            this.setState({ 
                type: type,
                bannerurl: bannerurl,
                bannercover: bannercover,
                bannertitle: bannertitle,
                bannerdetailth: bannerdetailth,
                bannerdetailen: bannerdetailen,
            });
        }
    }

    errorcheckThisProps(){
        window.location.href = `${process.env.PUBLIC_URL}/home`;
    }

    render(){
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].BannerDetail}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="bg-web-whitecontent">
                            <div className="row">
                                <div className="col-12 px-0">
                                    <img className="img-fluid img-banner-detail" src={this.state.bannercover} />
                                </div>
                            </div>
                            <div className="ptmaxcard-content">
                                <div className="row py-5">
                                    <div className="col-12">
                                    { ReactHtmlParser(this.state.bannerdetailth) }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <SweetAlert   
                        show={this.state.errorcheckThisProps}
                        confirmBtnCssClass="btn btn-block btn-error-redeem"
                        confirmBtnText={<FormattedMessage id="btnok" />}
                        onConfirm={() => this.errorcheckThisProps()}
                    >
                        <span>
                            <img 
                                src={process.env.PUBLIC_URL +"/images/cancel.png"}
                                className="py-3" 
                                alt="sena"
                                width="100"
                            />
                            <div className="t-36 mt-4"><FormattedMessage id="errormessage" /></div>
                        </span>
                    </SweetAlert>
                </div>
            </IntlProvider>
        )
    }
}