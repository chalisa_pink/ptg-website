import React from 'react';
import { homeAction } from '../../../_actions/homeAction';
import { newActions } from '../../../_actions/newActions';
import { registerAction } from '../../../_actions/registerActions';
import Carousel from 'react-bootstrap/Carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import '../../home/home.css';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import LazyLoad from 'react-lazy-load';
import SweetAlert from 'react-bootstrap-sweetalert';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
import queryString from 'query-string';

var Cipher = require('aes-ecb'); 
var crypto = require('crypto');
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

export class home extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            dataBanner: [],
            iconDisplay: [],
            showDropDown:'none',
            modal: null,
            show: false ,
            chkLengthPromotion:0,
            Profile : "",
            CardItem : "",
            GetCustomerProfile: {},
            check_showPopup: true,
            getTokenCardId: "",
            isHavepromotionDisPlay: false,
            fields: []
        };
    }

    componentDidMount(){
        let dont_show_popup_banner = localStorage.getItem("isDontShowPopupBanner");

        var array_dont_show_popup_banner = []
        if(dont_show_popup_banner!=null && dont_show_popup_banner!=""){
            array_dont_show_popup_banner = JSON.parse("[" + dont_show_popup_banner + "]");
        }
        this.getBannerInHome();        
        this.getInfoProfileCard();
        this.getNewsList();
        this.setState({
            dont_show_popup_banner: array_dont_show_popup_banner
        })
    }

    handleCkeckDontShow(e, id){
        let { fields, dont_show_popup_banner } = this.state;
        fields[e.target.id] = e.target.checked
        if(e.target.checked == true){
            dont_show_popup_banner.push(id)
        }else{
            let indexOf_id = dont_show_popup_banner.indexOf(id)
            dont_show_popup_banner.splice(indexOf_id, 1);
        }
        localStorage.setItem("isDontShowPopupBanner", `${dont_show_popup_banner}`);
        this.setState({ fields, dont_show_popup_banner })
    }

    showPopup(popuplist){
        let show = false
        alert = ""
            if(this.state.dont_show_popup_banner != "" && this.state.dont_show_popup_banner != null && popuplist.isShow == false){
                if( (this.state.dont_show_popup_banner).find(e => e == popuplist.bannerId) ){
                    show = false
                    alert = ""
                }else{
                    show = true
                    
                    alert = (
                        <div className="home-popupbanner">
                            <SweetAlert
                                custom
                                style={{ backgroundColor:'#fff0', color: 'white'}}
                                showConfirm={false}
                            >
                                <div className="row">
                                    <div className="col-9 col-md-9 p-0 text-left">
                                    </div>
                                    <div className="col-3 col-md-3 p-0 text-right">
                                        <button type="button" className="btn home-popupbanner-btn" onClick={() => this.onClosePopup()}>Close</button>
                                    </div>
                                </div>
                                <div className="row row-banner">
                                    <div className="col-12 p-0">
                                        <img src={popuplist.banner_Cover} onClick={this.redirectTolinkout(popuplist.banner_URL)} className="img-banner"/>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12 col-md-12 p-0 text-left">
                                        <label className="label_checkpopup" style={{display: ( popuplist.isShow == false ? 'flex' : 'none')}}>
                                            ไม่ต้องการแสดงโฆษณานี้อีก
                                            <input type="checkbox" id="ck_isshow" checked={this.state.fields['ck_isshow']} name="ck_isshow" onClick = {(e) => this.handleCkeckDontShow(e, popuplist.bannerId)} />
                                            <span className="checkbox_checkpopup"></span>
                                        </label>
                                    </div>

                                </div>
                                
                            </SweetAlert>
                        </div>
                    );  
                }
            }else{
                show = true
                alert = (
                    <div className="home-popupbanner">
                        <SweetAlert
                            custom
                            style={{ backgroundColor:'#fff0', color: 'white' }}
                            showConfirm={false}
                        >
                            <div className="row" >
                                <div className="col-9 col-md-9 p-0 text-left">
                                    
                                </div>
                                <div className="col-3 col-md-3 p-0 text-right">
                                    <button type="button" className="btn home-popupbanner-btn" onClick={() => this.onClosePopup()}>Close</button>
                                </div>
                            </div>
                            <div className="row row-banner">
                                <div className="col-12 p-0">
                                    <img src={popuplist.banner_Cover} className="img-banner"/>
                                </div>
                            </div>
                            <div className="row" >
                                <div className="col-12 col-md-12 p-0 text-left">
                                    <label className="label_checkpopup" style={{display: ( popuplist.isShow == false ? 'flex' : 'none')}}>
                                        ไม่ต้องการแสดงโฆษณานี้อีก
                                        <input type="checkbox" id="ck_isshow" checked={this.state.fields['ck_isshow']} name="ck_isshow" onClick = {(e) => this.handleCkeckDontShow(e, popuplist.bannerId)} />
                                        <span className="checkbox_checkpopup"></span>
                                    </label>
                                </div>
                            </div>
                            
                        </SweetAlert>
                    </div>
                );  
            }
        
       this.setState({ show: show, modal: alert });
    }

    onClosePopup(){
        sessionStorage.setItem("btnCheckClosePopup","true");
        this.setState({ show: false, modal: null })
    }

    onConfirm () {
        this.setState({ show: false, modal: null });
    }

    getInfoProfileCard(){
        if(localStorage.getItem('2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu') === "true"){
            var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
            var ProfileData = localStorage.getItem('Profile')
            var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
            ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
            var ProfileArray = JSON.parse(ProfileDecrypt);
            var Profile = ProfileArray[0];
            var formData = new FormData();
            formData.append('TOKEN_ID', Profile.tkvsm)
            formData.append('CUSTOMER_ID' , Profile.customer_id)
            formData.append('userId' , Profile.userId)
            formData.append('udId' , "")
            formData.append('deviceOs' , "")
            registerAction.GetCustomerProfile(formData).then(e => {
                if(e.data.isSuccess == true){
                    var resp = JSON.parse(e.data.data.data)
                    var getToken = e.data.data.tokenId
                    var info = resp.CUSTOMER_PROFILE_INFO[0]
                    this.setState({ 
                        GetCustomerProfile: info,
                        getTokenCardId: getToken,
                    })
                } else {
                    this.setState({ GetCustomerProfile: "" })
                }
            })
            if(localStorage.getItem('CardItem') && localStorage.getItem('CardItem') !== '' && localStorage.getItem('CardItem') !== "false"){
                var CardItemData = localStorage.getItem('CardItem');
                var CardItemDecrypt = Cipher.decrypt(keyCipher, CardItemData);
                CardItemDecrypt = CardItemDecrypt.replace(/[\u0000-\u0019]+/g,""); 
                var CardItem = JSON.parse(CardItemDecrypt);
                this.setState({ CardItem : CardItem })
            }else{
                this.setState({ CardItem : "" })
            }
            this.setState({ Profile : Profile })
        } else {
            this.setState({ Profile : "" })
        }
    }

    getBannerInHome(){
        var lang = 'TH';
        var cardType = '!';
        homeAction.getBanner(lang,cardType).then(e => {
            if(e.data.isSuccess === true){
                for(var i in e.data.data.popuplist){
                    if(e.data.data.popuplist[i].banner_Cover !== ""){
                        // if(e.data.data.popuplist[i].banner_Cover !== localStorage.getItem("imgPopupChk")){
                            if(sessionStorage.getItem("btnCheckClosePopup") !== "true"){
                                this.showPopup(e.data.data.popuplist[i]);
                            }   
                        // } else{
                            // localStorage.setItem("btnCheckClosePopup",false);
                        // }
                    }
                }
                let adslist = (e.data.data.adslist).map(d => {
                    d.convertCreateDate = new Date(d.createDate)
                    return d;
                  })

                const sortedAdslist = adslist.sort((a, b) => b.convertCreateDate > a.convertCreateDate ? 1: -1);
                this.setState({ 
                    dataBanner : sortedAdslist, 
                    dataIcon : e.data.data.bannerIconDetail ,
                    dataPromotionRedeem : e.data.data.promotion_Not_Promote ,
                    chkLengthPromotion : e.data.data.promotion_Not_Promote.length
                })
            }
        })
    }
    showBtn(showBtn){
        if(showBtn === 'none'){
            this.setState({ showDropDown : 'block' })
        }else{
            this.setState({ showDropDown : 'none' })
        }
    }
    getNewsList(){
        newActions.getNew().then(e => {
            if(e.data.isSuccess === true){
                var resp = e.data.data
                this.setState({
                    dataPromotion : e.data.data,
                    isHavepromotionDisPlay: resp.length == 0 ? false : true
                })
            }else{
                this.setState({
                    dataPromotion : ''
                })
            }
        })
    }
    showModalLogin(){
        alert = (
            <SweetAlert
                custom
                closeOnClickOutside = {true}
                showConfirm = {false}
            >
                <div className="row">
                    <div className="col-6">
                        <img style={{width:'100%'}} src={`${process.env.PUBLIC_URL}/images/Untitled-1-02.jpg`} />
                    </div>
                    <div className="col-6">
                        <div className="header-sigin"><FormattedMessage id="signin" /></div>
                        <div className="my-3 label-header">
                            <label className="label-text-pc"><FormattedMessage id="phoneno" /></label>
                            <input 
                                className="form-control-set"
                            />
                        </div>
                        <div className="my-3 label-header">
                            <label className="label-text-pc"><FormattedMessage id="passwprd" /></label>
                            <input 
                                className="form-control-set"
                            />
                        </div>
                        <div className="label-forgot"><FormattedMessage id="forgotpass" /> ?</div>
                        <div className="loginandregis-btn text-center">
                            <div style={{marginTop: '5px'}}><FormattedMessage id="signon" /></div>
                        </div>
                        <div className="loginregis-fb-btn" onClick={() => this.regisFacebook()}>
                            <div style={{marginTop: '5px'}}><FormattedMessage id="signinwithfacebook" /></div>
                        </div>
                        <div className="loginregis-gm-btn">
                            <div style={{marginTop: '5px'}}><FormattedMessage id="signinwithgmail" /></div>
                        </div>
                        <div className="loginandregis-btn mt-3">
                            <div style={{marginTop: '5px'}}><a href=""><FormattedMessage id="signonnew" /></a></div>
                        </div>
                    </div>
                </div>
            </SweetAlert>
        );
        this.setState({ show: true, modal: alert });
    }

    linkToBanner = ev => {
        var type = ev.currentTarget.dataset.type
        // var bannerurl = "https://crmpartner.pt.co.th/apptracking/banner/6?limit=10&#param#"
        var bannerurl = ev.currentTarget.dataset.bannerurl
        var bannercover = ev.currentTarget.dataset.bannercover
        var bannertitle = ev.currentTarget.dataset.bannertitle
        var bannerdetailth = ev.currentTarget.dataset.bannerdetailth
        var bannerdetailen = ev.currentTarget.dataset.bannerdetailen
        var promotionId =  ev.currentTarget.dataset.promotionid
        // 2 = url
        // 1 = redeem
        // 0 = html
        if(localStorage.getItem('2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu') != "true"){ // ยังไม่ได้ login 
            if(bannerurl){
                var setLink = bannerurl.replace('#param#', "")
                window.location.href = setLink;
            // }else{
            //     window.location.href=`http://ptmaxcard.com/landing/cover.html`
            }
        }else if(this.state.GetCustomerProfile.CUSTOMER_ID == undefined){  //login, แต่ไม่มี card
            if(bannerurl){
                window.location.href=bannerurl
            // }else{
            //     window.location.href=`http://ptmaxcard.com/landing/cover.html`
            }
        }else{ // login แต่มี card
            var n = bannerurl.indexOf("#param#");
            if(n > -1){
                this.redirectTolinkout(bannerurl);
            }else{
                if(type == 0){
                    this.props.history.push({
                        pathname:`${process.env.PUBLIC_URL}/bannerdetail`,
                        state: {
                            type: type,
                            bannerurl: bannerurl,
                            bannercover: bannercover,
                            bannertitle: bannertitle,
                            bannerdetailth: bannerdetailth,
                            bannerdetailen: bannerdetailen,
                        }
                    });
                } else if(type == 1){
                    this.props.history.push({
                        pathname:`${process.env.PUBLIC_URL}/privileges/detail`,
                        state: {
                            promotionId: promotionId
                        }
                    });
                } else if(type == 2){
                    window.location.href = `${bannerurl}`;
                }
            }
        }
    }

    redirectTolinkout(bannerurl){
        const { Profile, CardItem, GetCustomerProfile, getTokenCardId } = this.state
            var maxCardId = CardItem == "" ? "" : CardItem.carD_NO
            var customerId = Profile == "" ? "" : Profile.customer_id
            var citizeN_ID = Profile == "" ? "" : Profile.citizeN_ID
            var phoneNo = Profile == "" ? "" : Profile.phoneNo
            var email = GetCustomerProfile == "" ? "" : GetCustomerProfile.USER_EMAIL
            var TokenId = getTokenCardId == "" ? "" : getTokenCardId
            var data = `{"customerId":"${customerId}","IdCard":"${citizeN_ID}","maxCardId":"${maxCardId}","telNo":"${phoneNo}","email":"${email}","tokenID":"${TokenId}"}`
            console.log(data);
            const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019";
            const IV = "xRtyUw5Pt+58ZxqA";
            let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENC_KEY), IV);
            let encrypted = cipher.update(data);
            encrypted = Buffer.concat([encrypted, cipher.final()]);
            var setParam = encrypted.toString('base64');
            var linkWebView = bannerurl
            var link = linkWebView.replace('#param#', encodeURIComponent(setParam))
            window.location.href=link
    }

    gotopolicy(){
        // window.location.href = `http://www.ptmaxcard.com/PrivacyPolicy.php`;
        window.location.href = `${process.env.PUBLIC_URL}/PrivacyPolicy`
    }

    redirectTolink = (e,type) => {
        if(type === "promotion"){
            window.location.href = `${process.env.PUBLIC_URL}/news/detail/news/${e.target.id}`
        }else{
            window.location.href = `${process.env.PUBLIC_URL}/privileges/detail/${e.target.id}`;
        }
    }

    onMaxCardPlusLink(){
        const { Profile, CardItem, GetCustomerProfile, getTokenCardId } = this.state
        var maxCardId = CardItem == "" ? "" : CardItem.carD_NO
        var customerId = Profile == "" ? "" : Profile.customer_id
        var citizeN_ID = Profile == "" ? "" : Profile.citizeN_ID
        var phoneNo = Profile == "" ? "" : Profile.phoneNo
        var email = GetCustomerProfile == "" ? "" : GetCustomerProfile.USER_EMAIL
        var TokenId = getTokenCardId == "" ? "" : getTokenCardId
        var data = `{
            "customerId" : "${customerId}",
            "IdCard" : "${citizeN_ID}",
            "maxCardId" : "${maxCardId}",
            "telNo" : "${phoneNo}",
            "email" : "${email}",
            "tokenID" : "${TokenId}"
        }`
        // console.log("setParam", data)
        const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019";
        const IV = "xRtyUw5Pt+58ZxqA";
        let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENC_KEY), IV);
        let encrypted = cipher.update(data);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        var setParam = encrypted.toString('base64');
        var isLink = "https://crmpartner.pt.co.th/maxplus?token=#param#&limit=0"
        // var isLink = "https://crmpartner-uat.pt.co.th/maxplus?token=#param#&limit=0"
        var setLink = isLink.replace('#param#', setParam)
        window.location.href = setLink;
    }

    render(){
        const ArrowLeft = (props) => (
            <button
                {...props}
                className={'prev'}/>
        );
        const ArrowRight = (props) => (
            <button
                {...props}
                className={'next'}/>
        );

        var settingsNews = {
            infinite: this.state.infinite,
            speed: 200,
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: false
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 1,
                    infinite:true
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite:true
                  }
                }
            ]
        };

        var settingsPartner = {
            infinite: false,
            slidesToShow: 5,
            slidesToScroll: 1,
            responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    infinite: false
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    initialSlide: 1,
                    infinite:false
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite:false
                  }
                }
            ]
        };
        var disPlayBanner = [];
        var disPlayIcon = [];
        var displayPromotion = [];
        var bannerSlide = this.state.dataBanner;
        var iconDisplay = this.state.dataIcon;
        var promotionDisPlay = this.state.dataPromotion;
        var promotionRedeem = this.state.dataPromotionRedeem;
        var promotionRedeems = [];
        var displayPartner = [];
        var logoPartner = [
            `${process.env.PUBLIC_URL}/images/Logo_BU/LPG.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/AUTOBACS.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/COFFEE_WORLD.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/HUBBA.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/LAMINA.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/MAX.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/MAXNITRON.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/OTTERI01.jpg`,
            // `${process.env.PUBLIC_URL}/images/Logo_BU/OTTERI02.jpg`,
            // `${process.env.PUBLIC_URL}/images/Logo_BU/PRO_TRUCK.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/PT_GAS.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/PT.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/PUNTHAI.jpg`,
        ]
        for(var i in bannerSlide){
            disPlayBanner.push(
                <Carousel.Item>
                {/* <a 
                    onClick={this.linkToBanner}
                    key={bannerSlide[i].bannerId}
                    data-type={bannerSlide[i].type}
                    data-bannerurl={bannerSlide[i].banner_URL}
                    data-bannercover={bannerSlide[i].banner_Cover}
                    data-bannertitle={bannerSlide[i].banner_Title}
                    data-bannerdetailth={bannerSlide[i].banner_DetailTH}
                    data-bannerdetailen={bannerSlide[i].banner_DetailTH}
                    data-promotionid={bannerSlide[i].banner_Type}
                > */}
                    {/* <LazyLoad> */}
                    <div 
                        onClick={this.linkToBanner}
                        key={bannerSlide[i].bannerId}
                        data-type={bannerSlide[i].type}
                        data-bannerurl={bannerSlide[i].banner_URL}
                        data-bannercover={bannerSlide[i].banner_Cover}
                        data-bannertitle={bannerSlide[i].banner_Title}
                        data-bannerdetailth={bannerSlide[i].banner_DetailTH}
                        data-bannerdetailen={bannerSlide[i].banner_DetailTH}
                        data-promotionid={bannerSlide[i].banner_Type}
                        >
                        <img src={bannerSlide[i].banner_Cover} className="img-fluid img-banner-home" />
                    </div>
                    {/* </LazyLoad> */}
                {/* </a> */}
                </Carousel.Item>
            )
        }
        for(var i in iconDisplay){
            const { Profile, CardItem, GetCustomerProfile, getTokenCardId } = this.state
            var maxCardId = CardItem == "" ? "" : CardItem.carD_NO
            var customerId = Profile == "" ? "" : Profile.customer_id
            var citizeN_ID = Profile == "" ? "" : Profile.citizeN_ID
            var phoneNo = Profile == "" ? "" : Profile.phoneNo
            var email = GetCustomerProfile == "" ? "" : GetCustomerProfile.USER_EMAIL
            var TokenId = getTokenCardId == "" ? "" : getTokenCardId
            var data = `{
                "customerId" : "${customerId}",
                "IdCard" : "${citizeN_ID}",
                "maxCardId" : "${maxCardId}",
                "telNo" : "${phoneNo}",
                "email" : "${email}",
                "TokendID" : "${TokenId}"
            }`
            // console.log("setParam", data)
            const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019";
            const IV = "xRtyUw5Pt+58ZxqA";
            let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENC_KEY), IV);
            let encrypted = cipher.update(data);
            encrypted = Buffer.concat([encrypted, cipher.final()]);
            var setParam = encrypted.toString('base64');
            // LINK IN = 1
            // LINK OUT = 2

                if(iconDisplay[i].name == "สะสมแต้ม"){
                    var link = iconDisplay[i].linkWebView
                    var linkWebView = iconDisplay[i].linkWebView
                    if(iconDisplay[i].typelink == 1){
                        var link = iconDisplay[i].linkWebView
                    } else {
                        var link = linkWebView.replace('#param#', setParam)
                        // console.log(linkWebView.replace('#param#', setParam));
                    }
                } else if (iconDisplay[i].name == "โอนคะแนน"){
                    if(localStorage.getItem('2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu') == "true" && this.state.GetCustomerProfile.CUSTOMER_ID != undefined){
                        var link = iconDisplay[i].linkWebView
                        var linkWebView = iconDisplay[i].linkWebView
                        if(iconDisplay[i].typelink == 1){
                            var link = iconDisplay[i].linkWebView
                        } else {
                            var link = linkWebView.replace('#param#', setParam)
                            // console.log(linkWebView.replace('#param#', setParam));
                        }
                    }else{
                        link = iconDisplay[i].linkWebView
                    }
                } else if (iconDisplay[i].name == "อีแสตมป์"){
                    var link = '/privileges?tab=2'
                } else if (iconDisplay[i].name == "Max Service ส่งน้ำมันฉุกเฉิน 24 ช.ม."){
                    // var link = '/location'
                    var link = iconDisplay[i].linkWebView
                    var linkWebView = iconDisplay[i].linkWebView
                    if(iconDisplay[i].typelink == 1){
                        var link = iconDisplay[i].linkWebView
                    } else {
                        var link = linkWebView.replace('#param#', setParam)
                        // console.log(linkWebView.replace('#param#', setParam));
                    }
                }
            
                disPlayIcon.push(
                    <div className="col-6 p-2 LazyLoad is-visible">
                        <LazyLoad>
                            <a href={link}><img  className="img-fluid w-100 img-home-card" src={iconDisplay[i].image}/></a>
                        </LazyLoad>
                    </div>
                )
            
        }
        for(var i in promotionDisPlay){
            var promotionId = promotionDisPlay[i].id;
            displayPromotion.push(
                <div className="card home-cotentreward-slide-card" id={promotionId} onClick={(e) => this.redirectTolink(e,'promotion')}>
                    <LazyLoad>
                        <img src={promotionDisPlay[i].cover} id={promotionId} className="img-fluid w-100 home-cotentreward-slide-card-img"/>     
                    </LazyLoad>
                    <div className="home-cotentreward-slide-dotleft"></div>
                    <div className="home-cotentreward-slide-dotright"></div>
                    <div className="home-cotentreward-slide-card-body" id={promotionId}>
                        <div className="home-cotentreward-slide-detail line-clamp vertical-center" id={promotionId}>{promotionDisPlay[i].shotDes}</div>
                    </div>
                </div> 
            )
        }
        for(var i in promotionRedeem){
            let id = promotionRedeem[i].promotionId;
            let img = (promotionRedeem[i].imG_URL_NORMAL ? promotionRedeem[i].imG_URL_NORMAL : process.env.PUBLIC_URL+'/images/logo.jpg')
            promotionRedeems.push(               
                <div className="card home-cotentreward-slide-card" id={promotionRedeem[i].promotionId} onClick={(e) => this.redirectTolink(e,'promotionRedeem')}>
                    <LazyLoad>
                        <img src={img} id={promotionRedeem[i].promotionId} className="img-fluid w-100 home-cotentreward-slide-card-img"/>     
                    </LazyLoad>
                    <div className="home-cotentreward-slide-dotleft"></div>
                    <div className="home-cotentreward-slide-dotright"></div>
                    <div className="home-cotentreward-slide-card-body" id={promotionRedeem[i].promotionId}>
                        <div className="home-cotentreward-slide-detail line-clamp vertical-center" id={promotionRedeem[i].promotionId}>{promotionRedeem[i].redeeM_NAME}</div>
                    </div>
                </div> 
            )
        }
        for(var i in logoPartner){
            displayPartner.push(
                <div className="home-location-p1 text-center">
                    <img className="recommend-location-img" src={logoPartner[i]} />
                </div>
            )
        }

        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].home}
            >
                <div>
                <div className="bg-white">
                    <div className="container">
                        {/* <div className="LazyLoad is-visible home-banner-Carousel"> */}
                            <Carousel >
                                {disPlayBanner}
                            </Carousel>
                        {/* </div> */}
                    </div>
                </div>
                <div className="container">
                    <div className="home">
                        <div className="row maxcardplus-banner-content">
                            <div className="col-12 px-0">
                                <a onClick={() => this.onMaxCardPlusLink()} style={{ cursor: 'pointer' }}>
                                    <img src={`${process.env.PUBLIC_URL}/images/maxcardplus.jpg`} className="img-fluid d-block mx-auto w-100"/>
                                </a>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <div className="home-all-title"><FormattedMessage id="ptmaxcard" /></div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-4 col-4 px-0 text-center">
                                <a href={`${process.env.PUBLIC_URL}/privileges`}>
                                    <img className="fix-icon-home" src={`${process.env.PUBLIC_URL}/images/icon-02.jpg`} />
                                    <div className="font-body"><FormattedMessage id="redeem" /></div>
                                </a>
                            </div>
                            <div className="col-lg-4 col-4 px-0 text-center">
                                <a href={`${process.env.PUBLIC_URL}/news`}>
                                    <img className="fix-icon-home" src={`${process.env.PUBLIC_URL}/images/icon-03.jpg`} />
                                    <div className="font-body"><FormattedMessage id="promotion" /></div>
                                </a>
                            </div>
                            <div className="col-lg-4 col-4 px-0 text-center">
                                <a href={`${process.env.PUBLIC_URL}/location`}>
                                    <img className="fix-icon-home" src={`${process.env.PUBLIC_URL}/images/icon-06.jpg`} />
                                    <div className="font-body"><FormattedMessage id="findpt" /></div>
                                </a>
                            </div>
                        </div>

                        <div className="row deal-content">
                            <div className="col-lg-6 col-12">
                                <div className="row">
                                    {disPlayIcon}
                                </div>
                            </div>
                            <div className="col-lg-6 col-12 align-self-center">
                                <div className="home-content-ptcard">
                                    <div className="header-body-pt"><FormattedMessage id="ptmaxcard" /></div>
                                    <div className="header-body-pt"><FormattedMessage id="ptmaxcardtitle" /></div>
                                    <div className="content-body-pt">
                                        <FormattedMessage id="ptmaxcarddetail" />
                                        
                                    </div>
                                    <div className="my-3 d-flex">
                                        {/* <button className="btn btn-home-ptcardred w-100">สมัครบัตร</button> */}
                                        <button className="btn btn-home-ptcardgreen w-100" onClick={() => this.gotopolicy()}><FormattedMessage id="policy" /></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                {/* SLIDE REWARD */}
                <div className="bg-home-cotentreward">
                    <div className="container">
                        <div className="home home-cotentreward">
                            <div className="row">
                                <div className="col-lg-9 col-9 home-cotentreward-title"><FormattedMessage id="privilegestitle" /></div>
                                <div className="col-lg-3 col-3 text-right seeall">
                                    <a href={`${process.env.PUBLIC_URL}/privileges`} className="btn home-cotentreward-btn"><FormattedMessage id="seeall" /></a>
                                </div>
                            </div>
                            {this.state.chkLengthPromotion === 0 ? <div className="home-none-data"><FormattedMessage id="nodata" />!</div> : 
                            <div className="row">
                                <div className="col-lg-12 home-cotentreward-slide">
                                    <Slider {...settingsNews}>                           
                                        {promotionRedeems}
                                    </Slider>
                                </div>
                            </div>}
                        </div>
                    </div>
                </div>

                <div className="pc-pt bg-home-phone">
                    <div className="container">
                        <div className="home">
                            <div className="">
                                <div className="">
                                    <div className="home-all-title"><FormattedMessage id="apppttitle" /></div>
                                </div>
                            </div>
                            <div className="row align-items-center">
                                <div className="col-sm-12 col-md-6 text-center LazyLoad is-visible">
                                    <LazyLoad>
                                        <img className="img-fluid img-mobile-home" src={`/images/ptmobile.png`}/>
                                    </LazyLoad>
                                </div>
                                <div className="col-sm-12 col-md-6">
                                    <div className="row sub-contentbt">
                                        <div className="col-3 col-xs-3 col-sm-3 col-md-2">
                                            <img className="iconinsubcontent" src={`${process.env.PUBLIC_URL}/images/iconapp-01.png`}/>
                                        </div>
                                        <div className="col-9 col-xs-9 col-sm-9 col-md-10 textinsubcontent">     
                                            <div className="text-sub-content"><FormattedMessage id="apppt1title" /></div>
                                            <div className="text-sub-contentbody">
                                                <FormattedMessage id="apppt1detail1" /> <br />
                                                <FormattedMessage id="apppt1detail2" />
                                            </div>
                                        </div>
                                        <div className="col-3 col-xs-3 col-sm-3 col-md-2">
                                            <img className="iconinsubcontent" src={`${process.env.PUBLIC_URL}/images/iconapp-02.png`}/>
                                        </div>
                                        <div className="col-9 col-xs-9 col-sm-9 col-md-10 textinsubcontent">
                                            <div className="text-sub-content"><FormattedMessage id="apppt2title" /></div>
                                            <div className="text-sub-contentbody">
                                                <FormattedMessage id="apppt2detail1" /> <br />
                                                <FormattedMessage id="apppt2detail2" />
                                            </div>
                                        </div>
                                        <div className="col-3 col-xs-3 col-sm-3 col-md-2">
                                            <img className="iconinsubcontent" src={`${process.env.PUBLIC_URL}/images/iconapp-03.png`}/>
                                        </div>
                                        <div className="col-9 col-xs-9 col-sm-9 col-md-10 textinsubcontent">
                                            <div className="text-sub-content"><FormattedMessage id="apppt3title" /></div>
                                            <div className="text-sub-contentbody">
                                                <FormattedMessage id="apppt3detail1" /> <br />
                                                <FormattedMessage id="apppt3detail2" /> <br />
                                                <FormattedMessage id="apppt3detail3" />
                                            </div>
                                        </div>
                                        <div className="col-3 col-xs-3 col-sm-3 col-md-2">
                                            <img className="iconinsubcontent" src={`${process.env.PUBLIC_URL}/images/icon_app-04.png`}/>
                                        </div>
                                        <div className="col-9 col-xs-9 col-sm-9 col-md-10 textinsubcontent">
                                            <div className="text-sub-content"><FormattedMessage id="apppt4title" /></div>
                                            <div className="text-sub-contentbody">
                                                <FormattedMessage id="apppt4detail1" /> <br/>
                                                <FormattedMessage id="apppt4detail2" />
                                            </div>
                                        </div>
                                    </div> 
                                    <div className="my-3 d-flex">
                                        <a className="btn btn-home-ptcardred w-100" href="https://play.google.com/store/apps/details?id=com.ptgroup.maxrewards&hl=th&gl=US"><FormattedMessage id="downloadapp" /> Android </a>
                                        <a className="btn btn-home-ptcardgreen w-100" href="https://apps.apple.com/th/app/max-rewards/id1266579549?l=th"><FormattedMessage id="downloadapp" /> iOS</a>
                                    </div>
                                </div> 
                            </div> 
                        </div> 
                    </div>
                </div>

                {/* <div className="mobile-pt container-fluid bg-subcontents marginbt-subcontent">
                    <div className="home-all-title"><FormattedMessage id="apppttitle1" /> <br/><FormattedMessage id="apppttitle2" /></div>
                    <LazyLoad>
                        <div className="text-center my-3">
                            <img style={{width:'60%'}} src={`/images/mobile-02.png`}/>
                        </div>
                    </LazyLoad>
                    <div className="row">
                        <div className="col-3 col-xs-3 col-md-3 col-sm-3">
                            <img className="iconinsubcontent" src={`${process.env.PUBLIC_URL}/images/iconapp-01.png`}/>
                        </div>
                        <div className="col-9 col-xs-9 col-md-9 col-sm-9 content-sub">
                            <div className="text-sub-content"><FormattedMessage id="apppt1title" /></div>
                            <div className="text-sub-contentbody">
                                <FormattedMessage id="apppt1detail1" /> <br />
                                <FormattedMessage id="apppt1detail2" />
                            </div>
                        </div>
                        <div className="col-3 col-xs-3 col-md-3 col-sm-3">
                            <img className="iconinsubcontent" src={`${process.env.PUBLIC_URL}/images/iconapp-02.png`}/>
                        </div>
                        <div className="col-9 col-xs-9 col-md-9 col-sm-9 content-sub">
                            <div className="text-sub-content"><FormattedMessage id="apppt2title" /></div>
                            <div className="text-sub-contentbody">
                                <FormattedMessage id="apppt2detail2" /> <br />
                                <FormattedMessage id="apppt2detail2" />
                            </div>
                        </div>
                        <div className="col-3 col-xs-3 col-md-3 col-sm-3">
                            <img className="iconinsubcontent" src={`${process.env.PUBLIC_URL}/images/iconapp-03.png`}/>
                        </div>
                        <div className="col-9 col-xs-9 col-md-9 col-sm-9 content-sub"> 
                            <div className="text-sub-content"><FormattedMessage id="apppt3title" /></div>
                            <div className="text-sub-contentbody">
                                <FormattedMessage id="apppt3detail1" /> <br />
                                <FormattedMessage id="apppt3detail2" /> <br />
                                <FormattedMessage id="apppt3detail2" />
                            </div>
                        </div>
                        <div className="col-3 col-xs-3 col-md-3 col-sm-3">
                            <img className="iconinsubcontent" src={`${process.env.PUBLIC_URL}/images/icon_app-04.png`}/>
                        </div>
                        <div className="col-9 col-xs-9 col-md-9 col-sm-9 content-sub">
                            <div className="text-sub-content"><FormattedMessage id="apppt4title" /></div>
                            <div className="text-sub-contentbody">
                                <FormattedMessage id="apppt4detail1" /> <br/>
                                <FormattedMessage id="apppt4detail2" />
                            </div>
                        </div>
                    </div>
                    <div className="my-3 d-flex">
                        <a className="btn btn-home-ptcardred w-100" href="https://play.google.com/store/apps/details?id=com.ptgroup.maxrewards&hl=th&gl=US"><FormattedMessage id="downloadapp" /> Android </a>
                        <a className="btn btn-home-ptcardgreen w-100" href="https://apps.apple.com/th/app/max-rewards/id1266579549?l=th"><FormattedMessage id="downloadapp" /> iOS</a>
                    </div>
                </div> */}


                {/* SLIDE NEWS */}
                {this.state.isHavepromotionDisPlay ?
                <div className="bg-home-cotentreward">
                    <div className="container">
                        <div className="home home-cotentreward">
                            <div className="row">
                                <div className="col-lg-6 col-9 home-cotentreward-title"><FormattedMessage id="newsandactivities" /></div>
                                <div className="col-lg-6 col-3 text-right seeall">
                                    <a href={`${process.env.PUBLIC_URL}/news`} className="btn home-cotentreward-btn"><FormattedMessage id="seeall" /></a>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-12 home-cotentreward-slide">
                                    <Slider {...settingsNews}>                           
                                        {displayPromotion}
                                    </Slider>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                :
                <div></div> 
                }

                {/* ค้นหาร้านค้า */}
                <div className="container-fluid p-0">
                    <div className="home-location-img">
                        <img className="img-fluid w-100" src={`${process.env.PUBLIC_URL}/images/s-location.jpg`}/>
                        <div className="container">
                            {/* <div className="home"> */}
                                <div className="home-location-top">
                                    <div className="">
                                        <div className="home-all-title"><FormattedMessage id="location" /></div>
                                    </div>
                                </div>
                                <div className="home-location-bottom">
                                    <div className="row">
                                        <div className="col-lg-4 col-12 mx-auto">
                                            <div className="d-flex">
                                                {/* <a href={`${process.env.PUBLIC_URL}/location`} className="btn btn-home-ptcardred-location w-100"><FormattedMessage id="gasstation" /></a>
                                                <a href={`${process.env.PUBLIC_URL}/mappartner`} className="btn btn-home-ptcardgreen-location w-100"><FormattedMessage id="otherservice" /></a> */}
                                                <a href={`${process.env.PUBLIC_URL}/Mainlocation`} className="btn btn-home-ptcardgreen-location w-100"><FormattedMessage id="location" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {/* </div> */}
                        </div>
                    </div>
                </div>

                {/*  สถานที่แนะนำ */}
                {/* <div className="container">
                    <div className="home">
                        <div className="row">
                            <div className="col-12">
                                <div className="home-all-title-location"><FormattedMessage id="stationrecommend" /></div>
                            </div>
                        </div>
                        <div className="content-place-recommend">
                            <Slider {...settingsPartner}>                           
                                {displayPartner}
                            </Slider>
                        </div>
                    </div>
                </div> */}
                {this.state.modal}
                </div>
            </IntlProvider>
        )
    }
}