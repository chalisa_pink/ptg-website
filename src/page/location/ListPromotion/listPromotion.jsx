import React, { Component } from 'react';
import { IntlProvider, FormattedMessage } from "react-intl";
import { promotionActions } from '../../../_actions/promotionActions';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

var Cipher = require('aes-ecb'); 
export class listPromotion extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            carD_TYPE_ID : "",
            center: {
                lat: 13.7758095,
                lng: 100.57419329999999
            },
            dataPromotionList:[]
        }
            
    }

    componentDidMount(){
        if(localStorage.getItem('2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu') === "true"){
            var CardItemData = localStorage.getItem('CardItem');
            var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
            var CardItemDecrypt = Cipher.decrypt(keyCipher, CardItemData);
            CardItemDecrypt = CardItemDecrypt.replace(/[\u0000-\u0019]+/g,""); 
            var CardItem = JSON.parse(CardItemDecrypt);
            this.setState({ CardItem : CardItem })
            var carD_TYPE_ID = CardItem["carD_TYPE_ID"]
            sessionStorage.setItem("carD_TYPE_ID", carD_TYPE_ID)
        }
        
        const params = this.props.match.params;
        const typelink = params.type
        let promotionId = this.props.location.state.promotionId
        let promotionName = this.props.location.state.promotionName

        this.setState({ 
            ready:false, 
            error: null ,
            typelink: typelink,
            promotionId: promotionId,
            promotionName: promotionName,
        }); 
        this.getPromotionListByID(promotionId)
        
    }

    getPromotionListByID(id){
        var partnerId = id;
        var cardType = sessionStorage.getItem("carD_TYPE_ID") === null ? "" :sessionStorage.getItem("carD_TYPE_ID");
        sessionStorage.removeItem("carD_TYPE_ID")
        var language = this.state.language
        promotionActions.GetPromotionPartnerList(language, cardType, partnerId).then(e => {
            if (e.data.isSuccess === true) {
                this.setState({ 
                    dataPromotionList: e.data.data, 
                    showLoading: 'none'
                })
            } else {
                this.setState({ 
                    showLoading: 'none', 
                });
            }
        });
    }

    handleOpenlink(id){
        window.location.href=`${process.env.PUBLIC_URL}/privileges/detail/${id}`
    }

    handleOpenSearchStation(){
        var promotionName = this.props.location.state.promotionName
        if(this.state.typelink === "product"){
            this.props.history.push({
                pathname:`${process.env.PUBLIC_URL}/ListStation/${this.state.promotionId}`,
                state: {
                    promotionName: promotionName
                }
            });
        }else if(this.state.typelink === "partner"){
            window.location.href=`https://www.google.com/maps/search/${this.state.promotionName}`
        }
    }
    

    render() {

        var listDataPromotionList = []        
        if((this.state.dataPromotionList).length > 0){
            listDataPromotionList = this.state.dataPromotionList.map((item,i) => 
                <div class="col-6 col-md-3 mb-4">
                    <div class="card" onClick={(e)=>this.handleOpenlink(this.state.dataPromotionList[i].promotionId)}>
                        <img key={i} src={this.state.dataPromotionList[i].imG_URL_NORMAL} className="img-card-product-ptg" />
                        <div class="card-body">
                            <p class="card-text">{this.state.dataPromotionList[i].redeeM_NAME}</p>
                        </div>
                    </div>
                </div>
            )
        }else{
            listDataPromotionList = <div className="col-12 lengthzero mx-auto text-center py-2 t-20"><FormattedMessage id="noda" /></div>
        }
        return (
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].location}
            >
                <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                    <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                </div>

                <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                    <div className="container-fluid bg-news-content search-station">
                        <div className="row">
                            <div className="col-6 col-xs-6 col-md-6">
                                <p className="text-sub-head font-recommend">{this.state.promotionName}</p>
                            </div>
                            <div className="col-6 col-xs-6 col-md-6 text-right">
                                <button class="btn btn-home-ptcardgreen-location" onClick={(e)=>this.handleOpenSearchStation()}>ค้นหาร้านค้า</button>
                            </div>
                        </div>  

                        <div className="row my-4 promotion-product">
                            {listDataPromotionList}
                        </div>
                    </div>
                </div> 
                
            </IntlProvider>
        );
    }
}
 