import React, { Component } from 'react';
import { IntlProvider, FormattedMessage } from "react-intl";
import { ShopAction } from '../../../_actions/locationAction';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
 
export class ListStation extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            center: {
                lat: 13.7758095,
                lng: 100.57419329999999
            },
            producT_IDG : "",
            dataShop:[]
        }
            
    }

    componentDidMount(){

        let geoOptions = {  
            enableHighAccuracy:false,  
            timeOut: 1000, //20 second  
        };  
        navigator.geolocation.getCurrentPosition( 
            this.geoSuccess,  
            this.geoFailure,  
            geoOptions
        ); 
        const params = this.props.match.params;

        let promotionName = this.props.location.state.promotionName
        this.setState({ ready:false, error: null }); 
        this.setState({
            partnerId: params.id,
            promotionName: promotionName
        });
        
    }

    geoSuccess = (position) => {  
        this.setState({  
            ready:true, 
            lat: position.coords.latitude,
            lng:position.coords.longitude,
            where: { lat: position.coords.latitude, lng:position.coords.longitude }  
        })  
        this.getShopProduct()
    }  

    geoFailure = (err) => {  
        this.setState({
            error: err.message,
            lat: 13.7758095,
            lng: 100.57419329999999
        })
        this.getShopProduct()    
    }  



    openLink = (e) => {  
        var lat = this.state.lat
        var lng = this.state.lng
        var partnerId = e.currentTarget.dataset.id
        this.props.history.push({
            pathname:`${process.env.PUBLIC_URL}/location/detail/${partnerId}/${lat}/${lng}`
        });
    }  

    GetShopList(lat, lng, productlist){
        var lang = "th";
        var pageNo = 1;
        var textsearch = "";
        var distance = 10000;
        var province = "";
        var amphure = "";
        var isMap = false;
        var productlist = productlist;

        ShopAction._GetShopList(pageNo, lang, textsearch, distance, lat, lng, province, amphure, isMap ,productlist).then(e => {
            if(e.isSuccess === true){
                this.setState({ 
                    dataShop: e.data,
                    showLoading : 'none' })
            }else{
                this.setState({ 
                    showLoading : 'none' })
            }
        })
    }
    handleGotoLocation = (e) => {  
        console.log(this.state.producT_IDG);
        var producT_IDG = this.state.producT_IDG
        localStorage.setItem('producT_IDG', producT_IDG);
        this.props.history.push({
            pathname:`${process.env.PUBLIC_URL}/location`,
            state: {
                producT_ID: producT_IDG
            }
        });
    }  
    getShopProduct(){
        this.setState({ showLoading: 'block' })
        ShopAction.getShopProduct().then(e => {
            if(e.isSuccess === true){
                let productOther = e.data.productOther
                let productPartner = e.data.productPartner
                let productPetrol = e.data.productPetrol
                let list_product_all = productOther.concat(productPartner,  productPetrol)                
                let list_product = []

                for(var i in list_product_all){
                    if(list_product_all[i].producT_Group === this.state.partnerId){
                        list_product.push(list_product_all[i].producT_ID)
                        this.setState({ producT_IDG: ((list_product_all[i].producT_ID)) });
                    }
                }
                this.GetShopList(this.state.lat, this.state.lng, list_product.toString())
                this.setState({ 
                    dataShop: e.data
                })
            }else{
            }
        })
    }
    

    render() {

        let listDataShop = []
        if(this.state.dataShop.length > 0){
            listDataShop = this.state.dataShop.map((item,i) => 
                <div className="content d-flex mb-3">
                    <div className="">
                        <img key={i} src={this.state.dataShop[i].imG_URL_NORMAL} className="img-shop-ptg rounded" />
                    </div>

                    <div class="content-text">
                        <div class="">{this.state.dataShop[i].placename}</div>
                        <div class="text-green my-1">{this.state.dataShop[i].placename}</div>
                        <div class="text-gray mt-2">{this.state.dataShop[i].distance}</div>
                    </div>
                  
                    <div className="button-search ml-auto align-self-center p-2">
                        <button class="btn" data-id={this.state.dataShop[i].id} onClick={(e)=>this.openLink(e)}>ดูรายละเอียด</button>
                    </div>
          
                </div> 
            
            )
        }else{
            listDataShop = <div className="col-12 lengthzero mx-auto text-center py-2 t-20 text-gray"><FormattedMessage id="noda" /></div>
        }



        return (
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].location}
            >
                <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                    <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                </div>

                <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                    <div className="container bg-news-content search-station">
                        <div className="row">
                            <div className="col-xs-12 col-md-6">
                                <p className="text-sub-head font-recommend">{this.state.promotionName}</p>
                            </div>
                            <div className="col-xs-12 col-md-6 text-right">
                                <button class="btn btn-home-ptcardgreen-location" onClick={(e)=>this.handleGotoLocation()}>ดูแผนที่</button>
                            </div>
                        </div>  
                        <div className="row mt-2 mb-4">
                            <div className="col-12 shop">
                                {listDataShop}
                            </div>
                        </div>
                        
                    </div>
                </div> 
                
            </IntlProvider>
        );
    }
}
 