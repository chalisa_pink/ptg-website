import React from 'react';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

export class Mainlocation extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
        }
    }

    openGasstation(){
        window.location.href = `${process.env.PUBLIC_URL}/location`;
    }

    openPartnerStation(){
        window.location.href = `${process.env.PUBLIC_URL}/shoppartner`;
    }

    render(){
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].Mainlocation}
            >
                <div>
                    <div className="home-location-img">
                        <img className="img-fluid w-100 top-location-mt" src={`${process.env.PUBLIC_URL}/images/s-location.jpg`} />
                        <div className="container">
                            {/* <div className="home"> */}
                                <div className="home-location-bottom">
                                    <div className="col-lg-4 col-12 mx-auto">
                                        <div className="d-flex">
                                            <a href={`${process.env.PUBLIC_URL}/location`} className="btn btn-home-ptcardred-location w-100"> <FormattedMessage id="gasstation" /></a>
                                            {/* <a href={`${process.env.PUBLIC_URL}/shoppartner`} className="btn btn-home-ptcardgreen-location w-100"><FormattedMessage id="other" /></a> */}
                                            <a href={`${process.env.PUBLIC_URL}/mappartner`} className="btn btn-home-ptcardgreen-location w-100"><FormattedMessage id="other" /></a>
                                        </div>
                                    </div>
                                </div>
                            {/* </div> */}
                        </div>
                    </div> 
                    {/* <div className="mobile-location">
                        <div className="my-5">
                            <div className="row">
                                <div className="col-12 my-2"><a href={`${process.env.PUBLIC_URL}/location`} className="btn btn-home-ptcardred-location w-100"><FormattedMessage id="gasstation" /></a></div>
                                <div className="col-12"><a href={`${process.env.PUBLIC_URL}/shoppartner`} className="btn btn-home-ptcardgreen-location w-100"><FormattedMessage id="other" /></a></div>
                            </div>
                        </div>
                        <img className="img-fluid w-100 location-main-bg" src={`${process.env.PUBLIC_URL}/images/s-location.jpg`} />
                    </div> */}
                </div>
            </IntlProvider>
        )
    }
}