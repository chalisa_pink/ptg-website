import React, { Component } from 'react';
import { IntlProvider, FormattedMessage } from "react-intl";
import { ShopAction } from '../../../_actions/locationAction';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
 
export class SearchStation extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            center: {
                lat: 13.7758095,
                lng: 100.57419329999999
            },
            dataProductPTG:[],
            dataPartner:[],
            dataPromotionList:[]
        }
            
    }

    componentDidMount(){
        let geoOptions = {  
            enableHighAccuracy:false,  
            timeOut: 1000, //20 second  
        };  
        this.setState({ ready:false, error: null }); 
        navigator.geolocation.getCurrentPosition( 
            this.geoSuccess,  
            this.geoFailure,  
            geoOptions
        ); 

        this.getDataAll()
    }

    geoSuccess = (position) => {  
        this.setState({  
            ready:true, 
            lat: position.coords.latitude,
            lng:position.coords.longitude,
            where: { lat: position.coords.latitude, lng:position.coords.longitude }  
        })  
        // this.getShopProduct(position.coords.latitude, position.coords.longitude)

    }  

    geoFailure = (err) => {  
        this.setState({
            error: err.message,
            lat: 13.7758095,
            lng: 100.57419329999999
        });  
    }  

    openLink = (e,type) => {  
        var promotionId = e.currentTarget.dataset.id
        var promotionName = e.currentTarget.dataset.name
  
        this.props.history.push({
            pathname:`${process.env.PUBLIC_URL}/ListPromotion/${type}`,
            state: {
                promotionId: promotionId,
                promotionName: promotionName
            }
        });
    } 
    
    handleOpenLink(){
        window.location.href=`${process.env.PUBLIC_URL}/ListStationNear`
    }

    getDataAll(){
        this.setState({ showLoading: 'block' })
        var lang = "th";
        var cardtype = "";
        var partnerId = "";

        ShopAction.GetHome(lang,cardtype,partnerId).then(e => {
            if(e.isSuccess === true){
                this.setState({ 
                    dataProductPTG : e.data.product_ptg, 
                    dataPartner : e.data.partner, 
                    dataPromotionList : e.data.promotionList, 
                    showLoading : 'none' })
            }else{
                this.setState({ 
                    showLoading : 'none' })
            }
        })
    }

    PromotionDetail = (ev) => {
        var id = ev.currentTarget.dataset.id
        this.props.history.push({
            pathname:`${process.env.PUBLIC_URL}/privileges/detail/${id}`,
            state: {
                promotionId: id
            }
        });
    }

    

    render() {
        var listDataProductPTG = []
        var listDataPartner = []
        var listDataPromotionList = []

        if(this.state.dataProductPTG.length > 0){
            listDataProductPTG = this.state.dataProductPTG.map((item,i) => 
            <div className="card product">
                <div className="h-100 d-flex align-items-center">
                    <a 
                        onClick={(e)=>this.openLink(e,'product')} 
                        key={i}
                        data-id={this.state.dataProductPTG[i].partneR_ID}
                        data-name={this.state.dataProductPTG[i].partneR_NAME_EN}
                    >
                        <img key={i} src={this.state.dataProductPTG[i].imG_URL_NORMAL} className="img-small-product-ptg rounded" />
                    </a>
                </div>
            </div>
            )
        }else{
            listDataProductPTG = <div className="col-12 lengthzero mx-auto text-center py-2 t-20"><FormattedMessage id="noda" /></div>
        }  


        if(this.state.dataPartner.length > 0){
            listDataPartner = this.state.dataPartner.map((item,i) => 
                <div className="card product-partner">
                    <div className="h-100 d-flex align-items-center">
                        <a 
                            onClick={(e)=>this.openLink(e,'partner')} 
                            key={i}
                            data-id={this.state.dataPartner[i].partneR_ID}
                            data-name={this.state.dataPartner[i].partneR_NAME_EN}
                        >
                            <img key={i} src={this.state.dataPartner[i].imG_URL_NORMAL} className="img-normal-product-ptg rounded" />
                        </a>
                    </div>
                </div>
            )
        }else{
            listDataPartner = <div className="col-12 lengthzero mx-auto text-center py-2 t-20"><FormattedMessage id="noda" /></div>
        }
        
        if(this.state.dataPromotionList.length > 0){
            listDataPromotionList = this.state.dataPromotionList.map((item,i) =>                 
                <div class="card deal-product" key={i} data-id={this.state.dataPromotionList[i].promotionId} onClick={(e)=>this.PromotionDetail(e)} >
                    <div className="img-deal-product">
                        <img  src={this.state.dataPromotionList[i].imG_URL_NORMAL} class="img-card-product-ptg" />
                    </div>
                    <div class="card-body">
                        <p class="card-text">{this.state.dataPromotionList[i].redeeM_NAME}</p>
                    </div>
                </div>
            )
        }else{
            listDataPromotionList = <div className="col-12 lengthzero mx-auto text-center py-2 t-20"><FormattedMessage id="noda" /></div>
        }
        

        return (
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].location}
            >
                <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                    <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                </div>

                <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                    <div className="container-fluid bg-news-content search-station">
                        <div className="row">
                            <div className="col-xs-12 col-md-6">
                                <p className="text-sub-head font-recommend"><FormattedMessage id="findlocation" /></p>
                            </div>
                            <div className="col-xs-12 col-md-6 text-right">
                                <button class="btn btn-home-ptcardgreen-location" onClick={(e)=>this.handleOpenLink()}>ร้านค้าใกล้คุณ</button>
                            </div>
                        </div>  

                        <div className="row mb-4">
                            <div className="col-12">
                                <p className="text-body-sub">ร้านค้าและบริการ</p>
                                <div className="scrollmenu-ptg">
                                    {listDataProductPTG}
                                </div>
                            </div>
                        </div>
                        <div className="row mb-4">
                            <div className="col-12">
                                <p className="text-body-sub">ร้านค้าที่ร่วมรายการ</p>
                                <div className="scrollmenu-ptg">
                                    {listDataPartner}
                                </div>
                            </div>
                        </div>
                        <div className="row mb-4">
                            <div className="col-12">
                                <p className="text-body-sub">ดีลดีๆ ก็มีเยอะ</p>

                                <div className="scrollmenu-ptg">
                                    {listDataPromotionList}

                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div> 
                
            </IntlProvider>
        );
    }
}
 