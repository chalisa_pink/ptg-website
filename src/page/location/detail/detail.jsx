import React from "react";
import '../../location/location.css';
import { ShopAction } from '../../../_actions/locationAction';
// import Modal from 'react-bootstrap/Modal'
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

export class DetailMap extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            shopId: "",  
            lat: "",  
            lng: "", 
            GetShopById: {},
            showLoading: 'block',
            productOther: {},
            productPetrol: {},
        }; 
    }

    componentDidMount() {
        const params = this.props.match.params;
        this.setState({
            lang: params.lang,
            shopId: params.shopId,
            lat: params.lat,
            lng: params.lng,
        });
        this.handleGetShopById(params.lang, params.shopId, params.lat, params.lng)
    }

    handleGetShopById(lang, shopId, lat, lng){
        this.setState({ showLoading: 'block' })
        setTimeout(() => {
            ShopAction._GetShopById(lang, shopId, lat, lng).then(e => {
                if (e.isSuccess === true) {
                    this.setState({ 
                        GetShopById: e.data,
                        productPetrol: e.data.productPetrol,
                        productOther: e.data.productOther,
                        showLoading: 'none'
                    })
                } else {
                    this.setState({ showError: true });
                }
            });
        }, 2500);
    }

    onImageError(e) {
        e.target.setAttribute("src", `${process.env.PUBLIC_URL}/images/img_default.png`);
    }
    
    render() {
        var lat = this.state.GetShopById['latitude'];
        var lng = this.state.GetShopById['longitude'];
        let _urllink = `https://maps.google.com/maps?q=${lat},${lng}&hl=en&z=14&amp;output=embed`
        
        let { productPetrol } = this.state
        var productPetrolCards = [];
        for (var x in productPetrol) {
            productPetrolCards.push(
                <img 
                    src={productPetrol[x].producT_IMAGE}
                    className="img-fluid pr-1"
                    onError={(e) => this.onImageError(e)}
                    width="60"
                /> 
            );
        }

        let { productOther } = this.state
        var productOtherCards = [];
        for (var x in productOther) {
            productOtherCards.push(
                <img 
                    src={productOther[x].producT_IMAGE}
                    className="img-fluid pr-1"
                    onError={(e) => this.onImageError(e)}
                    width="60"
                /> 
            );
        }

        return (
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].DetailMap}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="container-fluid bg-news-content">
                            <div className="row py-4">
                                <div className="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-sm-12 mx-auto">
                                    <img 
                                        src={this.state.GetShopById['imG_URL_NORMAL']}
                                        className="img-fluid w-100"
                                        onError={(e) => this.onImageError(e)}
                                    />
                                </div>
                                <div className="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-sm-12 mx-auto">
                                    <div className="row content-logo-branch">
                                        <div className="col-12 text-center">
                                            <img 
                                                src={this.state.GetShopById['imG_URL_NORMAL']}
                                                className="img-fluid mx-auto"
                                                width="150"
                                                onError={(e) => this.onImageError(e)}
                                            />
                                        </div>
                                        <div className="col-12 mt-4">
                                            <div className="t-bold text-sub-head t-green"> {this.state.GetShopById['placename']} </div>
                                        </div>
                                        <div className="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-sm-12 mt-3">
                                            <div className="t-bold t-24"> <FormattedMessage id="location" /> </div>
                                            <div className="t-height-20 t-22"> {this.state.GetShopById['address']} </div>
                                        </div>
                                        <div className="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-sm-12 text-right mt-auto">
                                            <a 
                                                className="btn-success btn-location t-22"
                                                href={_urllink}
                                            >
                                                <FormattedMessage id="map" />
                                            </a>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="t-bold t-24"> <FormattedMessage id="service" /> </div>
                                            <div className="py-1">
                                                {productPetrolCards}
                                            </div>
                                            <div className="py-1">
                                                {productOtherCards}
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </IntlProvider>
        );
    }
}
    