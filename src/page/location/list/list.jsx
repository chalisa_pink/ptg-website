import React from "react";
import { ShopAction } from '../../../_actions/locationAction';
import '../../location/location.css';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

export class listShop extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            lang: 'th',
            textsearch: "",
            GetShopList: {},
        };
    }

    componentDidMount(){
        this.GetShopList(true,0);
    }
    
    GetShopList(status,page){
        setTimeout(() => {
            let { pageNo, lang, textsearch } = this.state;
            if(status === true){
                pageNo = 1;
            }else{
                pageNo = page + 1;
            }
            var isMap = false;
            var distance = sessionStorage.getItem("distance");
            var lat = sessionStorage.getItem("lat");
            var lng = sessionStorage.getItem("lng");
            var province = sessionStorage.getItem("province");
            var amphure = sessionStorage.getItem("amphure");
            var productPetrolFilter = sessionStorage.getItem("productPetrolFilter");
            var productOtherFilter = sessionStorage.getItem("productOtherFilter");
            if(productPetrolFilter == "" && productOtherFilter == ""){
                var productlist = "";
            } else {
                var productlist = productPetrolFilter + "," + productOtherFilter;
            }
            ShopAction._GetShopList(pageNo, lang, textsearch, distance, lat, lng, province, amphure, isMap ,productlist).then(e => {
                // console.log("_GetShopList", e)
                if (e.isSuccess === true) {
                    this.setState({ 
                        GetShopList: e.data, 
                        showLoading: 'none',
                        pageNo
                    })
                } else {
                    this.setState({ 
                        showLoading: 'none', 
                    });
                }
            });
        },500)
    }

    handleChangesearch = event => {
        const { value } = event.target;
        this.setState({ textsearch: value });
        this.GetShopList(true,this.state.pageNo);
    };

    onImageError(e) {
        e.target.setAttribute("src", `${process.env.PUBLIC_URL}/images/img_default.png`);
    }

    render() {
        var GetShopList = this.state.GetShopList;
        var GetShopListCard = [];
        if(GetShopList.length === 0){
            GetShopListCard.push(
                <div className="row py-5">
                    <div className="col-12 lengthzero mx-auto text-center py-2 t-20">
                        <FormattedMessage id="noda" />
                    </div>
                </div>
            );
        } else {
            for(var x in GetShopList){
                GetShopListCard.push(
                    <a href={`${process.env.PUBLIC_URL}/location/detail/${GetShopList[x].id}/${GetShopList[x].latitude}/${GetShopList[x].longitude}`} >
                        <div className="row location-border">
                            <div className="col-3 col-no-pading">
                                <img 
                                    className="img-fluid w-100" 
                                    src={GetShopList[x].imG_URL_NORMAL} 
                                    onError={(e) => this.onImageError(e)}
                                />
                            </div>
                            <div className="col-7">
                                <div className="d-flex">
                                    <div className="text-body-main t-bold text-line-mobile1 text-black">{GetShopList[x].placename}</div>
                                </div>
                                {/* <div className="text-sub-detail t-green text-line-mobile1 ">{GetShopList[x].locationname}</div> */}
                                <div className="text-detail t-green text-line-mobile1 ">{GetShopList[x].placename}</div>

                            </div>
                            <div className="col-2 col-no-pading">
                                <div className="d-flex">
                                    <div className="text-extra-small t-mute">{GetShopList[x].distance.includes("กม.") ? Number.parseFloat(GetShopList[x].distance).toFixed(1) + " กม." : Number.parseFloat(GetShopList[x].distance).toFixed(1) + " ม."}</div>
                                </div>
                            </div>
                        </div>
                    </a>
                )
            }
        }
        var listShop = messages[this.state.language].listShop
        return (
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].listShop}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="container-fluid bg-news-content">
                        {/* <div className="bg-ptmaxcard-content"> */}
                            <div className="row">
                                <div className="col-sm-8 col-xl-8 col-md-8 col-xs-8 col-lg-8 mx-auto">
                                    <div className="news-title"><FormattedMessage id="list" /></div>
                                </div>
                            </div>
                            <div className="row py-4">
                                <div className="col-sm-8 col-xl-8 col-md-8 col-xs-8 col-lg-8 mx-auto col-no-pading">
                                    <div className="location-card-body">
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="form-group mb-3">
                                                    <input 
                                                        type="text" 
                                                        className="form-control form-control-lg icon-search-gray location-search" 
                                                        placeholder={listShop.seachfind}
                                                        name="textsearch" 
                                                        value={this.state.textsearch}
                                                        onChange={this.handleChangesearch}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        {GetShopListCard}
                                        <div className="row">
                                            <div className="col-12 text-right">
                                                <button className="next-page btn btn-loadmore" onClick={() => this.GetShopList(false,this.state.pageNo)}>{'หน้าถัดไป >'}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </IntlProvider>
        );
    }
}