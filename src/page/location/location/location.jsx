import React, { Component } from 'react';
import { IntlProvider, FormattedMessage } from "react-intl";
import GoogleMapReact from 'google-map-react';
import MarkerClusterer from '@googlemaps/markerclustererplus';
import { Range } from 'react-range';

import { ShopAction } from '../../../_actions/locationAction';
import SweetAlert from "react-bootstrap-sweetalert";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
 
export class Location extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            center: {
                lat: 13.7758095,
                lng: 100.57419329999999
            },
            zoom: 11,
            getShopList: [],
            getShopProduct: [],
            productPetrol: [],
            productOther: [],
            getProvince: [],
            getAumpur: [],

            province: "",
            aumpur: "",
            aumpurName: "",
            provinceName: "",
            distance: '10000',

            productPetrolFilter: [],
            productOtherFilter: [],

            pageNo: 1,

            setDistance: [10],
            lat: 13.7758095,
            lng: 100.57419329999999,

            checkShowAlert: null
        };
    }

    componentDidMount(){
        var producT_IDG = localStorage.getItem('producT_IDG');
        if(producT_IDG === null){
            this.setState({
                productOtherFilter: [],
            })
        }
        if(producT_IDG !== null){
            this.setState({
                productOtherFilter: [producT_IDG],
            })
        }
        let geoOptions = {  
            enableHighAccuracy:false,  
            timeOut: 1000, //20 second  
        };
        this.setState({ ready:false, error: null });
    
        navigator.geolocation.getCurrentPosition( 
            this.geoSuccess,  
            this.geoFailure,  
            geoOptions
        ); 

        this.getPinMAp()
    }

    getPinMAp(){
        ShopAction.GetPinMAp().then(e => {
            if (e.isSuccess === true) {
                let arr_pinmap = []
                for(let i in e.data){
                    arr_pinmap.push(e.data[i].pinmap_image)
                }

                this.setState({ 
                   pinmap: arr_pinmap
                })
                
            } else {
                this.setState({ showError: true });
            }
        });
    }

    geoSuccess = (position) => {  
        this.setState({  
            ready:true, 
            lat: position.coords.latitude,
            lng:position.coords.longitude,
            where: { lat: position.coords.latitude, lng:position.coords.longitude }  
        })  
        this.getShopProduct(position.coords.latitude, position.coords.longitude)

    }  

    geoFailure = (err) => {  
        this.setState({
            error: err.message,
            lat: 13.7758095,
            lng: 100.57419329999999
        });  
        this.getShopProduct(13.7758095, 100.57419329999999)        
    }  

    getShopProduct(lat, lng){
        this.setState({ showLoading: 'block' })
        ShopAction.getShopProduct().then(e => {
            if (e.isSuccess === true) {
                this.setState({ 
                    getShopProduct: e.data,
                    productPetrol: e.data.productPetrol,
                    productOther: e.data.productOther,
                })
                this.getProvince(lat, lng)
            } else {
                this.setState({ showError: true });
            }
        });
    }

    getProvince(lat, lng){
        ShopAction.GetProvince().then(e => {
            if (e.isSuccess === true) {
                this.setState({ getProvince: e.data })
                this.getShopList(lat, lng)
            } else {
                this.setState({ showError: true });
            }
        });
    }

    getShopList(lat, lng){
        localStorage.removeItem('producT_IDG')
        this.setState({ showLoading: 'block' });
        const { pageNo, language, distance, province, aumpur, productPetrolFilter, productOtherFilter } = this.state
        
        if(productPetrolFilter.length == 0 && productOtherFilter.length == 0){
            var productlist = "";
        } else {
            var productlist = productPetrolFilter + "," + productOtherFilter;
        }
        var textsearch = ''
        var isMap = true
        sessionStorage.setItem("distance", distance)
        sessionStorage.setItem("lat", this.state.lat)
        sessionStorage.setItem("lng", this.state.lng)
        sessionStorage.setItem("province", province)
        sessionStorage.setItem("amphure", aumpur)
        sessionStorage.setItem("productPetrolFilter", productPetrolFilter)
        sessionStorage.setItem("productOtherFilter", productOtherFilter)
        ShopAction._GetShopList(pageNo, language, textsearch, distance, lat, lng, province, aumpur, isMap ,productlist).then(e => {
            if (e.isSuccess === true) {
                var getShopList = e.data
                if(getShopList.length == 0){
                    this.setState({ 
                        getShopList: getShopList,
                        showLoading: 'none'
                    });
                    this.onShowError('ไม่พบข้อมูลที่ค้นหา')
                } else {
                    this.setState({ 
                        getShopList: getShopList,
                        showLoading: 'none'
                    });
                }
            } else {
                this.setState({ showError: true });
            }
        });
    }

    getAumpur(e){
        var value = e.target.value;
        let index = e.nativeEvent.target.selectedIndex;
        var Title = e.target[index].text;
        var formData = new FormData();
        formData.append("codE_GUID", value);
        ShopAction.GetAmphure(formData).then(e => {
            this.setState({ 
                getAumpur: e.dropdowN_INFO,
                province: value,
                provinceName: Title
            })
        });
    }

    setAumpur(e){
        var value = e.target.value;
        let index = e.nativeEvent.target.selectedIndex;
        var Title = e.target[index].text;
        this.setState({ 
            aumpur: value,
            aumpurName: Title
        })
    }

    onSetDistance(e){
        var distance = e * 1000
        this.setState({ 
            setDistance: e,
            distance: distance
        })
    }

    onResetLocation(){
        this.setState({
            distance: "10000",
            provinceName: "", 
            province: "",
            aumpurName: "",
            aumpur: "",
            productPetrolFilter: [], 
            productOtherFilter: [],
        })
        localStorage.removeItem('producT_IDG')
    }

    onSetproductPetrol(value){
        var arrProduct = [...this.state.productPetrolFilter];
        var index = arrProduct.indexOf(value)
        if (index !== -1) {
            arrProduct.splice(index, 1);
        }else{
            arrProduct.push(value);
        }
        this.setState({
            productPetrolFilter: arrProduct,
        })
    }

    onSetproductOther(value){
        var arrProduct = [...this.state.productOtherFilter];
        var index = arrProduct.indexOf(value)
        if (index !== -1) {
            arrProduct.splice(index, 1);
        }else{
            arrProduct.push(value);
        }
        this.setState({
            productOtherFilter: arrProduct,
        })
    }

    onShowError(msg){
        alert = (
            <SweetAlert   
                show={true}
                confirmBtnCssClass="btn btn-block btn-error-redeem"
                confirmBtnText="ตกลง"
                onConfirm={() => this.onCloseError()}
            >
                <span>
                    <img 
                        src={process.env.PUBLIC_URL +"/images/cancel.png"}
                        className="py-3" 
                        alt="sena"
                        width="100"
                    />
                    <div className="t-36 mt-4">{msg}</div>
                </span>
            </SweetAlert>
        )
        this.setState({ 
            checkShowAlert: alert
        })
    }

    onCloseError(){
        this.setState({ 
            checkShowAlert: null
        })
    }

    onDetailMap(id, latitude, longitude){
        window.location.href = `${process.env.PUBLIC_URL}/location/detail/${id}/${latitude}/${longitude}`;
    }


    onGetDetailMap = (key, childProps) => {
        // const markerId = childProps.marker.get('id');
        // const index = this.props.markers.findIndex(m => m.get('id') === markerId);
        // console.log("childProps",childProps)
        // console.log("key",key)
        // if (this.props.onChildClick) {
        //   this.props.onChildClick(index);
        // }
    }

    markerClicked(marker) {
        // console.log("The marker that was clicked is", marker);
        // onClick={(img_src) => this.onDetailMap(img_src)}
        // you may do many things with the "marker" object, please see more on tutorial of the library's author:
       // https://github.com/istarkov/google-map-react/blob/master/API.md#onchildclick-func 
       // Look at their examples and you may have some ideas, you can also have the hover effect on markers, but it's a bit more complicated I think 
    }

    render() {
        const { getShopList, getShopProduct, productPetrol, productOther, getProvince, getAumpur, productPetrolFilter, productOtherFilter } = this.state
        var productPetrolCard = []
        var productOtherCard = []
        var getProvinceCards = []
        var getAumpurCards = []

        productPetrol.map((key,index) => {
            var checkProductPetrol = productPetrolFilter.find(x => x == productPetrol[index].producT_ID);
            productPetrolCard.push(
                <div className="pr-2">
                    <a onClick={() => this.onSetproductPetrol(productPetrol[index].producT_ID)}>
                        <img 
                            src={productPetrol[index].producT_IMAGE} 
                            className={`img-fluid ${checkProductPetrol != undefined ? "newmap-icon-shop" : "newmap-icon-shop-uncheck"}`}
                        />
                    </a>
                </div>
            );
        });

        productOther.map((key,index) => {
            var checkProductOther = productOtherFilter.find(x => x == productOther[index].producT_ID);
            productOtherCard.push(
                <div className="pr-2">
                    <a onClick={() => this.onSetproductOther(productOther[index].producT_ID)}>
                        <img 
                            src={productOther[index].producT_IMAGE} 
                            className={`img-fluid ${checkProductOther != undefined ? "newmap-icon-shop" : "newmap-icon-shop-uncheck"}`}
                        />
                    </a>
                </div>
            );
        });

        for (var x in getProvince) {
            getProvinceCards.push(
                <option value={getProvince[x].codE_GUID}>{getProvince[x].values}</option>
            );
        }

        for (var x in getAumpur) {
            getAumpurCards.push(
                <option value={getAumpur[x].codE_GUID}>{getAumpur[x].values}</option>
            );
        }

        var displayPartner = [];
        var logoPartner = [
            `${process.env.PUBLIC_URL}/images/Logo_BU/LPG.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/AUTOBACS.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/COFFEE_WORLD.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/HUBBA.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/LAMINA.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/MAX.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/MAXNITRON.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/OTTERI01.jpg`,
            // `${process.env.PUBLIC_URL}/images/Logo_BU/OTTERI02.jpg`,
            // `${process.env.PUBLIC_URL}/images/Logo_BU/PRO_TRUCK.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/PT_GAS.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/PT.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/PUNTHAI.jpg`,
        ]
        for(var i in logoPartner){
            displayPartner.push(
                // <div className="set-inline">
                //     <img className="place-rec" src={logoPartner[i]} />
                // </div>
                <div className="home-location-p1 text-center">
                    <img className="recommend-location-img" src={logoPartner[i]} />
                </div>
            )
        }
        var settingsPartner = {
            infinite: false,
            slidesToShow: 8,
            slidesToScroll: 1,
            responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    infinite: false
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    initialSlide: 1,
                    infinite:false
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite:false
                  }
                }
            ]
        };
        return (
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].location}
            >
                <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                    <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                </div>
                <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                    {this.state.checkShowAlert}
                    <div className="container-fluid bg-news-content">
                        <div className="row py-4">
                            <div className="col-12">
                                <div className="text-sub-head font-recommend"><FormattedMessage id="findlocation" /></div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-7 col-md-12">
                                <div className="newmap-contentmap">
                                    <GoogleMapReact
                                        bootstrapURLKeys={{ key: 'AIzaSyBCzuXKcYutFnaDg4p2hRQECRMqYIhCYjM' }}
                                        defaultCenter={this.state.center}
                                        defaultZoom={this.state.zoom}
                                    >
                                        {getShopList.map((marker, o) =>{
                                            var imgpin = ""                                            
                                            var arrTexts = [...this.state.pinmap]
                                            if(marker.pinname != "" && marker.pinname != null){
                                                imgpin = <img src={(arrTexts.filter(item => item.indexOf(marker.pinname) > -1)[0])} className="img-fluid" width="35" />
                                            }else{
                                                imgpin = <img src={`${process.env.PUBLIC_URL}/images/pinmap.png`} className="img-fluid" width="35" />
                                            }
                                            const AnyReactComponent = ({}) => <a onClick={() => this.onDetailMap(marker.id, marker.latitude, marker.longitude)}>{imgpin}</a>;
                                            return(
                                                <AnyReactComponent
                                                    code={marker.code}
                                                    lat={marker.latitude}
                                                    lng={marker.longitude}
                                                    img_src={marker.imG_URL_DEFAULT}
                                                  
                                                />
                                            )
                                        })}      
                                    </GoogleMapReact>
                                </div>
                            </div>
                            <div className="col-lg-5 col-md-12 newmap-content-filter">
                                <div className="d-flex">
                                    <div className="ml-auto">
                                        <a 
                                            onClick={() => this.onResetLocation()}
                                            className="t-20 btn-location-resetloca"
                                        >
                                            <FormattedMessage id="reset" />
                                        </a>
                                    </div>
                                </div>
                                <hr/>
                                <div className="pt-2 pb-2">
                                    <div className="newmap-subtitle-shop">ผลิตภัณฑ์</div>
                                    <div className="d-flex ">
                                        {productPetrolCard}  
                                    </div>
                                </div>
                                <div className="pt-4 pb-2">
                                    <div className="newmap-subtitle-shop">บริการ</div>
                                    <div className="d-flex ">
                                        {productOtherCard}  
                                    </div>
                                </div>
                                <hr/>
                                <div className="py-2">
                                    <div className="newmap-subtitle-shop">
                                        ค้นหา
                                    </div>
                                    <div className="">
                                        <select 
                                            className="form-control newmap-select-find"
                                            name="province"
                                            value={this.state.province} 
                                            onChange={(e) => this.getAumpur(e)}
                                        >
                                            <option selected hidden>ค้นหา</option>
                                            {getProvinceCards}
                                        </select>
                                    </div>
                                    <div className="pt-3">
                                        <select 
                                            className="form-control newmap-select-find"
                                            name="aumpur"
                                            value={this.state.aumpur} 
                                            onChange={(e) => this.setAumpur(e)}
                                        >
                                            <option selected hidden>ค้นหาอำเภอ</option>
                                            {getAumpurCards}
                                        </select>
                                    </div>
                                </div>
                                <hr/>
                                <div className="py-2">
                                    <div className="d-flex">
                                        <div className="newmap-subtitle-shop">
                                            ระยะการแสดงผล (จากตำแหน่งปัจจุบัน)
                                        </div>
                                        <div className="ml-auto newmap-subtitle-shop">
                                            {this.state.setDistance} กม.
                                        </div>
                                    </div>
                                    <div className="ml-auto newmap-distance py-4">
                                        {/* <input 
                                            className="distance-radius" 
                                            type="range" 
                                            min="1" 
                                            max="100" 
                                            value={this.state.distance}
                                            name="distance"
                                            onChange={(e) => {this.onSetDistance(e)}}
                                        />  */}
                                        <Range
                                            step={1}
                                            min={0}
                                            max={100}
                                            values={this.state.setDistance}
                                            onChange={(values) => {this.onSetDistance(values)}}
                                            renderTrack={({ props, children }) => (
                                                <div
                                                    {...props}
                                                    style={{
                                                        ...props.style,
                                                        height: '5px',
                                                        width: '100%',
                                                        backgroundColor: '#53a149',
                                                        borderRadius: 50
                                                    }}
                                                >
                                                  {children}
                                                </div>
                                            )}
                                            renderThumb={({ props }) => (
                                                <div
                                                    className="newmap-distance-icon"
                                                    {...props} style={{ 
                                                        ...props.style,
                                                    }}
                                                >
                                                    <img src={`${process.env.PUBLIC_URL}/images/pinmap.png`} className="img-fluid" width="25" />
                                                </div>
                                            )}
                                        />
                                    </div>
                                </div>
                                <div className="">
                                    <a 
                                        onClick={() => this.getShopList(this.state.lat, this.state.lng)}
                                        className="t-20 btn-block newmap-btn"
                                    >
                                        ค้นหาร้านค้า
                                    </a>
                                </div>
                                <div className="pt-2">
                                    <a href={`${process.env.PUBLIC_URL}/location/listShop`} className="t-20 btn-block newmap-btn" onClick={() => this.getShopList()}>
                                        <FormattedMessage id="seelist" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="row mt-5">
                            <div className="col-lg-12 my-5">
                                <div className="content-place-recommend">
                                    <Slider {...settingsPartner}>                           
                                        {displayPartner}
                                    </Slider>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </IntlProvider>
        );
    }
}
 