import React from 'react';
import '../../location/location.css';
import { ShopAction } from '../../../_actions/locationAction';
import FilterResults from 'react-filter-search';
import { IntlProvider, FormattedMessage } from "react-intl";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

export class Location2 extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            showLoadingDetail: 'block',
            getShopProduct: {},
            productPetrol: {},
            productOther: {},
            modalFilter: false,
            where: {lat:null, lng:null},  
            pageNo: "1",
            lang: "th",
            textsearch: "",
            distancePT: "10000",
            getPTLocation: [],
            coords:{},
            isMap: false,
            tablist: "fade",
            tabmap: "active",
            tabfilter: "fade",
            bgtabs: "",
            on: false,
            FAQList: [],
            value: "",
            GetProvince:{},
            GetAmphure:{},
            valueinputrange: 10 ,
            distance:"10",
            province:"",
            amphure:"",
            productlist1: {},
            productlist2: {},
            productlist3: {},
            productlist4: {},
            productPetrolFilter: [],
            productOtherFilter: [],
            ProvinceFind: null,
            AmphureFind: null,
            ProvinceFindName: "",
            AmphureFindName: "",
            setProvince: "",

            productOtherFilterCheckSplit: [],
            productPetrolFilterCheckSplit: [],
            
        };
    }

    onImageError(e) {
        e.target.setAttribute("src", `${process.env.PUBLIC_URL}/images/img_default.png`);
    }

    componentDidMount() {
        // if (navigator.geolocation) {
        //     console.log('2');
        //     navigator.geolocation.watchPosition(function(position) {
        //       console.log("Latitude is :", position.coords.latitude);
        //       console.log("Longitude is :", position.coords.longitude);
        //     });
        //   }

        if(sessionStorage.getItem("checkData") === "checkData"){
            this.checkDataDone()
        } else {
            this.checkDataNone()
        }
        let geoOptions = {  
            enableHighAccuracy:false,  
            timeOut: 1000, //20 second  
        };  
        this.setState({ready:false, error: null }); 

        navigator.geolocation.getCurrentPosition( 
            this.geoSuccess,  
            this.geoFailure,  
            geoOptions
        ); 



          
    }

    checkDataDone(){
        console.log('checkDataDone');
        // sessionStorage.setItem("checkProductpt", "")
        sessionStorage.setItem("checkData", "checkData")
        var productOtherFilterCheck = sessionStorage.getItem("productOtherFilter")
        var productOtherFilterCheckSplit = productOtherFilterCheck.split(',');
        var productPetrolFilterCheck = sessionStorage.getItem("productPetrolFilter")
        var productPetrolFilterCheckSplit = productPetrolFilterCheck.split(',');
        var distanceCheck = sessionStorage.getItem("distance")
        var distanceCheckCut = distanceCheck.substring(0, distanceCheck.length-3);
        var ProvinceFind = sessionStorage.getItem("province")
        var AmphureFind = sessionStorage.getItem("amphure")
        var ProvinceFindName = sessionStorage.getItem("ProvinceFindName")
        var AmphureFindName = sessionStorage.getItem("AmphureFindName")
        // sessionStorage.setItem("productPetrolFilterForCheck", "")
        // sessionStorage.setItem("productOtherFilterForCheck", "")
        this.setState({ 
            productOtherFilter: productOtherFilterCheckSplit, 
            productPetrolFilter: productPetrolFilterCheckSplit, 
            distance: distanceCheckCut, 
            ProvinceFind: ProvinceFind,
            AmphureFind: AmphureFind,
            ProvinceFindName: ProvinceFindName,
            AmphureFindName: AmphureFindName,
        })
        // this.handlelatlong()
    }

    checkDataNone(){
        this.setState({ showLoading: 'block' })
        setTimeout(() => {
            sessionStorage.setItem("province", "")
            sessionStorage.setItem("amphure", "")
            sessionStorage.setItem("productPetrolFilter", "")
            sessionStorage.setItem("productOtherFilter", "")
            sessionStorage.setItem("distance", "10000")
            sessionStorage.setItem("checkDataFilter", "")
            sessionStorage.setItem("checkDatacity", "")
            sessionStorage.setItem("checkData", "checkData")

            // sessionStorage.setItem("productPetrolFilterForCheck", "")
            // sessionStorage.setItem("productOtherFilterForCheck", "")
            // this.handlelatlong()
            // window.location.href="/map/home"
            this.setState({ showLoading: 'none' })
            window.location.reload(true);
        }, 3500);
    }

    geoSuccess = (position) => {  
        console.log('geoSuccess');

        this.setState({ showLoading: 'block' })
        setTimeout(() => {
            this.setState({  
                ready:true,  
                where: {lat: position.coords.latitude,lng:position.coords.longitude }  
            })  
            sessionStorage.setItem("lat", this.state.where.lat)
            sessionStorage.setItem("lng", this.state.where.lng)
            this.handlegetShopProduct()
            this.handlegetgetPTLocation()
            this.handleGetProvince()
            this.setState({ showLoading: 'none' })
        }, 3500);
    }  

    geoFailure = (err) => {  
        console.log('geoFailure');
        this.setState({ showLoading: 'block' })
        setTimeout(() => {
            this.setState({
                error: err.message
            });  
            sessionStorage.setItem("lat", "13.7758095")
            sessionStorage.setItem("lng", "100.57419329999999")
            this.handlegetShopProduct()
            this.handlegetgetPTLocation()
            this.handleGetProvince()
            this.setState({ showLoading: 'none' })
        }, 3500);
    }  

    handlegetShopProduct(){
        this.setState({ showLoadingDetail: 'block' })
        setTimeout(() => {
            ShopAction.getShopProduct().then(e => {
                if (e.isSuccess === true) {
                    this.setState({ getShopProduct: e.data })
                    this.setState({ productPetrol: e.data.productPetrol })
                    this.setState({ productOther: e.data.productOther })
                    this.setState({ showLoadingDetail: 'none' })
                } else {
                    this.setState({ showError: true });
                }
            });
        }, 2500);
    }

    handlegetgetPTLocation(){
        var lat = sessionStorage.getItem('lat');
		var lng = sessionStorage.getItem('lng');
		var province = sessionStorage.getItem('province');
		var amphure = sessionStorage.getItem('amphure');
        var distance = sessionStorage.getItem('distance');
        if(sessionStorage.getItem('checkDataFilter') === ""){
            var productlist = "";
          } else {
            var productlist = sessionStorage.getItem('productPetrolFilter') + "," + sessionStorage.getItem('productOtherFilter');
          }
        var isMap = true;

        const { pageNo, lang, textsearch} = this.state;
        
        ShopAction._GetShopList(pageNo, lang, textsearch, distance, lat, lng, province, amphure, isMap ,productlist).then(e => {
            if (e.isSuccess === true) {
                this.setState({ getPTLocation: e.data })
            } else {
                this.setState({ showError: true });
            }
        });
    }

    handleGetProvince(){
        ShopAction.GetProvince().then(e => {
            if (e.isSuccess === true) {
                this.setState({ GetProvince: e.data })
            } else {
                this.setState({ showError: true });
            }
        });
    }

    handleChangeSelect(e) {
        var value = e.target.value;
        this.setState({ codE_GUID: value });
        this.setState({ province: value });
        var formData = new FormData();
        formData.append("codE_GUID", value);
        ShopAction.GetAmphure(formData).then(e => {
            this.setState({ GetAmphure: e.dropdowN_INFO })
        });
        let index = e.nativeEvent.target.selectedIndex;
        var Title = e.target[index].text;
        this.setState({ setProvince : Title })
    }

    actioneArrProduct(value){
        var arrProduct = [...this.state.productPetrolFilter];
        var index = arrProduct.indexOf(value)
        if (index !== -1) {
            arrProduct.splice(index, 1);
        }else{
            arrProduct.push(value);
        }
        this.setState({productPetrolFilter: arrProduct});
        // sessionStorage.setItem('productPetrolFilter' , arrProduct)
        // sessionStorage.setItem("checkDataFilter", "checkDataFilter")
        // sessionStorage.setItem("productPetrolFilterForCheck", arrProduct)
    }

    actioneArrService(value){
        var arrProduct = [...this.state.productOtherFilter];
        var index = arrProduct.indexOf(value)
        if (index !== -1) {
            arrProduct.splice(index, 1);
        }else{
            arrProduct.push(value);
        }
        this.setState({productOtherFilter: arrProduct});
        // sessionStorage.setItem('productOtherFilter' , arrProduct)
        // sessionStorage.setItem("checkDataFilter", "checkDataFilter")
        // sessionStorage.setItem("productOtherFilterForCheck", arrProduct)
    }

    onHandleproductPetrol(e){
        this.actioneArrProduct(e.target.value)
    }

    onHandleproductOther(e){
        this.actioneArrService(e.target.value)
    }

    handleChangesearch = event => {
        const { value } = event.target;
        this.setState({ value });
    };

    handleChange = (e) => {
        const input = e.target;
        const value = input.value;
        this.setState({ [input.name]: value });
        let index = e.nativeEvent.target.selectedIndex;
        var Title = e.target[index].text;
        sessionStorage.setItem('AmphureFindName' , Title)
    };

    handleFormSubmit(e) {
        e.preventDefault();
        this.setState({ submitted: true });
        this.handleFilter();
    }

    handleFilter(){
        const { productOtherFilter, productPetrolFilter, setProvince } = this.state
        sessionStorage.setItem('productOtherFilter' , productOtherFilter)
        sessionStorage.setItem("checkDataFilter", "checkDataFilter")
        sessionStorage.setItem('productPetrolFilter' , productPetrolFilter)
        sessionStorage.setItem("checkDataFilter", "checkDataFilter")
        sessionStorage.setItem('ProvinceFindName' , setProvince )

        sessionStorage.setItem("province", this.state.province)
        sessionStorage.setItem("amphure", this.state.amphure)
        sessionStorage.setItem("checkDatacity", "checkDatacity")
        // sessionStorage.setItem("checkButton", "checkButton")
        window.location.href = `${process.env.PUBLIC_URL}/location`;
    }

    onResetLocation(){
        sessionStorage.clear();
        window.location.href = `${process.env.PUBLIC_URL}/location`;
    }

    render(){
        const { getPTLocation, value, productPetrolFilter, productOtherFilter } = this.state;

        let { GetProvince } = this.state
        var GetProvinceCards = [];
        for (var x in GetProvince) {
            GetProvinceCards.push(
                <option value={GetProvince[x].codE_GUID}>{GetProvince[x].values}</option>
            );
        }

        let { GetAmphure } = this.state
        var GetAmphureCards = [];
        for (var x in GetAmphure) {
            GetAmphureCards.push(
                <option value={GetAmphure[x].codE_GUID}>{GetAmphure[x].values}</option>
            );
        }

        let { productOther } = this.state
        var productOtherCards = [];
        for (var x in productOther) {
            if(sessionStorage.getItem("checkData") === "checkData"){
                if(productOtherFilter.includes(productOther[x].producT_ID) == true){
                    var checkIfDataOtherFilterTrue = true
                } else {
                    var checkIfDataOtherFilterTrue = false    
                }
            } else {
                var checkIfDataOtherFilterTrue = false  
            }
            productOtherCards.push(
                <span className="p-1">
                    <input 
                        id={productOther[x].id} 
                        type="checkbox" 
                        name={`productOther${productOther[x].id}`}
                        value={productOther[x].producT_ID} 
                        onClick={(e) => this.onHandleproductOther(e)}
                        checked={checkIfDataOtherFilterTrue}
                    />
                    <label htmlFor={productOther[x].id}>
                    <img 
                        src={productOther[x].producT_IMAGE}
                        className="img-fluid"
                        width="35"
                    />
                    </label>
                </span>
            );
        }

        let { productPetrol } = this.state
        var productPetrolCards = [];
        for (var x in productPetrol) {
            if(sessionStorage.getItem("checkData") === "checkData"){
                if(productPetrolFilter.includes(productPetrol[x].producT_ID) == true){
                    var checkIfDataPetrolFilterTrue = true
                } else {
                    var checkIfDataPetrolFilterTrue = false    
                } 
            } else {
                    var checkIfDataOtherFilterTrue = false  
                }
            productPetrolCards.push(
                <span className="p-1 py-1">
                    <input
                        id={productPetrol[x].id} 
                        type="checkbox" 
                        name={`productPetrol${productPetrol[x].id}`}
                        value={productPetrol[x].producT_ID} 
                        onClick={(e) => this.onHandleproductPetrol(e)}
                        checked={checkIfDataPetrolFilterTrue}
                    />
                    <label htmlFor={productPetrol[x].id}>
                        <img 
                            src={productPetrol[x].producT_IMAGE}
                            className="img-fluid checkbox-img"
                            width="35"
                        />
                    </label> 
                </span>
            );
        }

        var listShop = messages[this.state.language].location
        // console.log(this.state.language);

        var settingsPartner = {
            infinite: false,
            slidesToShow: 8,
            slidesToScroll: 1,
            responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    infinite: false
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    initialSlide: 1,
                    infinite:false
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite:false
                  }
                }
            ]
        };

        var displayPartner = [];
        var logoPartner = [
            `${process.env.PUBLIC_URL}/images/Logo_BU/LPG.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/AUTOBACS.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/COFFEE_WORLD.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/HUBBA.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/LAMINA.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/MAX.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/MAXNITRON.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/OTTERI01.jpg`,
            // `${process.env.PUBLIC_URL}/images/Logo_BU/OTTERI02.jpg`,
            // `${process.env.PUBLIC_URL}/images/Logo_BU/PRO_TRUCK.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/PT_GAS.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/PT.jpg`,
            `${process.env.PUBLIC_URL}/images/Logo_BU/PUNTHAI.jpg`,
        ]
        for(var i in logoPartner){
            displayPartner.push(
                // <div className="set-inline">
                //     <img className="place-rec" src={logoPartner[i]} />
                // </div>
                <div className="home-location-p1 text-center">
                    <img className="recommend-location-img" src={logoPartner[i]} />
                </div>
            )
        }

        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].location}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="container-fluid bg-news-content">
                            <div className="row pb-4">
                                <div className="col-12">
                                    <div className="text-sub-head font-recommend"><FormattedMessage id="findlocation" /></div>
                                </div>
                            </div>
                        {/* <div className="container-fluid bg-web-pri">
                            <div className="header-location-font">ค้นหาสถานีบริการ</div> */}
                            <div className="row mb-5">
                                <div className="col-sm-12 col-xs-12 col-lg-7">
                                    <div id="map-container">
                                        <div id="map" data-map-zoom="9" data-map-scroll="true"></div>
                                    </div>
                                </div>
                                <div className="col-sm-12 col-xs-12 col-lg-5">
                                    <div className={this.state.showLoadingDetail === 'none' ? "" : "settinPaddingMapFilter"}>
                                        <div className="loaderdetail mx-auto " style={{ display: this.state.showLoadingDetail }}></div>
                                    </div>
                                    <div style={{ display: (this.state.showLoadingDetail === 'block' ? 'none' : 'block') }}>
                                        <div className="content-top">
                                            <div className="row">
                                                {/* <div className="col-8">
                                                    <div className="t-18 t-bold subheader-location"><FormattedMessage id="find" /></div>
                                                </div> */}
                                                <div className="col-12 text-right btn-location-resetloca-content">
                                                    <a 
                                                        onClick={() => this.onResetLocation()}
                                                        className="t-20 btn-location-resetloca"
                                                    >
                                                        <FormattedMessage id="reset" />
                                                    </a>
                                                </div>
                                            </div>
                                            <hr/>
                                            <form onSubmit={(e) => this.handleFormSubmit(e)}> 
                                                <div className="">
                                                    <div className="mt-4">
                                                        <div className="t-18 t-bold subheader-location"><FormattedMessage id="product" /></div>
                                                    </div>
                                                    <div className="">
                                                        {productPetrolCards}
                                                    </div>
                                                </div>
                                                <div className="pt-3">
                                                    <div className="">
                                                        <div className="t-18 t-bold subheader-location"><FormattedMessage id="service" /></div>
                                                    </div>
                                                    <div className="">
                                                        {productOtherCards}
                                                    </div>
                                                </div>
                                                <hr/>
                                                <div className="row">
                                                    <div className="col-12 p-0">
                                                        <div className="t-18 t-bold subheader-location"><FormattedMessage id="find" /></div>
                                                    </div>
                                                    <div className="col-12 p-0">
                                                        <div className="input-group form-phone mb-3">
                                                            <select 
                                                                className="form-control select-pro arrow-down-black set-font-selected" 
                                                                id="selected"
                                                                name="province"
                                                                onChange={(e) => this.handleChangeSelect(e)}
                                                                value={this.state.province} 
                                                            >
                                                                <option selected hidden value={this.state.ProvinceFind} className="set-font-selected">
                                                                    {this.state.ProvinceFind == null ? listShop.find : this.state.ProvinceFindName}
                                                                </option>
                                                                {GetProvinceCards}
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="col-12 p-0">
                                                        <div className="input-group form-phone mb-3">
                                                            <select 
                                                                className="form-control select-pro arrow-down-black set-font-selected" 
                                                                id="selected"
                                                                name="amphure"
                                                                value={this.state.amphure} 
                                                                onChange={this.handleChange}
                                                            >
                                                                <option selected hidden value={this.state.AmphureFind}>
                                                                    {this.state.AmphureFind == null ? listShop.findampure : this.state.AmphureFindName}
                                                                </option>
                                                                {GetAmphureCards}
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr/>
                                                <div className="row">
                                                    <div className="col-12 d-flex t-18 t-bold p-0">
                                                        <div className="mr-auto subheader-location">
                                                            <span><FormattedMessage id="distance1" /> </span>
                                                            <span className="t-16"><FormattedMessage id="distance2" /></span>
                                                        </div>
                                                        <div className="kmdis" id="p1">{this.state.distance} <FormattedMessage id="km" /></div>
                                                    </div>
                                                    <div className="col-12 d-flex t-18 t-bold p-0">
                                                        <input className="distance-radius" type="range" min="1" max="100" step="1" value={this.state.distance}/> 
                                                    </div>
                                                </div>
                                                <div className="heigh-footer"></div>
                                                <div className="row fix-bottom py-3 text-center">
                                                    <div className="col-12">
                                                        <button type="submit" className="t-20 btn btn-block btn-green shadow-none localtion-btn-nextstep">
                                                            <FormattedMessage id="findshop" />
                                                        </button>
                                                    </div>
                                                </div>
                                                <div className="row fix-bottom py-1 text-center">
                                                    <div className="col-12">
                                                        <a href={`${process.env.PUBLIC_URL}/location/listShop`} className="t-20 btn btn-block btn-green shadow-none localtion-btn-nextstep">
                                                            <FormattedMessage id="seelist" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="mb-5">
                                <div className="content-place-recommend">
                                    <Slider {...settingsPartner}>                           
                                        {displayPartner}
                                    </Slider>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </IntlProvider>
        )
    }
}