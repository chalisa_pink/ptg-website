import React from "react";
import '../../location/location.css';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
import { ShopAction } from '../../../_actions/locationAction';
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

export class listPartner extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            lang: 'th',
            pageNo: 1,
            textsearch: "",
            GetShopList: {},
            GetPartnerList : {}
        };
    }

    componentDidMount(){
        if(sessionStorage.getItem("checkData") === "checkData"){
            this.checkDataDone()
        } else {
            this.checkDataNone()
        }
        let geoOptions = {  
            enableHighAccuracy:false,  
            timeOut: 2000, //20 second  
        };  
        this.setState({ready:false, error: null });  
            navigator.geolocation.getCurrentPosition( this.geoSuccess,  
            this.geoFailure,  
            geoOptions
        ); 
        setTimeout(() => {
            this.setState({ showLoading : 'none' })
        },500);
        this.getShopList();
        this.getPartnerList();
    }
    checkDataDone(){
        // sessionStorage.setItem("checkProductpt", "")
        sessionStorage.setItem("checkData", "checkData")
        var productOtherFilterCheck = sessionStorage.getItem("productOtherFilter")
        var productOtherFilterCheckSplit = productOtherFilterCheck.split(',');
        var productPetrolFilterCheck = sessionStorage.getItem("productPetrolFilter")
        var productPetrolFilterCheckSplit = productPetrolFilterCheck.split(',');
        var distanceCheck = sessionStorage.getItem("distance")
        var distanceCheckCut = distanceCheck.substring(0, distanceCheck.length-3);
        var ProvinceFind = sessionStorage.getItem("province")
        var AmphureFind = sessionStorage.getItem("amphure")
        var ProvinceFindName = sessionStorage.getItem("ProvinceFindName")
        var AmphureFindName = sessionStorage.getItem("AmphureFindName")
        this.setState({ 
            productOtherFilter: productOtherFilterCheckSplit, 
            productPetrolFilter: productPetrolFilterCheckSplit, 
            distance: distanceCheckCut, 
            ProvinceFind: ProvinceFind,
            AmphureFind: AmphureFind,
            ProvinceFindName: ProvinceFindName,
            AmphureFindName: AmphureFindName,
        })
        // this.handlelatlong()
    }

    checkDataNone(){
        this.setState({ showLoading: 'block' })
        setTimeout(() => {
            sessionStorage.setItem("province", "")
            sessionStorage.setItem("amphure", "")
            sessionStorage.setItem("productPetrolFilter", "")
            sessionStorage.setItem("productOtherFilter", "")
            sessionStorage.setItem("distance", "10000")
            sessionStorage.setItem("checkDataFilter", "")
            sessionStorage.setItem("checkDatacity", "")
            sessionStorage.setItem("checkData", "checkData")
            // this.handlelatlong()
            // window.location.href="/map/home"
            this.setState({ showLoading: 'none' })
        }, 3500);
    }

    geoSuccess = (position) => {  
        this.setState({ showLoading: 'block' })
        setTimeout(() => {
            this.setState({  
                ready:true,  
                where: {lat: position.coords.latitude,lng:position.coords.longitude }  
            })  
            sessionStorage.setItem("lat", this.state.where.lat)
            sessionStorage.setItem("lng", this.state.where.lng)
            this.setState({ showLoading: 'none' })
        }, 3500);
    }  

    getShopList(){
            const { pageNo, lang, textsearch } = this.state;
            var isMap = false;
            var distance = sessionStorage.getItem("distance");
            var lat = sessionStorage.getItem("lat");
            var lng = sessionStorage.getItem("lng");
            var province = sessionStorage.getItem("province");
            var amphure = sessionStorage.getItem("amphure");
            var productPetrolFilter = sessionStorage.getItem("productPetrolFilter");
            var productOtherFilter = sessionStorage.getItem("productOtherFilter");
            if(productPetrolFilter == "" && productOtherFilter == ""){
                var productlist = "";
            } else {
                var productlist = productPetrolFilter + "," + productOtherFilter;
            }
            ShopAction._GetShopList(pageNo, lang, textsearch, distance, lat, lng, province, amphure, isMap ,productlist).then(e => {
                if (e.isSuccess === true) {
                    this.setState({ 
                        GetShopList: e.data, 
                        showLoading: 'none',
                    })
                } else {
                    this.setState({ 
                        showLoading: 'none', 
                    });
                }
            });
    }

    getPartnerList(){
            const { pageNo, lang, textsearch } = this.state;
            ShopAction._GetPartnerList(lang,pageNo,textsearch).then(e => {
                if (e.data.isSuccess === true) {
                    this.setState({ 
                        GetPartnerList: e.data.data, 
                        showLoading: 'none',
                    })
                } else {
                    this.setState({ 
                        showLoading: 'none', 
                    });
                }
            });
    }

    onImageError(e) {
        e.target.setAttribute("src", `${process.env.PUBLIC_URL}/images/img_default.png`);
    }

    render() {
        var GetShopList = this.state.GetShopList;
        var GetShopListCard = [];
        var GetPartnerList = this.state.GetPartnerList;
        var getPartnerListCard = [];
        if(GetShopList.length === 0){
            GetShopListCard.push(
                <div className="row py-5">
                    <div className="col-12 lengthzero mx-auto text-center py-2 t-20">
                        <FormattedMessage id="noda" />
                    </div>
                </div>
            );
        } else {
            for(var x in GetShopList){
                GetShopListCard.push(
                    <a href={`${process.env.PUBLIC_URL}/location/detail/${GetShopList[x].id}/${GetShopList[x].latitude}/${GetShopList[x].longitude}`} >
                        <img 
                            className="img-fluid img-mg-10" 
                            width="70"
                            src={GetShopList[x].imG_URL_NORMAL} 
                            onError={(e) => this.onImageError(e)}
                        />
                    </a>
                )
            }
        }
        if(GetPartnerList.length === 0){
            getPartnerListCard.push(
                <div className="row py-5">
                    <div className="col-12 lengthzero mx-auto text-center py-2 t-20">
                        <FormattedMessage id="noda" />
                    </div>
                </div>
            );
        }else{
            for(var y in GetPartnerList){
                getPartnerListCard.push(
                    <a href={`${process.env.PUBLIC_URL}/location/partner/${GetPartnerList[y].partneR_NAME}/${GetPartnerList[y].partneR_ID}/${GetPartnerList[y].latitude === "" ? "-" : GetPartnerList[y].latitude}/${GetPartnerList[y].longitude === "" ? "-" : GetPartnerList[y].longitude}`} >
                        <img 
                            className="img-fluid img-mg-10" 
                            width="100"
                            src={GetPartnerList[y].imG_URL_NORMAL} 
                            onError={(e) => this.onImageError(e)}
                        />
                    </a>
                )
            }
        }
        return (
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].listShop}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="container-fluid bg-news-content">
                        {/* <div className="bg-ptmaxcard-content"> */}
                            <div className="row">
                                <div className="col-xl-8 col-sm-8 col-xs-8 col-md-8 mx-auto">
                                    <div className="news-title"><FormattedMessage id="list" /></div>
                                </div>
                            </div>
                            <div className="row py-4">
                                <div className="col-xl-8 col-sm-8 col-xs-8 col-md-8 mx-auto">
                                    <div className="location-card-body">
                                        {/* <div className="row mb-3">
                                            <div className="col-5 ft-size-head">ร้านค้าใกล้คุณ</div>
                                            <div className="col-7 text-right"><img src={`${process.env.PUBLIC_URL}/images/pin@3x.png`}/>1322 ห้วยขวาง กรุงเทพมหานคร</div>
                                        </div> */}
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="ft-size-head"><FormattedMessage id="shop" /></div>
                                                <div className="container hilight-group">
                                                    <div className="flex-nowrap">
                                                        {GetShopListCard}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="row">
                                                    <div className="col-6 ft-size-head px-0"><FormattedMessage id="shopin" /></div>
                                                    <div className="col-6 text-right ft-size-head"><a href={`${process.env.PUBLIC_URL}/location/partnerlist`}><FormattedMessage id="more" /></a></div>
                                                </div>
                                                <div className="container hilight-group shoppartner">
                                                    <div className="flex-nowrap">
                                                        {getPartnerListCard}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="row">
                                                    <div className="col-6 ft-size-head px-0"><FormattedMessage id="Privilege" /></div>
                                                    <div className="col-6 text-right ft-size-head"><a href={`${process.env.PUBLIC_URL}/location/partnerlist`}><FormattedMessage id="more" /></a></div>
                                                </div>
                                                <div className="container hilight-group shoppartner">
                                                    <div className="flex-nowrap">
                                                        {getPartnerListCard}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </IntlProvider>
        );
    }
}
    