import React from "react";
import { ShopAction } from '../../../_actions/locationAction';
import '../../location/location.css';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

export class PartnerList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            lang: 'th',
            textsearch: "",
            GetShopList: {},
        };
    }

    componentDidMount(){
        if(sessionStorage.getItem("checkData") === "checkData"){
            this.checkDataDone()
        } else {
            this.checkDataNone()
        }
        let geoOptions = {  
            enableHighAccuracy:false,  
            timeOut: 2000, //20 second  
        };  
        this.setState({ready:false, error: null });  
            navigator.geolocation.getCurrentPosition( this.geoSuccess,  
            this.geoFailure,  
            geoOptions
        ); 
    }

    checkDataDone(){
        // sessionStorage.setItem("checkProductpt", "")
        sessionStorage.setItem("checkData", "checkData")
        var productOtherFilterCheck = sessionStorage.getItem("productOtherFilter")
        var productOtherFilterCheckSplit = productOtherFilterCheck.split(',');
        var productPetrolFilterCheck = sessionStorage.getItem("productPetrolFilter")
        var productPetrolFilterCheckSplit = productPetrolFilterCheck.split(',');
        var distanceCheck = sessionStorage.getItem("distance")
        var distanceCheckCut = distanceCheck.substring(0, distanceCheck.length-3);
        var ProvinceFind = sessionStorage.getItem("province")
        var AmphureFind = sessionStorage.getItem("amphure")
        var ProvinceFindName = sessionStorage.getItem("ProvinceFindName")
        var AmphureFindName = sessionStorage.getItem("AmphureFindName")
        this.setState({ 
            productOtherFilter: productOtherFilterCheckSplit, 
            productPetrolFilter: productPetrolFilterCheckSplit, 
            distance: distanceCheckCut, 
            ProvinceFind: ProvinceFind,
            AmphureFind: AmphureFind,
            ProvinceFindName: ProvinceFindName,
            AmphureFindName: AmphureFindName,
        })
        // this.handlelatlong()
    }

    checkDataNone(){
        this.setState({ showLoading: 'block' })
        setTimeout(() => {
            sessionStorage.setItem("province", "")
            sessionStorage.setItem("amphure", "")
            sessionStorage.setItem("productPetrolFilter", "")
            sessionStorage.setItem("productOtherFilter", "")
            sessionStorage.setItem("distance", "10000")
            sessionStorage.setItem("checkDataFilter", "")
            sessionStorage.setItem("checkDatacity", "")
            sessionStorage.setItem("checkData", "checkData")
            // this.handlelatlong()
            // window.location.href="/map/home"
            this.setState({ showLoading: 'none' })
            this.GetpartnerList(true,0)
        }, 3500);
    }

    geoSuccess = (position) => {  
        this.setState({ showLoading: 'block' })
        setTimeout(() => {
            this.setState({  
                ready:true,  
                where: {lat: position.coords.latitude,lng:position.coords.longitude }  
            })  
            sessionStorage.setItem("lat", this.state.where.lat)
            sessionStorage.setItem("lng", this.state.where.lng)
            this.setState({ showLoading: 'none' })
            this.GetpartnerList(true,0)
        }, 3500);
    }  
    
    GetpartnerList(status,page){
            var pageNo = 1;
            var lang = "th";
            var textsearch = "";
            if(sessionStorage.getItem('lat') === null){
                var lat = 13.7757709;
                var lng = 100.5741795;
            } else {
                var lat = sessionStorage.getItem('lat');
                var lng = sessionStorage.getItem('lng');
            }
            var province ="";
            var amphure = "";
            var distance = "10000";
            if(sessionStorage.getItem('checkDataFilter') === ""){
                var productlist = "";
            } else {
                var productlist = "";
                // var productlist = sessionStorage.getItem('productPetrolFilter') + "," + sessionStorage.getItem('productOtherFilter');
            }
            if(status === true){
                pageNo = 1;
            }else{
                pageNo = page + 1;
            }
            ShopAction._GetPartnerShopList(pageNo, lang, textsearch, distance, lat, lng, province, amphure, false ,productlist).then(e => {
                if (e.isSuccess === true) {
                    this.setState({ 
                        GetShopList: e.data, 
                        showLoading: 'none',
                        pageNo 
                    })
                } else {
                    this.setState({ 
                        showLoading: 'none', 
                    });
                }
            });
    }

    handleChangesearch = event => {
        const { value } = event.target;
        this.setState({ textsearch: value });
        this.GetpartnerList(true,this.state.pageNo);
    };

    onImageError(e) {
        e.target.setAttribute("src", `${process.env.PUBLIC_URL}/images/img_default.png`);
    }

    render() {
        var GetShopList = this.state.GetShopList;
        var GetShopListCard = [];
        if(GetShopList.length === 0){
            GetShopListCard.push(
                <div className="row py-5">
                    <div className="col-12 lengthzero mx-auto text-center py-2 t-20">
                        ไม่พบข้อมูล
                    </div>
                </div>
            );
        } else {
            for(var x in GetShopList){
                // console.log(GetShopList)
                GetShopListCard.push(
                    <a href={`${process.env.PUBLIC_URL}/location/partner/${GetShopList[x].placename}/${GetShopList[x].id}/${GetShopList[x].latitude === "" ? "-" : GetShopList[x].latitude}/${GetShopList[x].longitude === "" ? "-" : GetShopList[x].longitude}`} >
                        <div className="row location-border">
                            <div className="col-3">
                                <img 
                                    className="img-fluid w-100" 
                                    src={GetShopList[x].imG_URL_NORMAL} 
                                    onError={(e) => this.onImageError(e)}
                                />
                            </div>
                            <div className="col-9">
                                <div className="d-flex">
                                    <div className="t-30 t-bold">{GetShopList[x].partneR_NAME}</div>
                                    <div className="t-24 t-mute ml-auto">{GetShopList[x].distance}</div>
                                </div>
                                <div className="t-20 t-green">{GetShopList[x].locationname}</div>
                            </div>
                        </div>
                    </a>
                )
            }
        }
        var listShop = messages[this.state.language].listShop
        return (
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].listShop}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="container-fluid bg-news-content">
                        {/* <div className="bg-ptmaxcard-content"> */}
                            <div className="row">
                                <div className="col-xl-8 col-lg-8 col-md-8 col-xs-12 col-sm-12 mx-auto">
                                    <div className="news-title"><FormattedMessage id="list" /></div>
                                </div>
                            </div>
                            <div className="row py-4">
                                <div className="col-xl-8 col-lg-8 col-md-8 col-xs-12 col-sm-12 mx-auto">
                                    <div className="location-card-body">
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="form-group mb-3">
                                                    <input 
                                                        type="text" 
                                                        className="form-control form-control-lg icon-search-gray location-search" 
                                                        placeholder={listShop.seachfind}
                                                        name="textsearch" 
                                                        value={this.state.textsearch}
                                                        onChange={this.handleChangesearch}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        {GetShopListCard}
                                        <div className="row">
                                            <div className="col-12 text-right">
                                                <button className="next-page btn btn-loadmore" onClick={() => this.GetpartnerList(false,this.state.pageNo)}>{`${<FormattedMessage id="nextpage" />} >`}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </IntlProvider>
        );
    }
}