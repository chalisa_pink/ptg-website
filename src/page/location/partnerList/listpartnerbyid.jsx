import React from "react";
import { ShopAction } from '../../../_actions/locationAction';
import '../../location/location.css';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

export class partnerbyid extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            lang: 'th',
            pageNo: 1,
            textsearch: "",
            GetShopList: [],
        };
    }

    componentDidMount(){
        const params = this.props.match.params;
        this.setState({ textsearch : params.ptn })
        this.GetpartnerList(params.id,params.lat,params.lng)
    }

    GetpartnerList(id,lat,lng){
        setTimeout(() => {
            const { lang } = this.state;
            ShopAction.getPartnerById(lang,id,lat,lng).then(e => {
                if (e.isSuccess === true) {
                    this.setState({ 
                        GetShopList: e.data, 
                        showLoading: 'none',
                    })
                } else {
                    this.setState({ 
                        showLoading: 'none', 
                    });
                }
            });
        },500)
    }

    onImageError(e) {
        e.target.setAttribute("src", `${process.env.PUBLIC_URL}/images/img_default.png`);
    }

    render() {
        var GetShopLists = this.state.GetShopList;
        var lat = GetShopLists.latitude;
        var lng = GetShopLists.longitude;
        let _urllink = `https://maps.google.com/maps?q=${lat},${lng}&hl=en&z=14&amp;output=embed`
        return (
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].DetailMap}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="container-fluid bg-news-content">
                            <div className="row py-4">
                                <div className="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-sm-12 mx-auto">
                                    <img 
                                        src={GetShopLists.imG_URL_NORMAL}
                                        className="img-fluid w-100"
                                        onError={(e) => this.onImageError(e)}
                                    />
                                </div>
                                <div className="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-sm-12 mx-auto">
                                    <div className="row">
                                        <div className="col-12 text-center">
                                            <img 
                                                src={GetShopLists.imG_URL_NORMAL}
                                                className="img-fluid mx-auto"
                                                width="150"
                                                onError={(e) => this.onImageError(e)}
                                            />
                                        </div>
                                        <div className="col-12 mt-4">
                                            <div className="t-bold t-36 t-green"> {GetShopLists.placename} </div>
                                        </div>
                                        <div className="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-sm-12 mx-3">
                                            <div className="t-bold t-24"> <FormattedMessage id="location" /> </div>
                                            <div className="t-height-20 t-22"> {GetShopLists.address} </div>
                                        </div>
                                        <div className="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-sm-12 text-right mt-auto">
                                            <a 
                                                className="btn-success btn-location t-22"
                                                href={_urllink}
                                            >
                                                <FormattedMessage id="map" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </IntlProvider>
        );
    }
}