import React from 'react';
import '../../location/location.css';
import { ShopAction } from '../../../_actions/locationAction';
import FilterResults from 'react-filter-search';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

export class GoogleMapContainer extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            showLoadingDetail: 'block',
            getShopProduct: {},
            productPartner: {},
            modalFilter: false,
            where: {lat:null, lng:null},  
            productPartnerFilter: sessionStorage.getItem('productPartnerFilter'),
            pageNo: "1",
            lang: "th",
            textsearch: "",
            distancePT: "10000",
            getPTLocation: [],
            coords:{},
            isMap: false,
            tablist: "fade",
            tabmap: "active",
            tabfilter: "fade",
            bgtabs: "",
            on: false,
            FAQList: [],
            value: "",
            GetProvince:{},
            GetAmphure:{},
            valueinputrange: 10 ,
            distance:"10",
            province:"",
            amphure:"",
            ProvinceFind: null,
            AmphureFind: null,
            ProvinceFindName: "",
            AmphureFindName: "",
            setProvince: "",
        };
    }

    onImageError(e) {
        e.target.setAttribute("src", `${process.env.PUBLIC_URL}/images/img_default.png`);
    }

    componentDidMount() {
        if(sessionStorage.getItem("checkDataPartner") === "checkData"){
            // console.log('1');
            this.checkDataDone()
        } else {
            // console.log('2');
            this.checkDataNone()
        }
        let geoOptions = {  
            enableHighAccuracy:false,  
            timeOut: 2000, //20 second  
        };  
        this.setState({ready:false, error: null });  
            navigator.geolocation.getCurrentPosition( this.geoSuccess,  
            this.geoFailure,  
            geoOptions
        ); 
    }

    checkDataDone(){
        sessionStorage.setItem("checkDataPartner", "checkData")
        var productPartnerFilterCheck = sessionStorage.getItem("productPartnerFilter")
        var productPartnerFilterCheckSplit = productPartnerFilterCheck.split(',');
        var distanceCheck = sessionStorage.getItem("distancePartner")
        var distanceCheckCut = distanceCheck.substring(0, distanceCheck.length-3);
        var ProvinceFind = sessionStorage.getItem("provincePartner")
        var AmphureFind = sessionStorage.getItem("amphurePartner")
        var ProvinceFindName = sessionStorage.getItem("ProvincePartnerFindName")
        var AmphureFindName = sessionStorage.getItem("AmphurePartnerFindName")
        this.setState({ 
            productPartnerFilter: productPartnerFilterCheckSplit, 
            distance: distanceCheckCut, 
            ProvinceFind: ProvinceFind,
            AmphureFind: AmphureFind,
            ProvinceFindName: ProvinceFindName,
            AmphureFindName: AmphureFindName,
        })
    }

    checkDataNone(){
        this.setState({ showLoading: 'block' })
        setTimeout(() => {
            sessionStorage.setItem("provincePartner", "")
            sessionStorage.setItem("amphurePartner", "")
            sessionStorage.setItem("productPartnerFilter", "")
            sessionStorage.setItem("distancePartner", "10000")
            sessionStorage.setItem("checkDataPartnerFilter", "")
            sessionStorage.setItem("checkDataPartnercity", "")
            sessionStorage.setItem("checkDataPartner", "checkData")
            this.setState({ showLoading: 'none' })
            window.location.reload(true);
        }, 3500);
    }

    geoSuccess = (position) => {  
        this.setState({ showLoading: 'block' })
        setTimeout(() => {
            this.setState({  
                ready:true,  
                where: {lat: position.coords.latitude,lng:position.coords.longitude }  
            })  
            sessionStorage.setItem("lat", this.state.where.lat)
            sessionStorage.setItem("lng", this.state.where.lng)
            this.handlegetShopProduct()
            this.handlegetgetPTLocation()
            this.handleGetProvince()
            this.setState({ showLoading: 'none' })
        }, 3500);
    }  

    geoFailure = (err) => {  
        this.setState({ showLoading: 'block' })
        setTimeout(() => {
            this.setState({
                error: err.message
            });  
            sessionStorage.setItem("lat", "13.7758095")
            sessionStorage.setItem("lng", "100.57419329999999")
            this.handlegetShopProduct()
            this.handlegetgetPTLocation()
            this.handleGetProvince()
            this.setState({ showLoading: 'none' })
        }, 3500);
    }  

    handlegetShopProduct(){
        this.setState({ showLoadingDetail: 'block' })
        setTimeout(() => {
            ShopAction.getShopProduct().then(e => {
                if (e.isSuccess === true) {
                    this.setState({ getShopProduct: e.data })
                    this.setState({ 
                        productOther : e.data.productOther, 
                        productPetrol : e.data.productPetrol, 
                        productPartner : e.data.productPartner, 
                        productPartner_ongoogle : e.data.productPartner_ongoogle, 
    
                    })
                    
                    this.setState({ showLoadingDetail: 'none' })
                } else {
                    this.setState({ showError: true });
                }
            });
        }, 2500);
    }

    handlegetgetPTLocation(){
        var lat = sessionStorage.getItem('lat');
		var lng = sessionStorage.getItem('lng');
		var province = sessionStorage.getItem('provincePartner');
		var amphure = sessionStorage.getItem('amphurePartner');
        var distance = sessionStorage.getItem('distancePartner');
        if(sessionStorage.getItem('checkDataFilter') === ""){
            var productlist = "";
          } else {
            var productlist = sessionStorage.getItem('productPartnerFilter');
          }
        var isMap = true;

        const { pageNo, lang, textsearch} = this.state;
        
        ShopAction._GetShopList(pageNo, lang, textsearch, distance, lat, lng, province, amphure, isMap ,productlist).then(e => {
            if (e.isSuccess === true) {
                this.setState({ getPTLocation: e.data })
            } else {
                this.setState({ showError: true });
            }
        });
    }

    handleGetProvince(){
        ShopAction.GetProvince().then(e => {
            if (e.isSuccess === true) {
                this.setState({ GetProvince: e.data })
            } else {
                this.setState({ showError: true });
            }
        });
    }

    handleChangeSelect(e) {
        var value = e.target.value;
        this.setState({ codE_GUID: value });
        this.setState({ province: value });
        var formData = new FormData();
        formData.append("codE_GUID", value);
        ShopAction.GetAmphure(formData).then(e => {
            this.setState({ GetAmphure: e.dropdowN_INFO })
        });
        let index = e.nativeEvent.target.selectedIndex;
        var Title = e.target[index].text;
        this.setState({ setProvince : Title })
    }

    actioneArrProduct(value){
        var arrProduct = [...this.state.productPartnerFilter];
        var index = arrProduct.indexOf(value)
        if (index !== -1) {
            arrProduct.splice(index, 1);
        }else{
            arrProduct.push(value);
        }
        this.setState({productPartnerFilter: arrProduct});
    }

    onHandleproductPartner(e){
        console.log(e.target.value);
        this.actioneArrProduct(e.target.value)
    }

    handleChangesearch = event => {
        const { value } = event.target;
        this.setState({ value });
    };

    handleChange = (e) => {
        const input = e.target;
        const value = input.value;
        this.setState({ [input.name]: value });
        let index = e.nativeEvent.target.selectedIndex;
        var Title = e.target[index].text;
        sessionStorage.setItem('AmphureFindName' , Title)
    };

    handleFormSubmit(e) {
        e.preventDefault();
        this.setState({ submitted: true });
        this.handleFilter();
    }

    handleFilter(){
        const { productPartnerFilter, setProvince } = this.state
        sessionStorage.setItem("checkDataPartnerFilter", "checkDataFilter")
        sessionStorage.setItem('productPartnerFilter' , productPartnerFilter)
        sessionStorage.setItem("checkDataPartnerFilter", "checkDataFilter")
        sessionStorage.setItem('ProvincePartnerFindName' , setProvince )

        sessionStorage.setItem("provincePartner", this.state.province)
        sessionStorage.setItem("amphurePartner", this.state.amphure)
        sessionStorage.setItem("checkDataPartnercity", "checkDatacity")
        window.location.href = `${process.env.PUBLIC_URL}/mappartner`;
    }

    onResetLocation(){
        sessionStorage.clear();
        window.location.href = `${process.env.PUBLIC_URL}/mappartner`;
    }

    render(){
        const { getPTLocation, value, productPartnerFilter } = this.state;

        let { GetProvince } = this.state
        var GetProvinceCards = [];
        for (var x in GetProvince) {
            GetProvinceCards.push(
                <option value={GetProvince[x].codE_GUID}>{GetProvince[x].values}</option>
            );
        }

        let { GetAmphure } = this.state
        var GetAmphureCards = [];
        for (var x in GetAmphure) {
            GetAmphureCards.push(
                <option value={GetAmphure[x].codE_GUID}>{GetAmphure[x].values}</option>
            );
        }

        let { productPartner,  productOther, productPetrol, productPartner_ongoogle} = this.state
        var productPartnerCards = [];

        for (var x in productPartner) {
            if(sessionStorage.getItem("checkDataPartner") === "checkData"){
                if(productPartnerFilter.includes(productPartner[x].producT_ID) == true){
                    var checkIfDataPetrolFilterTrue = true
                } else {
                    var checkIfDataPetrolFilterTrue = false    
                } 
            } else {
                    var checkIfDataOtherFilterTrue = false  
            }

            productPartnerCards.push(
                <span className="p-1 py-1">
                    <input
                        id={productPartner[x].id} 
                        type="checkbox" 
                        name={`productPartner${productPartner[x].id}`}
                        value={productPartner[x].producT_ID} 
                        onClick={(e) => this.onHandleproductPartner(e)}
                        checked={checkIfDataPetrolFilterTrue}
                    />
                    <label htmlFor={productPartner[x].id}>
                        <img 
                            src={productPartner[x].producT_IMAGE}
                            className="img-fluid checkbox-img"
                            width="35"
                        />
                    </label> 
                </span>
            );
        }

        for (var x in productOther) {
            if(sessionStorage.getItem("checkDataPartner") === "checkData"){
                if(productPartnerFilter.includes(productOther[x].producT_ID) == true){
                    var checkIfDataPetrolFilterTrue = true
                } else {
                    var checkIfDataPetrolFilterTrue = false    
                } 
            } else {
                    var checkIfDataOtherFilterTrue = false  
            }

            productPartnerCards.push(
                <span className="p-1 py-1">
                    <input
                        id={productOther[x].id} 
                        type="checkbox" 
                        name={`productOther${productOther[x].id}`}
                        value={productOther[x].producT_ID} 
                        onClick={(e) => this.onHandleproductPartner(e)}
                        checked={checkIfDataPetrolFilterTrue}
                    />
                    <label htmlFor={productOther[x].id}>
                        <img 
                            src={productOther[x].producT_IMAGE}
                            className="img-fluid checkbox-img"
                            width="35"
                        />
                    </label> 
                </span>
            );
        }

        for (var x in productPetrol) {
            if(sessionStorage.getItem("checkDataPartner") === "checkData"){
                if(productPartnerFilter.includes(productPetrol[x].producT_ID) == true){
                    var checkIfDataPetrolFilterTrue = true
                } else {
                    var checkIfDataPetrolFilterTrue = false    
                } 
            } else {
                    var checkIfDataOtherFilterTrue = false  
            }

            productPartnerCards.push(
                <span className="p-1 py-1">
                    <input
                        id={productPetrol[x].id} 
                        type="checkbox" 
                        name={`productPetrol${productPetrol[x].id}`}
                        value={productPetrol[x].producT_ID} 
                        onClick={(e) => this.onHandleproductPartner(e)}
                        checked={checkIfDataPetrolFilterTrue}
                    />
                    <label htmlFor={productPetrol[x].id}>
                        <img 
                            src={productPetrol[x].producT_IMAGE}
                            className="img-fluid checkbox-img"
                            width="35"
                        />
                    </label> 
                </span>
            );
        }

        for (var x in productPartner_ongoogle) {
            if(sessionStorage.getItem("checkDataPartner") === "checkData"){
                if(productPartnerFilter.includes(productPartner_ongoogle[x].producT_ID) == true){
                    var checkIfDataPetrolFilterTrue = true
                } else {
                    var checkIfDataPetrolFilterTrue = false    
                } 
            } else {
                    var checkIfDataOtherFilterTrue = false  
            }

            productPartnerCards.push(
                <span className="p-1 py-1">
                    <input
                        id={productPartner_ongoogle[x].id} 
                        type="checkbox" 
                        name={`productPartner_ongoogle${productPartner_ongoogle[x].id}`}
                        value={productPartner_ongoogle[x].producT_ID} 
                        onClick={(e) => this.onHandleproductPartner(e)}
                        checked={checkIfDataPetrolFilterTrue}
                    />
                    <label htmlFor={productPartner_ongoogle[x].id}>
                        <img 
                            src={productPartner_ongoogle[x].producT_IMAGE}
                            className="img-fluid checkbox-img"
                            width="35"
                        />
                    </label> 
                </span>
            );
        }


        var listShop = messages[this.state.language].location
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].location}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="container-fluid bg-news-content">
                            <div className="row pb-4">
                                <div className="col-12">
                                    <div className="news-title"><FormattedMessage id="findlocation" /></div>
                                </div>
                            </div>
                            <div className="row mb-5">
                                <div className="col-sm-12 col-xs-12 col-lg-7">
                                    <div id="map-container">
                                        <div id="map" data-map-zoom="9" data-map-scroll="true"></div>
                                    </div>
                                </div>
                                <div className="col-sm-12 col-xs-12 col-lg-5">
                                    <div className={this.state.showLoadingDetail === 'none' ? "" : "settinPaddingMapFilter"}>
                                        <div className="loaderdetail mx-auto " style={{ display: this.state.showLoadingDetail }}></div>
                                    </div>
                                    <div style={{ display: (this.state.showLoadingDetail === 'block' ? 'none' : 'block') }}>
                                        <div className="container content-top">
                                            <div className="row">
                                                <div className="col-12 text-right btn-location-resetloca-content">
                                                    <a 
                                                        onClick={() => this.onResetLocation()}
                                                        className="t-20 btn-location-resetloca"
                                                    >
                                                        <FormattedMessage id="reset" />
                                                    </a>
                                                </div>
                                            </div>
                                            <hr/>
                                            <form onSubmit={(e) => this.handleFormSubmit(e)}> 
                                                <div className="">
                                                    <div className="mt-4">
                                                        <div className="t-18 t-bold subheader-location"><FormattedMessage id="shopnear" /></div>
                                                    </div>
                                                    <div className="">
                                                        {productPartnerCards}
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-12">
                                                        <div className="t-18 t-bold subheader-location"><FormattedMessage id="find" /></div>
                                                    </div>
                                                    <div className="col-12">
                                                        <div className="input-group form-phone mb-3">
                                                        <select 
                                                                className="form-control select-pro arrow-down-black set-font-selected" 
                                                                id="selected"
                                                                name="province"
                                                                onChange={(e) => this.handleChangeSelect(e)}
                                                                value={this.state.province} 
                                                            >
                                                                <option selected hidden value={this.state.ProvinceFind} className="set-font-selected">
                                                                    {this.state.ProvinceFind == null ? listShop.find : this.state.ProvinceFindName}
                                                                </option>
                                                                {GetProvinceCards}
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="col-12">
                                                        <div className="input-group form-phone mb-3">
                                                            <select 
                                                                className="form-control select-pro arrow-down-black set-font-selected" 
                                                                id="selected"
                                                                name="amphure"
                                                                value={this.state.amphure} 
                                                                onChange={this.handleChange}
                                                            >
                                                                <option selected hidden value={this.state.AmphureFind}>
                                                                    {this.state.AmphureFind == null ? listShop.findampure : this.state.AmphureFindName}
                                                                </option>
                                                                {GetAmphureCards}
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr/>
                                                <div className="row">
                                                    <div className="col-12 d-flex t-18 t-bold">
                                                        <div className="mr-auto subheader-location"><FormattedMessage id="distance" /></div>
                                                        <div className="kmdis" id="p1">{this.state.distance} <FormattedMessage id="km" /></div>
                                                    </div>
                                                    <div className="col-12 d-flex t-18 t-bold">
                                                        <input className="distance-radius" type="range" min="1" max="100" step="1" value={this.state.distance}/> 
                                                    </div>
                                                </div>
                                                <div className="heigh-footer"></div>
                                                <div className="row fix-bottom py-3 text-center">
                                                    <div className="col-12">
                                                        <button type="submit" className="t-20 btn btn-block btn-green shadow-none localtion-btn-nextstep">
                                                            <FormattedMessage id="findshop" />
                                                        </button>
                                                    </div>
                                                </div>
                                                {/* <div className="row fix-bottom py-1 text-center">
                                                    <div className="col-12">
                                                        <a href={`${process.env.PUBLIC_URL}/shoppartner`} className="t-20 btn btn-block btn-green shadow-none localtion-btn-nextstep">
                                                            <FormattedMessage id="seelist" />
                                                        </a>
                                                    </div>
                                                </div> */}
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </IntlProvider>
        )
    }
}