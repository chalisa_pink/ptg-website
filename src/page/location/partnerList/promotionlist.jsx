import React from "react";
import { promotionActions } from '../../../_actions/promotionActions';
import '../../location/location.css';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

export class PromotionList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            textsearch: "",
            GetPromotionList: {},
        };
    }

    componentDidMount(){
        const {
            match: { params }
        } = this.props;
        sessionStorage.setItem("productPartnerFilter", params.id)
        this.getPromotionListByID(params.id); 
    }

    
    getPromotionListByID(id){
            var partnerId = id;
            var cardType = "";
            var language = this.state.language
            
            promotionActions.GetPromotionPartnerList(language, cardType, partnerId).then(e => {
                if (e.data.isSuccess === true) {
                    this.setState({ 
                        GetPromotionList: e.data.data, 
                        showLoading: 'none'
                    })
                } else {
                    this.setState({ 
                        showLoading: 'none', 
                    });
                }
            });
    }

    handleChangesearch = event => {
        const { value } = event.target;
        this.setState({ textsearch: value });
        this.GetpartnerList(true,this.state.pageNo);
    };

    CheckOpenMap = e => {
        let bool = true
        if(bool){
            window.location.href=`${process.env.PUBLIC_URL}/mappartner`
        }else{
            window.location.href="https://www.google.co.th/maps/search/วัดพระแก้ว"
        }
    }

    onImageError(e) {
        e.target.setAttribute("src", `${process.env.PUBLIC_URL}/images/img_default.png`);
    }

    render() {
        var GetPromotionList = this.state.GetPromotionList;
        var GetPromotionListCard = [];

        if(GetPromotionList.length === 0){
            GetPromotionListCard.push(
                <div className="row py-5">
                    <div className="col-12 lengthzero mx-auto text-center py-2 t-20">
                        <FormattedMessage id="noda" />
                    </div>
                </div>
            );
        } else {
            for(var x in GetPromotionList){
                GetPromotionListCard.push(
                    <a href={`${process.env.PUBLIC_URL}/location/partner/${GetPromotionList[x].placename}/${GetPromotionList[x].id}/${GetPromotionList[x].latitude === "" ? "-" : GetPromotionList[x].latitude}/${GetPromotionList[x].longitude === "" ? "-" : GetPromotionList[x].longitude}`} >
                        <div className="row location-border">
                            <div className="col-3">
                                <img 
                                    className="img-fluid w-100" 
                                    src={GetPromotionList[x].imG_URL_NORMAL} 
                                    onError={(e) => this.onImageError(e)}
                                />
                            </div>
                            <div className="col-9">
                                <div className="d-flex">
                                    <div className="t-30 t-bold">{GetPromotionList[x].partneR_NAME}</div>
                                    <div className="t-24 t-mute ml-auto">{GetPromotionList[x].distance}</div>
                                </div>
                                <div className="t-20 t-green">{GetPromotionList[x].locationname}</div>
                            </div>
                        </div>
                    </a>
                )
            }
        }

        return (
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].lisPromotion}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="container-fluid bg-news-content">
                        {/* <div className="bg-ptmaxcard-content"> */}
                            <div className="row">
                                <div className="col-xl-8 col-lg-8 col-md-8 col-xs-12 col-sm-12 mx-auto">
                                    <div className="news-title"><FormattedMessage id="list" /></div>
                                </div>
                            </div>
                            <div className="row py-4">
                                <div className="col-xl-8 col-lg-8 col-md-8 col-xs-12 col-sm-12 mx-auto">
                                    <div className="location-card-body">
                                        {GetPromotionListCard}
                                        <div className="row">
                                            <div className="col-12 text-center">
                                                <button className="next-page btn btn-loadmore" onClick={() => this.CheckOpenMap()}><FormattedMessage id="search" /></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </IntlProvider>
        );
    }
}