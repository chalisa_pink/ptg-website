import React, {Component} from 'react';
import { ShopAction } from '../../../_actions/locationAction';
import '../../location/location.css';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
export class ShopPartner extends Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            dataPartner : [],
            dataPromotionList: [],
            dataProductOther: [],
            dataProductPetrol: [],
            dataProductPartner: [],
            dataProductPartner_ongoogle: [],
            showLoading: 'block',
            showLoadingDetail: 'block',
        };
    }

    componentDidMount() {
        let geoOptions = {  
            enableHighAccuracy:false,  
            timeOut: 2000, //20 second  
        };  
        navigator.geolocation.getCurrentPosition( 
            this.geoSuccess, 
            this.geoFailure, 
            geoOptions
        );
        this.setState({ready:false, error: null });   
    }

    onImageError(e) {
        e.target.setAttribute("src", `${process.env.PUBLIC_URL}/images/img_default.png`);
    }

    geoSuccess = (position) => {  
        this.setState({ showLoading: 'block' })
        // console.log(position);
        setTimeout(() => {
            this.setState({  
                ready:true,  
                where: {lat: position.coords.latitude,lng:position.coords.longitude }  
            })  
            sessionStorage.setItem("lat", this.state.where.lat)
            sessionStorage.setItem("lng", this.state.where.lng)
            this.setState({ showLoading: 'none' })
            this.getDataAll();
        //     this.getShopList();
        }, 3500);
    }  

    geoFailure = (err) => {  
        this.setState({ showLoading: 'block' })
        setTimeout(() => {
            this.setState({
                error: err.message
            });  
            sessionStorage.setItem("lat", "13.7758095")
            sessionStorage.setItem("lng", "100.57419329999999")
            this.setState({ showLoading: 'none' })
            this.getDataAll();
            // this.getShopList();
        }, 3500);
    }  

    getShopList(){
        this.setState({ showLoadingDetail: 'block' })
        const { pageNo, language, textsearch } = this.state;
        var isMap = false;
        var distance = "10000";
        var lat = sessionStorage.getItem("lat");
        var lng = sessionStorage.getItem("lng");
        var province = "";
        var amphure = "";
        var productPetrolFilter = sessionStorage.getItem("productPetrolFilter");
        var productOtherFilter = sessionStorage.getItem("productOtherFilter");
        if(productPetrolFilter == "" && productOtherFilter == ""){
            var productlist = "";
        } else {
            var productlist = "";
        }
        ShopAction._GetShopList(pageNo, language, textsearch, distance, lat, lng, province, amphure, isMap ,productlist).then(e => {
            if (e.isSuccess === true) {
                this.setState({ 
                    GetShopList: e.data, 
                    showLoadingDetail: 'none',
                })
            } else {
                this.setState({ 
                    showLoadingDetail: 'none', 
                });
            }
        });
    }

    getPartnerShopList(){
        this.setState({ showLoadingDetail: 'block' })
        var pageNo = 1;
        var lang = "th";
        var textsearch = "";
        if(sessionStorage.getItem('lat') === null){
            var lat = 13.7757709;
            var lng = 100.5741795;
        } else {
            var lat = sessionStorage.getItem('lat');
            var lng = sessionStorage.getItem('lng');
        }
        var province ="";
        var amphure = "";
        var distance = "10000";
        if(sessionStorage.getItem('checkDataFilter') === ""){
            var productlist = "";
        } else {
            var productlist = "";
        }
        ShopAction._GetPartnerShopList(pageNo, lang, textsearch, distance, lat, lng, province, amphure, true ,productlist).then(e => {
            if(e.isSuccess === true){
               this.setState({ datapartner : e.data , showLoadingDetail : 'none' })
            }else{
                // console.log(false)
            }
        })
    }

    getDataAll(){
        this.setState({ showLoadingDetail: 'block' })
        var lang = "th";
        var cardtype = "";
        var partnerId = "";
        if(sessionStorage.getItem('lat') === null){
            var lat = 13.7757709;
            var lng = 100.5741795;
        } else {
            var lat = sessionStorage.getItem('lat');
            var lng = sessionStorage.getItem('lng');
        }
        ShopAction.GetHome(lang,cardtype,partnerId).then(e => {
            console.log(e.data.product_ptg);
            if(e.isSuccess === true && e.data.product_ptg.length > 0){
                this.setState({ 
                    dataPartner : e.data.partner, 
                    dataProductOther : e.data.product.productOther, 
                    dataProductPetrol : e.data.product.productPetrol, 
                    dataProductPartner : e.data.product.productPartner, 
                    dataProductPartner_ongoogle : e.data.product.productPartner_ongoogle, 

                    dataPromotionList : e.data.promotionList, 
                    showLoadingDetail : 'none' })
            }else{
                console.log(false)
                this.setState({ 
                    dataPromotionList : e.data.promotionList, 
                    showLoadingDetail : 'none' })
                
            }
        })
    }
    
    render() {
        var dataPartner = this.state.dataPartner
        var dataPromotionList = this.state.dataPromotionList
        var dataProductOther = this.state.dataProductOther
        var dataProductPetrol = this.state.dataProductPetrol
        var dataProductPartner = this.state.dataProductPartner
        var dataProductPartner_ongoogle = this.state.dataProductPartner_ongoogle
        
        var dataPartnerListCard = [];
        var dataPromotionListCard = [];
        var dataProductOtherListCard = [];
        var dataProductPetrolListCard = [];
        var dataProductPartnerListCard = [];
        var dataProductPartner_ongoogleListCard = [];

        if(dataPartner.length === 0){
            dataPartnerListCard.push(
                <div className="row py-5">
                    <div className="col-12 lengthzero mx-auto text-center py-2 t-20">
                        <FormattedMessage id="noda" />
                    </div>
                </div>
            );
        }else{
            for(var y in dataPartner){
                dataPartnerListCard.push(
                    <a href={`${process.env.PUBLIC_URL}/shoppartner/promotion/${dataProductPetrol[y].producT_ID}`} >
                        <img 
                            className="img-fluid img-mg-10" 
                            width="100"
                            src={dataPartner[y].imG_URL_NORMAL} 
                            onError={(e) => this.onImageError(e)}
                        />
                    </a>
                )
            }
        }

        if(dataPromotionList.length === 0){
            dataPromotionListCard.push(
                <div className="row py-5">
                    <div className="col-12 lengthzero mx-auto text-center py-2 t-20">
                        <FormattedMessage id="noda" />
                    </div>
                </div>
            );
        }else{
            for(var y in dataPromotionList){
                dataPromotionListCard.push(
                    <a href={`${process.env.PUBLIC_URL}/shoppartner/promotion/${dataPromotionList[y].promotionId}`} >
                        <img 
                            className="img-fluid img-mg-10" 
                            width="100"
                            src={dataPromotionList[y].imG_URL_NORMAL} 
                            onError={(e) => this.onImageError(e)}
                        />
                    </a>
                )
            }
        }

        if(dataProductOther.length === 0){
            dataProductOtherListCard.push(
                <div className="row py-5">
                    <div className="col-12 lengthzero mx-auto text-center py-2 t-20">
                        <FormattedMessage id="noda" />
                    </div>
                </div>
            );
        }else{
            for(var y in dataProductOther){
                dataProductOtherListCard.push(
                    <a href={`${process.env.PUBLIC_URL}/shoppartner/promotion/${dataProductPetrol[y].producT_ID}`} >
                        <img 
                            className="img-fluid img-mg-10" 
                            width="100"
                            src={dataProductOther[y].producT_IMAGE} 
                            onError={(e) => this.onImageError(e)}
                        />
                    </a>
                )
            }
        }

        if(dataProductPetrol.length === 0){
            dataProductPetrolListCard.push(
                <div className="row py-5">
                    <div className="col-12 lengthzero mx-auto text-center py-2 t-20">
                        <FormattedMessage id="noda" />
                    </div>
                </div>
            );
        }else{
            for(var y in dataProductPetrol){
                dataProductPetrolListCard.push(
                    <a href={`${process.env.PUBLIC_URL}/shoppartner/promotion/${dataProductPetrol[y].producT_ID}`} >
                        <img 
                            className="img-fluid img-mg-10" 
                            width="100"
                            src={dataProductPetrol[y].producT_IMAGE} 
                            onError={(e) => this.onImageError(e)}
                        />
                    </a>
                )
            }
        }

        // if(dataProductPartner.length === 0){
        //     dataProductPartnerListCard.push(
        //         <div className="row py-5">
        //             <div className="col-12 lengthzero mx-auto text-center py-2 t-20">
        //                 <FormattedMessage id="noda" />
        //             </div>
        //         </div>
        //     );
        // }else{
            for(var y in dataProductPartner){
                dataProductPartnerListCard.push(
                    <a href={`${process.env.PUBLIC_URL}/shoppartner/promotion/${dataProductPartner[y].producT_ID}`} >
                        <img 
                            className="img-fluid img-mg-10" 
                            width="100"
                            src={dataProductPartner[y].producT_IMAGE} 
                            onError={(e) => this.onImageError(e)}
                        />
                    </a>
                )
            }
        // }

        // if(dataProductPartner_ongoogle.length === 0){
        //     dataProductPartner_ongoogleListCard.push(
        //         <div className="row py-5">
        //             <div className="col-12 lengthzero mx-auto text-center py-2 t-20">
        //                 <FormattedMessage id="noda" />
        //             </div>
        //         </div>
        //     );
        // }else{
            for(var y in dataProductPartner_ongoogle){
                dataProductPartner_ongoogleListCard.push(
                    <a href={`${process.env.PUBLIC_URL}/shoppartner/promotion/${dataProductPartner_ongoogle[y].producT_ID}`} >
                        <img 
                            className="img-fluid img-mg-10" 
                            width="100"
                            src={dataProductPartner_ongoogle[y].producT_IMAGE} 
                            onError={(e) => this.onImageError(e)}
                        />
                    </a>
                )
            }
        // }
        
        return (
        <IntlProvider
            locale={this.state.language}
            messages={messages[this.state.language].listPartner}
        >
          <div>
            <div className="row my-5 mt-3">
                <div className="col-12 mt-3">
                    <div className={this.state.showLoadingDetail === 'none' ? "" : "settinPaddingMapFilter"}>
                        <div className="loaderdetail mx-auto " style={{ display: this.state.showLoadingDetail }}></div>
                    </div>
                    <div style={{ display: (this.state.showLoadingDetail === 'block' ? 'none' : 'block') }}>
                        <div className="container content-top">
                            {/* <div className="row">
                                <div className="col-12">
                                    <div className="ft-size-head"><FormattedMessage id="shopnear" /></div>
                                    <div className="container hilight-group">
                                        <div className="flex-nowrap">
                                            {GetShopListCard}
                                        </div>
                                    </div>
                                </div>
                            </div> */}
                            <div className="row">
                                <div className="col-12">
                                    <div className="row">
                                        <div className="col-6 ft-size-head px-0"><FormattedMessage id="shop" /></div>
                                        {/* <div className="col-6 text-right ft-size-head"><a href={`${process.env.PUBLIC_URL}/location/partnerlist`}><FormattedMessage id="more" /></a></div> */}
                                    </div>
                                    <div className="container hilight-group shoppartner">
                                        <div className="flex-nowrap">
                                            {dataProductOtherListCard} 
                                            {dataProductPetrolListCard} 
                                            {dataProductPartnerListCard} 
                                            {dataProductPartner_ongoogleListCard}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <div className="row">
                                        <div className="col-6 ft-size-head px-0"><FormattedMessage id="shopin" /></div>
                                        {/* <div className="col-6 text-right ft-size-head"><a href={`${process.env.PUBLIC_URL}/location/partnerlist`}><FormattedMessage id="more" /></a></div> */}
                                    </div>
                                    <div className="container hilight-group shoppartner">
                                        <div className="flex-nowrap">
                                            {dataPartnerListCard}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <div className="row">
                                        <div className="col-6 ft-size-head px-0"><FormattedMessage id="Privilege" /></div>
                                        <div className="col-6 text-right ft-size-head"></div>
                                    </div>
                                    <div className="container hilight-group shoppartner">
                                        <div className="flex-nowrap">
                                            {dataPromotionListCard}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        </IntlProvider>
        );
    }
}