import React from "react";
import SweetAlert from 'react-bootstrap-sweetalert';
import { registerAction } from '../../_actions/registerActions'; 
import { maxcardActions } from '../../_actions/maxcardActions'; 
import firebase from 'firebase';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../translations/en.json";
import intlMessageTH from "../../translations/th.json";
var Cipher = require('aes-ecb');
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

export class Loginweb extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            show: false,
            modal: null,
            errors: {},
            errorsFocus: {},
            fields: {},
            user: null,
            userid: '',
        };
    }

    componentDidMount(){
        this.showModalLogin()
    }

   
    loginSocialGm = (data) => {
        var phone_no = '';
        if(data.phoneNumber !== null){
            phone_no = data.phoneNumber;
        }
        let formData = new FormData();
        formData.append('socialId' , data.uid)
        formData.append('socialType' , 'gmail')
        formData.append('email',data.email)
        formData.append('phone',phone_no)
        formData.append('udId',this.state.ipaddress)
        formData.append('tokenId','')
        formData.append('playerId','WEBSITE')

        registerAction.loginSocial(formData).then(e => {
            if(e.data.errCode === 10){
                this.props.history.push({
                    pathname: `${process.env.PUBLIC_URL}/register/web`,
                    state: {
                      socialid : data.uid,
                      socialType: 'gmail',
                      udId:this.state.ipaddress,
                      playerId : 'WEBSITE',
                      email:data.email,
                      phone:phone_no,
                    }
                  });
            }else if(e.data.isSuccess === true){
                var Profile = [];
                Profile.push({
                    "userId" : e.data.data.userId,
                    "firstName" : e.data.data.firstName,
                    "lastName" : e.data.data.lastName,
                    "phoneNo" : e.data.data.phoneNo,
                    "ptCardNo" : e.data.data.ptCardNo,
                    "email" : e.data.data.email,
                    "birthDay" : e.data.data.birthDay,
                    "tkmb" : e.data.data.tokenMobileApp,
                    "tkvsm" : e.data.data.tokeN_ID,
                    "customer_id":e.data.data.customeR_ID,
                    "isMember":e.data.data.isMemberPTG,
                    "full_address":e.data.data.fulL_ADDRESS,
                    "titlE_NAME_ID" :e.data.data.titlE_NAME_ID,
                    "titlE_NAME_REMARK":e.data.data.titlE_NAME_REMARK,
                    "citizeN_TYPE_ID":e.data.data.citizeN_TYPE_ID,
                    "citizeN_ID":e.data.data.citizeN_ID,
                    "idCard":e.data.data.idCard,
                    "customeR_STATUS":e.data.data.customeR_STATUS,
                    "isActive":e.data.data.isActive,
                    "gender" : e.data.data.gender
                });
                var Profilestringify = JSON.stringify(Profile);
                var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
                var ProfileEncrypt = Cipher.encrypt(keyCipher, Profilestringify);
                localStorage.setItem("Profile", ProfileEncrypt);
                localStorage.setItem("2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu", "true")
                this.setState({ 
                    uid : e.data.data.userId,
                    phone : e.data.data.phoneNo,
                    email : e.data.data.email,
                    ptCardNo : e.data.data.ptCardNo,
                    firstName : e.data.data.firstName,
                    lastName : e.data.data.lastName,
                    birthDay : e.data.data.birthDay,
                    gender : e.data.data.gender,
                    facebookId : e.data.data.facebookId,
                    gmailId:e.data.data.gmailId,
                    idCard:e.data.data.idCard,
                    pinCode:e.data.data.pinCode,
                    isActive:e.data.data.isActive,
                    tokeN_ID:e.data.data.tokeN_ID,
                    customeR_ID:e.data.data.customeR_ID,
                    citizeN_TYPE_ID:e.data.data.citizeN_TYPE_ID,
                    citizeN_ID:e.data.data.citizeN_ID,
                    titlE_NAME_ID:e.data.data.titlE_NAME_ID,
                    titlE_NAME_REMARK:e.data.data.titlE_NAME_REMARK,
                    fulL_ADDRESS:e.data.data.fulL_ADDRESS,
                    customeR_STATUS:e.data.data.customeR_STATUS,
                    isMemberPTG:e.data.data.isMemberPTG,
                    tokenMobileApp:e.data.data.tokenMobileApp,
                    show: false, 
                    modal: null 
                })
                if(e.data.data.ptCardNo === null){
                    localStorage.setItem("CardItem", "")
                } else {
                    this.getCard(e.data.data.userId)
                }
            }
        })
    }
   
    setupLogout(){
        // localStorage.clear();
        window.location.href=`${process.env.PUBLIC_URL}/home`
        // var that = this;
        // auth().signOut().then(function() {
        //   that.setState({user: null});
        // }).catch(function(error) {
        //     console.log(error)
        // });
    }
    loginSocialFb = (data) => {
            var phone_no = '';
            if(data.phoneNumber !== null){
                phone_no = data.phoneNumber;
            }
            let formData = new FormData();
            formData.append('socialId' , data.uid)
            formData.append('socialType' , 'facebook')
            formData.append('email',data.email)
            formData.append('phone',phone_no)
            formData.append('udId',this.state.ipaddress)
            formData.append('tokenId','')
            formData.append('playerId','WEBSITE')
            registerAction.loginSocial(formData).then(e => {
                if(e.data.errCode === 10){
                    this.props.history.push({
                        pathname: `${process.env.PUBLIC_URL}/register/web`,
                        state: {
                          socialid : data.uid,
                          socialType: 'facebook',
                          udId:this.state.ipaddress,
                          playerId : 'WEBSITE',
                          email:data.email,
                          phone:phone_no,
                        }
                      });
                }else if(e.data.isSuccess === true){
                    var Profile = [];
                    Profile.push({
                        "userId" : e.data.data.userId,
                        "firstName" : e.data.data.firstName,
                        "lastName" : e.data.data.lastName,
                        "phoneNo" : e.data.data.phoneNo,
                        "ptCardNo" : e.data.data.ptCardNo,
                        "email" : e.data.data.email,
                        "birthDay" : e.data.data.birthDay,
                        "tkmb" : e.data.data.tokenMobileApp,
                        "tkvsm" : e.data.data.tokeN_ID,
                        "customer_id":e.data.data.customeR_ID,
                        "isMember":e.data.data.isMemberPTG,
                        "full_address":e.data.data.fulL_ADDRESS,
                        "titlE_NAME_ID" :e.data.data.titlE_NAME_ID,
                        "titlE_NAME_REMARK":e.data.data.titlE_NAME_REMARK,
                        "citizeN_TYPE_ID":e.data.data.citizeN_TYPE_ID,
                        "citizeN_ID":e.data.data.citizeN_ID,
                        "idCard":e.data.data.idCard,
                        "customeR_STATUS":e.data.data.customeR_STATUS,
                        "isActive":e.data.data.isActive,
                        "gender" : e.data.data.gender
                    });
                    var Profilestringify = JSON.stringify(Profile)
                    var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
                    var ProfileEncrypt = Cipher.encrypt(keyCipher, Profilestringify);
                    localStorage.setItem("Profile", ProfileEncrypt);
                    localStorage.setItem("2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu", "true")
                    this.setState({ 
                        uid : e.data.data.userId,
                        phone : e.data.data.phoneNo,
                        email : e.data.data.email,
                        ptCardNo : e.data.data.ptCardNo,
                        firstName : e.data.data.firstName,
                        lastName : e.data.data.lastName,
                        birthDay : e.data.data.birthDay,
                        gender : e.data.data.gender,
                        facebookId : e.data.data.facebookId,
                        gmailId:e.data.data.gmailId,
                        idCard:e.data.data.idCard,
                        pinCode:e.data.data.pinCode,
                        isActive:e.data.data.isActive,
                        tokeN_ID:e.data.data.tokeN_ID,
                        customeR_ID:e.data.data.customeR_ID,
                        citizeN_TYPE_ID:e.data.data.citizeN_TYPE_ID,
                        citizeN_ID:e.data.data.citizeN_ID,
                        titlE_NAME_ID:e.data.data.titlE_NAME_ID,
                        titlE_NAME_REMARK:e.data.data.titlE_NAME_REMARK,
                        fulL_ADDRESS:e.data.data.fulL_ADDRESS,
                        customeR_STATUS:e.data.data.customeR_STATUS,
                        isMemberPTG:e.data.data.isMemberPTG,
                        tokenMobileApp:e.data.data.tokenMobileApp,
                        show: false, 
                        modal: null 
                    })
                    if(e.data.data.ptCardNo === null){
                        localStorage.setItem("CardItem", "")
                    } else {
                        this.getCard(e.data.data.userId)
                    }
                }
            })
    }
    handleChange(e){
        let { fields , errors , errorsFocus } = this.state;
        if(e.target.name === 'USERNAME'){
            fields['USERNAME'] = e.target.value;
        }else if(e.target.name === 'PASSWORD'){
            fields['PASSWORD'] = e.target.value;
        }else{
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
        }

        this.setState({ 
            errorsFocus,
            errors, 
            fields
        })
    }
    submitForm(e){
        e.preventDefault();
        let { fields} = this.state; 
        fields[e.target.name] = e.target.value;
        if(this.validateForm()){
            (async () => {
                // var ipaddress = await publicIp.v4();
                let formData = new FormData();
                formData.append('userName' , fields['USERNAME']);
                formData.append('password' , fields['PASSWORD']);
                // formData.append('udId',ipaddress);
                formData.append('tokenId','');
                formData.append('playerId','WEBSITE');
                registerAction.loginWebsite(formData).then( e => {
                    if(e.data.isSuccess === true){
                        var Profile = [];
                        Profile.push({
                            "userId" : e.data.data.userId,
                            "firstName" : e.data.data.firstName,
                            "lastName" : e.data.data.lastName,
                            "phoneNo" : e.data.data.phoneNo,
                            "ptCardNo" : e.data.data.ptCardNo,
                            "email" : e.data.data.email,
                            "birthDay" : e.data.data.birthDay,
                            "tkmb" : e.data.data.tokenMobileApp,
                            "tkvsm" : e.data.data.tokeN_ID,
                            "customer_id":e.data.data.customeR_ID,
                            "isMember":e.data.data.isMemberPTG,
                            "full_address":e.data.data.fulL_ADDRESS,
                            "titlE_NAME_ID" :e.data.data.titlE_NAME_ID,
                            "titlE_NAME_REMARK":e.data.data.titlE_NAME_REMARK,
                            "citizeN_TYPE_ID":e.data.data.citizeN_TYPE_ID,
                            "citizeN_ID":e.data.data.citizeN_ID,
                            "idCard":e.data.data.idCard,
                            "customeR_STATUS":e.data.data.customeR_STATUS,
                            "isActive":e.data.data.isActive,
                            "gender" : e.data.data.gender
                        });
                        var Profilestringify = JSON.stringify(Profile)
                        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
                        var ProfileEncrypt = Cipher.encrypt(keyCipher, Profilestringify);
                        localStorage.setItem("Profile", ProfileEncrypt);
                        localStorage.setItem("2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu", "true")
                        this.setState({ 
                            uid : e.data.data.userId,
                            phone : e.data.data.phoneNo,
                            email : e.data.data.email,
                            ptCardNo : e.data.data.ptCardNo,
                            firstName : e.data.data.firstName,
                            lastName : e.data.data.lastName,
                            birthDay : e.data.data.birthDay,
                            gender : e.data.data.gender,
                            facebookId : e.data.data.facebookId,
                            gmailId:e.data.data.gmailId,
                            idCard:e.data.data.idCard,
                            pinCode:e.data.data.pinCode,
                            isActive:e.data.data.isActive,
                            tokeN_ID:e.data.data.tokeN_ID,
                            customeR_ID:e.data.data.customeR_ID,
                            citizeN_TYPE_ID:e.data.data.citizeN_TYPE_ID,
                            citizeN_ID:e.data.data.citizeN_ID,
                            titlE_NAME_ID:e.data.data.titlE_NAME_ID,
                            titlE_NAME_REMARK:e.data.data.titlE_NAME_REMARK,
                            fulL_ADDRESS:e.data.data.fulL_ADDRESS,
                            customeR_STATUS:e.data.data.customeR_STATUS,
                            isMemberPTG:e.data.data.isMemberPTG,
                            tokenMobileApp:e.data.data.tokenMobileApp,
                            show: false, 
                            modal: null 
                        })
                        if(e.data.data.ptCardNo === null){
                            localStorage.setItem("CardItem", "")
                        } else {
                            this.getCard(e.data.data.userId)
                        }
                    }else{
                        var msg = e.data.errMsg;
                        var img = `${process.env.PUBLIC_URL}/images/cancel.png`;
                        this.modalCheckSubmit(msg,img);
                    }
                    window.location.reload();
                })
            })();
            
        }else{
            this.showModalLogin('errorFocus',<FormattedMessage id="plsinputdata" />)
        }
    }
    validateForm(){
        let fields = this.state.fields;
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;

        if(!fields['USERNAME']){
            formIsValid = false;
            errors["USERNAME"] = <FormattedMessage id="plsinputdata" />
            errorsFocus["USERNAME"] = 'errorFocus'
        }
        if(!fields['PASSWORD']){
            formIsValid = false;
            errors["PASSWORD"] = <FormattedMessage id="plsinputdata" />
            errorsFocus["PASSWORD"] = 'errorFocus'
        }

        this.setState({
            errors: errors,
            errorsFocus: errorsFocus
        });
      
        return formIsValid;
    }
    modalCheckSubmit(res, img) {
    alert = (
      <SweetAlert
        custom
        showCloseButton
        closeOnClickOutside={false}
        focusConfirmBtn={false}
        title=""
        customIcon={img}
        showConfirm={false}
        showCancelButton
        onCancel={() => this.handleChoice(false)}
        onConfirm={() => this.handleChoice(true)}
      >
        <div className="iconClose" onClick={() => this.handleChoice(false)}></div>
        <div className="fontSizeCase">{res}</div>
      </SweetAlert>
    );
    this.setState({ show: true, modal: alert });
    }

    handleChoice() {
        window.location.href=`${process.env.PUBLIC_URL}/home`
    }

    showModalLogin(errorsFocus,errors){
        alert = (
            <SweetAlert
                custom
                closeOnClickOutside = {true}
                showConfirm = {false}
                customClass='setpositionmodal'
            >
                <div className="row pb-3">
                    <div className="col-lg-6 col-sm-12 mobile">
                        <img className="img-fluid w-100" src={`${process.env.PUBLIC_URL}/images/Untitled-1-02.jpg`} />
                    </div>
                    <div className="col-lg-6 col-sm-12 px-4">
                        <div className="text-right" onClick={(e) => this.handleChoice(e)}>
                            <img className="img-fluid" src={`${process.env.PUBLIC_URL}/images/cancel.png`} width="30" />
                        </div>
                        <form onSubmit={(e) => this.submitForm(e)} className="pt-3">
                            <div className="header-sigin"><FormattedMessage id="signin" /></div>
                            <div className="form-group pt-3 text-left">
                                <label className="header-sigin-labelmail"><FormattedMessage id="phoneno" /></label>
                                <input 
                                    type="text"
                                    className={`form-control-set login-input ${errorsFocus}`}
                                    name="USERNAME"
                                    onChange={(e) => this.handleChange(e)}
                                />
                            </div>
                            <div className="errorMsg USERNAME">{errors}</div>
                            <div className="form-group pt-2 text-left">
                                <label className="header-sigin-labelpass"><FormattedMessage id="password" /></label>
                                <input 
                                    type="password"
                                    className={`form-control-set login-input ${errorsFocus}`}
                                    name="PASSWORD"
                                    onChange={(e) => this.handleChange(e)}
                                />
                            </div>
                            <div className="errorMsg PASSWORD">{errors}</div>
                            <div className="pt-2">
                                <a onClick={() => window.location.href = `${process.env.PUBLIC_URL}/forgotpassword`}>
                                    <div className="label-forgot"><FormattedMessage id="forgotpass" /></div>
                                </a>
                            </div>
                            <button className="btn-block btn-signin-mail mt-3 mb-2" type="sumbit">
                                <div style={{marginTop: '5px'}}><FormattedMessage id="login" /></div>
                            </button>
                        </form>
                        <button className="btn-block btn-signin-fb" onClick={() => this.login()}>
                            <div style={{marginTop: '5px'}}><FormattedMessage id="loginface" /></div>
                        </button>
                        <button className="btn-block btn-signin-gm mb-4" onClick={() => this.logingm()}>
                            <div style={{marginTop: '5px'}}><FormattedMessage id="logingmail" /></div>
                        </button>
                        <button className="btn-block btn-signin-mail" onClick={() => window.location.href = `${process.env.PUBLIC_URL}/register`}>
                            <div style={{marginTop: '5px'}}><FormattedMessage id="regisnew" /></div>
                        </button>
                    </div>
                </div>
            </SweetAlert>
        );
        this.setState({ show: true, modal: alert });
    }


    getCard(userId){
        maxcardActions.getCardList(userId).then(e => {
            var CardItem = e.data.data[0]
            var CardItemstringify = JSON.stringify(CardItem)
            var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
            var CardItemEncrypt = Cipher.encrypt(keyCipher, CardItemstringify);
            localStorage.setItem("CardItem", CardItemEncrypt);
            var resp = e.data.data[0].carD_TYPE_ID
        })
    }

    render(){ 
        return (
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].Loginweb}
            >
                <div>
                    {this.state.modal}
                </div>
            </IntlProvider>
        )
    }
}
