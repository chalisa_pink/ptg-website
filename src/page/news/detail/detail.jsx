import React from 'react';
import { newActions } from '../../../_actions/newActions'; 
import moment from "moment-timezone";
import 'moment/locale/th';
import '../../news/news.css';
import { IntlProvider, FormattedMessage } from "react-intl";
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

export class NewsDetail extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            lang: 'th',
            allData: {},
            NewallData: {},
            PromotionallData: {},
            onShowDetail: false,
            showLoadingDetail: 'none',
        };
    }
    componentDidMount(){
        var type = this.props.match.params.type
        var id = this.props.match.params.id
        if(type == "news"){
            this.getNews(id);
        } else {
            this.getPromotion(id);
        }
    }

    getNews(id){
        newActions.getNewByID(id).then(e => {
            console.log("getNewByID", e)
            // if(e.data.isSuccess === true){
            //     var resp = e.data.data
            //     var res = resp.find(x => x.id == id);
            //     this.setState({ allData : res })
            //     this.setState({ NewallData : e.data.data })
            //     this.setState({ showLoading : 'none'})
            // } else {
            //     this.setState({ onErrorgetNews : true })
            // }
            if(e.data.isSuccess === true){
                var resp = e.data.data[0]
                this.setState({ allData : e.data.data[0] })
                this.setState({ showLoading : 'none'})
            } else {
                this.setState({ onErrorgetNews : true })
            }
        })
    }

    getPromotion(id){
        setTimeout(() => {
            newActions.getPromotionByID(id).then(e => {
                // if(e.data.isSuccess === true){
                //     var resp = e.data.data
                //     var res = resp.find(x => x.id == id);
                //     this.setState({ allData : res })
                //     this.setState({ promotionData : e.data.data })
                //     this.setState({ showLoading : 'none'})
                // } else {
                //     this.setState({ onErrorgetNews : true })
                // }
                if(e.data.isSuccess === true){
                    var resp = e.data.data[0]
                    this.setState({ allData : e.data.data[0] })
                    this.setState({ showLoading : 'none'})
                } else {
                    this.setState({ onErrorgetNews : true })
                }
            })
        },500)
    }

    onCheckUlrDetail(url){
        if(url === "-"){
            this.setState({
                showLoadingDetail: 'block',
            })
            setTimeout(() => {
                this.setState({
                    onShowDetail: !this.state.onShowDetail,
                    showLoadingDetail: 'none',
                })
            },1000)
        } else {
            window.location.href=url
        }
    }

    render(){
        const { onShowDetail } = this.state
        var data = this.state.allData
        if(this.state.lang === "th"){
            var title = data.title
            var shotDes = data.shotDes
            var modifyDate = moment(data.modifyDate,'YYYY-MM-DD').add(+543,'y').locale('th').format('LL')
            var url = data.url
            var detail = data.detail
        }else{
            var title = data.title
            var shotDes = data.shotDes
            var modifyDate = moment(data.modifyDate,'YYYY-MM-DD').add('y').locale('en').format('LL')
            var url = data.urlen
            var detail = data.detailEN

        }
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].NewsDetail}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="container padding-header">
                            <div className="row">
                                <div className="col-12">
                                    <img className="img-fluid w-100" src={data.cover} />
                                </div>
                                <div className="col-12 py-3">
                                    <div className="t-bold py-4 t-46">{title}</div>
                                        <div className="t-26 text-word-wrap">{shotDes}</div>
                                        {/* {onShowDetail ?
                                        <div></div>
                                        :
                                        <a className="py-2 text-newsub-detail"  onClick={() => this.onCheckUlrDetail(url)}>
                                            click read more
                                        </a> 
                                        } */}
                                    <div className="t-mute text-sub-detail"><FormattedMessage id="date" /> {modifyDate}</div>
                                    <div className="card-condition">
                                            { ReactHtmlParser(detail) }
                                        </div>
                                </div>
                            </div>
                            {/* <div>
                                <div style={{ display: (this.state.showLoadingDetail === 'block' ? 'block' : 'none') }}>
                                    <div className="loaderdetail mx-auto " style={{ display: this.state.showLoadingDetail }}></div>
                                </div>
                                {onShowDetail ?
                                    <div style={{ display: (this.state.showLoadingDetail === 'block' ? 'none' : 'block') }}>
                                        <div className="card-condition">
                                            { ReactHtmlParser(detail) }
                                        </div>
                                    </div>
                                :
                                <div></div>
                                }
                                </div> */}
                        </div>
                    </div>
                </div>
            </IntlProvider>
        )
    }
}
