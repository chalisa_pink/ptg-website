import React from 'react';
import { newActions } from '../../../_actions/newActions'; 
import moment from "moment-timezone";
import 'moment/locale/th';
import LazyLoad from 'react-lazy-load';
import '../../news/news.css';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};


export class News extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            isHovered: true,
            showLoading: 'block',
            onErrorgetNews: false,
            newsData: {},
            promotionData: {},
            lang: 'th',
        };
        this.handleHover = this.handleHover.bind(this);
    }
    componentDidMount(){
        this.getNews();
    }

    getNews(){
        setTimeout(() => {
            newActions.getNew().then(e => {
                if(e.data.isSuccess === true){
                    console.log("newsData", e.data.data)
                    this.setState({ newsData : e.data.data })
                } else {
                    this.setState({ onErrorgetNews : true })
                }
                this.getPromotion();
            })
        },500)
    }

    getPromotion(){
        setTimeout(() => {
            newActions.getPromotion().then(e => {
                if(e.data.isSuccess === true){
                    this.setState({ promotionData : e.data.data })
                } else {
                    this.setState({ onErrorgetPromotion : true })
                }
                this.setState({ showLoading : 'none'})
            })
        },500)
    }

    handleHover(){
        this.setState(prevState => ({
            isHovered: !prevState.isHovered
        }));
    }

    newsDetail = ev => {
        var newsid = ev.currentTarget.dataset.newsid
        var type = 'news'
        var newsurl = ev.currentTarget.dataset.newsurl
        var newsurlen = ev.currentTarget.dataset.newsurlen
        // if(newsurl == ""){
            this.props.history.push({
                pathname:`${process.env.PUBLIC_URL}/news/detail/${type}/${newsid}`,
                state: {
                    tpye: 'news',
                    id: newsid
                }
            });
        // } else {
        //     if(this.state.language == "th"){
        //         window.location.href=newsurl
        //     } else {
        //         window.location.href=newsurlen
        //     }
        // }
    }

    promotionDetail = ev => {
        var promotionid = ev.currentTarget.dataset.promotionid
        var type = 'promotion'

        this.props.history.push({
            pathname:`${process.env.PUBLIC_URL}/news/detail/${type}/${promotionid}`,
            state: {
                tpye: 'promotion',
                id: promotionid
            }
        });
    }

    render(){
        const btnClass = this.state.isHovered ? "block" : "none";
        var newsData = this.state.newsData;
        var newsDataCard = [];
        if(newsData.length === 0){
            newsDataCard.push(
                <div className="col-12 lengthzero mx-auto text-center py-2 t-20">
                    ไม่พบข้อมูล
                </div>
            );
        } else {
            for(var x in newsData){
                if(this.state.lang === "th"){
                    var cover = newsData[x].cover
                    var title = newsData[x].title
                    var shotDes = newsData[x].shotDes
                    var modifyDate = moment(newsData[x].modifyDate,'YYYY-MM-DD').add(+543,'y').locale('th').format('LL')
                }else{
                    var cover = newsData[x].cover
                    var title = newsData[x].title
                    var shotDes = newsData[x].shotDes
                    var modifyDate = moment(newsData[x].modifyDate,'YYYY-MM-DD').add('y').locale('en').format('LL')
                }
                newsDataCard.push(
                    <div className="col-lg-3 col-xs-6 text-center">
                        <a 
                            onClick={this.newsDetail}
                            key={newsData[x].id}
                            data-newsid={newsData[x].id}
                            data-newsurl={newsData[x].url}
                            data-newsurlen={newsData[x].urlen}
                        >
                        <div className="card news-card">
                            <div className="card-body news-cardbody">
                                <div className="LazyLoad is-visible"><LazyLoad><img className="w-100 card-news-img" src={cover} /></LazyLoad></div>
                                <div className="">
                                    <div className="card-news-title">{title}</div>
                                    <div className="card-news-shotDes line-clamp">{shotDes}</div>
                                </div>
                                <div className="d-flex pt-3">
                                    <div className="card-news-date">{modifyDate}</div>
                                    {/* <div className="ml-auto ">
                                        <a className="card-news-more" ><FormattedMessage id="readmore" /></a>
                                    </div> */}
                                </div>
                            </div>
                        </div>
                        </a>
                    </div>
                )
            }
        }
        var promotionData = this.state.promotionData;
        var promotionDataCard = [];
        if(promotionData.length === 0){
            promotionDataCard.push(
                <div className="col-12 mx-auto text-center py-2 t-20">
                    ไม่พบข้อมูล
                </div>
            );
        } else {
            for(var x in promotionData){
                if(this.state.lang === "th"){
                    var cover = promotionData[x].cover
                    var title = promotionData[x].title
                    var shotDes = promotionData[x].shotDes
                    var modifyDate = moment(promotionData[x].modifyDate,'YYYY-MM-DD').add(+543,'y').locale('th').format('LL')
                }else{
                    var cover = promotionData[x].cover
                    var title = promotionData[x].title
                    var shotDes = promotionData[x].shotDes
                    var modifyDate = moment(promotionData[x].modifyDate,'YYYY-MM-DD').add('y').locale('en').format('LL')
                }
                promotionDataCard.push(
                    <div className="col-lg-3 col-xs-6 text-center">
                        <a 
                            onClick={this.promotionDetail}
                            key={promotionData[x].id}
                            data-promotionid={promotionData[x].id}
                        >
                        <div className="card news-card">
                            <div className="card-body news-cardbody">
                                <div className="LazyLoad is-visible"><LazyLoad><img className="w-100 card-news-img" src={cover} /></LazyLoad></div>
                                <div className="">
                                    <div className="card-news-title">{title}</div>
                                    <div className="card-news-shotDes">{shotDes}</div>
                                </div>
                                <div className="d-flex pt-3">
                                    <div className="card-news-date">{modifyDate}</div>
                                    {/* <div className="ml-auto ">
                                        <a className="card-news-more" ><FormattedMessage id="readmore" /></a>
                                    </div> */}
                                </div>
                            </div>
                        </div>
                        </a>
                        {/* <div className="card-pri">
                            <button 
                                className="hover-news pb-3 px-0" 
                                onClick={this.promotionDetail}
                                key={promotionData[x].id}
                                data-promotionid={promotionData[x].id}
                            >
                                <img className="img-news card-img-top"  alt="Card image cap" src={cover} />
                                <div className="my-3 set-detail-col px-2">
                                    <span className="setdetailpri">
                                        <div className="sub-head-title text-left">{title}</div>
                                        <div className="content-sub text-left">{shotDes}</div>
                                    </span>
                                </div>
                                <div className="row">
                                    <div className="col-8 datetime-sub pl-2">{modifyDate}</div>
                                    <div className="col-4 readmore-sub">อ่านต่อ</div>
                                </div> 
                            </button>
                        </div> */}
                    </div>
                )
            }
        }
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].News}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="container-fluid bg-news-content">
                            <div className="row">
                                <div className="col-12">
                                    <div className="news-title"><FormattedMessage id="newsandactivities" /></div>
                                </div>
                            </div>
                            <div className="row py-4">
                                <div className="col-12">
                                    <ul className="nav nav-news-tab justify-content-center">
                                        <li className="nav-item">
                                            <a className="nav-link active" data-toggle="pill" href="#news"><FormattedMessage id="news" /></a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" data-toggle="pill" href="#promotion"><FormattedMessage id="promotion" /></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="row pb-4">
                                <div className="col-12 px-0">
                                    <div className="news-tab tab-content">
                                        <div className="tab-pane active" id="news">
                                            <div className="row px-0">
                                                {newsDataCard}
                                            </div>
                                        </div>
                                        <div className="tab-pane fade" id="promotion">
                                            <div className="row">
                                                {promotionDataCard}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <div className="pc-pt container-fluid bg-news-content">
                            <div className="header-privilege">ข่าวสารกิจกรรม</div>
                            <div className="set-title-pr">
                                <div className="set-content-header">
                                    <ul className="nav nav-tabs">
                                        <li className="nav-item nav-mt">
                                            <a className="nav-link active" data-toggle="pill" href="#news">ข่าวสาร</a>
                                        </li>
                                        <li className="nav-item nav-mt">
                                            <a className="nav-link" data-toggle="pill" href="#promotion">โปรโมชัน</a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="news-tab tab-content">
                                    <div className="tab-pane active" id="news">
                                        <div className="row set-mt-content">
                                            {newsDataCard}
                                        </div>
                                    </div>
                                    <div className="tab-pane fade" id="promotion">
                                        <div className="row set-mt-content">
                                            {promotionDataCard}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  */}
                    </div>
                </div>
            </IntlProvider>
        )
    }
}
