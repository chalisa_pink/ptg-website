import React from 'react';
import { promotionActions } from '../../../_actions/promotionActions'; 
import { registerAction } from '../../../_actions/registerActions';
import moment from "moment-timezone";
import Barcode  from 'react-barcode';
import QRCode from 'qrcode.react';
import Rating from "react-rating";
import SweetAlert from "react-bootstrap-sweetalert";
import { Loginweb } from '../../loginweb';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');

export class PrivilegesAddress extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            PromotionDetail: "",
            havedata: "block",
            nodata: "none",
            PromotionDetail: {},
            CardItem: {},
            errorRedeem: false,
            errorTokenExpired: false,
            errorRedeemMsg: "",
            errorcheckThisPropsRedeem: false,
            GetCustomerProfile: {},
            checkExpireToken: "",
            checkTypeEstamp: false
        };
    }

    componentDidMount(){
        if(this.props.location.state == undefined){
            this.setState({ errorcheckThisPropsRedeem: true });
        } else {
            var PromotionDetail = this.props.location.state.PromotionDetail
            var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
            var ProfileData = localStorage.getItem('Profile')
            var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
            ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
            var ProfileArray = JSON.parse(ProfileDecrypt);
            var Profile = ProfileArray[0];
            var CardItemData = localStorage.getItem('CardItem')
            var CardItemDecrypt = Cipher.decrypt(keyCipher, CardItemData);
            CardItemDecrypt = CardItemDecrypt.replace(/[\u0000-\u0019]+/g,""); 
            var CardItem = JSON.parse(CardItemDecrypt);
            if(PromotionDetail.redeeM_POINT_TYPE == 5) {
                this.setState({ checkTypeEstamp: true })
            } else {
                this.setState({ checkTypeEstamp: false })
            }
            this.setState({ 
                userId: Profile.userId,
                customer_id: Profile.customer_id,
                tkmb: Profile.tkmb,
                Profile: Profile,
                CardItem: CardItem,
                PromotionDetail: PromotionDetail,
            });
            this.GetCustomerProfile(Profile.tkvsm, Profile.customer_id, Profile.userId)
        }
    }

    GetCustomerProfile(tkvsm, customer_id, userId){
        var formData = new FormData();
        formData.append('TOKEN_ID', tkvsm)
        formData.append('CUSTOMER_ID' , customer_id)
        formData.append('userId' , userId)
        formData.append('udId' , "")
        formData.append('deviceOs' , "")
        setTimeout(() => {
            registerAction.GetCustomerProfile(formData).then(e => {
                if(e.data.isSuccess == true){
                    var resp = JSON.parse(e.data.data.data)
                    var info = resp.CUSTOMER_PROFILE_INFO[0]
                    this.setState({ 
                        GetCustomerProfile: info,
                    })
                } else {
                    this.setState({ showLoading : 'none'})
                }
            })
            this.setState({ showLoading : 'none'})
        },500)
    }

    InsertRedeemPromotion(){
        this.setState({ showLoading: 'block' })
        const { Profile, PromotionDetail, CardItem, GetCustomerProfile } = this.state;
        var userId = Profile.userId
        setTimeout(() => {
            var dataRedeem = {
                "TokenMobileApp": Profile.tkmb,
                "UserId": 0,
                "CUSTOMER_ID": Profile.customer_id,
                "CARD_MASTER_ID": CardItem.carD_MASTER_ID,
                "SHIPPING_TYPE": 2,
                "SHIPPING_SHOP_ID": "",
                "REDEEM_CODE": PromotionDetail.redeeM_CODE,
                "FullAddress": GetCustomerProfile.FULL_ADDRESS,
                "SOURCE_DATA": 16
            }
            promotionActions.InsertRedeemPromotion(userId, dataRedeem).then(e => {
                if(e.data.isSuccess == true){
                    this.setState({ InsertRedeemPromotion: e.data.data })
                    this.props.history.push({
                        pathname:`${process.env.PUBLIC_URL}/privileges/address/success`,
                        state: {
                            PromotionDetail: PromotionDetail,
                            GetCustomerProfile: GetCustomerProfile,
                        }
                    });
                } else if(e.data.errCode == 42){
                    this.setState({ 
                        errorTokenExpired: true,
                        errorRedeemMsg: e.data.errMsg
                    })
                } else {
                    this.setState({ 
                        errorRedeem: true,
                        errorRedeemMsg: e.data.errMsg
                    })
                }
                this.setState({ showLoading: 'none' })
            })
        },500)
    }

    onConfirmErrorRedeem(){
        window.location.href = `${process.env.PUBLIC_URL}/privileges`;
    }

    errorcheckThisPropsRedeem(){
        window.location.href = `${process.env.PUBLIC_URL}/privileges`;
    }

    errorTokenExpired(){
        this.setState({ 
            checkExpireToken: <Loginweb/>,
            errorTokenExpired: false 
        })
    }

    render(){
        const { PromotionDetail, CardItem, GetCustomerProfile } = this.state;
        var carD_TOTAL_POINT = Math.floor(CardItem.carD_TOTAL_POINT)
        var carD_ESTAMP = Math.floor(CardItem.carD_ESTAMP)
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].PrivilegesAddress}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="bg-ptmaxcard-content">
                        <div className="ptmaxcard-content">
                                <div className="row py-5">
                                    <div className="col-lg-10 col-md-6 mx-auto">
                                        <div className="reward-redeem-card-body">
                                            <div className="row">
                                                <div className="col-lg-5 col-md-6 px-0">
                                                    <img className="img-fluid w-100 h-100 img-reward-branch" onError={(e) => this.onImageError(e)} src={PromotionDetail.imG_URL_NORMAL == null ? `${process.env.PUBLIC_URL}/images/logo.jpg` : PromotionDetail.imG_URL_NORMAL} />
                                                    <span className="newitem-historydetail" style={{ display: (PromotionDetail.isNew == true ? 'block' : 'none') }}><FormattedMessage id="news" /></span>
                                                </div>
                                                <div className="col-lg-7 col-md-6 reward-redeem-p-5">
                                                    <div className="historydetail-content">
                                                        <div className="historydetail-title">{PromotionDetail.redeeM_NAME == null ? <FormattedMessage id="nodata" /> : PromotionDetail.redeeM_NAME}</div>
                                                        <Rating
                                                            {...this.props} 
                                                            initialRating={0}
                                                            placeholderRating={PromotionDetail.ratingStar == null ? "0" : PromotionDetail.ratingStar}
                                                            emptySymbol={<img src={process.env.PUBLIC_URL +"/images/stargray.png"} className="icon" width="15"/>}
                                                            placeholderSymbol={<img src={process.env.PUBLIC_URL +"/images/stargreen.png"} className="icon" width="15"/>}
                                                            readonly
                                                        /> 
                                                        <span className="px-2 t-24">({PromotionDetail.ratingStar == null ? "0" : PromotionDetail.ratingStar})</span>
                                                        <hr/>
                                                        <div className="mt-1">
                                                        <div className="historydetail-inside-title"><FormattedMessage id="addresssend" /></div>
                                                            <div className="reward-address mt-3">
                                                                <div className="t-green">{GetCustomerProfile.FULL_NAME_TH}</div>
                                                                <div>{GetCustomerProfile.FULL_ADDRESS} </div>
                                                                <div className="t-green">{GetCustomerProfile.PHONE_NO}</div>
                                                            </div>
                                                        </div>
                                                        <div className="historydetail-howto mt-4">
                                                            <div><FormattedMessage id="tutorial" /></div>
                                                            <div><FormattedMessage id="tutorialdetail" /></div>
                                                        </div>
                                                        <hr/>
                                                        <div className="d-flex t-bold t-green t-22 mt-2">
                                                            <div className="mr-auto"><FormattedMessage id="pointhave" /></div>
                                                            <div className="">
                                                                {this.state.checkTypeEstamp ? carD_ESTAMP : carD_TOTAL_POINT } คะแนน
                                                            </div>
                                                        </div>
                                                        <div className="d-flex t-bold t-green t-22 mb-3">
                                                            <div className="mr-auto"><FormattedMessage id="pointall" /></div>
                                                            <div className="">
                                                                {this.state.checkTypeEstamp ? PromotionDetail.redeeM_STAMP : PromotionDetail.redeeM_TOTAL_POINT} คะแนน
                                                            </div>
                                                        </div>
                                                        <button onClick={() => this.InsertRedeemPromotion()} disabled={this.state.checked} className="btn btn-block rewarddetail-btnredeem"><FormattedMessage id="btnconfirm" /></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <SweetAlert   
                        show={this.state.errorRedeem}
                        confirmBtnCssClass="btn btn-block btn-error-redeem"
                        confirmBtnText={<FormattedMessage id="btnok" />}
                        onConfirm={() => this.onConfirmErrorRedeem()}
                    >
                        <span>
                            <img 
                                src={process.env.PUBLIC_URL +"/images/cancel.png"}
                                className="py-3" 
                                alt="sena"
                                width="100"
                            />
                            <div className="t-36 mt-4 t-height-36">{this.state.errorRedeemMsg}</div>
                        </span>
                    </SweetAlert>
                    <SweetAlert   
                        show={this.state.errorTokenExpired}
                        confirmBtnCssClass="btn btn-block btn-error-redeem"
                        confirmBtnText={<FormattedMessage id="btnok" />}
                        onConfirm={() => this.errorTokenExpired()}
                    >
                        <span>
                            <img 
                                src={process.env.PUBLIC_URL +"/images/cancel.png"}
                                className="py-3" 
                                alt="sena"
                                width="100"
                            />
                            <div className="t-36 mt-4">{this.state.errorRedeemMsg}</div>
                        </span>
                    </SweetAlert>
                    <SweetAlert   
                        show={this.state.errorcheckThisPropsRedeem}
                        confirmBtnCssClass="btn btn-block btn-error-redeem"
                        confirmBtnText={<FormattedMessage id="btnok" />}
                        onConfirm={() => this.errorcheckThisPropsRedeem()}
                    >
                        <span>
                            <img 
                                src={process.env.PUBLIC_URL +"/images/cancel.png"}
                                className="py-3" 
                                alt="sena"
                                width="100"
                            />
                            <div className="t-36 mt-4"><FormattedMessage id="errorMessage" /></div>
                        </span>
                    </SweetAlert>
                    {this.state.checkExpireToken}
                </div>
            </IntlProvider>
        )
    }
}

