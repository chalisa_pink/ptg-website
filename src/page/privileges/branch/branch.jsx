import React from 'react';
import { promotionActions } from '../../../_actions/promotionActions'; 
import { maxcardActions } from '../../../_actions/maxcardActions';
import moment from "moment-timezone";
import Barcode  from 'react-barcode';
import QRCode from 'qrcode.react';
import Rating from "react-rating";
import SweetAlert from "react-bootstrap-sweetalert";
import { Loginweb } from '../../loginweb';
import Header from '../../../_pagebuilder/Header';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');

export class PrivilegesBranch extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'none',
            PromotionDetail: "",
            havedata: "block",
            nodata: "none",
            PromotionDetail: {},
            CardItem: {},
            errorRedeem: false,
            errorTokenExpired: false,
            errorRedeemMsg: "",
            errorcheckThisPropsRedeem: false,
            checkTypeEstamp: false
        };
    }

    componentDidMount(){
        if(this.props.location.state == undefined){
            this.setState({ errorcheckThisPropsRedeem: true });
        } else {
            var PromotionDetail = this.props.location.state.PromotionDetail
            var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
            var ProfileData = localStorage.getItem('Profile')
            var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
            ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
            var ProfileArray = JSON.parse(ProfileDecrypt);
            var Profile = ProfileArray[0];
            var CardItemData = localStorage.getItem('CardItem')
            var CardItemDecrypt = Cipher.decrypt(keyCipher, CardItemData);
            CardItemDecrypt = CardItemDecrypt.replace(/[\u0000-\u0019]+/g,""); 
            var CardItem = JSON.parse(CardItemDecrypt);
            if(PromotionDetail.redeeM_POINT_TYPE == 5) {
                this.setState({ checkTypeEstamp: true })
            } else {
                this.setState({ checkTypeEstamp: false })
            }
            this.setState({ 
                userId: Profile.userId,
                customer_id: Profile.customer_id,
                tkmb: Profile.tkmb,
                Profile: Profile,
                CardItem: CardItem,
                PromotionDetail: PromotionDetail,
            });
        }
    }

    PrivilegesBranchSearch(){
        this.props.history.push({
            pathname:`${process.env.PUBLIC_URL}/privileges/branch/search`,
            state: {
                PromotionDetail: this.state.PromotionDetail
            }
        });
    }

    errorcheckThisPropsRedeem(){
        window.location.href = `${process.env.PUBLIC_URL}/privileges`;
    }

    render(){
        const { PromotionDetail, CardItem } = this.state;
        var carD_TOTAL_POINT = Math.floor(CardItem.carD_TOTAL_POINT)
        var carD_ESTAMP = Math.floor(CardItem.carD_ESTAMP)
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].PrivilegesBranch}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="bg-ptmaxcard-content">
                            <div className="ptmaxcard-content">
                                <div className="row py-5">
                                    <div className="col-lg-10 col-md-6 mx-auto">
                                        <div className="reward-redeem-card-body">
                                            <div className="row">
                                                <div className="col-lg-5 col-md-6 px-0">
                                                    <img className="img-fluid w-100 h-100 img-reward-branch" onError={(e) => this.onImageError(e)} src={PromotionDetail.imG_URL_NORMAL == null ? `${process.env.PUBLIC_URL}/images/logo.jpg` : PromotionDetail.imG_URL_NORMAL} />
                                                    <span className="newitem-historydetail" style={{ display: (PromotionDetail.isNew == true ? 'block' : 'none') }}><FormattedMessage id="news" /></span>
                                                </div>
                                                <div className="col-lg-7 col-md-6 reward-redeem-p-5">
                                                    <div className="historydetail-content">
                                                        <div className="historydetail-title">{PromotionDetail.redeeM_NAME == null ? <FormattedMessage id="nodata" /> : PromotionDetail.redeeM_NAME}</div>
                                                        <Rating
                                                            {...this.props} 
                                                            initialRating={0}
                                                            placeholderRating={PromotionDetail.ratingStar == null ? "0" : PromotionDetail.ratingStar}
                                                            emptySymbol={<img src={process.env.PUBLIC_URL +"/images/stargray.png"} className="icon" width="15"/>}
                                                            placeholderSymbol={<img src={process.env.PUBLIC_URL +"/images/stargreen.png"} className="icon" width="15"/>}
                                                            readonly
                                                        /> 
                                                        <span className="px-2 t-24">({PromotionDetail.ratingStar == null ? "0" : PromotionDetail.ratingStar})</span>
                                                        <hr/>
                                                        <div className="mt-1">
                                                        <div className="historydetail-inside-title"><FormattedMessage id="selectbranch" /></div>
                                                        <a onClick={() => this.PrivilegesBranchSearch()}>
                                                                <div className="reward-branch-input mt-3">
                                                                    <FormattedMessage id="findbranch" />
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div className="historydetail-howto mt-4">
                                                            <div><FormattedMessage id="tutorial" /></div>
                                                            <div><FormattedMessage id="tutorialdetail" /></div>
                                                        </div>
                                                        <hr/>
                                                        <div className="d-flex t-bold t-green t-22 mt-2">
                                                            <div className="mr-auto"><FormattedMessage id="pointhave" /></div>
                                                            <div className="">
                                                                {this.state.checkTypeEstamp ? carD_ESTAMP : carD_TOTAL_POINT } คะแนน
                                                            </div>
                                                        </div>
                                                        <div className="d-flex t-bold t-green t-22 mb-3">
                                                            <div className="mr-auto"><FormattedMessage id="pointall" /></div>
                                                            <div className="">
                                                                {this.state.checkTypeEstamp ? PromotionDetail.redeeM_STAMP : PromotionDetail.redeeM_TOTAL_POINT} คะแนน
                                                            </div>
                                                        </div>
                                                        {/* <button onClick={() => this.RedeemReward()} disabled={this.state.checked} className="btn btn-block rewarddetail-btnredeem">ยืนยัน</button> */}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <SweetAlert   
                                show={this.state.errorRedeem}
                                confirmBtnCssClass="btn btn-block btn-error-redeem"
                                confirmBtnText={<FormattedMessage id="btnok" />}
                                onConfirm={() => this.onConfirmErrorRedeem()}
                            >
                                <span>
                                    <img 
                                        src={process.env.PUBLIC_URL +"/images/cancel.png"}
                                        className="py-3" 
                                        alt="sena"
                                        width="100"
                                    />
                                    <div className="t-36 mt-4">{this.state.errorRedeemMsg}</div>
                                </span>
                            </SweetAlert>
                            <SweetAlert   
                                show={this.state.errorTokenExpired}
                                confirmBtnCssClass="btn btn-block btn-error-redeem"
                                confirmBtnText={<FormattedMessage id="btnok" />}
                                onConfirm={() => this.errorTokenExpired()}
                            >
                                <span>
                                    <img 
                                        src={process.env.PUBLIC_URL +"/images/cancel.png"}
                                        className="py-3" 
                                        alt="sena"
                                        width="100"
                                    />
                                    <div className="t-36 mt-4">{this.state.errorRedeemMsg}</div>
                                </span>
                            </SweetAlert>
                            <SweetAlert   
                                show={this.state.errorcheckThisPropsRedeem}
                                confirmBtnCssClass="btn btn-block btn-error-redeem"
                                confirmBtnText={<FormattedMessage id="btnok" />}
                                onConfirm={() => this.errorcheckThisPropsRedeem()}
                            >
                                <span>
                                    <img 
                                        src={process.env.PUBLIC_URL +"/images/cancel.png"}
                                        className="py-3" 
                                        alt="sena"
                                        width="100"
                                    />
                                    <div className="t-36 mt-4"><FormattedMessage id="errorMessage" /></div>
                                </span>
                            </SweetAlert>
                        </div>
                    </div>
                </div>
            </IntlProvider>
        )
    }
}

