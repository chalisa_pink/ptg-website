import React from 'react';
import { promotionActions } from '../../../../_actions/promotionActions'; 
import moment from "moment-timezone";
import Barcode  from 'react-barcode';
import QRCode from 'qrcode.react';
import Rating from "react-rating";
import SweetAlert from "react-bootstrap-sweetalert";
import { registerAction } from '../../../../_actions/registerActions'; 
import FilterResults from 'react-filter-search';
import { Loginweb } from '../../../loginweb';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../../translations/en.json";
import intlMessageTH from "../../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');

export class PrivilegesBranchSearch extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'none',
            showLoadingDetail: 'none',
            PromotionDetail: "",
            havedata: "block",
            nodata: "none",
            PromotionDetail: {},
            CardItem: {},
            errorRedeem: false,
            errorTokenExpired: false,
            errorRedeemMsg: "",
            GetPTBranch: [],
            branchName: "",
            branchAddress: "",
            branchId: "",
            BranchSearchvalue: '',
            errorcheckThisPropsRedeem: false,
            checkExpireToken: "",
            checkTypeEstamp: false
        };
    }

    componentDidMount(){
        if(this.props.location.state == undefined){
            this.setState({ errorcheckThisPropsRedeem: true });
        } else {
            var PromotionDetail = this.props.location.state.PromotionDetail
            var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
            var ProfileData = localStorage.getItem('Profile')
            var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
            ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
            var ProfileArray = JSON.parse(ProfileDecrypt);
            var Profile = ProfileArray[0];
            var CardItemData = localStorage.getItem('CardItem')
            var CardItemDecrypt = Cipher.decrypt(keyCipher, CardItemData);
            CardItemDecrypt = CardItemDecrypt.replace(/[\u0000-\u0019]+/g,""); 
            var CardItem = JSON.parse(CardItemDecrypt);
            if(PromotionDetail.redeeM_POINT_TYPE == 5) {
                this.setState({ checkTypeEstamp: true })
            } else {
                this.setState({ checkTypeEstamp: false })
            }
            this.setState({ 
                userId: Profile.userId,
                customer_id: Profile.customer_id,
                tkmb: Profile.tkmb,
                Profile: Profile,
                CardItem: CardItem,
                PromotionDetail: PromotionDetail,
            });
            this.getProvince()
        }
    }

    getProvince(){
        setTimeout(() => {
            registerAction.getProvinceNew().then(e => {
                var resp = e.data.dropdowN_INFO
                this.setState({ 
                    getProvince : resp,
                    showLoading : 'none'
                })
            })
        },500)
    }

    GetPTBranch = (e) => {
        const value = e.target.value;
        this.setState({ 
            showLoadingDetail: 'block',
            Province: value,
            BranchSearch: false,
            BranchSearchvalue:""
        })
        setTimeout(() => {
            promotionActions.GetPTBranch(value).then(e => {
                var resp = e.data.data
                this.setState({ 
                    GetPTBranch : resp,
                    showLoadingDetail: 'none'
                })
            })
        },500)
    } 

    handleChange = (e) => {
        const branchName = e.target.value;
        const branchAddress = e.target.dataset.address;
        const branchId = e.target.id;
        this.setState({ 
            branchName: branchName, 
            branchAddress: branchAddress,
            branchId: branchId
        });
    };

    GetPTBranchSearch = event => {
        const { value } = event.target;
        this.setState({ 
            BranchSearchvalue: value, 
            BranchSearch: true,
        });
    };

    InsertRedeemPromotion(){
        this.setState({ showLoading: 'block' })
        const { Profile, PromotionDetail, CardItem, branchAddress, branchId, branchName } = this.state;
        var userId = Profile.userId
        setTimeout(() => {
            var dataRedeem = {
                "TokenMobileApp": Profile.tkmb,
                "UserId": 0,
                "CUSTOMER_ID": Profile.customer_id,
                "CARD_MASTER_ID": CardItem.carD_MASTER_ID,
                "SHIPPING_TYPE": 1,
                "SHIPPING_SHOP_ID": branchId,
                "REDEEM_CODE": PromotionDetail.redeeM_CODE,
                "FullAddress": "",
                "SOURCE_DATA": 16
            }
            promotionActions.InsertRedeemPromotion(userId, dataRedeem).then(e => {
                if(e.data.isSuccess == true){
                    this.setState({ InsertRedeemPromotion: e.data.data })
                    this.props.history.push({
                        pathname:`${process.env.PUBLIC_URL}/privileges/branch/success`,
                        state: {
                            PromotionDetail: PromotionDetail,
                            branchName: branchName,
                            branchAddress: branchAddress,
                        }
                    });
                } else if(e.data.errCode == 42){
                    this.setState({ 
                        errorTokenExpired: true,
                        errorRedeemMsg: e.data.errMsg
                    })
                } else {
                    this.setState({ 
                        errorRedeem: true,
                        errorRedeemMsg: e.data.errMsg
                    })
                }
                this.setState({ showLoading: 'none' })
            })
        },500)
    }

    onConfirmErrorRedeem(){
        window.location.href = `${process.env.PUBLIC_URL}/privileges`;
    }

    errorcheckThisPropsRedeem(){
        window.location.href = `${process.env.PUBLIC_URL}/privileges`;
    }

    errorTokenExpired(){
        this.setState({ 
            checkExpireToken: <Loginweb/>,
            errorTokenExpired: false 
        })
    }

    render(){
        const { PromotionDetail, CardItem, getProvince, GetPTBranch, BranchSearchvalue } = this.state;
        var getProvinceCard = [];
        for(var p in getProvince){
            getProvinceCard.push(
                <option value={getProvince[p].codE_GUID}>{getProvince[p].values}</option>
            )
        }

        var GetPTBranchCard = [];
        for(var x in GetPTBranch){
            GetPTBranchCard.push(
                <div>
                    <input type="radio" data-address={GetPTBranch[x].address} name="branchName" value={GetPTBranch[x].shoP_NAME} id={GetPTBranch[x].orG_SHOP_ID} />
                    <label className="list-group-item search-listgitem" for={GetPTBranch[x].orG_SHOP_ID}>
                        <div className="t-28 t-green">{GetPTBranch[x].shoP_NAME}</div>
                        <div className="t-24 t-gray">{GetPTBranch[x].address}</div>
                    </label>
                </div>
            )
        }
        var carD_TOTAL_POINT = Math.floor(CardItem.carD_TOTAL_POINT)
        var carD_ESTAMP = Math.floor(CardItem.carD_ESTAMP)
        var PrivilegesBranchSearch = messages[this.state.language].PrivilegesBranchSearch
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].PrivilegesBranchSearch}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="bg-ptmaxcard-content">
                            <div className="ptmaxcard-content">
                                <div className="row py-5">
                                    <div className="col-lg-7 col-md-6 pb-3">
                                        <div className="reward-redeem-card-body h-100">
                                            <div className="row reward-redeem-p-5-1">
                                                <div className="col-12">
                                                    <div className="form-group">
                                                        <select 
                                                            className="form-control reward-branchsearch-select"
                                                            onChange={(e) => this.GetPTBranch(e)}
                                                            name="GetPTProvince"
                                                        >
                                                            <option selected hidden>{PrivilegesBranchSearch.selectprovinc}</option>
                                                            {getProvinceCard}
                                                        </select>
                                                    </div>
                                                    <div className="form-group">
                                                        <input 
                                                            className="form-control reward-branchsearch-input" 
                                                            placeholder={PrivilegesBranchSearch.inputstation}
                                                            name="GetPTBranch"
                                                            value={this.state.BranchSearchvalue}
                                                            onChange={(e) => this.GetPTBranchSearch(e)}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="col-12 py-3">
                                                    <div className="loadingGifLoadmore mx-auto" style={{ display: this.state.showLoadingDetail}}>
                                                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                                                    </div>
                                                    <div style={{ display: (this.state.showLoadingDetail === 'block' ? 'none' : 'block') }}>
                                                        <div className="list-group search-list" onChange={(e) => this.handleChange(e)}>
                                                            <div style={{ display: (this.state.BranchSearch ? 'none' : 'block') }}>{GetPTBranchCard}</div>
                                                            <div style={{ display: (this.state.BranchSearch ? 'block' : 'none') }}>
                                                                <FilterResults
                                                                    value={BranchSearchvalue}
                                                                    data={GetPTBranch}
                                                                    renderResults={results => (
                                                                        <div>
                                                                            {results.map(el => (
                                                                                <div>
                                                                                    <input type="radio" data-address={el.address} name="branchName" value={el.shoP_NAME} id={el.orG_SHOP_ID} />
                                                                                    <label className="list-group-item search-listgitem" for={el.orG_SHOP_ID}>
                                                                                        <div className="t-28 t-green">{el.shoP_NAME}</div>
                                                                                        <div className="t-24 t-gray">{el.address}</div>
                                                                                    </label>
                                                                                </div>
                                                                            ))}
                                                                        </div>
                                                                    )}
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 col-md-6">
                                        <div className="reward-redeem-card-body">
                                            <div className="row">
                                                <div className="col-12">
                                                    {/* <img className="img-fluid w-100 img-reward-branch" onError={(e) => this.onImageError(e)} src={PromotionDetail.imG_URL_DEFAULT == null ? `${process.env.PUBLIC_URL}/images/logo.jpg` : PromotionDetail.imG_URL_DEFAULT} /> */}
                                                    <div className="historydetail-content reward-redeem-p-5-2">
                                                        <div className="historydetail-title">{PromotionDetail.redeeM_NAME == null ? <FormattedMessage id="nodata" /> : PromotionDetail.redeeM_NAME}</div>
                                                        <Rating
                                                            {...this.props} 
                                                            initialRating={0}
                                                            placeholderRating={PromotionDetail.ratingStar == null ? "0" : PromotionDetail.ratingStar}
                                                            emptySymbol={<img src={process.env.PUBLIC_URL +"/images/stargray.png"} className="icon" width="15"/>}
                                                            placeholderSymbol={<img src={process.env.PUBLIC_URL +"/images/stargreen.png"} className="icon" width="15"/>}
                                                            readonly
                                                        /> 
                                                        <span className="px-2 t-24">({PromotionDetail.ratingStar == null ? "0" : PromotionDetail.ratingStar})</span>
                                                        <hr/>
                                                        <div className="mt-1">
                                                        <div className="historydetail-inside-title"><FormattedMessage id="selectbranchgetproduct" /></div>
                                                        <div className="reward-branchsearch-choose mt-2">
                                                                {this.state.branchName == "" ? <FormattedMessage id="selectbranch" /> : this.state.branchName}
                                                            </div>
                                                        </div>
                                                        <div className="mt-4">
                                                        <div className="historydetail-inside-title"><FormattedMessage id="branchaddress" /></div>
                                                        <div className="reward-branchsearch-choose mt-2">
                                                                {this.state.branchAddress == "" ? <FormattedMessage id="selectbranchaddress" /> : this.state.branchAddress}
                                                            </div>
                                                        </div>
                                                        <div className="historydetail-howto mt-4">
                                                            <div><FormattedMessage id="tutorial" /></div>
                                                            <div><FormattedMessage id="tutorialdetail" /></div>
                                                        </div>
                                                        <hr/>
                                                        <div className="d-flex t-bold t-green t-22 mt-4">
                                                            <div className="mr-auto"><FormattedMessage id="pointhave" /></div>
                                                            <div className="">
                                                                {this.state.checkTypeEstamp ? carD_ESTAMP : carD_TOTAL_POINT } คะแนน
                                                            </div>
                                                        </div>
                                                        <div className="d-flex t-bold t-green t-22 mb-3">
                                                            <div className="mr-auto"><FormattedMessage id="pointall" /></div>
                                                            <div className="">
                                                                {this.state.checkTypeEstamp ? PromotionDetail.redeeM_STAMP : PromotionDetail.redeeM_TOTAL_POINT} คะแนน
                                                            </div>
                                                        </div>
                                                        <button onClick={() => this.InsertRedeemPromotion()} disabled={this.state.branchName == "" ? true : false} className="btn btn-block rewarddetail-btnredeem"><FormattedMessage id="btnconfirm" /></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <SweetAlert   
                        show={this.state.errorRedeem}
                        confirmBtnCssClass="btn btn-block btn-error-redeem"
                        confirmBtnText={<FormattedMessage id="btnok" />}
                        onConfirm={() => this.onConfirmErrorRedeem()}
                    >
                        <span>
                            <img 
                                src={process.env.PUBLIC_URL +"/images/cancel.png"}
                                className="py-3" 
                                alt="sena"
                                width="100"
                            />
                            <div className="t-36 mt-4 t-height-36">{this.state.errorRedeemMsg}</div>
                        </span>
                    </SweetAlert>
                    <SweetAlert   
                        show={this.state.errorTokenExpired}
                        confirmBtnCssClass="btn btn-block btn-error-redeem"
                        confirmBtnText={<FormattedMessage id="btnok" />}
                        onConfirm={() => this.errorTokenExpired()}
                    >
                        <span>
                            <img 
                                src={process.env.PUBLIC_URL +"/images/cancel.png"}
                                className="py-3" 
                                alt="sena"
                                width="100"
                            />
                            <div className="t-36 mt-4">{this.state.errorRedeemMsg}</div>
                        </span>
                    </SweetAlert>
                    <SweetAlert   
                        show={this.state.errorcheckThisPropsRedeem}
                        confirmBtnCssClass="btn btn-block btn-error-redeem"
                        confirmBtnText={<FormattedMessage id="btnok" />}
                        onConfirm={() => this.errorcheckThisPropsRedeem()}
                    >
                        <span>
                            <img 
                                src={process.env.PUBLIC_URL +"/images/cancel.png"}
                                className="py-3" 
                                alt="sena"
                                width="100"
                            />
                            <div className="t-36 mt-4"><FormattedMessage id="errorMessage" /></div>
                        </span>
                    </SweetAlert>
                    {this.state.checkExpireToken}
                </div>
            </IntlProvider>
        )
    }
}

