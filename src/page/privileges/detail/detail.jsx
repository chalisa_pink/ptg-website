import React from 'react';
import { promotionActions } from '../../../_actions/promotionActions'; 
import '../../privileges/privileges.css';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import moment from "moment-timezone";
import 'moment/locale/th';
import SweetAlert from "react-bootstrap-sweetalert";
import { Loginweb } from '../../loginweb';
import { registerAction } from '../../../_actions/registerActions';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";

var Cipher = require('aes-ecb'); 
var crypto = require('crypto');
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

export class Detail extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            lang: 'th',
            promotionId: "",
            PromotionDetail: {},
            CardItem:{},
            setIcon : 'fas fa-plus',
            chkIcon : 'false',
            noteQuatity : "",
            checkedButtonRedeem: false,
            errorcheckThisPropsRedeem: false,
            checkExpireToken: "",
            checkTypeEstamp: false,
            GetCustomerProfile: {},
            getTokenCardId: "",
            errorRedeem: false,
            errorTokenExpired: false,
            errorRedeemMsg: "",
            modal: null,
            show: false ,
        };
    }

    componentDidMount(){
        // if(this.props.location.state == undefined){
        //     this.setState({ errorcheckThisPropsRedeem: true });
        // } else {
            if(localStorage.getItem('2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu') === "true"){
                var CardItem
                var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
                var ProfileData = localStorage.getItem('Profile')
                var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
                ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
                var ProfileArray = JSON.parse(ProfileDecrypt);
                var Profile = ProfileArray[0];
                var formData = new FormData();
                formData.append('TOKEN_ID', Profile.tkvsm)
                formData.append('CUSTOMER_ID' , Profile.customer_id)
                formData.append('userId' , Profile.userId)
                formData.append('udId' , "")
                formData.append('deviceOs' , "")
                registerAction.GetCustomerProfile(formData).then(e => {
                    if(e.data.isSuccess == true){
                        var getToken = e.data.data.tokenId
                        var resp = JSON.parse(e.data.data.data)
                        var info = resp.CUSTOMER_PROFILE_INFO[0]
                        this.setState({ 
                            GetCustomerProfile: info,
                            getTokenCardId: getToken, 
                        })
                    } else {
                        this.setState({ GetCustomerProfile: "" })
                    }
                })
                if(localStorage.getItem('CardItem')){
                    var CardItemData = localStorage.getItem('CardItem')
                    var CardItemDecrypt = Cipher.decrypt(keyCipher, CardItemData);
                    CardItemDecrypt = CardItemDecrypt.replace(/[\u0000-\u0019]+/g,""); 
                    CardItem = JSON.parse(CardItemDecrypt);
                }
                var promotionId = this.props.match.params.id;
                this.getDetailPromotionByID(promotionId, CardItem)
                this.setState({ 
                    Profile:Profile,  
                    CardItem:CardItem,  
                    promotionId:promotionId
                })
            } else {
                var promotionId = this.props.match.params.id
                this.getDetailPromotionByID(promotionId, null)
                this.setState({ 
                    promotionId:promotionId
                })
                // this.setState({ checkExpireToken: <Loginweb/> })
            }
        // }
    }

    modalCheckRedeem(res, img) {
        alert = (
          <SweetAlert
            custom
            showCancel
            showCloseButton
            confirmBtnText={<FormattedMessage id="confirm" />}
            cancelBtnText={<FormattedMessage id="cancel" />}
            confirmBtnBsStyle="success"
            cancelBtnBsStyle="light"
            // closeOnClickOutside={false}
            // focusConfirmBtn={false}
            // title=""
            customIcon={img}
            showConfirm={true}
            showCancel={true}
            onCancel={() => this.handleChoice(false)}
            onConfirm={() => this.handleChoice(true)}
            reverseButtons={true}
          >
            <div className="iconClose" onClick={() => this.handleChoice(false)}></div>
            <div className="fontSizeCase">{res}</div>
          </SweetAlert>
        );
        this.setState({ show: true, modal: alert });
    }

    handleChoice(choice) {
        if (choice === false) {
            this.setState({ show: false , modal: null })
        }else{
            this.setState({ show: false , modal: null })
            this.RedeemReward() // CHECK ไปแลกรางวัล
        }
            
    }

    showPopup(popuplist){
        alert = (
            <div className="home-popupbanner">
                <SweetAlert
                    custom
                    style={{ backgroundColor:'#fff0', color: 'white' }}
                    showConfirm={false}
                >
                    <div className="row">
                        <div className="col-12 text-right">
                            <button type="button" className="btn home-popupbanner-btn" onClick={() => this.onClosePopup()}>Close</button>
                        </div>
                        <div className="col-12">
                            <img src={popuplist} className="img-fluid"/>
                        </div>
                    </div>
                </SweetAlert>
            </div>
       );
       this.setState({ show: true, modal: alert });
    }

    onClosePopup(){
        this.setState({ show: false, modal: null })
    }

    getDetailPromotionByID(promotionId, CardItem){
        setTimeout(() => {
            promotionActions.getDetailPromotionByID(promotionId).then(e => {
                console.log("getDetailPromotionByID", e)
                if(e.data.isSuccess == true){
                    var resq = e.data.data
                    this.checkRedeemTransaction(resq, CardItem)

                    if(e.data.data.img_url_spread != "" && e.data.data.img_url_spread != null){
                        this.showPopup(e.data.data.img_url_spread);
                    }
                } else {
                    this.setState({ 
                        PromotionDetail: e.data.data,
                        showLoading : 'none'
                    })
                }
            })
        },500)
    }

    checkRedeemTransaction(promotion, CardItem){
        if(localStorage.getItem('2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu') !== "true"){
            this.setState({ 
                PromotionDetail: promotion,
                showLoading : 'none',
                checkedButtonRedeem: false
            })
        }else if(CardItem == null){
            this.setState({ 
                PromotionDetail: promotion,
                showLoading : 'none',
                checkedButtonRedeem: false
            })
        } else {
            promotionActions.checkRedeemTransection(promotion.promotionId, CardItem.carD_TYPE_ID).then(e => {
                if(e.data.isSuccess == true){
                    if(promotion.caN_REDEEM_STATUS == "Y" && promotion.redeeM_STATUS == "Y"){
                        if(promotion.redeeM_POINT_TYPE == 5) { // CHECK ว่าเป็น E-Stamp ไหม
                            if(Math.floor(CardItem.carD_ESTAMP) < Math.floor(promotion.redeeM_STAMP)){ // CHECK ว่ามีคะแนน E-Stamp ไหม
                                if(promotion.quantitY_REMAIN <= 0){ // CHECK ถ้าจำนวนของรางวัลหมด == 0 ให้ปิดปุ่มแลกและขึ้นข้อความ
                                    var noteQuatity = <div> <FormattedMessage id="sorrynorewardselect" /> <br /> <FormattedMessage id="plsselectanother" /></div>
                                }
                                this.setState({ 
                                    noteQuatity: noteQuatity, 
                                    checkedButtonRedeem: true,
                                    PromotionDetail: promotion,
                                    showLoading : 'none',
                                    checkTypeEstamp: true,
                                })
                            } else if(Math.floor(promotion.redeeM_STAMP) == 0){ // CHECK ถ้าของรางวัลคะแนน == 0 ให้เปิดปุ่ม
                                this.setState({ 
                                    PromotionDetail: promotion,
                                    showLoading : 'none',
                                    checkedButtonRedeem: false,
                                    checkTypeEstamp: true,
                                })
                            } else {
                                this.setState({ 
                                    PromotionDetail: promotion,
                                    showLoading : 'none',
                                    checkedButtonRedeem: false,
                                    checkTypeEstamp: true,
                                })
                            }
                        } else {
                            if(Math.floor(CardItem.carD_POINT) < promotion.redeeM_TOTAL_POINT){ // CHECK ว่ามีคะแนน Card ไหม
                                if(promotion.quantitY_REMAIN <= 0){ // CHECK ถ้าจำนวนของรางวัลหมด == 0 ให้ปิดปุ่มแลกและขึ้นข้อความ
                                    var noteQuatity = <div> <FormattedMessage id="sorrynorewardselect" /> <br /> <FormattedMessage id="plsselectanother" /></div>
                                }
                                this.setState({ 
                                    noteQuatity: noteQuatity, 
                                    checkedButtonRedeem: true,
                                    PromotionDetail: promotion,
                                    showLoading : 'none',
                                    checkTypeEstamp: false,
                                })
                            } else if(promotion.redeeM_TOTAL_POINT == 0){ // CHECK ถ้าของรางวัลคะแนน == 0 ให้เปิดปุ่ม
                                this.setState({ 
                                    PromotionDetail: promotion,
                                    showLoading : 'none',
                                    checkedButtonRedeem: false,
                                    checkTypeEstamp: false,
                                })
                            } else {
                                this.setState({ 
                                    PromotionDetail: promotion,
                                    showLoading : 'none',
                                    checkedButtonRedeem: false,
                                    checkTypeEstamp: false,
                                })
                            }
                        }
                    } else {
                        if(promotion.redeeM_POINT_TYPE == 5) { 
                            this.setState({ 
                                PromotionDetail: promotion,
                                showLoading : 'none',
                                checkedButtonRedeem: true,
                                checkTypeEstamp: true,
                            })
                        } else {
                            this.setState({ 
                                PromotionDetail: promotion,
                                showLoading : 'none',
                                checkedButtonRedeem: true,
                                checkTypeEstamp: false,
                            })
                        }
                    }
                } else {
                    this.setState({ 
                        PromotionDetail: promotion,
                        showLoading : 'none',
                        checkedButtonRedeem: true,
                        checkTypeEstamp: false,
                    })
                }
            })
        }
    }

    checkIcon(e){
        if(e.target.id === 'false'){
            this.setState({ chkIcon : 'true' , setIcon : 'fas fa-minus'});
        }else{
            this.setState({ chkIcon : 'false' , setIcon : 'fas fa-plus' });
        }
    }

    backToReward(){
        // window.location.href = `${process.env.PUBLIC_URL}/privileges`;
        window.history.back();
    }

    RedeemReward(){
        const { PromotionDetail } = this.state
        if(PromotionDetail.urlWebView == "" || PromotionDetail.urlWebView == null || PromotionDetail.urlWebView == undefined){
            if(PromotionDetail.shippinG_TYPE == 1){
                this.props.history.push({
                    pathname:`${process.env.PUBLIC_URL}/privileges/branch`,
                    state: {
                        PromotionDetail: PromotionDetail,
                    }
                });
            } else if(PromotionDetail.shippinG_TYPE == 2){
                this.props.history.push({
                    pathname:`${process.env.PUBLIC_URL}/privileges/address`,
                    state: {
                        PromotionDetail: PromotionDetail,
                    }
                });
            } else if(PromotionDetail.shippinG_TYPE == 3){
                this.setState({ showLoading: 'block' })
                this.onQrInsertRedeem()
                // this.props.history.push({
                //     pathname:`${process.env.PUBLIC_URL}/privileges/qr`,
                //     state: {
                //         PromotionDetail: PromotionDetail,
                //     }
                // });
            }
        } else {
            const { Profile, CardItem, GetCustomerProfile, getTokenCardId } = this.state
            var maxCardId = CardItem == "" ? "" : CardItem.carD_NO
            var customerId = Profile == "" ? "" : Profile.customer_id
            var citizeN_ID = Profile == "" ? "" : Profile.citizeN_ID
            var phoneNo = Profile == "" ? "" : Profile.phoneNo
            var email = GetCustomerProfile == "" ? "" : GetCustomerProfile.USER_EMAIL
            var TokenId = getTokenCardId == "" ? "" : getTokenCardId
            var data = `{
                "customerId" : "${customerId}",
                "IdCard" : "${citizeN_ID}",
                "maxCardId" : "${maxCardId}",
                "telNo" : "${phoneNo}",
                "email" : "${email}",
                "TokendID" : "${TokenId}"
            }`
            const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019";
            const IV = "xRtyUw5Pt+58ZxqA";
            let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENC_KEY), IV);
            let encrypted = cipher.update(data);
            encrypted = Buffer.concat([encrypted, cipher.final()]);
            var setParam = encrypted.toString('base64');
            var urlWebView = PromotionDetail.urlWebView
            var link = urlWebView.replace('#param#', setParam)
            window.location.href=link
        }
    }

    onQrInsertRedeem(){
        const { PromotionDetail, Profile, CardItem } = this.state
        setTimeout(() => {
            var dataRedeem = {
                "TokenMobileApp": Profile.tkmb,
                "UserId": 0,
                "CUSTOMER_ID": Profile.customer_id,
                "CARD_MASTER_ID": CardItem.carD_MASTER_ID,
                "SHIPPING_TYPE": 4,
                "SHIPPING_SHOP_ID": "",
                "REDEEM_CODE": PromotionDetail.redeeM_CODE,
                "FullAddress": "",
                "SOURCE_DATA": 16
            }
            // console.log("dataRedeem",dataRedeem)
            promotionActions.InsertRedeemPromotion(Profile.userId, dataRedeem).then(e => {
                console.log("InsertRedeemPromotion", e)
                if(e.data.isSuccess == true){
                    this.props.history.push({
                        pathname:`${process.env.PUBLIC_URL}/privileges/qr`,
                        state: {
                            PromotionDetail: PromotionDetail,
                            PromotionInsertRedeem: e.data.data[0]
                        }
                    });
                } else if(e.data.errCode == 42){
                    this.setState({ 
                        errorTokenExpired: true,
                        errorRedeemMsg: e.data.errMsg
                    })
                } else {
                    this.setState({ 
                        errorRedeem: true,
                        errorRedeemMsg: e.data.errMsg,
                    })
                }
            })
        },500)
    }

    checkBeforeRedeemReward(){
        if(localStorage.getItem('2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu') != "true"){ // ยังไม่ได้ login 
            this.setState({ checkExpireToken: <Loginweb/> })
            
        }else if(localStorage.getItem('CardItem') == ""){  //login, แต่ไม่มี card
            window.location.href=`${process.env.PUBLIC_URL}/PTmaxcard/register/nocard`
            // }else if(){ //login, มี card, แต่ point ไม่พอ
        } else {
            //popup
            let icon = `${process.env.PUBLIC_URL}/images/info.png`
            let text = <FormattedMessage id="confirmredeem" />
            this.modalCheckRedeem(text,icon)
        }
    }

    clickInterest(){
        if(localStorage.getItem('2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu') != "true"){ // ยังไม่ได้ login 
            this.setState({ checkExpireToken: <Loginweb/> })
        }else{
            const { PromotionDetail } = this.state
            const { Profile, CardItem, GetCustomerProfile, getTokenCardId } = this.state
            var maxCardId = CardItem == "" ? "" : CardItem.carD_NO
            var customerId = Profile == "" ? "" : Profile.customer_id
            var citizeN_ID = Profile == "" ? "" : Profile.citizeN_ID
            var phoneNo = Profile == "" ? "" : Profile.phoneNo
            var email = GetCustomerProfile == "" ? "" : GetCustomerProfile.USER_EMAIL
            var TokenId = getTokenCardId == "" ? "" : getTokenCardId
            var data = `{
                "customerId" : "${customerId}",
                "IdCard" : "${citizeN_ID}",
                "maxCardId" : "${maxCardId}",
                "telNo" : "${phoneNo}",
                "email" : "${email}",
                "TokendID" : "${TokenId}"
            }`
            const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019";
            const IV = "xRtyUw5Pt+58ZxqA";
            let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENC_KEY), IV);
            let encrypted = cipher.update(data);
            encrypted = Buffer.concat([encrypted, cipher.final()]);
            var setParam = encrypted.toString('base64');
            var urlWebView = PromotionDetail.urlWebView
            var link = urlWebView.replace('#param#', setParam)
            window.location.href=link
        }
    }

    errorcheckThisPropsRedeem(){
        window.location.href = `${process.env.PUBLIC_URL}/privileges`;
    }

    onConfirmErrorRedeem(){
        this.setState({ 
            errorRedeem: false,
            showLoading: 'none',
            errorRedeemMsg: "",
        })
    }

    errorTokenExpired(){
        this.setState({ 
            checkExpireToken: <Loginweb/>,
            errorTokenExpired: false 
        })
    }

    render(){
        console.log(this.state.Profile);


        var PromotionDetail = this.state.PromotionDetail
        var starT_DATE = moment(PromotionDetail.starT_DATE).format('L');  
        var enD_DATE = moment(PromotionDetail.enD_DATE).format('L'); 
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].Detail}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="container-fluid bg-news-content">
                            <div className="container-rewarddetail">
                                <div className="row">
                                    <div className="col-12">
                                        <div className="rewarddetail-title" onClick={() => this.backToReward()}><i className="fas fa-chevron-left icon-back pr-4"></i><span className="rewarddetail-titletextt my-auto"><FormattedMessage id="reward" /></span></div>
                                    </div>
                                </div>
                            </div>
                            <div className="container-rewarddetail">
                                <div className="row py-5">
                                    <div className="col-lg-7 col-md-12">
                                        <img className="img-fluid w-100 rewarddetail-img" src={PromotionDetail.imG_URL_NORMAL}/>
                                    </div>
                                    <div className="col-lg-5 col-md-12 py-2">
                                        <div className="rewarddetail-content">
                                            <div className="rewarddetail-titlecointent">{PromotionDetail.redeeM_NAME}</div>
                                            <div className="rewarddetail-subtitlecointent">{PromotionDetail.shorT_DESC}</div>
                                            {/* <div className="mt-auto"> */}
                                            <div className="pt-5">
                                                <div className="rewarddetail-point">{this.state.checkTypeEstamp ? PromotionDetail.redeeM_STAMP : PromotionDetail.redeeM_TOTAL_POINT} คะแนน</div>
                                                <div className="t-red t-20">{this.state.noteQuatity !== '' ? this.state.noteQuatity : ''}</div>
                                                {
                                                    this.state.PromotionDetail.redeeM_CODE  == null || this.state.PromotionDetail.redeeM_CODE == ""
                                                    ? <button onClick={() => this.clickInterest()} disabled={this.state.checkedButtonRedeem} className="btn btn-block rewarddetail-btnredeem"><FormattedMessage id="clickinterest" /></button>
                                                    : <button onClick={() => this.checkBeforeRedeemReward()} disabled={this.state.checkedButtonRedeem} className="btn btn-block rewarddetail-btnredeem"><FormattedMessage id="redeempoint" /></button>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                        <div className="rewarddetail-descriptitle"><FormattedMessage id="rewarddetail" /></div>
                                        <div className="rewarddetail-descripdetail">
                                            { PromotionDetail.redeeM_DESC == "" ? <FormattedMessage id="nodata" /> : ReactHtmlParser(PromotionDetail.redeeM_DESC) }
                                        </div>
                                        <div className="rewarddetail-descriptitle mt-4"><FormattedMessage id="timeredeemreward" /></div>
                                        <div className="rewarddetail-descripdetail"><FormattedMessage id="getReward" /> {starT_DATE} <FormattedMessage id="to" /> {enD_DATE}</div>
                                        <button className="btn rewarddetail-dot dot my-4 ml-0 mr-3 " id={this.state.chkIcon} type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" onClick={(e) => this.checkIcon(e)}>
                                            <i className={`${this.state.setIcon} icon-dot`} ></i>
                                        </button>
                                        <span className="rewarddetail-condition"><FormattedMessage id="condition" /></span>
                                        <div className="collapse" id="collapseExample">
                                            <div className="card-condition">
                                                { ReactHtmlParser(PromotionDetail.redeeM_CONDITION) }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <SweetAlert   
                        show={this.state.errorcheckThisPropsRedeem}
                        confirmBtnCssClass="btn btn-block btn-error-redeem"
                        confirmBtnText="ตกลง"
                        onConfirm={() => this.errorcheckThisPropsRedeem()}
                    >
                        <span>
                            <img 
                                src={process.env.PUBLIC_URL +"/images/cancel.png"}
                                className="py-3" 
                                alt="sena"
                                width="100"
                            />
                            <div className="t-36 mt-4"><FormattedMessage id="errorMessage" /></div>
                        </span>
                    </SweetAlert>
                    <SweetAlert   
                        show={this.state.errorRedeem}
                        confirmBtnCssClass="btn btn-block btn-error-redeem"
                        confirmBtnText="ตกลง"
                        onConfirm={() => this.onConfirmErrorRedeem()}
                    >
                        <span>
                            <img 
                                src={process.env.PUBLIC_URL +"/images/cancel.png"}
                                className="py-3" 
                                alt="sena"
                                width="100"
                            />
                            <div className="t-36 mt-4">{this.state.errorRedeemMsg}</div>
                        </span>
                    </SweetAlert>
                    <SweetAlert   
                        show={this.state.errorTokenExpired}
                        confirmBtnCssClass="btn btn-block btn-error-redeem"
                        confirmBtnText="ตกลง"
                        onConfirm={() => this.errorTokenExpired()}
                    >
                        <span>
                            <img 
                                src={process.env.PUBLIC_URL +"/images/cancel.png"}
                                className="py-3" 
                                alt="sena"
                                width="100"
                            />
                            <div className="t-36 mt-4">{this.state.errorRedeemMsg}</div>
                        </span>
                    </SweetAlert>
                    {this.state.checkExpireToken}
                    {this.state.modal}
                </div>
            </IntlProvider>
        )
    }
}