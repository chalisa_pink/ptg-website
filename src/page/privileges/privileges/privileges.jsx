import React from 'react';
import { promotionActions } from '../../../_actions/promotionActions'; 
import { maxcardActions } from '../../../_actions/maxcardActions'; 
import SweetAlert from "react-bootstrap-sweetalert"; 
import LazyLoad from 'react-lazy-load';
import '../../privileges/privileges.css';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb'); 

export class Privileges extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            active:'',
            showLoading: 'block',
            showLoadingDetail: 'block',
            lang: 'th',
            PromotionList: [],
            getCateList: [],
            Profile: "",
            allactiveTab: "active",
            isRedeemHover: {},
            isRedeemHoverEStamp : {},
            pageNo: 1,
            redeeM_CATEGORY_ID: 'all',
            isHideLoadmoreList : true,
            isHideLoadmoreListEStamp : true,
            showLoadingLoadmore: 'none',
            showLoadingLoadmoreEStamp: 'none',
            PromotionEStamp: [],
            pageNoEStamp: 1,
            CardItem: {},
            checkRewardActive: true,
        };
        this.PromotionDetail = this.PromotionDetail.bind(this);
    }

    componentDidMount(){
        var url = this.props.location;
        var group = url.search.substr(5);
        // console.log("group",group)
        if(group == 2){
            this.setState({ checkRewardActive : false  })
        } else {
            this.setState({ checkRewardActive : true  })
        }
        if(localStorage.getItem('2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu') === "true"){
            var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
                var ProfileData = localStorage.getItem('Profile')
                var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
                ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
                var ProfileArray = JSON.parse(ProfileDecrypt);
                var Profile = ProfileArray[0];

            if(localStorage.getItem('CardItem') !== '' && localStorage.getItem('CardItem') !== "false"){
                var CardItemData = localStorage.getItem('CardItem');
                var CardItemDecrypt = Cipher.decrypt(keyCipher, CardItemData);
                CardItemDecrypt = CardItemDecrypt.replace(/[\u0000-\u0019]+/g,""); 
                var CardItem = JSON.parse(CardItemDecrypt);
                this.setState({ 
                    Profile : Profile,
                    CardItem : CardItem
                })
                this.CardList(Profile.userId, this.state.pageNo);
            }else{
                
                this.CardList(Profile.userId, this.state.pageNo);
            }
        } else {
            this.CardList(1, this.state.pageNo);
        }
    }

    CardList(userId, pageNo){
        setTimeout(() => {
            maxcardActions.getCardList(userId).then(e => {
                this.getCateList(pageNo);
            })
        },500)
    }

    getCateList(pageNo){
        setTimeout(() => {
            promotionActions.getCateList().then(e => {
                if(e.data.isSuccess === true){
                    this.setState({ getCateList : e.data.data })
                    this.GetPromotionList("all", pageNo);
                    this.GetPromotion_EStamp(pageNo)
                } else {
                    this.setState({ onErrorgetPromotionList : true })
                }
                this.setState({ showLoading : 'none'})
            })
        },500)
    }

    GetPromotionList(redeeM_CATEGORY_ID, pageNo){
        // เช็คว่าเรียบ tab เดิมหรือเปล่า ถ้าไม่ใช่ให้เคลียค่า PromotionList
        if(redeeM_CATEGORY_ID != this.state.redeeM_CATEGORY_ID){
            var pageNoCheck = 1
            this.setState({ pageNo : 1 })
            this.setState({ PromotionList : [] })
            this.setState({ isHideLoadmoreList : true })
            this.setState({ showLoadingDetail : 'block'})
        } else {
            var pageNoCheck = pageNo
            this.setState({ showLoadingLoadmore : 'block'})
        }
        // เช็คเป็น tab ไหนและให้ active ตาม tab
        if(redeeM_CATEGORY_ID == "all"){
            var categoryId = ""
            this.setState({ allactiveTab : 'active'})
        } else {
            var categoryId = redeeM_CATEGORY_ID
            this.setState({ allactiveTab : ''})
        }
        this.setState({ activeTab : redeeM_CATEGORY_ID})
        this.setState({ redeeM_CATEGORY_ID : redeeM_CATEGORY_ID})
        var cardType = this.state.CardItem['carD_TYPE_ID'] == null ? "" : this.state.CardItem['carD_TYPE_ID']
        var lang = this.state.lang
        setTimeout(() => {
            promotionActions.GetPromotionList(lang, cardType, categoryId, pageNoCheck).then(e => {
                console.log("GetPromotionList", e)
                if(e.data.isSuccess == true){
                    this.setState({ pageNo: Number(this.state.pageNo) + 1 })
                    var dataList = this.state.PromotionList.concat(e.data.data);
                    this.setState({ PromotionList: dataList })
                    this.setState({ showLoadingLoadmore : 'none'})
                } else {
                    var PromotionList = this.state.PromotionList
                    if(PromotionList.length != 0){
                        this.setState({ isHideLoadmoreList : false })
                    } else {
                        this.setState({ PromotionList : "" })
                        this.setState({ pageNo: 1 })
                        this.setState({ isHideLoadmoreList : false })
                    }
                }
                this.setState({ showLoadingDetail : 'none'})
            })
        },500)
    }

    GetPromotion_EStamp(pageNoEStamp){
        if(pageNoEStamp != 1){
            this.setState({ showLoadingLoadmoreEStamp : 'block'})
        } else {
            this.setState({ showLoadingDetail : 'block'})
        }
        var cardType = this.state.CardItem['carD_TYPE_ID']
        if(cardType === undefined || cardType === null){
            cardType = "!";
        }
        var lang = this.state.lang
        var categoryId = ""
        setTimeout(() => {
            promotionActions.GetPromotion_EStamp(lang, cardType, categoryId, pageNoEStamp).then(e => {
                if(e.data.isSuccess == true){
                    this.setState({ pageNoEStamp: Number(this.state.pageNoEStamp) + 1 })
                    var dataList = this.state.PromotionEStamp.concat(e.data.data);
                    this.setState({ PromotionEStamp: dataList })
                    this.setState({ showLoadingLoadmoreEStamp : 'none'})
                } else {
                    var PromotionEStamp = this.state.PromotionEStamp
                    if(PromotionEStamp.length != 0){
                        this.setState({ isHideLoadmoreListEStamp : false })
                    } else {
                        this.setState({ PromotionEStamp : "" })
                        this.setState({ pageNoEStamp: 1 })
                        this.setState({ isHideLoadmoreListEStamp : false })
                    }
                }
                this.setState({ showLoadingDetail : 'none'})
            })
        },500)
    }

    hoverCard = (ev) => {
        var index = ev.currentTarget.dataset.index
        const list = Object.assign({}, this.state.isRedeemHover);
        list['hover-' + index] = !list['hover-' + index];
        this.setState({
            isRedeemHover: list
        })
    }

    hoverCardEStamp = (ev) => {
        var index = ev.currentTarget.dataset.index
        const list = Object.assign({}, this.state.isRedeemHoverEStamp);
        list['hover-' + index] = !list['hover-' + index];
        this.setState({
            isRedeemHoverEStamp: list
        })
    }

    PromotionDetail = (ev) => {
        var id = ev.currentTarget.dataset.id
        this.props.history.push({
            pathname:`${process.env.PUBLIC_URL}/privileges/detail/${id}`,
            state: {
                promotionId: id
            }
        });
    }

    render(){
        var getCateList = this.state.getCateList;
        var getCateListCard = getCateList.map(( item,i ) => {
            return (
                <li className="nav-item text-body-sub" key={i}>
                    <a 
                        className={`nav-link ${this.state.activeTab == item.redeeM_CATEGORY_ID ? "active" : "" } text-center privilege-width-a`}  
                        onClick={() => this.GetPromotionList(item.redeeM_CATEGORY_ID, this.state.pageNo)} 
                    >
                        {item.cataloG_NAME}
                    </a>
                </li>
            )
        })
        var PromotionList = this.state.PromotionList;
        var PromotionListCard = [];
        if(PromotionList === ""){
            PromotionListCard.push(
                <div className="col-12 lengthzero mx-auto text-center py-5 t-20">
                    <FormattedMessage id="noda" />
                </div>
            );
        } else {
            for(var x in PromotionList){
                PromotionListCard.push(
                    <div className="col-lg-3 col-md-4 col-xs-6 p-0">
                        <a 
                            onClick={this.PromotionDetail}
                            key={x}
                            data-id={PromotionList[x].promotionId}
                            data-index={x}
                            onMouseOver={this.hoverCard} 
                            onMouseOut={this.hoverCard} 
                        >
                            <div className="card reward-card">
                                <div className="card-body reward-cardbody">
                                    <div className="row">
                                        <div className="col-md-12 col-sm-5 col-5 pl-0">
                                            <div className="text-center">{PromotionList[x].imG_URL_NORMAL === "" ? <img className="privilege-width-80" src={`${process.env.PUBLIC_URL}/images/img_default.png`} />: <div className="LazyLoad is-visible"><LazyLoad><img className="privilege-width-80" src={PromotionList[x].imG_URL_NORMAL} /></LazyLoad></div>}</div>
                                        </div>
                                        <div className="col-md-12 col-sm-7 col-7 p-0">
                                            <div className="">
                                                <div className="card-reward-title text-detail text-line-mobile-promotion">{PromotionList[x].redeeM_NAME}</div>
                                            </div>
                                            <div className="btn-redeem">
                                                {/* <div style={{ display: (this.state.isRedeemHover['hover-' + x] ? 'block' : 'none') }}><a className="card-reward-more" ><FormattedMessage id="redeem" /></a></div> */}
                                                <a className="card-reward-more text-detail" ><FormattedMessage id="redeem" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                )
            }
        }

        var PromotionEStamp = this.state.PromotionEStamp;
        var PromotionEStampCard = [];
        if(PromotionEStamp === ""){
            PromotionEStampCard.push(
                <div className="col-12 lengthzero mx-auto text-center py-5 t-20">
                    <FormattedMessage id="noda" />
                </div>
            );
        } else {
            for(var y in PromotionEStamp){
                PromotionEStampCard.push(
                    <div className="col-lg-3 col-md-4 col-xs-6 p-0">
                        <a 
                            onClick={this.PromotionDetail}
                            key={y}
                            data-id={PromotionEStamp[y].promotionId}
                            data-index={y}
                            onMouseOver={this.hoverCardEStamp} 
                            onMouseOut={this.hoverCardEStamp} 
                        >
                            <div className="card reward-card">
                                <div className="card-body reward-cardbody">
                                    <div className="row">
                                        <div className="col-md-12 col-sm-5 col-5 pl-0">
                                            <div className="text-center">{PromotionEStamp[y].imG_URL_NORMAL === "" ? <img className="privilege-width-80" src={`${process.env.PUBLIC_URL}/images/img_default.png`} />: <div className="LazyLoad is-visible"><LazyLoad><img className="privilege-width-80" src={PromotionEStamp[y].imG_URL_NORMAL} /></LazyLoad></div>}</div>
                                        </div>
                                        <div className="col-md-12 col-sm-7 col-7 p-0">
                                            <div className="">
                                                <div className="card-reward-title text-detail text-line-mobile-promotion">{PromotionEStamp[y].redeeM_NAME}</div>
                                            </div>
                                            <div className="btn-redeem">
                                                {/* <div style={{ display: (this.state.isRedeemHover['hover-' + x] ? 'block' : 'none') }}><a className="card-reward-more" ><FormattedMessage id="redeem" /></a></div> */}
                                                <a className="card-reward-more text-detail" ><FormattedMessage id="redeem" /></a>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </a>
                    </div>
                )
            }
        }
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].Privileges}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="container-fluid bg-news-content">
                            <div className="row">
                                <div className="col-12">
                                    <div className="text-sub-head font-recommend"><FormattedMessage id="reward" /></div>
                                </div>
                            </div>
                            <div className="">
                                <div className="">
                                    <div className="tab-peromotion text-body-sub">
                                        <ul className="nav nav-promotion-tab justify-content-center">
                                            <li className="nav-item m-2">
                                                <a className={`nav-link ${this.state.checkRewardActive ? 'active' : ''}`} data-toggle="pill" href="#reward"><FormattedMessage id="reward" /></a>
                                            </li>
                                            <li className="nav-item m-2">
                                                <a className={`nav-link ${this.state.checkRewardActive ? '' : 'active'}`} data-toggle="pill" href="#estamp"><FormattedMessage id="estamp" /></a>
                                            </li>
                                            <li className="nav-item m-2">
                                                <a className={`nav-link`} href="https://pro.pt.co.th:8082/NEW_CRM_PTF/Landing/cover.html"><FormattedMessage id="pointtranfer" /></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="news-tab tab-content">
                                        <div className={`tab-pane ${this.state.checkRewardActive ? 'active' : 'fade'}`} id="reward">
                                            <div className="py-3 row privitabCateDesktop">
                                                <div className="col-12">
                                                    <ul className="nav nav-reward-tab  scroll-custom-privilege">
                                                        <li className="nav-item text-body-sub">
                                                            <a className={`nav-link ${this.state.allactiveTab} text-center`} onClick={() => this.GetPromotionList('all', this.state.pageNo)} ><FormattedMessage id="all" /></a>
                                                        </li>
                                                        {getCateListCard}
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className="row pt-3 pb-3 privitabCateMobile">
                                                <div className="col-12">
                                                    <nav className="navbar navbar-expand-md navbar-light">
                                                        <a className="navbar-brand" href="#"></a>
                                                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                                                            <span className="navbar-toggler-icon"></span>
                                                        </button>
                                                        <div className="collapse navbar-collapse" id="collapsibleNavbar">
                                                            <ul className="navbar-nav nav-reward-tab-mobile">
                                                                <li className="nav-item">
                                                                    <a className={`nav-link ${this.state.allactiveTab} privilege-width-a`} onClick={() => this.GetPromotionList('all', this.state.pageNo)} ><FormattedMessage id="all" /></a>
                                                                </li>
                                                                {getCateListCard}
                                                            </ul>
                                                        </div>
                                                    </nav>
                                                </div>
                                            </div>
                                            <div className="loadingGifDetail mx-auto" style={{ display: this.state.showLoadingDetail}}>
                                                <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                                            </div>
                                            <div style={{ display: (this.state.showLoadingDetail === 'block' ? 'none' : 'block') }}>
                                                <div className="row row-eq-height pb-4">
                                                    {PromotionListCard}
                                                </div>
                                                <div className="row pb-4" style={{ display : (this.state.isHideLoadmoreList ? 'block' : 'none')}}>
                                                    <div className="col-12 text-center">
                                                        <div className="loadingGifLoadmore mx-auto" style={{ display: this.state.showLoadingLoadmore}}>
                                                            <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                                                        </div>
                                                        <div style={{ display: (this.state.showLoadingLoadmore === 'block' ? 'none' : 'block') }}>
                                                            <a onClick={() => this.GetPromotionList(this.state.redeeM_CATEGORY_ID, this.state.pageNo)} className="btn btn-loadmore text-detail"><FormattedMessage id="seemore" /></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className={`tab-pane ${this.state.checkRewardActive ? 'fade' : 'active'}`} id="estamp">
                                            <div style={{ display: (this.state.showLoadingDetail === 'block' ? 'none' : 'block') }}>
                                                <div className="row pb-4">
                                                    <div className="col-12 text-center my-3 privilege-remark-estamp"><FormattedMessage id="remark_estamp" /></div>
                                                    {PromotionEStampCard}
                                                </div>
                                                <div className="row pb-4" style={{ display : (this.state.isHideLoadmoreListEStamp ? 'block' : 'none')}}>
                                                    <div className="col-12 text-center">
                                                        <div className="loadingGifLoadmore mx-auto" style={{ display: this.state.showLoadingLoadmoreEStamp}}>
                                                            <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                                                        </div>
                                                        <div style={{ display: (this.state.showLoadingLoadmoreEStamp === 'block' ? 'none' : 'block') }}>
                                                            <a onClick={() => this.GetPromotion_EStamp(this.state.pageNoEStamp)} className="btn btn-loadmore text-detail"><FormattedMessage id="seemore" /></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className={`tab-pane`} id="pointtran"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </IntlProvider>
        )
    }
}

