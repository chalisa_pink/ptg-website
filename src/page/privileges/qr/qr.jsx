import React from 'react';
import { promotionActions } from '../../../_actions/promotionActions'; 
import { maxcardActions } from '../../../_actions/maxcardActions';
import moment from "moment-timezone";
import Barcode  from 'react-barcode';
import QRCode from 'qrcode.react';
import Rating from "react-rating";
import SweetAlert from "react-bootstrap-sweetalert";
import { Loginweb } from '../../loginweb';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
import Timer from 'react-compound-timer';
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');

export class PrivilegesQR extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'none',
            PromotionDetail: "",
            havedata: "block",
            nodata: "none",
            PromotionDetail: {},
            errorRedeem: false,
            errorTokenExpired: false,
            errorRedeemMsg: "",
            checkExpireToken: "",
            PromotionInsertRedeem: {},
        };
    }

    componentDidMount(){
        var PromotionDetail = this.props.location.state.PromotionDetail
        var PromotionInsertRedeem = this.props.location.state.PromotionInsertRedeem
        console.log("PromotionInsertRedeem", PromotionInsertRedeem)
        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
        var ProfileData = localStorage.getItem('Profile')
        var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
        ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        var ProfileArray = JSON.parse(ProfileDecrypt);
        var Profile = ProfileArray[0];
        var CardItemData = localStorage.getItem('CardItem')
        var CardItemDecrypt = Cipher.decrypt(keyCipher, CardItemData);
        CardItemDecrypt = CardItemDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        var CardItem = JSON.parse(CardItemDecrypt);
        this.setState({ 
            userId: Profile.userId,
            customer_id: Profile.customer_id,
            tkmb: Profile.tkmb,
            Profile: Profile,
            CardItem: CardItem,
            PromotionDetail: PromotionDetail,
            PromotionInsertRedeem: PromotionInsertRedeem,
        });
        var redeeM_CODE = PromotionDetail.redeeM_CODE
        // this.InsertRedeemPromotion(Profile.tkmb, Profile.userId,  Profile.customer_id, CardItem.carD_MASTER_ID, redeeM_CODE);
        
    }

    // InsertRedeemPromotion(tkmb, userId, customer_id, carD_MASTER_ID, redeeM_CODE){
    //     setTimeout(() => {
    //         var dataRedeem = {
    //             "TokenMobileApp": tkmb,
    //             "UserId": 0,
    //             "CUSTOMER_ID": customer_id,
    //             "CARD_MASTER_ID": carD_MASTER_ID,
    //             "SHIPPING_TYPE": 4,
    //             "SHIPPING_SHOP_ID": "",
    //             "REDEEM_CODE": redeeM_CODE,
    //             "FullAddress": ""
    //         }
    //         promotionActions.InsertRedeemPromotion(userId, dataRedeem).then(e => {
    //             if(e.data.isSuccess == true){
    //                 this.setState({ 
    //                     InsertRedeemPromotion: e.data.data,
    //                     showLoading: 'none' 
    //                 })
    //             } else if(e.data.errCode == 42){
    //                 this.setState({ 
    //                     errorTokenExpired: true,
    //                     errorRedeemMsg: e.data.errMsg
    //                 })
    //             } else {
    //                 this.setState({ 
    //                     errorRedeem: true,
    //                     errorRedeemMsg: e.data.errMsg,
    //                 })
    //             }
    //         })
    //     },500)
    // }

    onConfirmErrorRedeem(){
        window.location.href = `${process.env.PUBLIC_URL}/privileges`;
    }

    errorTokenExpired(){
        this.setState({ 
            checkExpireToken: <Loginweb/>,
            errorTokenExpired: false 
        })
    }

    render(){
        const { PromotionDetail, PromotionInsertRedeem } = this.state;
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].PrivilegesQR}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="bg-ptmaxcard-content">
                            <div className="ptmaxcard-content">
                                <div className="row py-5">
                                    <div className="col-lg-7 col-md-6 mx-auto">
                                        <div className="reward-redeem-card-body">
                                            <div style={{ display: this.state.havedata }}>
                                                <div className="row">
                                                    <div className="col-12 px-0">
                                                        <img className="img-fluid w-100 img-reward-redeem" src={PromotionDetail.imG_URL_DEFAULT} />
                                                    </div>
                                                    <div className="col-12 p-5 mx-auto">
                                                        <div className="historydetail-content">
                                                            <div className="historydetail-title">{PromotionDetail.redeeM_NAME}</div>
                                                            <Rating
                                                                {...this.props} 
                                                                initialRating={0}
                                                                placeholderRating={PromotionDetail.ratingStar}
                                                                emptySymbol={<img src={process.env.PUBLIC_URL +"/images/stargray.png"} className="icon" width="15"/>}
                                                                placeholderSymbol={<img src={process.env.PUBLIC_URL +"/images/stargreen.png"} className="icon" width="15"/>}
                                                                readonly
                                                            /> 
                                                            <span className="px-2 t-24">({PromotionDetail.ratingStar})</span>
                                                            {
                                                                PromotionInsertRedeem.redeeM_PARTNER_CODE == null 
                                                                ?
                                                                <div></div>
                                                                :
                                                                <div>
                                                                    <div className="historydetail-titlebarcode my-3">{PromotionInsertRedeem.redeeM_PARTNER_CODE}</div>
                                                                    <div className="news-tab tab-content text-center my-4">
                                                                        <div className="tab-pane active" id="qrcode">
                                                                            <QRCode value={`${PromotionInsertRedeem.redeeM_PARTNER_CODE}`} size={200} flat={true} displayValue={false}/>
                                                                        </div>
                                                                        <div className="tab-pane" id="barcode">
                                                                            <Barcode value={`${PromotionInsertRedeem.redeeM_PARTNER_CODE}`} width={2} height={100} flat={true} displayValue={false}/>
                                                                        </div>
                                                                    </div>
                                                                    <div className="class-count-time my-3">
                                                                        <Timer>
                                                                            <Timer.Hours/> : <Timer.Minutes /> : <Timer.Seconds/>
                                                                        </Timer>
                                                                    </div>
                                                                </div>
                                                            }
                                                            <div className="historydetail-howto my-2">
                                                                <div><FormattedMessage id="tutorial" /></div>
                                                                <div><FormattedMessage id="tutorialdetail" /></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {
                                                    PromotionInsertRedeem.redeeM_PARTNER_CODE == null 
                                                    ?
                                                    <div></div>
                                                    :
                                                    <ul className="nav nav-reward-redeem-tab nav-justified justify-content-center mt-auto">
                                                        <li className="nav-item">
                                                            <a className="nav-link redeem-tab-left active" data-toggle="pill" href="#qrcode">QR code</a>
                                                        </li>
                                                        <li className="nav-item ">
                                                            <a className="nav-link redeem-tab-right navestamponly" data-toggle="pill" href="#barcode">Bar code</a>
                                                        </li>
                                                    </ul>
                                                }
                                            </div>
                                            <div className="row" style={{ display: this.state.nodata }}>
                                                <div className="col-12 lengthzero mx-auto text-center py-5 t-20">
                                                    <FormattedMessage id="nodata" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <SweetAlert   
                        show={this.state.errorRedeem}
                        confirmBtnCssClass="btn btn-block btn-error-redeem"
                        confirmBtnText={<FormattedMessage id="btnok" />}
                        onConfirm={() => this.onConfirmErrorRedeem()}
                    >
                        <span>
                            <img 
                                src={process.env.PUBLIC_URL +"/images/cancel.png"}
                                className="py-3" 
                                alt="sena"
                                width="100"
                            />
                            <div className="t-36 mt-4">{this.state.errorRedeemMsg}</div>
                        </span>
                    </SweetAlert>
                    <SweetAlert   
                        show={this.state.errorTokenExpired}
                        confirmBtnCssClass="btn btn-block btn-error-redeem"
                        confirmBtnText={<FormattedMessage id="btnok" />}
                        onConfirm={() => this.errorTokenExpired()}
                    >
                        <span>
                            <img 
                                src={process.env.PUBLIC_URL +"/images/cancel.png"}
                                className="py-3" 
                                alt="sena"
                                width="100"
                            />
                            <div className="t-36 mt-4">{this.state.errorRedeemMsg}</div>
                        </span>
                    </SweetAlert>
                    {this.state.checkExpireToken}
                </div>
            </IntlProvider>
        )
    }
}

