import React from 'react';
import '../../ptmax/ptmax.css';
import { registerAction } from '../../../_actions/registerActions'; 
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');

export class PTsidebar extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            Profile: "",
            SidebarActive: "",
        };
    }

    componentDidMount(){
        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
        var ProfileData = localStorage.getItem('Profile')
        var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
        ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        var ProfileArray = JSON.parse(ProfileDecrypt);
        var Profile = ProfileArray[0];
        this.setState({ Profile: Profile })
        let location = window.location.pathname;
        this.setSidebarActive(location)
    }

    setSidebarActive(location){
        if(location == "/PTmaxcard"){
            this.setState({ SidebarActive: "PTmaxcard" })
        } else if(location == "/PTmaxcard/editprofile"){
            this.setState({ SidebarActive: "Editprofile" })
        } else if(location == "/PTmaxcard/editprofilemore"){
            this.setState({ SidebarActive: "EditprofileMore" })
        } else if(location == "/PTmaxcard/history"){
            this.setState({ SidebarActive: "History" })
        } else if(location == "/PTmaxcard/report"){
            this.setState({ SidebarActive: "Report" })
        } else if(location == "/PTmaxcard/condition"){
            this.setState({ SidebarActive: "Condition" })
        } else if(location == "/PTmaxcard/question"){
            this.setState({ SidebarActive: "Question" })
        } else if(location == "/PTmaxcard/historycard" || location == "/PTmaxcard/historycard/detail"){
            this.setState({ SidebarActive: "HistoryCard" })
        }
    }

    SidebarActive(active){
        if(active == 1){
            window.location.href=`${process.env.PUBLIC_URL}/PTmaxcard`
        } else if(active == 2){
            window.location.href=`${process.env.PUBLIC_URL}/PTmaxcard/editprofile`
        } else if(active == 3){
            window.location.href=`${process.env.PUBLIC_URL}/PTmaxcard/editprofilemore`
        } else if(active == 4){
            window.location.href=`${process.env.PUBLIC_URL}/PTmaxcard/history`
        } else if(active == 5){
            window.location.href=`${process.env.PUBLIC_URL}/PTmaxcard/historycard`
        } else if(active == 6){
            window.location.href=`${process.env.PUBLIC_URL}/PTmaxcard/report`
        } else if(active == 7){
            window.location.href=`${process.env.PUBLIC_URL}/PTmaxcard/condition`
        } else if(active == 8){
            window.location.href=`${process.env.PUBLIC_URL}/PTmaxcard/question`
        } 
    }

    setupLogout(){
        localStorage.clear();
        window.location.href=`${process.env.PUBLIC_URL}/home`
        // var that = this;
        // auth().signOut().then(function() {
        //   that.setState({user: null});
        // }).catch(function(error) {
        //     console.log(error)
        // });
    }

    render(){
        var Profile = this.state.Profile
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].PTsidebar}
            >
            <div className="card-menuptmaxcard">
                <div className="cardbody-menuptmaxcard">
                    <div className="row pt-sidebar-top">
                        <div className="col-3 my-auto">
                            <img className="img-fluid" width="60" src={`${process.env.PUBLIC_URL}/images/iconlogin-01.png`} />
                        </div>
                        <div className="col-9">
                            <div className="pt-sidebar-title">{Profile.firstName}</div>
                        </div>
                    </div>
                    <div className="row pt-sidebar-bottom">
                        <div className="col-12 pt-sidebar-menu">
                            <a onClick={() => this.SidebarActive(1)} >
                                <div className={`pt-sidebar-a row ${this.state.SidebarActive == "PTmaxcard" ? "active" : "" }`}>
                                    <div className="col-3"></div>
                                    <div className="col-9 pt-sidebar-text"><FormattedMessage id="ptmax" /></div>
                                </div>
                            </a>
                            <a onClick={() => this.SidebarActive(2)}>
                                <div className={`pt-sidebar-a row ${this.state.SidebarActive == "Editprofile" ? "active" : "" }`}>
                                    <div className="col-3"></div>
                                    <div className="col-9 pt-sidebar-text"><FormattedMessage id="editprofile" /></div>
                                </div>
                            </a>
                            <a onClick={() => this.SidebarActive(3)}>
                                <div className={`pt-sidebar-a row ${this.state.SidebarActive == "EditprofileMore" ? "active" : "" }`}>
                                    <div className="col-3"></div>
                                    <div className="col-9 pt-sidebar-text"><FormattedMessage id="editmore" /></div>
                                </div>
                            </a>
                            <a onClick={() => this.SidebarActive(4)}>
                                <div className={`pt-sidebar-a row ${this.state.SidebarActive == "History" ? "active" : "" }`}>
                                    <div className="col-3"></div>
                                    <div className="col-9 pt-sidebar-text"><FormattedMessage id="history" /></div>
                                </div>
                            </a>
                            <a onClick={() => this.SidebarActive(5)}>
                                <div className={`pt-sidebar-a row ${this.state.SidebarActive == "HistoryCard" ? "active" : "" }`}>
                                    <div className="col-3"></div>
                                    <div className="col-9 pt-sidebar-text"><FormattedMessage id="historycard" /></div>
                                </div>
                            </a>
                            <a onClick={() => this.SidebarActive(6)}>
                                <div className={`pt-sidebar-a row ${this.state.SidebarActive == "Report" ? "active" : "" }`}>
                                    <div className="col-3"></div>
                                    <div className="col-9 pt-sidebar-text"><FormattedMessage id="report" /></div>
                                </div>
                            </a>
                            <a onClick={() => this.SidebarActive(7)}>
                                <div className={`pt-sidebar-a row ${this.state.SidebarActive == "Condition" ? "active" : "" }`}>
                                    <div className="col-3"></div>
                                    <div className="col-9 pt-sidebar-text"><FormattedMessage id="condtion" /></div>
                                </div>
                            </a>
                            <a onClick={() => this.SidebarActive(8)}>
                                <div className={`pt-sidebar-a row ${this.state.SidebarActive == "Question" ? "active" : "" }`}>
                                    <div className="col-3"></div>
                                    <div className="col-9 pt-sidebar-text"><FormattedMessage id="qanda" /></div>
                                </div>
                            </a>
                            <a onClick={() => this.setupLogout()} >
                                <div className="pt-sidebar-a row">
                                    <div className="col-3"></div>
                                    <div className="col-9 pt-sidebar-signout"><FormattedMessage id="signout" /></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            </IntlProvider>
        )
    }
}


export default PTsidebar;
