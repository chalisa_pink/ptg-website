import React from 'react';
import '../../ptmax/ptmax.css';
import { maxcardActions } from '../../../_actions/maxcardActions'; 
import SweetAlert from "react-bootstrap-sweetalert"; 
import { PTsidebar } from '../PTsidebar';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');

export class myMaxCard extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            dataCard : [],
            showLoading: 'block',
            content_page: 'none',
            Profile: "",
            GetCardAndPointInfo: [],
        };
    }
    componentDidMount(){
        if(localStorage.getItem('CardItem') === "" || localStorage.getItem('CardItem') === null){
            window.location.href = `${process.env.PUBLIC_URL}/PTmaxcard/register/nocard`;
        }else if(localStorage.getItem('CardItem') === 'false'){
            this.modalAlert();
        }else{
            var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
            var ProfileData = localStorage.getItem('Profile');
            var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
            ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
            var ProfileArray = JSON.parse(ProfileDecrypt);
            var Profile = ProfileArray[0];
            var CardItemData = localStorage.getItem('CardItem');
            var CardItemDecrypt = Cipher.decrypt(keyCipher, CardItemData);
            CardItemDecrypt = CardItemDecrypt.replace(/[\u0000-\u0019]+/g,""); 
            var CardItem = JSON.parse(CardItemDecrypt);
            var carD_TYPE_ID = CardItem.carD_TYPE_ID;
            this.setState({ 
                Profile : Profile, 
                carD_TYPE_ID: carD_TYPE_ID
            })
            this.getCard(Profile.userId);
        }
    }

    getCard(userId){
        var showLoading = '';
        var  content_page = '';
        maxcardActions.getCardList(userId).then(e => {
            console.log("getCardList", e)
            if(e.data.isSuccess === true){
                showLoading = 'none';
                content_page = 'block';
                var res = e.data.data
                this.setState({ dataCard : e.data.data })
                this.onGetCardAndPointInfo(userId, res)
            }else{
                showLoading = 'none';
                content_page = 'block';
                this.modalAlert(userId);
            }
            var th = this
            setTimeout(function(){
                th.setState({ showLoading : showLoading})
                th.setState({ content_page: content_page })
            },2000)
        })
    }

    onGetCardAndPointInfo(userId, dataCard){
        var showLoading = '';
        var  content_page = '';
        maxcardActions.GetCardAndPointInfo(userId).then(e => {
            if(e.data.isSuccess === true){
                showLoading = 'none';
                content_page = 'block';
                var resp = e.data.data.carD_MEMBER_INFO
                this.setState({ GetCardAndPointInfo : resp })
            }else{
                showLoading = 'none';
                content_page = 'block';
                this.modalAlert();
            }
            var th = this
            setTimeout(function(){
                th.setState({ showLoading : showLoading})
                th.setState({ content_page: content_page })
            },2000)
        })
    }

    modalAlert(){
        alert = (
            <SweetAlert
                custom
                showCloseButton
                closeOnClickOutside={false}
                focusConfirmBtn={false}
                title=""
                showConfirm={false}
                showCancelButton
                onCancel={() => this.handleChoice(false)}
                onConfirm={() => this.handleChoice(true)}
            >
                <div className="iconClose" onClick={() => this.handleChoice(false)}><i className="fas fa-times sizeCloseBtn"></i>
                    <img style={{width:"25%"}} src={`${process.env.PUBLIC_URL}/images/cancel.png`} />
                    <div className="my-3 alert-dismiss">
                        <FormattedMessage id="errorSignIn" />
                    </div>
                </div>
            </SweetAlert>
        )
        this.setState({ show : true , modal : alert });
    }

    handleChoice(choice) {
        this.setState({ show:true , modal:alert });
        window.location.href = `${process.env.PUBLIC_URL}/home`
    }

    // handleTohistory = ev => {
    //     var cardmemberid = ev.currentTarget.dataset.memberid
    //     this.props.history.push({
    //         pathname:`${process.env.PUBLIC_URL}/PTmaxcard/history`,
    //         state: {
    //             carD_MEMBER_ID: cardmemberid
    //         }
    //     });
    // }

    ChangecarDTYPE = ev => {
        var indexcard = ev.currentTarget.dataset.indexcard
        var dataCard = this.state.dataCard
        var CardItem = dataCard[indexcard]
        var CardItemstringify = JSON.stringify(CardItem)
        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
        var CardItemEncrypt = Cipher.encrypt(keyCipher, CardItemstringify);
        localStorage.setItem("CardItem", CardItemEncrypt);
        var resp = dataCard[indexcard].carD_TYPE_ID
        this.setState({ carD_TYPE_ID: resp })
    }

    onGetCardAndPointInfoDetail(carD_POINTS){
        var GetCardAndPointInfoCard = []
        if(carD_POINTS){
            carD_POINTS.map((key,index) => {
                if(carD_POINTS[index].poinT_TYPE_ID !== "c1e70d5e-bcae-ea11-a812-000d3a851c27" && carD_POINTS[index].poinT_TYPE_ID !== "fed5cde5-3395-ea11-a812-000d3a859015" && carD_POINTS[index].poinT_TYPE_ID !== "ef79d02c-bcae-ea11-a812-000d3a851c27"){
                    GetCardAndPointInfoCard.push(
                        <div className="row mt-2 px-0">
                            <div className="col-lg-5 col-7 pl-0 ptmaxpoint-pointtext">
                                {carD_POINTS[index].poinT_TYPE_NAME}
                            </div>
                            <div className="col-lg-2 col-5 pr-0 ptmaxpoint-pointpoint">
                                {carD_POINTS[index].point}
                            </div>
                        </div>
                    );
                }
            });
        }
        return GetCardAndPointInfoCard;
    }

    render(){
        const { GetCardAndPointInfo } = this.state
        var Profile = this.state.Profile
        var cardList = this.state.dataCard;
        // console.log(cardList)
        var disPlayCard = [];
        for(var x in cardList){
            if(this.state.carD_TYPE_ID == cardList[x].carD_TYPE_ID){
                var cardnoselect = ""
            } else {
                var cardnoselect = "card-noselect"
            }

            if(this.state.GetCardAndPointInfo[x]){
                var carD_POINTS = this.state.GetCardAndPointInfo[x].carD_POINTS
                var getNormalPoint = (carD_POINTS ? carD_POINTS.find(o => o.poinT_TYPE_ID === "c1e70d5e-bcae-ea11-a812-000d3a851c27") : "")
                var getEStamp = (carD_POINTS ? carD_POINTS.find(o => o.poinT_TYPE_ID === "fed5cde5-3395-ea11-a812-000d3a859015") : "")
                var getSpecialPoint = (carD_POINTS ? carD_POINTS.find(o => o.poinT_TYPE_ID === "ef79d02c-bcae-ea11-a812-000d3a851c27") : "")
                // var getEmployeeCoin = carD_POINTS.find(o => o.poinT_TYPE_ID === "cc3194cd-f391-eb11-b1ac-00224816e93d");

                let getNormalPoint_point = (getNormalPoint ? getNormalPoint.point : 0)
                let getSpecialPoint_point = (getSpecialPoint ? getSpecialPoint.point : 0)

                var setPoint = parseInt(getNormalPoint_point) + parseInt(getSpecialPoint_point)
                var setEStamp = getEStamp ? parseInt(getEStamp.point) : 0
                // if(getEmployeeCoin !== undefined){
                //     var setEmployeeCoin = parseInt(getEmployeeCoin.point)
                // }
            }

            disPlayCard.push(
                <a 
                    onClick={this.ChangecarDTYPE}
                    key={x}
                    data-indexcard={x}
                >
                    <div className={`pb-5 row ${cardnoselect}`}>
                        <div className="ptmaxpoint-pointcard col-xs-5 col-sm-5 col-md-5 col-lg-5 text-center pr-5 pb-3">
                            <img className="img-card ptmaxpoint-pointcardimage" src={cardList[x].carD_IMAGE} />
                        </div>
                        <div className="ptmaxpoint-pointcardtext col-xs-7 col-sm-7 col-md-7 col-lg-7 bd-card-control mt-3">
                            <div className="name-card">{(GetCardAndPointInfo[x] ? GetCardAndPointInfo[x].carD_TYPE_NAME  : "")}</div>
                            <div className="ptno-card mt-1">{GetCardAndPointInfo[x] ? GetCardAndPointInfo[x].carD_NO: ""}</div>

                            <div className="row mt-4 px-0">
                                <div className="col-lg-5 col-7 pl-0 ptmaxpoint-pointtext">
                                    Point
                                </div>
                                <div className="col-lg-2 col-5 pr-0 ptmaxpoint-pointpoint">
                                    {setPoint}
                                </div>
                            </div>
                            <div className="row mt-2 px-0">
                                <div className="col-lg-5 col-7 pl-0 ptmaxpoint-pointtext">
                                    {(getEStamp ? getEStamp.poinT_TYPE_NAME : '')}
                                </div>
                                <div className="col-lg-2 col-5 pr-0 ptmaxpoint-pointpoint">
                                    {setEStamp}
                                </div>
                            </div>
                            {(GetCardAndPointInfo[x] ? this.onGetCardAndPointInfoDetail(GetCardAndPointInfo[x].carD_POINTS) : "")}

                            
                            <div className="detail-point text-sub-detail mt-4">{cardList[x] ? parseInt(cardList[x].poinT_WILL_EXPIRED) : ""} <FormattedMessage id="point" /> | {cardList[x] ? parseInt(cardList[x].estamP_WILL_EXPIRED) : ""} <FormattedMessage id="pointestamp" /> {cardList[x] ? cardList[x].poinT_WILL_EXPIRED_DATE : ""}</div> 
                                                       
                            {/* <div className="detail-point">{parseInt(cardList[i].poinT_WILL_EXPIRED)} <FormattedMessage id="point" /> | {parseInt(cardList[i].estamP_WILL_EXPIRED)} <FormattedMessage id="pointestamp" /> 28-12-19</div> */}
                            {/* <button className="insert-reward"><FormattedMessage id="rewardcanuse" /></button> */}
                        </div>
                    </div>
                </a>
            )
        // }
        }
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].myMaxCard}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading }}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="bg-ptmaxcard-content">
                            <div className="ptmaxcard-content">
                                <div className="row">
                                    <div className="col-xl-3 col-lg-4 col-md-5 col-sm-6">
                                        <PTsidebar/>
                                    </div>
                                    <div className="col-xl-9 col-lg-8 col-md-7 col-sm-6">
                                        <div className="editprofile-title py-3"><FormattedMessage id="ptmaxcard" /></div>
                                        <div className="">{disPlayCard}</div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </IntlProvider>
        )
    }
}


