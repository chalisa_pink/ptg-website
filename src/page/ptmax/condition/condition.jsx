import React from 'react';
import '../../ptmax/ptmax.css';
import { PTsidebar } from '../PTsidebar';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

var Cipher = require('aes-ecb');

export class condition extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            Profile: "",
            showLoading: "block"
        };
    }
    componentDidMount(){
        var ProfileData = localStorage.getItem('Profile')
        this.handleChangeTabs(1)
        if(ProfileData){
            setTimeout(() => {
                var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
                var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
                ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
                var ProfileArray = JSON.parse(ProfileDecrypt);
                var Profile = ProfileArray[0];
                this.setState({ Profile , ProfileData})
            },500)
        }


    }

    handleChangeTabs(tab){
        this.setState({ showLoading : 'none'})
        if(tab == 1){
            this.setState({ 
                tab1: "active",
                tab2: "",
                tab3: "",
                tab4: "",
                content1: "block",
                content2: "none",
                content3: "none",
                content4: "none",
            });
        } else if(tab == 2){
            this.setState({ 
                tab1: "",
                tab2: "active",
                tab3: "",
                tab4: "",
                content1: "none",
                content2: "block",
                content3: "none",
                content4: "none",
            });
        } else if(tab == 3){
            this.setState({ 
                tab1: "",
                tab2: "",
                tab3: "active",
                tab4: "",
                content1: "none",
                content2: "none",
                content3: "block",
                content4: "none",
            });
        } else if(tab == 4){
            this.setState({ 
                tab1: "",
                tab2: "",
                tab3: "",
                tab4: "active",
                content1: "none",
                content2: "none",
                content3: "none",
                content4: "block",
            });
        }
    }

    render(){
        var Profile = this.state.Profile
        var ProfileData = this.state.ProfileData
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].condition}
            >
            <div>
                <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                    <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                </div>
                <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                    <div className="bg-ptmaxcard-content">
                        <div className="ptmaxcard-content">
                            <div className="row">
                                <div className= {(ProfileData ? "col-lg-3 col-md-6" : "")}>
                                    {(ProfileData ? <PTsidebar/> : '')}
                                </div>
                                <div className={(ProfileData ? "col-lg-9 col-md-6" : "col-12")}>
                                    <div className="text-sub-head text-white py-3"><FormattedMessage id="conditon" /></div>
                                    <div className="ptmaxcard-regis-card-header">
                                        <ul className="nav nav-pills nav-justified ptmaxcard-condi-navall" role="tablist">
                                            <li className="nav-item ptmaxcard-condi-nav-1">
                                                <a onClick={() => this.handleChangeTabs(1)} className={`nav-link ptmaxcard-condi-nav ${this.state.tab1}`}><FormattedMessage id="conditonptmax" /> PT MAX</a>
                                            </li>
                                            <li className="nav-item ptmaxcard-condi-nav-2">
                                                <a onClick={() => this.handleChangeTabs(2)} className={`nav-link ptmaxcard-condi-nav ${this.state.tab2}`}><FormattedMessage id="reward" /> PT MAX</a>
                                            </li>
                                            <li className="nav-item ptmaxcard-condi-nav-3">
                                                <a onClick={() => this.handleChangeTabs(3)} className={`nav-link ptmaxcard-condi-nav ${this.state.tab3}`}><FormattedMessage id="redeem" /></a>
                                            </li>
                                            <li className="nav-item ptmaxcard-condi-nav-4">
                                                <a onClick={() => this.handleChangeTabs(4)} className={`nav-link ptmaxcard-condi-nav ${this.state.tab4}`}><FormattedMessage id="datepoint" /></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="ptmaxcard-condi-card-body">
                                        <div style={{ display: this.state.content1 }} >
                                            <ul className="ptmaxcard-condi-content">
                                                <li><FormattedMessage id="con1" /></li>
                                                <li><FormattedMessage id="con2" /></li>
                                                <li><FormattedMessage id="con3" /></li>
                                                <li><FormattedMessage id="con4" /></li>
                                                <li><FormattedMessage id="con5" /></li>
                                                <li><FormattedMessage id="con6" /></li>
                                                <li><FormattedMessage id="con7" /></li>
                                                <li><FormattedMessage id="con8" /></li>
                                                <li><FormattedMessage id="con9" /></li>
                                                <li><FormattedMessage id="con10" /></li>
                                                <li><FormattedMessage id="con11" /></li>
                                                <li><FormattedMessage id="con12" /></li>
                                                <li><FormattedMessage id="con13" /></li>
                                                <li><FormattedMessage id="con14" /></li>
                                                <li><FormattedMessage id="con15" /></li>
                                                <li><FormattedMessage id="con16" /></li>
                                                {/* <li><FormattedMessage id="con17" /></li>
                                                <li><FormattedMessage id="con18" /></li>
                                                <li><FormattedMessage id="con19" /></li>
                                                <li><FormattedMessage id="con20" /></li>
                                                <li><FormattedMessage id="con21" /></li>
                                                <li><FormattedMessage id="con22" /></li>
                                                <li><FormattedMessage id="con23" /></li>
                                                <li><FormattedMessage id="con24" /></li> */}
                                            </ul>
                                        </div>
                                        <div className="ptmaxcard-condi-content" style={{ display: this.state.content2 }} >
                                            <ul className="">
                                                <div className=""><FormattedMessage id="con2-1" /></div>
                                                <li><FormattedMessage id="con2-1-1" /></li>
                                                <img src={`${process.env.PUBLIC_URL}/images/img-condition2.jpg`} />
                                                <div className=""><FormattedMessage id="con2-2" /></div>
                                                <li><FormattedMessage id="con2-2-1" /></li>
                                                <div className=""><FormattedMessage id="con2-3" /></div>
                                                <li><FormattedMessage id="con2-3-1" /></li>
                                            </ul>
                                        </div>
                                        <div style={{ display: this.state.content3 }} >
                                            <ul className="ptmaxcard-condi-content">
                                                <li><FormattedMessage id="con3-1" /></li>
                                                <li><FormattedMessage id="con3-2" /></li>
                                                <li><FormattedMessage id="con3-3" /></li>
                                                <li><FormattedMessage id="con3-4" /></li>
                                                <li><FormattedMessage id="con3-5" /></li>
                                            </ul>
                                        </div>
                                        <div style={{ display: this.state.content4 }} >
                                            <ul className="ptmaxcard-condi-content">
                                                <li><FormattedMessage id="con34" /></li>
                                                <li><span className="t-green t-bold"><FormattedMessage id="remark" /></span> <FormattedMessage id="remark1" /></li>
                                            </ul>
                                        </div>
                                    </div>    
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </IntlProvider>
        )
    }
}


