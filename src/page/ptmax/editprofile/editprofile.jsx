import React from 'react';
import '../../ptmax/ptmax.css';
import { registerAction } from '../../../_actions/registerActions'; 
import { maxcardActions } from '../../../_actions/maxcardActions';
import { PTsidebar } from '../PTsidebar';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
import moment from "moment";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');
var ERROR_USER_EMAIL = false;
export class editprofile extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: "block",
            getProvince: {},
            getAmphure: {},
            getTumbon: {},
            getPostcode: {},
            getMaritalStatus: {},
            fields:{
                CARD_ID: "",
                FNAME_TH: "",
                LNAME_TH: "",
                GENDER_ID: "",
                BIRTH_DATE: "",
                MARITAL_STATUS: "",
                USER_EMAIL: "",
                PHONE_NO: "",
                ADDR_ADDRESS: "",
                ADDR_MOO: "",
                ADDR_VILLEGE: "",
                ADDR_SOI: "",
                ADDR_STREET: "",
                ADDR_ORG_PROVINCE_ID: "",
                ADDR_ORG_AMPHURE_ID: "",
                ADDR_ORG_SUB_DISTRICT_ID: "",
                ADDR_ORG_POSTALCODE_ID: "",
                ADDR_OTHER: "",
            },
            TITLE_NAME_REMARK: "none",
            errors: {},
            errorsFocus: {
                CARD_ID: "",
                FNAME_TH: "",
                LNAME_TH: "",
                GENDER_ID: "",
                BIRTH_DATE: "",
                MARITAL_STATUS: "",
                USER_EMAIL: "",
                PHONE_NO: "",
                ADDR_ADDRESS: "",
                ADDR_MOO: "",
                ADDR_VILLEGE: "",
                ADDR_SOI: "",
                ADDR_STREET: "",
                ADDR_ORG_PROVINCE_ID: "",
                ADDR_ORG_AMPHURE_ID: "",
                ADDR_ORG_SUB_DISTRICT_ID: "",
                ADDR_ORG_POSTALCODE_ID: "",
                ADDR_OTHER: "",
            },
            Profile: "",
            SidebarActive: "Editprofile"
        };
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount(){
        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
        var ProfileData = localStorage.getItem('Profile');
        var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
        ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        var ProfileArray = JSON.parse(ProfileDecrypt);
        var Profile = ProfileArray[0];
        if(localStorage.getItem('CardItem')){
            // console.log("Profile : ", Profile)
            if(Profile.customer_id === null){
                // console.log("Profile.customer_id === null")
                this.getCustomerId(Profile.userId);
            }else{
                // console.log("Profile.customer_id != null")
                this.GetCustomerProfile(Profile.tkvsm, Profile.customer_id, Profile.userId);
            }
            this.getDropdownlist_type("TITLE").then(result => {
                this.setState({ title_name: result.dropdowN_INFO });
            });
            this.getDropdownlist_type("GENDER").then(result => {
                this.setState({ gender: result.dropdowN_INFO });
            });
            this.getDropdownlist_type("MARITAL_STATUS").then(result => {
                this.setState({ marital_status: result.dropdowN_INFO });
            });
            this.setState({ Profile , Profile });
        }else{
            window.location.href = `${process.env.PUBLIC_URL}/PTmaxcard/register/nocard`;
        }
    }

    getCustomerId(userid){
        maxcardActions.getCustomerId(userid).then(e => {
            // console.log("getCustomerId : ", e)
            if(e.data.isSuccess === true){
                this.GetCustomerProfile(e.data.data.tokenId,e.data.data.customerId, userid);
            }
        })
    }

    getDropdownlist_type(title) {
        return registerAction.getDropdownlist(title);
    }

    GetCustomerProfile(tkvsm, customer_id, userId){
        var formData = new FormData();
        formData.append('TOKEN_ID', tkvsm)
        formData.append('CUSTOMER_ID' , customer_id)
        formData.append('userId' , userId)
        formData.append('udId' , "")
        formData.append('deviceOs' , "WEBSITE")
        setTimeout(() => {
            registerAction.GetCustomerProfile(formData).then(e => {
                // console.log("GetCustomerProfile : ", e)
                if(e.data.isSuccess == true){
                    var resp = JSON.parse(e.data.data.data)
                    var info = resp.CUSTOMER_PROFILE_INFO[0]
                    // console.log("GetCustomerProfile : ",info)
                    if(info.CARD_TYPE_ID != 2){
                        this.setState({ 
                            fields: {
                                ...this.state.fields,
                                // CARD_ID: info.CARD_ID,
                                // FNAME_TH: info.FNAME_TH,
                                // LNAME_TH: info.LNAME_TH,
                                // TITLE_NAME_ID: info.TITLE_NAME_ID,
                                // GENDER_ID: info.GENDER_ID,
                                // BIRTH_DATE: info.BIRTH_DATE,
                                // MARITAL_STATUS: info.MARITAL_STATUS,
                                // USER_EMAIL: info.USER_EMAIL,
                                // PHONE_NO: info.PHONE_NO,
                                // ADDR_ADDRESS: info.ADDR_ADDRESS,
                                // ADDR_MOO: info.ADDR_MOO,
                                // ADDR_VILLEGE: info.ADDR_VILLEGE,
                                // ADDR_SOI: info.ADDR_SOI,
                                // ADDR_STREET: info.ADDR_STREET,
                                // ADDR_ORG_PROVINCE_ID: info.ADDR_ORG_PROVINCE_ID,
                                // ADDR_ORG_AMPHURE_ID: info.ADDR_ORG_AMPHURE_ID,
                                // ADDR_ORG_SUB_DISTRICT_ID: info.ADDR_ORG_SUB_DISTRICT_ID,
                                // ADDR_ORG_POSTALCODE_ID: info.ADDR_ORG_POSTALCODE_ID,
                                // ADDR_OTHER: info.ADDR_OTHER,
                                ABOUT_SOURCE: info.ABOUT_SOURCE,
                                ACT_EN_CONCERT: info.ACT_EN_CONCERT,
                                ACT_EN_COUNTRY_CONCERT: info.ACT_EN_COUNTRY_CONCERT,
                                ACT_EN_MOVIES: info.ACT_EN_MOVIES,
                                ACT_EN_MUSIC: info.ACT_EN_MUSIC,
                                ACT_EN_OTHERS: info.ACT_EN_OTHERS,
                                ACT_EN_OTHERS_REMARK: info.ACT_EN_OTHERS_REMARK,
                                ACT_EN_THEATER: info.ACT_EN_THEATER,
                                ACT_HNGOUT_COUPLE: info.ACT_HNGOUT_COUPLE,
                                ACT_HNGOUT_FAMILY: info.ACT_HNGOUT_FAMILY,
                                ACT_HNGOUT_FRIEND: info.ACT_HNGOUT_FRIEND,
                                ACT_HNGOUT_NO: info.ACT_HNGOUT_NO,
                                ACT_ID_BAKING: info.ACT_ID_BAKING,
                                ACT_ID_COOKING: info.ACT_ID_COOKING,
                                ACT_ID_DECORATE: info.ACT_ID_DECORATE,
                                ACT_ID_GAMING: info.ACT_ID_GAMING,
                                ACT_ID_GARDENING: info.ACT_ID_GARDENING,
                                ACT_ID_INTERNET: info.ACT_ID_INTERNET,
                                ACT_ID_OTHERS: info.ACT_ID_OTHERS,
                                ACT_ID_OTHERS_REMARK: info.ACT_ID_OTHERS_REMARK,
                                ACT_ID_READING: info.ACT_ID_READING,
                                ACT_OD_ABROAD: info.ACT_OD_ABROAD,
                                ACT_OD_ADVANTURE: info.ACT_OD_ADVANTURE,
                                ACT_OD_BEAUTY: info.ACT_OD_BEAUTY,
                                ACT_OD_CLUB: info.ACT_OD_CLUB,
                                ACT_OD_HOROSCOPE: info.ACT_OD_HOROSCOPE,
                                ACT_OD_MOMCHILD: info.ACT_OD_MOMCHILD,
                                ACT_OD_OTHERS: info.ACT_OD_OTHERS,
                                ACT_OD_OTHERS_REMARK: info.ACT_OD_OTHERS_REMARK,
                                ACT_OD_PHILANTHROPY: info.ACT_OD_PHILANTHROPY,
                                ACT_OD_PHOTO: info.ACT_OD_PHOTO,
                                ACT_OD_RALLY: info.ACT_OD_RALLY,
                                ACT_OD_RESTAURANT: info.ACT_OD_RESTAURANT,
                                ACT_OD_SHOPPING: info.ACT_OD_SHOPPING,
                                ACT_OD_UPCOUNTRY: info.ACT_OD_UPCOUNTRY,
                                ACT_SPT_CYCLING: info.ACT_SPT_CYCLING,
                                ACT_SPT_FITNESS: info.ACT_SPT_FITNESS,
                                ACT_SPT_FOOTBALL: info.ACT_SPT_FOOTBALL,
                                ACT_SPT_OTHERS: info.ACT_SPT_OTHERS,
                                ACT_SPT_OTHERS_REMARK: info.ACT_SPT_OTHERS_REMARK,
                                ACT_SPT_RUN: info.ACT_SPT_RUN,
                                ADDR_ADDRESS: info.ADDR_ADDRESS,
                                ADDR_MOO: info.ADDR_MOO,
                                ADDR_ORG_AMPHURE_ID: info.ADDR_ORG_AMPHURE_ID,
                                ADDR_ORG_POSTALCODE_ID: info.ADDR_ORG_POSTALCODE_ID,
                                ADDR_ORG_PROVINCE_ID: info.ADDR_ORG_PROVINCE_ID,
                                ADDR_ORG_SUB_DISTRICT_ID: info.ADDR_ORG_SUB_DISTRICT_ID,
                                ADDR_OTHER: info.ADDR_OTHER,
                                ADDR_SOI: info.ADDR_SOI,
                                ADDR_STREET: info.ADDR_STREET,
                                ADDR_VILLEGE: info.ADDR_VILLEGE,
                                BIRTH_DATE: info.BIRTH_DATE,
                                CARD_ID: info.CARD_ID,
                                CARD_TYPE_ID: info.CARD_TYPE_ID,
                                CUSTOMER_ID: info.CUSTOMER_ID,
                                CUSTOMER_STATUS: info.CUSTOMER_STATUS,
                                EDUCATION_ID: info.EDUCATION_ID,
                                FNAME_TH: info.FNAME_TH,
                                FULL_ADDRESS: info.FULL_ADDRESS,
                                FULL_NAME_TH: info.FULL_NAME_TH,
                                GENDER_ID: info.GENDER_ID,
                                INCOME_ID: info.INCOME_ID,
                                LNAME_TH: info.LNAME_TH,
                                MARITAL_STATUS: info.MARITAL_STATUS,
                                OCCAPATION_REMARK: info.OCCAPATION_REMARK,
                                OCCUPATION_ID: info.OCCUPATION_ID,
                                PHONE_HOME: info.PHONE_HOME,
                                PHONE_NO: info.PHONE_NO,
                                PMT_BEAUTY: info.PMT_BEAUTY,
                                PMT_FOOD: info.PMT_FOOD,
                                PMT_OTHERS: info.PMT_OTHERS,
                                PMT_OTHERS_REMARK: info.PMT_OTHERS_REMARK,
                                PMT_SHOPPING: info.PMT_SHOPPING,
                                PMT_SPORT_AND_CAR: info.PMT_SPORT_AND_CAR,
                                PMT_TRAVEL: info.PMT_TRAVEL,
                                REF_CARD_NO: info.REF_CARD_NO,
                                REGIS_DATE: info.REGIS_DATE,
                                SUBSCRIBE_EMAIL: info.SUBSCRIBE_EMAIL,
                                SUBSCRIBE_LETTER: info.SUBSCRIBE_LETTER,
                                SUBSCRIBE_NONE: info.SUBSCRIBE_NONE,
                                SUBSCRIBE_SMS: info.SUBSCRIBE_SMS,
                                TITLE_NAME_ID: info.TITLE_NAME_ID,
                                TITLE_NAME_REMARK: info.TITLE_NAME_REMARK,
                                USER_EMAIL: info.USER_EMAIL,
                                ALREADY_EMAIL: info.USER_EMAIL,
                            }
                        })
                        this.getMaritalStatus(info.MARITAL_STATUS, info.ADDR_ORG_PROVINCE_ID, info.ADDR_ORG_AMPHURE_ID, info.ADDR_ORG_SUB_DISTRICT_ID, info.ADDR_ORG_POSTALCODE_ID)
                    } else {
                        this.setState({ 
                            fields: {
                                ...this.state.fields,
                                // CARD_ID: info.CARD_ID,
                                // FNAME_TH: info.FNAME_TH,
                                // LNAME_TH: info.LNAME_TH,
                                // TITLE_NAME_ID: info.TITLE_NAME_ID,
                                // GENDER_ID: info.GENDER_ID,
                                // BIRTH_DATE: info.BIRTH_DATE,
                                // MARITAL_STATUS: info.MARITAL_STATUS,
                                // USER_EMAIL: info.USER_EMAIL,
                                // PHONE_NO: info.PHONE_NO,
                                // ADDR_ADDRESS: info.ADDR_ADDRESS,
                                // ADDR_MOO: info.ADDR_MOO,
                                // ADDR_VILLEGE: info.ADDR_VILLEGE,
                                // ADDR_SOI: info.ADDR_SOI,
                                // ADDR_STREET: info.ADDR_STREET,
                                // ADDR_ORG_PROVINCE_ID: info.ADDR_ORG_PROVINCE_ID,
                                // ADDR_ORG_AMPHURE_ID: info.ADDR_ORG_AMPHURE_ID,
                                // ADDR_ORG_SUB_DISTRICT_ID: info.ADDR_ORG_SUB_DISTRICT_ID,
                                // ADDR_ORG_POSTALCODE_ID: info.ADDR_ORG_POSTALCODE_ID,
                                // ADDR_OTHER: info.ADDR_OTHER,

                                ABOUT_SOURCE: info.ABOUT_SOURCE,
                                ACT_EN_CONCERT: info.ACT_EN_CONCERT,
                                ACT_EN_COUNTRY_CONCERT: info.ACT_EN_COUNTRY_CONCERT,
                                ACT_EN_MOVIES: info.ACT_EN_MOVIES,
                                ACT_EN_MUSIC: info.ACT_EN_MUSIC,
                                ACT_EN_OTHERS: info.ACT_EN_OTHERS,
                                ACT_EN_OTHERS_REMARK: info.ACT_EN_OTHERS_REMARK,
                                ACT_EN_THEATER: info.ACT_EN_THEATER,
                                ACT_HNGOUT_COUPLE: info.ACT_HNGOUT_COUPLE,
                                ACT_HNGOUT_FAMILY: info.ACT_HNGOUT_FAMILY,
                                ACT_HNGOUT_FRIEND: info.ACT_HNGOUT_FRIEND,
                                ACT_HNGOUT_NO: info.ACT_HNGOUT_NO,
                                ACT_ID_BAKING: info.ACT_ID_BAKING,
                                ACT_ID_COOKING: info.ACT_ID_COOKING,
                                ACT_ID_DECORATE: info.ACT_ID_DECORATE,
                                ACT_ID_GAMING: info.ACT_ID_GAMING,
                                ACT_ID_GARDENING: info.ACT_ID_GARDENING,
                                ACT_ID_INTERNET: info.ACT_ID_INTERNET,
                                ACT_ID_OTHERS: info.ACT_ID_OTHERS,
                                ACT_ID_OTHERS_REMARK: info.ACT_ID_OTHERS_REMARK,
                                ACT_ID_READING: info.ACT_ID_READING,
                                ACT_OD_ABROAD: info.ACT_OD_ABROAD,
                                ACT_OD_ADVANTURE: info.ACT_OD_ADVANTURE,
                                ACT_OD_BEAUTY: info.ACT_OD_BEAUTY,
                                ACT_OD_CLUB: info.ACT_OD_CLUB,
                                ACT_OD_HOROSCOPE: info.ACT_OD_HOROSCOPE,
                                ACT_OD_MOMCHILD: info.ACT_OD_MOMCHILD,
                                ACT_OD_OTHERS: info.ACT_OD_OTHERS,
                                ACT_OD_OTHERS_REMARK: info.ACT_OD_OTHERS_REMARK,
                                ACT_OD_PHILANTHROPY: info.ACT_OD_PHILANTHROPY,
                                ACT_OD_PHOTO: info.ACT_OD_PHOTO,
                                ACT_OD_RALLY: info.ACT_OD_RALLY,
                                ACT_OD_RESTAURANT: info.ACT_OD_RESTAURANT,
                                ACT_OD_SHOPPING: info.ACT_OD_SHOPPING,
                                ACT_OD_UPCOUNTRY: info.ACT_OD_UPCOUNTRY,
                                ACT_SPT_CYCLING: info.ACT_SPT_CYCLING,
                                ACT_SPT_FITNESS: info.ACT_SPT_FITNESS,
                                ACT_SPT_FOOTBALL: info.ACT_SPT_FOOTBALL,
                                ACT_SPT_OTHERS: info.ACT_SPT_OTHERS,
                                ACT_SPT_OTHERS_REMARK: info.ACT_SPT_OTHERS_REMARK,
                                ACT_SPT_RUN: info.ACT_SPT_RUN,
                                ADDR_ADDRESS: info.ADDR_ADDRESS,
                                ADDR_MOO: info.ADDR_MOO,
                                ADDR_ORG_AMPHURE_ID: info.ADDR_ORG_AMPHURE_ID.toUpperCase(),
                                ADDR_ORG_POSTALCODE_ID: info.ADDR_ORG_POSTALCODE_ID.toUpperCase(),
                                ADDR_ORG_PROVINCE_ID: info.ADDR_ORG_PROVINCE_ID.toUpperCase(),
                                ADDR_ORG_SUB_DISTRICT_ID: info.ADDR_ORG_SUB_DISTRICT_ID.toUpperCase(),
                                ADDR_OTHER: info.ADDR_OTHER,
                                ADDR_SOI: info.ADDR_SOI,
                                ADDR_STREET: info.ADDR_STREET,
                                ADDR_VILLEGE: info.ADDR_VILLEGE,
                                BIRTH_DATE: info.BIRTH_DATE,
                                CARD_ID: info.CARD_ID,
                                CARD_TYPE_ID: info.CARD_TYPE_ID,
                                CUSTOMER_ID: info.CUSTOMER_ID,
                                CUSTOMER_STATUS: info.CUSTOMER_STATUS,
                                EDUCATION_ID: info.EDUCATION_ID,
                                FNAME_TH: info.FNAME_TH,
                                FULL_ADDRESS: info.FULL_ADDRESS,
                                FULL_NAME_TH: info.FULL_NAME_TH,
                                GENDER_ID: info.GENDER_ID,
                                INCOME_ID: info.INCOME_ID,
                                LNAME_TH: info.LNAME_TH,
                                MARITAL_STATUS: info.MARITAL_STATUS,
                                OCCAPATION_REMARK: info.OCCAPATION_REMARK,
                                OCCUPATION_ID: info.OCCUPATION_ID,
                                PHONE_HOME: info.PHONE_HOME,
                                PHONE_NO: info.PHONE_NO,
                                PMT_BEAUTY: info.PMT_BEAUTY,
                                PMT_FOOD: info.PMT_FOOD,
                                PMT_OTHERS: info.PMT_OTHERS,
                                PMT_OTHERS_REMARK: info.PMT_OTHERS_REMARK,
                                PMT_SHOPPING: info.PMT_SHOPPING,
                                PMT_SPORT_AND_CAR: info.PMT_SPORT_AND_CAR,
                                PMT_TRAVEL: info.PMT_TRAVEL,
                                REF_CARD_NO: info.REF_CARD_NO,
                                REGIS_DATE: info.REGIS_DATE,
                                SUBSCRIBE_EMAIL: info.SUBSCRIBE_EMAIL,
                                SUBSCRIBE_LETTER: info.SUBSCRIBE_LETTER,
                                SUBSCRIBE_NONE: info.SUBSCRIBE_NONE,
                                SUBSCRIBE_SMS: info.SUBSCRIBE_SMS,
                                TITLE_NAME_ID: info.TITLE_NAME_ID,
                                TITLE_NAME_REMARK: info.TITLE_NAME_REMARK,
                                USER_EMAIL: info.USER_EMAIL,
                                ALREADY_EMAIL: info.USER_EMAIL,
                            }
                        })
                        this.getMaritalStatusDefalt(info.MARITAL_STATUS)
                    }
                } else {
                    this.setState({ showLoading : 'none'})
                }
            })
            this.setState({ showLoading : 'none'})
        },500)
    }

    getMaritalStatusDefalt(MARITAL_STATUS){
        var status = "MARITAL_STATUS"
        registerAction.getDropdownlist(status).then(e => {
            var resp = e.data.dropdowN_INFO
            var getTITLE_NAME_ID = resp.find(o => o.codE_GUID === MARITAL_STATUS ? MARITAL_STATUS.toUpperCase() : MARITAL_STATUS);
            this.setState({ 
                getMaritalStatus : resp,
                getTITLE_NAME_IDCustomerProfile : getTITLE_NAME_ID.values,
                showLoading : 'none'
            })
        })
    }

    getMaritalStatus(MARITAL_STATUS, ADDR_ORG_PROVINCE_ID, ADDR_ORG_AMPHURE_ID, ADDR_ORG_SUB_DISTRICT_ID, ADDR_ORG_POSTALCODE_ID){
        var status = "MARITAL_STATUS"
        registerAction.getDropdownlist(status).then(e => {
            var resp = e.data.dropdowN_INFO
            var getTITLE_NAME_ID = resp.find(o => o.codE_GUID === MARITAL_STATUS ? MARITAL_STATUS.toUpperCase() : MARITAL_STATUS);
            this.setState({ 
                getMaritalStatus : resp,
                getTITLE_NAME_IDCustomerProfile : getTITLE_NAME_ID.values
            })
            this.getProvinceNew(ADDR_ORG_PROVINCE_ID, ADDR_ORG_AMPHURE_ID, ADDR_ORG_SUB_DISTRICT_ID, ADDR_ORG_POSTALCODE_ID)
        })
    }

    getProvinceNew(ADDR_ORG_PROVINCE_ID, ADDR_ORG_AMPHURE_ID, ADDR_ORG_SUB_DISTRICT_ID, ADDR_ORG_POSTALCODE_ID){
        var PROVINCE_ID = ADDR_ORG_PROVINCE_ID.toUpperCase()
        registerAction.getProvinceNew().then(e => {
            var resp = e.data.dropdowN_INFO
            var findProvin = resp.find(o => o.codE_GUID === PROVINCE_ID);
            this.setState({ 
                getProvince : resp,
                getProvinceCustomerProfile : findProvin.values,
                fields: {
                    ...this.state.fields,
                    ADDR_ORG_PROVINCE_ID: findProvin.codE_GUID.toLowerCase()
                },
                showLoading : 'none'
            })
            this.getAmphure(ADDR_ORG_PROVINCE_ID, ADDR_ORG_AMPHURE_ID, ADDR_ORG_SUB_DISTRICT_ID, ADDR_ORG_POSTALCODE_ID)
        })
    }

    getAmphure(value, ADDR_ORG_AMPHURE_ID, ADDR_ORG_SUB_DISTRICT_ID, ADDR_ORG_POSTALCODE_ID){
        var PROVINCE_ID = value.toUpperCase()
        registerAction.getAmphure(value).then(e => {
            var resp = e.data.data
            var data_ADDR_ORG_AMPHURE_ID = this.state.fields.ADDR_ORG_AMPHURE_ID
            if(ADDR_ORG_AMPHURE_ID != "onChange"){
                var findAmphure = resp.find(o => o.codE_GUID === data_ADDR_ORG_AMPHURE_ID.toUpperCase());
                this.setState({ 
                    getAmphure : resp,
                    getAmphureCustomerProfile : findAmphure.values,
                    fields: {
                        ...this.state.fields,
                        ADDR_ORG_AMPHURE_ID: findAmphure.codE_GUID.toUpperCase(),
                        ADDR_ORG_PROVINCE_ID: value.toUpperCase()
                    }
                })
                this.getTumbon(ADDR_ORG_AMPHURE_ID, ADDR_ORG_SUB_DISTRICT_ID, ADDR_ORG_POSTALCODE_ID)
            } else {
                let { fields } = this.state;
                fields['ADDR_ORG_AMPHURE_ID'] = ''
                fields['ADDR_ORG_SUB_DISTRICT_ID'] = ''
                fields['ADDR_ORG_POSTALCODE_ID'] = ''
                this.setState({ fields })
                var findAmphure = resp.find(o => o.codE_GUID === value ? value.toUpperCase() : value);
                this.setState({ 
                    getAmphure : resp,
                    getAmphureCustomerProfile : findAmphure.values,
                    fields: {
                        ...this.state.fields,
                        ADDR_ORG_AMPHURE_ID: findAmphure.codE_GUID.toLowerCase()
                    }
                })
            }
        })
    }

    getTumbon(value, ADDR_ORG_SUB_DISTRICT_ID, ADDR_ORG_POSTALCODE_ID){
        // var ADDR_ORG_SUB_DISTRICT_ID = this.state.fields.ADDR_ORG_SUB_DISTRICT_ID;
        registerAction.getTumbon(value).then(e => {
            var resp = e.data.data;
            if(ADDR_ORG_SUB_DISTRICT_ID != "onChange"){
                var findTumbon = resp.find(o => o.codE_GUID === this.state.fields.ADDR_ORG_SUB_DISTRICT_ID.toUpperCase());
                this.setState({ 
                    getTumbon : resp,
                    getTumbonCustomerProfile : findTumbon.values,
                    getPostcodeCustomerProfile : findTumbon.code,
                    fields: {
                        ...this.state.fields,
                        ADDR_ORG_SUB_DISTRICT_ID: findTumbon.codE_GUID.toUpperCase()
                    }
                })
                this.getPostcode(ADDR_ORG_SUB_DISTRICT_ID, ADDR_ORG_POSTALCODE_ID)
            } else {
                var findTumbon = resp.find(o => o.codE_GUID === value ? value.toUpperCase() : value);
                this.setState({ 
                    getTumbon : resp,
                    getTumbonCustomerProfile : findTumbon.values,
                    // getPostcodeCustomerProfile : findTumbon.code,
                    fields: {
                        ...this.state.fields,
                        ADDR_ORG_SUB_DISTRICT_ID: findTumbon.codE_GUID.toLowerCase()
                    }
                })
            }
        })
    }

    getPostcode(value){
        registerAction.getPostcode(value).then(e => {
            var resp = e.data.data
            var findPostcode = resp.find(o => o.codE_GUID === value ? value.toUpperCase() : value);
            this.setState({ 
                getPostcode : resp,
                getPostcodeCustomerProfile : findPostcode.values,
                fields: {
                    ...this.state.fields,
                    ADDR_ORG_POSTALCODE_ID: findPostcode.codE_GUID.toUpperCase()
                }
            })
        })
    }

    handleChangepostcode(e){
        let value = e.target.value
        this.setState({ 
            fields: {
                ...this.state.fields,
                ADDR_ORG_POSTALCODE_ID: value.toLowerCase()
            }
        })
    }
    
    handleChange = (e) => {
        const input = e.target;
        const value = input.value;
        let { fields , errors , errorsFocus } = this.state;
        fields[e.target.name] = e.target.value;
        errors[e.target.name] = null;
        errorsFocus[e.target.name] = ''

        if(e.target.name === "ADDR_ORG_PROVINCE_ID"){
            fields['ADDR_ORG_AMPHURE_ID'] = ""
            fields['ADDR_ORG_SUB_DISTRICT_ID'] = ""
            fields['ADDR_ORG_POSTALCODE_ID'] = ""
            this.getAmphure(value, "onChange")
        }
        if(e.target.name === "ADDR_ORG_AMPHURE_ID"){
            fields['ADDR_ORG_SUB_DISTRICT_ID'] = "";
            fields['ADDR_ORG_POSTALCODE_ID'] = ""
            this.getTumbon(value, "onChange")
        }
        if(e.target.name === "ADDR_ORG_SUB_DISTRICT_ID"){
            fields['ADDR_ORG_POSTALCODE_ID'] = ""
            this.getPostcode(value, "onChange")
        }

        this.setState({ [input.name]: value , errors, fields });

        
    };

    handleSubmit(e) {
        e.preventDefault();
        this.setState({ submitted: true });
        this.updateProfile()
    }

    validateForm(){
        let fields = this.state.fields;
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;
    
        if(!fields["CARD_ID"]){
            formIsValid = false;
            errors["CARD_ID"] = "ALERT_CARD_ID"
            errorsFocus["CARD_ID"] = 'errorFocus'
        }
        if(!fields["FNAME_TH"]){
            formIsValid = false;
            errors["FNAME_TH"] = <FormattedMessage id="inputfirstname" />
            errorsFocus["FNAME_TH"] = 'errorFocus'
        }
        if(!fields["LNAME_TH"]){
            formIsValid = false;
            errors["LNAME_TH"] = <FormattedMessage id="inputlastname" />
            errorsFocus["LNAME_TH"] = 'errorFocus'
        }
        if(!fields["GENDER_ID"]){
            formIsValid = false;
            errors["GENDER_ID"] = "ALERT_GENDER_ID"
            errorsFocus["GENDER_ID"] = 'errorFocus'
        }
        if(!fields["BIRTH_DATE"]){
            formIsValid = false;
            errors["BIRTH_DATE"] = "ALERT_BIRTH_DATE"
            errorsFocus["BIRTH_DATE"] = 'errorFocus'
        }
        if(!fields["MARITAL_STATUS"]){
            formIsValid = false;
            errors["MARITAL_STATUS"] = <FormattedMessage id="plsselectstatus" />
            errorsFocus["MARITAL_STATUS"] = 'errorFocus'
        }
        if(!fields["PHONE_NO"]){
            formIsValid = false;
            errors["PHONE_NO"] = <FormattedMessage id="inputphoneno" />
            errorsFocus["PHONE_NO"] = 'errorFocus'
        }
        if(!fields["ADDR_ADDRESS"]){
            formIsValid = false;
            errors["ADDR_ADDRESS"] = <FormattedMessage id="inputaddressno" />
            errorsFocus["ADDR_ADDRESS"] = 'errorFocus'
        }
        if(!fields["ADDR_ORG_PROVINCE_ID"]){
            formIsValid = false;
            errors["ADDR_ORG_PROVINCE_ID"] = <FormattedMessage id="selectprovince" />
            errorsFocus["ADDR_ORG_PROVINCE_ID"] = 'errorFocus'
        }
        if(!fields["ADDR_ORG_AMPHURE_ID"]){
            formIsValid = false;
            errors["ADDR_ORG_AMPHURE_ID"] = <FormattedMessage id="selectampure" />
            errorsFocus["ADDR_ORG_AMPHURE_ID"] = 'errorFocus'
        }
        if(!fields["ADDR_ORG_SUB_DISTRICT_ID"]){
            formIsValid = false;
            errors["ADDR_ORG_SUB_DISTRICT_ID"] = <FormattedMessage id="selectdistrict" />
            errorsFocus["ADDR_ORG_SUB_DISTRICT_ID"] = 'errorFocus'
        } 
        if(!fields["ADDR_ORG_POSTALCODE_ID"]){
            formIsValid = false;
            errors["ADDR_ORG_POSTALCODE_ID"] = <FormattedMessage id="selectpostcode" />
            errorsFocus["ADDR_ORG_POSTALCODE_ID"] = 'errorFocus'
        } 
        // if(!fields["ADDR_OTHER"]){
        //     formIsValid = false;
        //     errors["ADDR_OTHER"] = "ALERT_ADDR_OTHER" 
        //     errorsFocus["ADDR_OTHER"] = 'errorFocus'
        // } 
        if(fields["ALREADY_EMAIL"] == fields["USER_EMAIL"]){
            var validEmail = this.validateEmail(fields["USER_EMAIL"]);
            if (validEmail === false) {
                formIsValid = false;
                errors["USER_EMAIL"] = <FormattedMessage id="inputemail" />
                errorsFocus["USER_EMAIL"] = 'errorFocus'
            }
        } else {
            var validEmail = this.validateEmail(fields["USER_EMAIL"]);
            if (validEmail === false) {
                formIsValid = false;
                errors["USER_EMAIL"] = <FormattedMessage id="inputemail" />
                errorsFocus["USER_EMAIL"] = 'errorFocus'
            } else {
                var chkValidEmail = this.checkEmail();
                if (chkValidEmail === true) {
                    formIsValid = false;
                    errors["USER_EMAIL"] = "2"
                    errorsFocus["USER_EMAIL"] = 'errorFocus'
                }
            }
        }

        this.setState({
            errors: errors,
            errorsFocus: errorsFocus
        })
        return formIsValid;
    }

    updateProfile(){
        if(this.validateForm()){
            // const { MARITAL_STATUS, USER_EMAIL, ADDR_ADDRESS, ADDR_MOO, ADDR_VILLEGE, ADDR_SOI, ADDR_STREET, ADDR_ORG_PROVINCE_ID, ADDR_ORG_AMPHURE_ID, ADDR_ORG_SUB_DISTRICT_ID, ADDR_ORG_POSTALCODE_ID, ADDR_OTHER } = this.state.fields
            // console.log("MARITAL_STATUS", MARITAL_STATUS)
            // console.log("USER_EMAIL", USER_EMAIL)
            // console.log("ADDR_ADDRESS", ADDR_ADDRESS)
            // console.log("ADDR_MOO", ADDR_MOO)
            // console.log("ADDR_VILLEGE", ADDR_VILLEGE)
            // console.log("ADDR_SOI", ADDR_SOI)
            // console.log("ADDR_STREET", ADDR_STREET)
            // console.log("ADDR_ORG_PROVINCE_ID", ADDR_ORG_PROVINCE_ID)
            // console.log("ADDR_ORG_AMPHURE_ID", ADDR_ORG_AMPHURE_ID)
            // console.log("ADDR_ORG_SUB_DISTRICT_ID", ADDR_ORG_SUB_DISTRICT_ID)
            // console.log("ADDR_ORG_POSTALCODE_ID", ADDR_ORG_POSTALCODE_ID)
            // console.log("ADDR_OTHER", ADDR_OTHER)
            const { fields, Profile } = this.state
            // registerAction.updateAdditionalProfile(fields, Profile.tkvsm, Profile.customer_id, Profile.userId).then(e => {
            //     console.log("updateAdditionalProfile", e)
            // })
            this.props.history.push({
                pathname:`${process.env.PUBLIC_URL}/PTmaxcard/otpeditprfile`,
                state: {
                    fieldsEditProfile: fields
                }
            });
        }
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    checkEmail(email){
        registerAction.checkEmailExist(email).then(e => {
            var resp = e.data.responsE_INFO;
            if (resp.rescode !== "000") {
              ERROR_USER_EMAIL = false
            } else {
              ERROR_USER_EMAIL = true
            }
            return ERROR_USER_EMAIL
        });
    }

    setupLogout(){
        // localStorage.clear();
        window.location.href=`${process.env.PUBLIC_URL}/home`
        // var that = this;
        // auth().signOut().then(function() {
        //   that.setState({user: null});
        // }).catch(function(error) {
        //     console.log(error)
        // });
    }

    render(){
        var Profile = this.state.Profile
        const { getMaritalStatus, getProvince, getAmphure, getTumbon, getPostcode } = this.state
        var getMaritalStatusCard = [];
        for(var s in getMaritalStatus){
            getMaritalStatusCard.push(
                <option value={getMaritalStatus[s].code}>{getMaritalStatus[s].values}</option>  
            )
        }
        var getProvinceCard = [];
        for(var p in getProvince){
            getProvinceCard.push(
                <option value={getProvince[p].codE_GUID}>{getProvince[p].values}</option>
            )
        }
        var getAmphureCard = [];
        for(var a in getAmphure){
            getAmphureCard.push(
                <option value={getAmphure[a].codE_GUID}>{getAmphure[a].values}</option>    
            )
        }
        var getTumbonCard = [];
        for(var t in getTumbon){
            getTumbonCard.push(
                <option value={getTumbon[t].codE_GUID}>{getTumbon[t].values}</option> 
            )
        }
        var getPostcodeCard = [];
        for(var o in getPostcode){
            getPostcodeCard.push(
                <option value={getPostcode[o].codE_GUID}>{getPostcode[o].values}</option>  
            )
        }
        var setLangeditprofile = messages[this.state.language].editprofile

        if(this.state.fields["TITLE_NAME_ID"] == 1){
            var TITLE_NAME_ID = "Mr. (นาย)"
        } else if(this.state.fields["TITLE_NAME_ID"] == 2){
            var TITLE_NAME_ID = "Ms. (นาง)"
        } else if(this.state.fields["TITLE_NAME_ID"] == 3){
            var TITLE_NAME_ID = "Miss. (นางสาว)"
        } else if(this.state.fields["TITLE_NAME_ID"] == 4){
            var TITLE_NAME_ID = "Other. (อื่นๆ)"
        }
        if(this.state.fields["GENDER_ID"] == 1){
            var GENDER_ID = "ชาย"
        } else {
            var GENDER_ID = "หญิง"
        }

        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].editprofile}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="bg-ptmaxcard-content">
                            <div className="ptmaxcard-content">
                                <div className="row">
                                    <div className="col-lg-3 col-md-6">
                                        <PTsidebar/>
                                    </div>
                                    <div className="col-lg-9 col-md-6">
                                        
                                        <div className="editprofile-title py-3"><FormattedMessage id="editprofile" /></div>
                                            <div className="editprofile-content py-3">
                                                <form onSubmit={(e) => this.handleSubmit(e)}>
                                                    <div className="row">
                                                        <div className="col-lg-6 col-md-12">
                                                            <div className="header-profile"><FormattedMessage id="profile" /></div>
                                                            <div className="form-group">
                                                                <label><span className="editprofile-required pr-2">*</span><FormattedMessage id="cardid" /></label>
                                                                <input
                                                                    type="text"
                                                                    className={`form-control input_editprofile disable_editprofile ${this.state.errorsFocus['CARD_ID']}`}
                                                                    maxLength="13"
                                                                    name="CARD_ID"
                                                                    onChange={e => this.handleChange(e)}
                                                                    placeholder={this.state.errors["CARD_ID"] == "" ? setLangeditprofile.cardid : this.state.errors["CARD_ID"]}
                                                                    value={this.state.fields["CARD_ID"]}
                                                                />
                                                            </div>
                                                            <div className="form-group">
                                                                <label><span className="editprofile-required pr-2">*</span><FormattedMessage id="titlename" /></label>
                                                                <select
                                                                    className={`form-control input_editprofile disable_editprofile ${this.state.errorsFocus['TITLE_NAME_ID']}`}
                                                                    name="TITLE_NAME_ID"
                                                                >
                                                                    <option selected hidden>
                                                                        {TITLE_NAME_ID}
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            <div className="form-group">
                                                                <label><span className="editprofile-required pr-2">*</span><FormattedMessage id="firstname" /></label>
                                                                <input
                                                                    type="text"
                                                                    className={`form-control input_editprofile disable_editprofile ${this.state.errorsFocus['FNAME_TH']}`}
                                                                    name="FNAME_TH"
                                                                    onChange={e => this.handleChange(e)}
                                                                    placeholder={this.state.errors["FNAME_TH"] == "" ? setLangeditprofile.inputfirstname : this.state.errors["FNAME_TH"]}
                                                                    value={this.state.fields['FNAME_TH']}
                                                                />
                                                            </div>
                                                            <div className="form-group">
                                                                <label><span className="editprofile-required pr-2">*</span><FormattedMessage id="lastname" /></label>
                                                                <input
                                                                    type="text"
                                                                    className={`form-control input_editprofile disable_editprofile ${this.state.errorsFocus['LNAME_TH']}`}
                                                                    name="LNAME_TH"
                                                                    onChange={e => this.handleChange(e)}
                                                                    placeholder={this.state.errors["LNAME_TH"] == "" ? setLangeditprofile.inputlastname : this.state.errors["LNAME_TH"]}
                                                                    value={this.state.fields['LNAME_TH']}
                                                                />
                                                            </div>
                                                            <div className="form-group">
                                                                <label><span className="editprofile-required pr-2">*</span><FormattedMessage id="gender" /></label>
                                                                <select
                                                                    className={`form-control input_editprofile disable_editprofile ${this.state.errorsFocus['GENDER_ID']}`}
                                                                    name="GENDER_ID"
                                                                    onChange={e => this.handleChange(e)}
                                                                    value={this.state.fields["GENDER_ID"]}
                                                                >
                                                                    <option selected hidden>{GENDER_ID}</option>
                                                                </select>
                                                            </div>
                                                            <div className="form-group">
                                                                <label><span className="editprofile-required pr-2">*</span><FormattedMessage id="birthday" /></label>
                                                                <div className="form-control input_form disabled" disabled={true}>{(moment(this.state.fields['BIRTH_DATE']).format('DD/MM/YYYY'))}</div>
                                                                <input
                                                                    type="hidden"
                                                                    name="BIRTH_DATE"
                                                                    onChange={e => this.handleChange(e)}
                                                                    className={`form-control input_editprofile disable_editprofile ${this.state.errorsFocus['BIRTH_DATE']}`}
                                                                    placeholder={this.state.errors["BIRTH_DATE"] == "" ? setLangeditprofile.birthday : this.state.errors["BIRTH_DATE"]}
                                                                    value={this.state.fields['BIRTH_DATE']}
                                                                /> 
                                                            </div>
                                                            <div className="form-group">
                                                                <label><span className="editprofile-required pr-2">*</span><FormattedMessage id="status" /></label>
                                                                <select
                                                                    className={`form-control input_editprofile ${this.state.errorsFocus['MARITAL_STATUS']}`}
                                                                    name="MARITAL_STATUS"
                                                                    onChange={e => this.handleChange(e)}
                                                                    // value={this.state.fields['MARITAL_STATUS']}
                                                                    value={this.state.fields['MARITAL_STATUS'] == 0 ? this.state.fields['MARITAL_STATUS'] = '' : this.state.fields['MARITAL_STATUS']}
                                                                >
                                                                    <option selected hidden>{this.state.fields["TITLE_NAME_ID"] == "" ? setLangeditprofile.plsselectstatus : this.state.getTITLE_NAME_IDCustomerProfile}</option>
                                                                    {getMaritalStatusCard}
                                                                </select>
                                                            </div>
                                                            <div className="header-profile mt-5"><FormattedMessage id="infocontact" /></div>
                                                            <div className="form-group">
                                                                <label><span className="editprofile-required pr-2">*</span><FormattedMessage id="email" /></label>
                                                                <input
                                                                    type="text"
                                                                    className={`form-control input_editprofile ${this.state.errorsFocus['USER_EMAIL']}`}
                                                                    name="USER_EMAIL"
                                                                    onChange={e => this.handleChange(e)}
                                                                    placeholder={setLangeditprofile.inputemail}
                                                                    value={this.state.fields['USER_EMAIL']}
                                                                />
                                                                <div className="errorMsg USER_EMAIL">{this.state.errors["USER_EMAIL"]}</div>
                                                            </div>
                                                            <div className="form-group">
                                                                <label><span className="editprofile-required pr-2">*</span><FormattedMessage id="phoneno" /></label>
                                                                <input
                                                                    type="tel"
                                                                    maxLength="10"
                                                                    className={`form-control input_editprofile disable_editprofile ${this.state.errorsFocus['PHONE_NO']} input_editprofile ${(this.state.fields["PHONE_NO"] ? `` : '')}`}
                                                                    name="PHONE_NO"
                                                                    onChange={e => this.handleChange(e)}
                                                                    placeholder={this.state.errors["PHONE_NO"] == "" ? setLangeditprofile.inputphoneno : this.state.errors["PHONE_NO"]}
                                                                    value={this.state.fields["PHONE_NO"]}
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6 col-md-12">
                                                        {/* <form className="form-horizontal">
                                                            <div className="bg_full">
                                                            <div className="container-fluid">
                                                                <div className="content_page">
                                                                <div className="head_title_agree"></div>
                                                                <div className="form-group">
                                                                    <label>
                                                                    <span className="label-validate">* </span>
                                                                    <span className="font-header-label pl-2"><FormattedMessage id="infoaddress" /></span>
                                                                    </label>
                                                                    <textarea 
                                                                    className={`form-control input_form ptmaxcard-regis-input ${this.state.errorsFocus['ADDR_OTHER']}`}
                                                                    id="exampleFormControlTextarea1" 
                                                                    rows="3" 
                                                                    name="ADDR_OTHER" 
                                                                    value={this.state.fields.ADDR_OTHER} 
                                                                    onChange={(e) => this.handleChange(e)} 
                                                                    placeholder={this.state.errors["ADDR_OTHER"] == null ? setLangmyMaxCard.inputmoredata : this.state.errors["ADDR_OTHER"]}
                                                                    >
                                                                    </textarea>
                                                                </div>
                                                                <div className="errorMsg ADDR_OTHER">{this.state.errors["ADDR_OTHER"]}</div>
                                                                </div>
                                                            </div>
                                                            </div>
                                                        </form> */}
                                                        <div className="header-profile"><FormattedMessage id="addressdata" /></div>
                                                        {this.state.fields['CARD_TYPE_ID'] == 2 ? 
                                                            <div className="form-group">
                                                                <label><span className="editprofile-required pr-2">*</span><FormattedMessage id="addressmore" /></label>
                                                                <textarea 
                                                                    className="form-control input_editprofile bd-field" 
                                                                    rows="3" 
                                                                    name="ADDR_OTHER" 
                                                                    value={this.state.fields['ADDR_OTHER']} 
                                                                    onChange={(e) => this.handleChange(e)} 
                                                                    placeholder={setLangeditprofile.inputaddressmore}
                                                                >
                                                                </textarea>
                                                            </div>
                                                        :
                                                            <>
                                                            <div className="form-group">
                                                                <label><span className="editprofile-required pr-2">*</span><FormattedMessage id="addressno" /></label>
                                                                <input 
                                                                    type="text" 
                                                                    className={`form-control input_editprofile ${this.state.errorsFocus['ADDR_ADDRESS']}`} 
                                                                    maxLength="10" 
                                                                    name="ADDR_ADDRESS" 
                                                                    onChange={(e) => this.handleChange(e)} 
                                                                    value={this.state.fields['ADDR_ADDRESS']}
                                                                    placeholder={setLangeditprofile.inputaddressno}
                                                                />
                                                                <div className="errorMsg ADDR_ADDRESS">{this.state.errors["ADDR_ADDRESS"]}</div>
                                                            </div>
                                                            <div className="form-group">
                                                                <label><FormattedMessage id="moo" /></label>
                                                                <input
                                                                    type="text"
                                                                    className={`form-control input_editprofile`}
                                                                    maxLength="3"
                                                                    name="ADDR_MOO"
                                                                    onChange={(e) => this.handleChange(e)}
                                                                    value={this.state.fields['ADDR_MOO']}
                                                                    placeholder={setLangeditprofile.inputmoo}
                                                                />
                                                            </div>
                                                            <div className="form-group">
                                                                <label><FormattedMessage id="villege" /></label>
                                                                <input 
                                                                    type="text" 
                                                                    className="form-control input_editprofile" 
                                                                    name="ADDR_VILLEGE" 
                                                                    onChange={(e) => this.handleChange(e)} 
                                                                    value={this.state.fields['ADDR_VILLEGE']} 
                                                                    placeholder={setLangeditprofile.inputvillege}
                                                                />
                                                            </div>
                                                            <div className="form-group">
                                                                <label><FormattedMessage id="soi" /></label>
                                                                <input 
                                                                    type="text" 
                                                                    className="form-control input_editprofile" 
                                                                    name="ADDR_SOI" 
                                                                    onChange={(e) => this.handleChange(e)} 
                                                                    value={this.state.fields['ADDR_SOI']} 
                                                                    placeholder={setLangeditprofile.inputsoi}
                                                                />
                                                            </div>
                                                            <div className="form-group">
                                                                <label><FormattedMessage id="street" /></label>
                                                                <input 
                                                                    type="text" 
                                                                    className="form-control input_editprofile bd-field" 
                                                                    name="ADDR_STREET" 
                                                                    onChange={(e) => this.handleChange(e)} 
                                                                    value={this.state.fields['ADDR_STREET']} 
                                                                    placeholder={setLangeditprofile.inputstreet}
                                                                />
                                                            </div>
                                                            <div className="form-group">
                                                                <label><span className="editprofile-required pr-2">*</span><FormattedMessage id="province" /></label>
                                                                <select
                                                                    className={`form-control bd-field input_editprofile ${this.state.errorsFocus['ADDR_ORG_PROVINCE_ID']}`} 
                                                                    name="ADDR_ORG_PROVINCE_ID"
                                                                    onChange={(e) => this.handleChange(e)} 
                                                                    value={this.state.fields['ADDR_ORG_PROVINCE_ID'] == 0 ? this.state.fields['ADDR_ORG_PROVINCE_ID'] = '' : this.state.fields['ADDR_ORG_PROVINCE_ID']}
                                                                >
                                                                    <option selected hidden>{this.state.fields["ADDR_ORG_PROVINCE_ID"] == 0 ? setLangeditprofile.selectprovince : this.state.getProvinceCustomerProfile}</option>
                                                                    {getProvinceCard}
                                                                </select>
                                                            </div>
                                                            <div className="form-group">
                                                                <label><span className="editprofile-required pr-2">*</span><FormattedMessage id="ampure" /></label>
                                                                <select
                                                                    className={`form-control bd-field input_editprofile ${this.state.errorsFocus['ADDR_ORG_AMPHURE_ID']}`} 
                                                                    name="ADDR_ORG_AMPHURE_ID"
                                                                    onChange={(e) => this.handleChange(e)} 
                                                                    value={this.state.fields['ADDR_ORG_AMPHURE_ID'] == 0 ? this.state.fields['ADDR_ORG_AMPHURE_ID'] = '' : this.state.fields['ADDR_ORG_AMPHURE_ID']}
                                                                >
                                                                <option selected hidden>{this.state.fields["ADDR_ORG_AMPHURE_ID"] == 0 ? setLangeditprofile.selectampure : this.state.getAmphureCustomerProfile}</option>
                                                                    {getAmphureCard}
                                                                </select>
                                                                <div className="errorMsg ADDR_ORG_AMPHURE_ID">{this.state.errors["ADDR_ORG_AMPHURE_ID"]}</div>
                                                            </div>
                                                            <div className="form-group">
                                                                <label><span className="editprofile-required pr-2">*</span><FormattedMessage id="district" /></label>
                                                                <select
                                                                    className={`form-control bd-field input_editprofile ${this.state.errorsFocus['ADDR_ORG_SUB_DISTRICT_ID']}`} 
                                                                    name="ADDR_ORG_SUB_DISTRICT_ID"
                                                                    onChange={e => this.handleChange(e)}
                                                                    // onChange={(e) => this.getPostcode(e.target.value, "onChange")}
                                                                    value={this.state.fields['ADDR_ORG_SUB_DISTRICT_ID'] == 0 ? this.state.fields['ADDR_ORG_SUB_DISTRICT_ID'] = '' : this.state.fields['ADDR_ORG_SUB_DISTRICT_ID']}
                                                                >
                                                                <option selected hidden>{this.state.fields["ADDR_ORG_SUB_DISTRICT_ID"] == 0 ? setLangeditprofile.selectdistrict : this.state.getTumbonCustomerProfile}</option>
                                                                    {getTumbonCard}
                                                                </select>
                                                                <div className="errorMsg ADDR_ORG_SUB_DISTRICT_ID">{this.state.errors["ADDR_ORG_SUB_DISTRICT_ID"]}</div>
                                                            </div>
                                                            <div className="form-group">
                                                                <label><span className="editprofile-required pr-2">*</span><FormattedMessage id="postcode" /></label>
                                                                <select 
                                                                    className={`form-control bd-field input_editprofile ${this.state.errorsFocus['ADDR_ORG_POSTALCODE_ID']}`} 
                                                                    name="ADDR_ORG_POSTALCODE_ID" 
                                                                    onChange={(e) => this.handleChangepostcode(e)}
                                                                    value={this.state.fields['ADDR_ORG_POSTALCODE_ID'] == 0 ? this.state.fields['ADDR_ORG_POSTALCODE_ID'] = '' : (this.state.fields['ADDR_ORG_POSTALCODE_ID'] !== '' ? this.state.fields['ADDR_ORG_POSTALCODE_ID'] : '')}
                                                                >
                                                                <option selected hidden>{this.state.fields["ADDR_ORG_POSTALCODE_ID"] == 0 ? setLangeditprofile.selectpostcode : this.state.getPostcodeCustomerProfile}</option>
                                                                    {getPostcodeCard}
                                                                </select>
                                                                <div className="errorMsg ADDR_ORG_POSTALCODE_ID">{this.state.errors["ADDR_ORG_POSTALCODE_ID"]}</div>

                                                            </div>
                                                            <div className="form-group">
                                                                <label><FormattedMessage id="addressmore" /></label>
                                                                <textarea 
                                                                    className="form-control input_editprofile bd-field" 
                                                                    rows="3" 
                                                                    name="ADDR_OTHER" 
                                                                    value={this.state.fields['ADDR_OTHER']} 
                                                                    onChange={(e) => this.handleChange(e)} 
                                                                    placeholder={setLangeditprofile.inputaddressmore}
                                                                >
                                                                </textarea>
                                                            </div>
                                                            </>
                                                        }
                                                        </div>
                                                        <div className="btn_nextstep">
                                                            <button 
                                                                type="submit" 
                                                                className="btn btn-editprofilesumit" 
                                                            >
                                                                <FormattedMessage id="btnconfirm" />
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </IntlProvider>
        )
    }
}


