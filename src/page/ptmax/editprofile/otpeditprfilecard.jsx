import React from "react";
import '../../register/register.css';
import { registerAction } from '../../../_actions/registerActions'; 
import SweetAlert from "react-bootstrap-sweetalert";
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');

var OTP_All = ""

export class otpeditprfilecard extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'none',
            fieldsEditProfile: {},
            fields: {},
            resp: [],
            show: false,
            modal: null,
            secondsElapsed: 0,
            setOTP: '', 
            setOTPAll: '',
            Profile: '',
        } 
    }

    componentDidMount(){
        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
        var ProfileData = localStorage.getItem('Profile')
        var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
        ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        var ProfileArray = JSON.parse(ProfileDecrypt);
        var Profile = ProfileArray[0];
        this.setState({ Profile : Profile })
        // if(this.props.location.state.data) {
        //     let newObject = Object.assign(this.props.location.state , this.state.fields)
        //     this.setState({ fields: newObject })
        // }
        var fieldsEditProfile = this.props.location.state.fieldsEditProfile
        var phoneno = fieldsEditProfile.PHONE_NO
        this.setState({ 
            fieldsEditProfile : fieldsEditProfile,
            phoneno : phoneno
        })
        // this.sentOTP(phoneno)
        this.startTime();
        document.addEventListener("keydown", this.deleteBtnFunction, false);
    }

    startTime() {
        var _this = this;
        this.countdown = setInterval(function() {
          if(_this.state.secondsElapsed > 0){
            _this.setState({ 
              secondsElapsed: _this.state.secondsElapsed - 1 
            });
          }         
        }, 1000);
    }

    resetTime() {
        this.sentOTP(this.state.phoneno);
        this.reset = this.setState({
          secondsElapsed: (this.state.secondsElapsed = 60)
        });
    }

    getTime() {
        if(this.state.secondsElapsed % 60 > 0){
            var textOTP = this.showCountdown();
        }else{
            var textOTP = <button className="text-green t-22" onClick={() => this.resetTime()}>คลิกเพื่อรับ OTP</button>
        }
        return (textOTP);
      }

    showCountdown= (e) => {
        return (`${messages[this.state.language].otpeditprfilecard.sendotpin}` + (this.state.secondsElapsed % 60) + ` ${messages[this.state.language].otpeditprfilecard.secion}`)
    }

    sentOTP(phone_no){
        var type = 'Register';
        registerAction.sendOTP(phone_no,type).then( e => {
            if(e.data.isSuccess === true){
                this.setState({ resp : e.data.data })
            }else{
                this.setState({ resp: e.data.data })
            }
        })
    }

    verifyOTPClick(setOTP,otpkey){
        var Profile = this.state.Profile
        var OTP_All = setOTP
        var formData = new FormData();
        formData.append('otp_Key_No',otpkey)
        formData.append('eventType','ChangePassword')
        formData.append('eventNo',this.state.fieldsEditProfile)
        formData.append('value', OTP_All)
        registerAction.verifyOTP(formData).then(e => {
            if(e.data.isSuccess === true){
                this.setState({ resp : e.data.data })
                var fields = this.state.fieldsEditProfile
                var customer_id = Profile.customer_id;
                var tkvsm = Profile.tkvsm;
                var userId = Profile.userId
                // console.log("fields", fields)
                registerAction.updateAdditionalProfile(fields, tkvsm, customer_id, userId).then(e => {
                    // console.log("updateAdditionalProfile", e)
                    if(e.data.isSuccess === true){
                        var imgPopup = `${process.env.PUBLIC_URL}/images/checked.png`;
                        var msg = e.data.errMsg
                        this.modalCheckSubmit(msg,imgPopup,true)
                    }else{
                        var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
                        var msg = e.data.errMsg
                        this.modalCheckSubmit(msg,imgPopup,false)
                    }
                })
            }else{
                var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
                var msg = e.data.errMsg
                this.modalCheckSubmit(msg,imgPopup)
            }
        })
    } 

    modalCheckSubmit(msg,img, check){
        alert = (
            <SweetAlert
              custom
              showCloseButton
              closeOnClickOutside={false}
              focusConfirmBtn={false}
              title=""
              customIcon={img}
              showConfirm={false}
              showCancelButton
              onCancel={() => this.handleChoice(check)}
              onConfirm={() => this.handleChoice(check)}
            >
              <div className="iconClose" onClick={() => this.handleChoice(check)}></div>
              <div className="fontSizeCase">{msg}</div>
            </SweetAlert>
          );
          this.setState({ show: true, modal: alert });
    }

    handleChoice(bool){
        if(bool == true){
            this.setState({ modal : null , show : false })
            window.location.href = `${process.env.PUBLIC_URL}/PTmaxcard`;
        }else{
            this.setState({ modal : null , show : false })
        }
    }

    maxLengthCheck = (object) => {
        if (object.target.value.length > object.target.maxLength) {
            object.target.value = object.target.value.slice(0, object.target.maxLength)
        }
    }

    handleChange(e){
        this.setState({ 
            setOTP : e.target.value.replace(/[^0-9]/g, ''),
        })
    }

    render(){
        var fieldsEditProfile = this.state.fieldsEditProfile
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].otpeditprfilecard}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="bgcolor-ptmaxcard-content">
                            <div className="container">
                                <div className="row">
                                    <div className="col-12">
                                        <div className="editprofile-content">
                                            <div className="t-30"><FormattedMessage id="otpsendto" /> {fieldsEditProfile.PHONE_NO}</div>
                                            <hr/>
                                            <div className="form-group text-center p-5 m-4">
                                                <input
                                                    type="tel"
                                                    maxLength="6"
                                                    onInput={this.maxLengthCheck}
                                                    className="inputTel"
                                                    onChange={(e) => this.handleChange(e)}
                                                    className={`form-control input_editprofile text-center`}
                                                    value={this.state.setOTP}
                                                />
                                            </div>
                                            <div className="mt-4 text-center text-gray t-22"><FormattedMessage id="remarkotp" /> : {this.state.resp.ref_No}</div>
                                            <div className="mt-5 text-center text-green">
                                                <div>{this.getTime()}</div>  
                                            </div>
                                            <div className="btn_nextstep p-3 text-center">
                                                <div className="otp-btn"><button ref="7" id="7" type="button" onClick={() => this.verifyOTPClick(this.state.setOTP,this.state.resp.otP_Key_No)} className="btn btn-otpeditprofile btn-block"><FormattedMessage id="confirmotp" /></button></div>
                                                <div className="text-center text-gray mt-3"><FormattedMessage id="otpsix" /></div>
                                            </div>
                                            {this.state.modal}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </IntlProvider>
        )
    }
} 
