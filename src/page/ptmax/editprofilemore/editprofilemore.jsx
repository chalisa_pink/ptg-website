import React from 'react';
import '../../ptmax/ptmax.css';
import { registerAction } from '../../../_actions/registerActions'; 
import { maxcardActions } from '../../../_actions/maxcardActions';
import { PTsidebar } from '../PTsidebar';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
import SweetAlert from "react-bootstrap-sweetalert";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

var Cipher = require('aes-ecb');
var ERROR_USER_EMAIL = false

var disabled_block = 'form-control input_form disabled';
var entertain_remark_display = 'form-control input_form disabled';
var sport_remark_display = 'form-control input_form disabled';
var outside_remark_display = 'form-control input_form disabled';
var inside_remark_display = 'form-control input_form disabled';
export class editprofilemore extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: "block",
            show: false,
            modal: null,
            getProvince: {},
            getAmphure: {},
            getTumbon: {},
            getPostcode: {},
            getMaritalStatus: {},
            fields:{
                CARD_ID: "",
                FNAME_TH: "",
                LNAME_TH: "",
                GENDER_ID: "",
                BIRTH_DATE: "",
                MARITAL_STATUS: "",
                USER_EMAIL: "",
                PHONE_NO: "",
                ADDR_ADDRESS: "",
                ADDR_MOO: "",
                ADDR_VILLEGE: "",
                ADDR_SOI: "",
                ADDR_STREET: "",
                ADDR_ORG_PROVINCE_ID: "",
                ADDR_ORG_AMPHURE_ID: "",
                ADDR_ORG_SUB_DISTRICT_ID: "",
                ADDR_ORG_POSTALCODE_ID: "",
                ADDR_OTHER: "",
            },
            TITLE_NAME_REMARK: "none",
            errors: {},
            errorsFocus: {
                CARD_ID: "",
                FNAME_TH: "",
                LNAME_TH: "",
                GENDER_ID: "",
                BIRTH_DATE: "",
                MARITAL_STATUS: "",
                USER_EMAIL: "",
                PHONE_NO: "",
                ADDR_ADDRESS: "",
                ADDR_MOO: "",
                ADDR_VILLEGE: "",
                ADDR_SOI: "",
                ADDR_STREET: "",
                ADDR_ORG_PROVINCE_ID: "",
                ADDR_ORG_AMPHURE_ID: "",
                ADDR_ORG_SUB_DISTRICT_ID: "",
                ADDR_ORG_POSTALCODE_ID: "",
                ADDR_OTHER: "",
            },
            Profile: "",
            CAR_BRAND_REMARK: "none",
            OCCAPATION_REMARK: "none",
            forCheckDefaltACT_HNGOUT: true
        };
        this.getRow = this.getRow.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleGameClik(ev) {
        let { fields } = this.state;
        if(ev.target.name === "promotion") {
          if (ev.target.id === "PMT_OTHERS") {
            if(ev.target.checked === true) {
              disabled_block = 'form-control input_form'
              fields[ev.target.id] = ev.target.checked
            } else {
              disabled_block = 'form-control input_form disabled'
              fields['PMT_OTHERS_REMARK'] = ''
              fields[ev.target.id] = ev.target.checked
            }
          } else {
            if(fields['PMT_OTHERS'] === true) {
              disabled_block = 'form-control input_form'
            } else {
              disabled_block = 'form-control input_form disabled'
            //   fields['PMT_OTHERS_REMARK'] = ''
            }
            fields[ev.target.id] = ev.target.checked
          }
          this.setState( {disabled: (ev.target.checked === "" ? false : true)} )
        } else {
          fields[ev.target.id] = ev.target.value
        }
          this.setState({ fields })
      } 

    checboxClick(ev) {
        let { fields } = this.state;
        if(ev.target.name === "news") {
          fields[ev.target.id] = ev.target.checked
          if (ev.target.id === "SUBSCRIBE_NONE") {
            if(ev.target.checked === true) {
              fields['SUBSCRIBE_EMAIL'] = false
              fields['SUBSCRIBE_LETTER'] = false
              fields['SUBSCRIBE_SMS'] = false
            }
          } else {
            fields['SUBSCRIBE_NONE'] = false
          }
        }else if(ev.target.name === "activities"){
            
            this.setState({ forCheckDefaltACT_HNGOUT: false })
          fields[ev.target.id] = ev.target.checked
          if (ev.target.id === "ACT_HNGOUT_NO") {
            if(ev.target.checked === true) {
              fields['ACT_HNGOUT_FRIEND'] = false
              fields['ACT_HNGOUT_COUPLE'] = false
              fields['ACT_HNGOUT_FAMILY'] = false
            }
          } else {
            fields['ACT_HNGOUT_NO'] = false
          }
        }else {
          fields[ev.target.id] = ev.target.value
        }
        this.setState({ fields })
      }

    getRow(row) {
        this.setState({ selectedRow: row });
    }

    componentDidMount(){
        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
        var ProfileData = localStorage.getItem('Profile');
        var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
        ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        var ProfileArray = JSON.parse(ProfileDecrypt);
        var Profile = ProfileArray[0];
        
        if(localStorage.getItem('CardItem')){
            var CardItemData = localStorage.getItem('CardItem');
            var CardItemDecrypt = Cipher.decrypt(keyCipher, CardItemData);
            CardItemDecrypt = CardItemDecrypt.replace(/[\u0000-\u0019]+/g,""); 
            var CardItem = JSON.parse(CardItemDecrypt);

            if(Profile.customer_id === null){
                this.getCustomer(Profile.userId,CardItem);
            }else{
                this.getCartype(Profile.userId,CardItem,Profile.tkvsm,Profile.customer_id);
            }
            this.setState({ Profile , Profile })
            this.getDropdownlist_type("CAR_TYPE").then(result => {this.setState({ cartype: result.data.dropdowN_INFO })});
            this.getDropdownlist_type("CAR_BRAND").then(result => {this.setState({ car_brand: result.data.dropdowN_INFO })});
            this.getDropdownlist_type("OCCUPATION").then(result => {this.setState({ occupation: result.data.dropdowN_INFO })});
            this.getDropdownlist_type("EDUCATION").then(result => {this.setState({ education: result.data.dropdowN_INFO })});
            this.getDropdownlist_type("INCOME").then(result => {this.setState({ income: result.data.dropdowN_INFO })}); 
        }else{
            window.location.href = `${process.env.PUBLIC_URL}/PTmaxcard/register/nocard`;
        }
    }

    getCartype(userid , CardItem ,tokenId,customerId){
        var userid = userid;
        var cardmemberid = CardItem.carD_MEMBER_ID;
        maxcardActions.getCardList(userid).then(e => {
          if(e.data.isSuccess === true){
            var dataCard = e.data.data
            for(var i in dataCard){
              if(cardmemberid === dataCard[i].carD_MEMBER_ID){
                var car_type = dataCard[i].carD_CAR_TYPE_ID
                var car_brand = dataCard[i].caR_BRAND
                var car_brand_remark = dataCard[i].caR_BRAND_REMARK
              }else{
                // console.log(false)
              }
            } 
          }else{
            // console.log(false)
          }
          this.GetCustomerProfile(tokenId, customerId, userid , car_type , car_brand , car_brand_remark);
          this.setState ({ CAR_TYPE : car_type , CAR_BRAND : car_brand , CAR_BRAND_REMARK : car_brand_remark })
        });
    }

    getCustomer(userid,carditem){
        maxcardActions.getCustomerId(userid).then(e => {
            if(e.data.isSuccess === true){
                this.getCartype(userid,carditem,e.data.data.tokenId,e.data.data.customerId);
            }
        })
    }
    
    getDropdownlist_type(title) {
      return registerAction.getDropdownlist(title);
    }

    GetCustomerProfile(tkvsm, customer_id,userId,car_type,car_brand,car_remark){
        let { fields , CAR_BRAND_REMARK , OCCAPATION_REMARK } = this.state;
        var formData = new FormData();
        formData.append('TOKEN_ID', tkvsm)
        formData.append('CUSTOMER_ID' , customer_id)
        formData.append('userId' , userId)
        formData.append('udId' , "")
        formData.append('deviceOs' , "WEBSITE")
        setTimeout(() => {
            registerAction.GetCustomerProfile(formData).then(e => {
                if(e.data.isSuccess == true){
                    var resp = JSON.parse(e.data.data.data);
                    var info = resp.CUSTOMER_PROFILE_INFO[0];
                    this.setState({ 
                        fields: {
                            ...this.state.fields,                          
                            ABOUT_SOURCE: info.ABOUT_SOURCE,
                            ACT_EN_CONCERT: info.ACT_EN_CONCERT,
                            ACT_EN_COUNTRY_CONCERT: info.ACT_EN_COUNTRY_CONCERT,
                            ACT_EN_MOVIES: info.ACT_EN_MOVIES,
                            ACT_EN_MUSIC: info.ACT_EN_MUSIC,
                            ACT_EN_OTHERS: info.ACT_EN_OTHERS,
                            ACT_EN_OTHERS_REMARK: info.ACT_EN_OTHERS_REMARK,
                            ACT_EN_THEATER: info.ACT_EN_THEATER,
                            ACT_HNGOUT_COUPLE: info.ACT_HNGOUT_COUPLE,
                            ACT_HNGOUT_FAMILY: info.ACT_HNGOUT_FAMILY,
                            ACT_HNGOUT_FRIEND: info.ACT_HNGOUT_FRIEND,
                            ACT_HNGOUT_NO: info.ACT_HNGOUT_NO,
                            ACT_ID_BAKING: info.ACT_ID_BAKING,
                            ACT_ID_COOKING: info.ACT_ID_COOKING,
                            ACT_ID_DECORATE: info.ACT_ID_DECORATE,
                            ACT_ID_GAMING: info.ACT_ID_GAMING,
                            ACT_ID_GARDENING: info.ACT_ID_GARDENING,
                            ACT_ID_INTERNET: info.ACT_ID_INTERNET,
                            ACT_ID_OTHERS: info.ACT_ID_OTHERS,
                            ACT_ID_OTHERS_REMARK: info.ACT_ID_OTHERS_REMARK,
                            ACT_ID_READING: info.ACT_ID_READING,
                            ACT_OD_ABROAD: info.ACT_OD_ABROAD,
                            ACT_OD_ADVANTURE: info.ACT_OD_ADVANTURE,
                            ACT_OD_BEAUTY: info.ACT_OD_BEAUTY,
                            ACT_OD_CLUB: info.ACT_OD_CLUB,
                            ACT_OD_HOROSCOPE: info.ACT_OD_HOROSCOPE,
                            ACT_OD_MOMCHILD: info.ACT_OD_MOMCHILD,
                            ACT_OD_OTHERS: info.ACT_OD_OTHERS,
                            ACT_OD_OTHERS_REMARK: info.ACT_OD_OTHERS_REMARK,
                            ACT_OD_PHILANTHROPY: info.ACT_OD_PHILANTHROPY,
                            ACT_OD_PHOTO: info.ACT_OD_PHOTO,
                            ACT_OD_RALLY: info.ACT_OD_RALLY,
                            ACT_OD_RESTAURANT: info.ACT_OD_RESTAURANT,
                            ACT_OD_SHOPPING: info.ACT_OD_SHOPPING,
                            ACT_OD_UPCOUNTRY: info.ACT_OD_UPCOUNTRY,
                            ACT_SPT_CYCLING: info.ACT_SPT_CYCLING,
                            ACT_SPT_FITNESS: info.ACT_SPT_FITNESS,
                            ACT_SPT_FOOTBALL: info.ACT_SPT_FOOTBALL,
                            ACT_SPT_OTHERS: info.ACT_SPT_OTHERS,
                            ACT_SPT_OTHERS_REMARK: info.ACT_SPT_OTHERS_REMARK,
                            ACT_SPT_RUN: info.ACT_SPT_RUN,
                            ADDR_ADDRESS: info.ADDR_ADDRESS,
                            ADDR_MOO: info.ADDR_MOO,
                            ADDR_ORG_AMPHURE_ID: info.ADDR_ORG_AMPHURE_ID.toUpperCase(),
                            ADDR_ORG_POSTALCODE_ID: info.ADDR_ORG_POSTALCODE_ID.toUpperCase(),
                            ADDR_ORG_PROVINCE_ID: info.ADDR_ORG_PROVINCE_ID.toUpperCase(),
                            ADDR_ORG_SUB_DISTRICT_ID: info.ADDR_ORG_SUB_DISTRICT_ID.toUpperCase(),
                            ADDR_OTHER: info.ADDR_OTHER,
                            ADDR_SOI: info.ADDR_SOI,
                            ADDR_STREET: info.ADDR_STREET,
                            ADDR_VILLEGE: info.ADDR_VILLEGE,
                            BIRTH_DATE: info.BIRTH_DATE,
                            CARD_ID: info.CARD_ID,
                            CARD_TYPE_ID: info.CARD_TYPE_ID,
                            CUSTOMER_ID: info.CUSTOMER_ID,
                            CUSTOMER_STATUS: info.CUSTOMER_STATUS,
                            EDUCATION_ID: info.EDUCATION_ID,
                            FNAME_TH: info.FNAME_TH,
                            FULL_ADDRESS: info.FULL_ADDRESS,
                            FULL_NAME_TH: info.FULL_NAME_TH,
                            GENDER_ID: info.GENDER_ID,
                            INCOME_ID: info.INCOME_ID,
                            LNAME_TH: info.LNAME_TH,
                            MARITAL_STATUS: info.MARITAL_STATUS,
                            OCCAPATION_REMARK: info.OCCAPATION_REMARK,
                            OCCUPATION_ID: info.OCCUPATION_ID,
                            PHONE_HOME: info.PHONE_HOME,
                            PHONE_NO: info.PHONE_NO,
                            PMT_BEAUTY: info.PMT_BEAUTY,
                            PMT_FOOD: info.PMT_FOOD,
                            PMT_OTHERS: info.PMT_OTHERS,
                            PMT_OTHERS_REMARK: info.PMT_OTHERS_REMARK,
                            PMT_SHOPPING: info.PMT_SHOPPING,
                            PMT_SPORT_AND_CAR: info.PMT_SPORT_AND_CAR,
                            PMT_TRAVEL: info.PMT_TRAVEL,
                            REF_CARD_NO: info.REF_CARD_NO,
                            REGIS_DATE: info.REGIS_DATE,
                            SUBSCRIBE_EMAIL: info.SUBSCRIBE_EMAIL,
                            SUBSCRIBE_LETTER: info.SUBSCRIBE_LETTER,
                            SUBSCRIBE_NONE: info.SUBSCRIBE_NONE,
                            SUBSCRIBE_SMS: info.SUBSCRIBE_SMS,
                            TITLE_NAME_ID: info.TITLE_NAME_ID,
                            TITLE_NAME_REMARK: info.TITLE_NAME_REMARK,
                            USER_EMAIL: info.USER_EMAIL,
                            ALREADY_EMAIL: info.USER_EMAIL,
                            CAR_TYPE : car_type,
                            CAR_BRAND : car_brand,
                            CAR_BRAND_REMARK : car_remark,
                            forCheckDefaltACT_HNGOUT: true
                        }
                    })
                    if(fields['CAR_BRAND'] == 9 || car_brand == 9){
                        CAR_BRAND_REMARK = "block";
                    }else{
                        CAR_BRAND_REMARK = "none";
                    }
                    if (fields.CAR_BRAND === 9) {
                        CAR_BRAND_REMARK = "block";
                    }
                    if (info.OCCUPATION_ID === 5) {
                        OCCAPATION_REMARK = "block";
                        this.setState({ OCCAPATION_REMARK })
                    }
                    this.getMaritalStatus(info.MARITAL_STATUS)
                } else {
                    this.setState({ showLoading : 'none'})
                }
            })
            this.setState({ showLoading : 'none' , fields , CAR_BRAND_REMARK})
        },500);
    }

    handleGameClick(ev){
        let { fields } = this.state;
        if(ev.target.name === "entertain") {
          if (ev.target.id === "ACT_EN_OTHERS") {
            if(ev.target.checked === true) {
              entertain_remark_display = 'form-control input_form'
              fields[ev.target.id] = ev.target.checked
            } else {
              entertain_remark_display = 'form-control input_form disabled'
              fields['ACT_EN_OTHERS_REMARK'] = ''
              fields[ev.target.id] = ev.target.checked
            }
          } else {
            if(fields['ACT_EN_OTHERS'] === true) {
              entertain_remark_display = 'form-control input_form'
            } else {
              entertain_remark_display = 'form-control input_form disabled'
            //   fields['ACT_EN_OTHERS_REMARK'] = ''
            }
            fields[ev.target.id] = ev.target.checked
          }
          this.setState( {disabled_entertain: (ev.target.checked === "" ? false : true)} )
        } else if(ev.target.name === "sport") {
          if (ev.target.id === "ACT_SPT_OTHERS") {
            if(ev.target.checked === true) {
              sport_remark_display = 'form-control input_form'
              fields[ev.target.id] = ev.target.checked
            } else {
              sport_remark_display = 'form-control input_form disabled'
              fields['ACT_SPT_OTHERS_REMARK'] = ''
              fields[ev.target.id] = ev.target.checked
            }
          } else {
            if (fields['ACT_SPT_OTHERS'] === true) {
              sport_remark_display = 'form-control input_form'
            } else {
              sport_remark_display = 'form-control input_form disabled'
            //   fields['ACT_SPT_OTHERS_REMARK'] = ''
            }
            fields[ev.target.id] = ev.target.checked
          }
          this.setState( {disabled_sport: (ev.target.checked === "" ? false : true)} )
        } else if(ev.target.name === "outside") {
          if (ev.target.id === "ACT_OD_OTHERS") {
            if(ev.target.checked === true) {
              outside_remark_display = 'form-control input_form'
              fields[ev.target.id] = ev.target.checked
            } else {
              outside_remark_display = 'form-control input_form disabled'
              fields['ACT_OD_OTHERS_REMARK'] = ''
              fields[ev.target.id] = ev.target.checked
            }
          } else {
            if (fields['ACT_OD_OTHERS'] === true){
              outside_remark_display = 'form-control input_form'
            } else {
              outside_remark_display = 'form-control input_form disabled'
            //   fields['ACT_OD_OTHERS_REMARK'] = ''
            }
            fields[ev.target.id] = ev.target.checked
          }  
          this.setState( {disabled_outside: (ev.target.checked === "" ? false : true)} )
        } else if(ev.target.name === "inside") {
          if (ev.target.id === "ACT_ID_OTHERS") {
            if(ev.target.checked === true) {
              inside_remark_display = 'form-control input_form'
              fields[ev.target.id] = ev.target.checked
            } else {
              inside_remark_display = 'form-control input_form disabled'
              fields['ACT_ID_OTHERS_REMARK'] = ''
              fields[ev.target.id] = ev.target.checked
            }
          } else {
            if ( fields['ACT_ID_OTHERS'] === true) {
              inside_remark_display = 'form-control input_form'
            } else {
              inside_remark_display = 'form-control input_form disabled'
            //   fields['ACT_ID_OTHERS_REMARK'] = ''
            }
            fields[ev.target.id] = ev.target.checked
          }  
          this.setState( {disabled_inside: (ev.target.checked === "" ? false : true)} )
        } else if(ev.target.name === "inside") { 
          fields['ACT_HNGOUT_FRIEND'] = false
          fields['ACT_HNGOUT_COUPLE'] = false
          fields['ACT_HNGOUT_FAMILY'] = false
          fields['ACT_HNGOUT_NO'] = false
          fields[ev.target.id] = ev.target.checked
        } else {
          fields[ev.target.id] = ev.target.value
        }
        this.setState({ fields })
      }

      dropdownlistCheck(e) {
        let { errors, errorsFocus ,fields, CAR_BRAND_REMARK, OCCAPATION_REMARK } = this.state;
        if(e.target.name === "CAR_BRAND") {
          if(e.target.value === '9'){
            CAR_BRAND_REMARK = 'block'
          } else {
            CAR_BRAND_REMARK = 'none'
            fields["CAR_BRAND_REMARK"] = '';
            errors["CAR_BRAND_REMARK"]  = null;
            errorsFocus["CAR_BRAND_REMARK"]  = ''
          }
          errors[e.target.name] = null;
          fields[e.target.name] = parseInt(e.target.value)
          errorsFocus[e.target.name] = ''
        }else if(e.target.name === "OCCUPATION_ID") {
          if(e.target.value === "5"){
            OCCAPATION_REMARK = 'block'
          } else {
            OCCAPATION_REMARK = 'none'
            fields["OCCAPATION_REMARK"] = '';
            errors["OCCAPATION_REMARK"]  = null;
            errorsFocus["OCCAPATION_REMARK"]  = ''
          }
          errors[e.target.name] = null;
          fields[e.target.name] = parseInt(e.target.value)
          errorsFocus[e.target.name] = ''
        } else {
          fields[e.target.name] = e.target.value;
          errors[e.target.name] = null;
          errorsFocus[e.target.name] = ''
        }
        this.setState({ errors, errorsFocus, fields, CAR_BRAND_REMARK, OCCAPATION_REMARK})
      }

    getMaritalStatus(MARITAL_STATUS){
        var status = "MARITAL_STATUS"
        registerAction.getDropdownlist(status).then(e => {
            var resp = e.data.dropdowN_INFO
            var getTITLE_NAME_ID = resp.find(o => o.codE_GUID === MARITAL_STATUS ? MARITAL_STATUS.toUpperCase() : MARITAL_STATUS);
            this.setState({ 
                getMaritalStatus : resp,
                getTITLE_NAME_IDCustomerProfile : getTITLE_NAME_ID.values
            })
        })
    }
    
    handleChange = (e) => {
        const input = e.target;
        const value = input.value;
        let { fields , errors , errorsFocus } = this.state;
        fields[e.target.name] = e.target.value;
        errors[e.target.name] = null;
        errorsFocus[e.target.name] = ''
        this.setState({ [input.name]: value , errors });
    };

    handleSubmit(e) {
        e.preventDefault();
        this.setState({ submitted: true });
        this.updateProfile()
    }

    validateForm(){
        let fields = this.state.fields;
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;
    
        if(!fields["CARD_ID"]){
            formIsValid = false;
            errors["CARD_ID"] = "ALERT_CARD_ID"
            errorsFocus["CARD_ID"] = 'errorFocus'
        }
        if(!fields["FNAME_TH"]){
            formIsValid = false;
            errors["FNAME_TH"] = "ALERT_FNAME_TH"
            errorsFocus["FNAME_TH"] = 'errorFocus'
        }
        if(!fields["LNAME_TH"]){
            formIsValid = false;
            errors["LNAME_TH"] = "ALERT_LNAME_TH" 
            errorsFocus["LNAME_TH"] = 'errorFocus'
        }
        if(!fields["GENDER_ID"]){
            formIsValid = false;
            errors["GENDER_ID"] = "ALERT_GENDER_ID"
            errorsFocus["GENDER_ID"] = 'errorFocus'
        }
        if(!fields["BIRTH_DATE"]){
            formIsValid = false;
            errors["BIRTH_DATE"] = "ALERT_BIRTH_DATE"
            errorsFocus["BIRTH_DATE"] = 'errorFocus'
        }
        if(!fields["MARITAL_STATUS"]){
            formIsValid = false;
            errors["MARITAL_STATUS"] = "ALERT_MARITAL_STATUS"
            errorsFocus["MARITAL_STATUS"] = 'errorFocus'
        }
        if(!fields["PHONE_NO"]){
            formIsValid = false;
            errors["PHONE_NO"] = "ALERT_PHONE_NO"
            errorsFocus["PHONE_NO"] = 'errorFocus'
        }
        if(!fields["ADDR_ADDRESS"]){
            formIsValid = false;
            errors["ADDR_ADDRESS"] = "ALERT_ADDR_ADDRESS"
            errorsFocus["ADDR_ADDRESS"] = 'errorFocus'
        }
        if(!fields["ADDR_ORG_PROVINCE_ID"]){
            formIsValid = false;
            errors["ADDR_ORG_PROVINCE_ID"] = "ALERT_ADDR_ORG_PROVINCE_ID" 
            errorsFocus["ADDR_ORG_PROVINCE_ID"] = 'errorFocus'
        }
        if(!fields["ADDR_ORG_AMPHURE_ID"]){
            formIsValid = false;
            errors["ADDR_ORG_AMPHURE_ID"] = "ALERT_ADDR_ORG_AMPHURE_ID"
            errorsFocus["ADDR_ORG_AMPHURE_ID"] = 'errorFocus'
        }
        if(!fields["ADDR_ORG_SUB_DISTRICT_ID"]){
            formIsValid = false;
            errors["ADDR_ORG_SUB_DISTRICT_ID"] = "ALERT_ADDR_ORG_SUB_DISTRICT_ID"
            errorsFocus["ADDR_ORG_SUB_DISTRICT_ID"] = 'errorFocus'
        } 
        if(!fields["ADDR_ORG_POSTALCODE_ID"]){
            formIsValid = false;
            errors["ADDR_ORG_POSTALCODE_ID"] = "ALERT_ADDR_ORG_POSTALCODE_ID"
            errorsFocus["ADDR_ORG_POSTALCODE_ID"] = 'errorFocus'
        } 
        if(!fields["ADDR_OTHER"]){
            formIsValid = false;
            errors["ADDR_OTHER"] = "ALERT_ADDR_OTHER" 
            errorsFocus["ADDR_OTHER"] = 'errorFocus'
        } 
        if(fields["ALREADY_EMAIL"] == fields["USER_EMAIL"]){
            var validEmail = this.validateEmail(fields["USER_EMAIL"]);
            if (validEmail === false) {
                formIsValid = false;
                errors["USER_EMAIL"] = "ALERT_USER_EMAIL"
                errorsFocus["USER_EMAIL"] = 'errorFocus'
            }
        } else {
            var validEmail = this.validateEmail(fields["USER_EMAIL"]);
            if (validEmail === false) {
                formIsValid = false;
                errors["USER_EMAIL"] = "ALERT_USER_EMAIL"
                errorsFocus["USER_EMAIL"] = 'errorFocus'
            } else {
                var chkValidEmail = this.checkEmail();
                if (chkValidEmail === true) {
                    formIsValid = false;
                    errors["USER_EMAIL"] = "ALERT_USER_EMAIL"
                    errorsFocus["USER_EMAIL"] = 'errorFocus'
                }
            }
        }

        this.setState({
            errors: errors,
            errorsFocus: errorsFocus
        })
        return formIsValid;
    }

    updateProfile(){
        const { fields, Profile } = this.state
        var customer_id = Profile.customer_id;
        var tkvsm = Profile.tkvsm;
        var userId = Profile.userId
        // if(this.validateForm()){
            // const { fields, Profile } = this.state
            // this.props.history.push({
            //     pathname:`${process.env.PUBLIC_URL}/PTmaxcard/otpeditprfile`,
            //     state: {
            //         fieldsEditProfile: fields
            //     }
            // });
        // }
        registerAction.updateAdditionalProfile(fields, tkvsm, customer_id, userId).then(e => {
            if(e.data.isSuccess === true){
                var imgPopup = `${process.env.PUBLIC_URL}/images/checked.png`;
                var msg = e.data.errMsg
                this.modalCheckSubmit(msg,imgPopup, true)
            }else{
                var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
                var msg = e.data.errMsg
                this.modalCheckSubmit(msg,imgPopup, false)
            }
        })
    }

    modalCheckSubmit(msg,img, check){
        alert = (
            <SweetAlert
              custom
              showCloseButton
              closeOnClickOutside={false}
              focusConfirmBtn={false}
              title=""
              customIcon={img}
              showConfirm={false}
              showCancelButton
              onCancel={() => this.handleChoice(check)}
              onConfirm={() => this.handleSuccess(check)}
            >
              <div className="iconClose" onClick={() => this.handleChoice(check)}></div>
              <div className="fontSizeCase">{msg}</div>
            </SweetAlert>
          );
          this.setState({ show: true, modal: alert });
    }

    handleChoice(bool){
        if(bool == true){
            this.setState({ modal : null , show : false })
            window.location.href = `${process.env.PUBLIC_URL}/PTmaxcard`;
        }else{
            this.setState({ modal : null , show : false })
        }
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    checkEmail(email){
        registerAction.checkEmailExist(email).then(e => {
            var resp = e.data.responsE_INFO;
            if (resp.rescode !== "000") {
              ERROR_USER_EMAIL = false
            } else {
              ERROR_USER_EMAIL = true
            }
            return ERROR_USER_EMAIL
        });
    }

  

    renderAdditional(type) {
        const { occupation, education, income, cartype, car_brand} = this.state;
        var option = []
        var name_title;
        if (type === 'type_cartype') {
          name_title = cartype 
        } else if (type === 'type_brand') {
            name_title = car_brand 
        } else if (type === 'type_occupation') {
              name_title = occupation 
        } else if (type === 'type_education'){
          name_title = education 
        } else {
          name_title = income 
        }
        for(var i in name_title) {
          option.push(<option key={i} value={name_title[i].code}>{name_title[i].values}</option>)
        }
        return option
    }

    setupLogout(){
        // localStorage.clear();
        window.location.href=`${process.env.PUBLIC_URL}/home`
        // var that = this;
        // auth().signOut().then(function() {
        //   that.setState({user: null});
        // }).catch(function(error) {
        //     console.log(error)
        // });
    }

    render(){
        var Profile = this.state.Profile;
        var setLangeditprofilemore = messages[this.state.language].editprofilemore;
        if(this.state.forCheckDefaltACT_HNGOUT == true){
            if(this.state.fields['ACT_HNGOUT_NO'] == 0){
                var ACT_HNGOUT_NO = false
            } else {
                var ACT_HNGOUT_NO = true
            }
        } else {
            if(this.state.fields['ACT_HNGOUT_NO'] === true){
                var ACT_HNGOUT_NO = true
            } else {
                var ACT_HNGOUT_NO = false
            }
        }
        
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].editprofilemore}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="bg-ptmaxcard-content">
                            <div className="ptmaxcard-content">
                                <div className="row">
                                    <div className="col-lg-3 col-md-5 col-sm-6">
                                        <PTsidebar/>
                                    </div>
                                    <div className="col-lg-9 col-md-7 col-sm-6">
                                        
                                        <div className="editprofile-title py-3"><FormattedMessage id="editmore" /></div>
                                            <div className="editprofile-content py-3">
                                                <form onSubmit={(e) => this.handleSubmit(e)}>
                                                    <div className="row">
                                                        <div className="col-sm-12 col-md-12 col-sm-6">
                                                            
                                                            <div className="form-group">
                                                                <label className="addtional"><FormattedMessage id="cartyp" /></label>
                                                                <select className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['CAR_TYPE']}`}
                                                                    name="CAR_TYPE" 
                                                                    id='exampleFormControlSelect1'
                                                                    value={this.state.fields['CAR_TYPE']}
                                                                    onChange={(e) => this.dropdownlistCheck(e)}
                                                                >
                                                                    <option value="0">{setLangeditprofilemore.cartypeselet}</option>
                                                                    {this.renderAdditional('type_cartype')}
                                                                </select>
                                                                <div className="errorMsg CAR_TYPE">{this.state.errors["CAR_TYPE"]}</div>
                                                            </div>
                                                            <div className="form-group">
                                                                <label className="addtional"><FormattedMessage id="brand" /></label>
                                                                <select 
                                                                    className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['CAR_BRAND']}`} 
                                                                    name="CAR_BRAND" 
                                                                    value={this.state.fields['CAR_BRAND']}
                                                                    id='exampleFormControlSelect1' 
                                                                    onChange={(e) => this.dropdownlistCheck(e)}
                                                                >
                                                                    <option value="0">{setLangeditprofilemore.brandselect}</option>
                                                                    {this.renderAdditional('type_brand')}
                                                                </select>
                                                                <div className="errorMsg CAR_BRAND">{this.state.errors['CAR_BRAND']}</div>
                                                            </div>
                                                            <div className="form-group" style={{ display: this.state.CAR_BRAND_REMARK}}>
                                                                <input
                                                                    type="text"
                                                                    className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['CAR_BRAND_REMARK']} input_form`}
                                                                    name="CAR_BRAND_REMARK"
                                                                    value={this.state.fields['CAR_BRAND_REMARK']}
                                                                    onChange={e => this.dropdownlistCheck(e)}
                                                                    id="exampleFormControlInput1"
                                                                    placeholder={setLangeditprofilemore.brandinput}
                                                                />
                                                                <div className="errorMsg CAR_BRAND_REMARK">{this.state.errors['CAR_BRAND_REMARK']}</div>
                                                            </div>
                                                            <div className="form-group">
                                                                <label className="addtional"><FormattedMessage id="occapation" /></label>
                                                                <select 
                                                                    className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['OCCUPATION_ID']}`}
                                                                    name="OCCUPATION_ID" 
                                                                    value={this.state.fields['OCCUPATION_ID']}
                                                                    id='exampleFormControlSelect1' 
                                                                    onChange={(e) => this.dropdownlistCheck(e)}
                                                                >
                                                                    <option value="0" >{setLangeditprofilemore.occapationselect}</option>
                                                                    {this.renderAdditional('type_occupation')}
                                                                </select>
                                                                <div className="errorMsg OCCUPATION_ID">{this.state.errors['OCCUPATION_ID']} </div>
                                                            </div>
                                                            <div className="form-group" style={{ display: this.state.OCCAPATION_REMARK}}>
                                                                <input
                                                                    type="text"
                                                                    className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['OCCAPATION_REMARK']} input_form`}
                                                                    name="OCCAPATION_REMARK"
                                                                    value={this.state.fields['OCCAPATION_REMARK']}
                                                                    onChange={e => this.dropdownlistCheck(e)}
                                                                    id="exampleFormControlInput1"
                                                                    placeholder={setLangeditprofilemore.occapationinput}
                                                                />
                                                                <div className="errorMsg OCCAPATION_REMARK">{this.state.errors['OCCAPATION_REMARK']}</div> 
                                                            </div>
                                                            <div className="form-group">
                                                                <label className="addtional"><FormattedMessage id="education" /></label>
                                                                <select 
                                                                    className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['EDUCATION_ID']}`}
                                                                    name="EDUCATION_ID" 
                                                                    value={this.state.fields['EDUCATION_ID']}
                                                                    id='exampleFormControlSelect1' 
                                                                    onChange={(e) => this.dropdownlistCheck(e)}
                                                                >
                                                                    <option value="0">{setLangeditprofilemore.educationselect}</option>
                                                                    {this.renderAdditional('type_education')}
                                                                </select>
                                                                <div className="errorMsg EDUCATION_ID">{this.state.errors["EDUCATION_ID"]} </div>
                                                            </div>
                                                            <div className="form-group">
                                                                <label className="addtional"><FormattedMessage id="income" /></label>
                                                                <select  
                                                                    className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['INCOME_ID']}`}
                                                                    name="INCOME_ID" 
                                                                    value={this.state.fields['INCOME_ID']}
                                                                    id='exampleFormControlSelect1' 
                                                                    onChange={(e) => this.dropdownlistCheck(e)}
                                                                >
                                                                    <option value="0">{setLangeditprofilemore.incomeselect}</option>
                                                                    {this.renderAdditional('type_income')}
                                                                </select>
                                                                <div className="errorMsg INCOME_ID">{this.state.errors["INCOME_ID"]}</div>
                                                            </div>
                                                            {/* <label className="addtional">ท่านยินดีรับข่าวสาร/โปรโมชั่นจากทางใด(เลือกได้มากกว่า 1 ข้อ)</label>
                                                            <div className="row">
                                                                <div className="col">
                                                                    <label className="check_conditions_select">จดหมาย
                                                                        <input type="checkbox" id="SUBSCRIBE_LETTER" name="news"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_LETTER']) ? (this.state.fields['SUBSCRIBE_LETTER'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false} />
                                                                        <span className="checkmark_condition_select"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="check_conditions_select">อีเมล
                                                                        <input type="checkbox" id="SUBSCRIBE_EMAIL" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_EMAIL']) ? (this.state.fields['SUBSCRIBE_EMAIL'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false} />
                                                                        <span className="checkmark_condition_select"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                    <div className="col">
                                                                    <label className="check_conditions_select">ข้อความทางโทรศัพท์มือถือ (SMS)
                                                                        <input type="checkbox" id="SUBSCRIBE_SMS" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_SMS']) ? (this.state.fields['SUBSCRIBE_EMAIL'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false} />
                                                                        <span className="checkmark_condition_select"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="check_conditions_select">ไม่ประสงค์รับข่าวสาร
                                                                        <input type="checkbox" id="SUBSCRIBE_NONE" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_NONE']) ? (this.state.fields['SUBSCRIBE_NONE'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false } />
                                                                        <span className="checkmark_condition_select"></span>
                                                                    </label>
                                                                </div>
                                                            </div> */}
                                                            <label className="addtional"><FormattedMessage id="pmtpromotion" /></label>
                                                            <div className="row row-no-padding">
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="pmtfood" />
                                                                        <input type="checkbox" id="PMT_FOOD" checked={this.state.fields['PMT_FOOD']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="pmttravel" />
                                                                        <input type="checkbox" id="PMT_TRAVEL" checked={this.state.fields['PMT_TRAVEL']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="pmtshopping" />
                                                                        <input type="checkbox" id="PMT_SHOPPING" checked={this.state.fields['PMT_SHOPPING']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="pmtbeauty" />
                                                                        <input type="checkbox" id="PMT_BEAUTY" checked={this.state.fields['PMT_BEAUTY']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="pmtsport" />
                                                                        <input type="checkbox" id="PMT_SPORT_AND_CAR" checked={this.state.fields['PMT_SPORT_AND_CAR']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="pmtothers" />
                                                                        <input type="checkbox" id="PMT_OTHERS" checked={this.state.fields['PMT_OTHERS']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                    <div className="form-group">
                                                                        <input 
                                                                            type="text" 
                                                                            id="PMT_OTHERS_REMARK" 
                                                                            value={this.state.fields['PMT_OTHERS_REMARK']} 
                                                                            className={`${disabled_block} ptmaxcard-regis-input `} 
                                                                            placeholder={setLangeditprofilemore.actenotherremark}
                                                                            value={this.state.fields['PMT_OTHERS_REMARK']}  
                                                                            onChange={(e) => this.handleGameClik(e)} 
                                                                        />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <label className="addtional"><FormattedMessage id="actenactention" /></label>
                                                            <div><label><FormattedMessage id="actenenter" /></label></div>
                                                            <div className="row row-no-padding">
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actenmusic" />
                                                                        <input type="checkbox" id="ACT_EN_MUSIC" checked={this.state.fields['ACT_EN_MUSIC']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actenconcert" />
                                                                        <input type="checkbox" id="ACT_EN_CONCERT" checked={this.state.fields['ACT_EN_CONCERT']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actencountry" />
                                                                        <input type="checkbox" id="ACT_EN_COUNTRY_CONCERT" checked={this.state.fields['ACT_EN_COUNTRY_CONCERT']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actenmovies" />
                                                                        <input type="checkbox" id="ACT_EN_MOVIES" checked={this.state.fields['ACT_EN_MOVIES']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actentheater" />
                                                                        <input type="checkbox" id="ACT_EN_THEATER" checked={this.state.fields['ACT_EN_THEATER']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actenothercheck" />
                                                                        <input type="checkbox" id="ACT_EN_OTHERS" checked={this.state.fields['ACT_EN_OTHERS']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                    <div className="form-group">
                                                                        <input 
                                                                            type="text" 
                                                                            id="ACT_EN_OTHERS_REMARK" 
                                                                            className={`${entertain_remark_display} ptmaxcard-regis-input `} 
                                                                            placeholder={setLangeditprofilemore.actenother}
                                                                            value={this.state.fields['ACT_EN_OTHERS_REMARK']}  
                                                                            onChange={(e) => this.handleGameClick(e)} 
                                                                        />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-sm-12 col-md-12 col-sm-6">
                                                            <label><FormattedMessage id="sport" /></label>
                                                            <div className="row row-no-padding">
                                                                <div className="col">
                                                                <label className="selectoneonly"><FormattedMessage id="sptfitness" />
                                                                    <input type="checkbox" id="ACT_SPT_FITNESS" checked={this.state.fields['ACT_SPT_FITNESS']} name="sport" onClick = {(e) => this.handleGameClick(e)} />
                                                                    <span className="checkmark_selectoneonly"></span>
                                                                </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="sptrun" />
                                                                        <input type="checkbox" id="ACT_SPT_RUN" checked={this.state.fields['ACT_SPT_RUN']} name="sport" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="sptcycling" />
                                                                        <input type="checkbox" id="ACT_SPT_CYCLING" checked={this.state.fields['ACT_SPT_CYCLING']} name="sport" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="sptfootball" />
                                                                        <input type="checkbox" id="ACT_SPT_FOOTBALL" checked={this.state.fields['ACT_SPT_FOOTBALL']} name="sport" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="sptother" />
                                                                        <input type="checkbox" id="ACT_SPT_OTHERS" checked={this.state.fields['ACT_SPT_OTHERS']} name="sport"  onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                    <div className="form-group">
                                                                        <input 
                                                                            type="text" 
                                                                            id="ACT_SPT_OTHERS_REMARK" 
                                                                            className={`${sport_remark_display} ptmaxcard-regis-input `} 
                                                                            placeholder={setLangeditprofilemore.otherremrk}
                                                                            value={this.state.fields['ACT_SPT_OTHERS_REMARK']}  
                                                                            onChange={(e) => this.handleGameClick(e)} 
                                                                        />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <label className="addtional"><FormattedMessage id="actoutdoor" /></label>
                                                            <div className="row row-no-padding">
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actshopping" />
                                                                        <input type="checkbox" id="ACT_OD_SHOPPING" checked={this.state.fields['ACT_OD_SHOPPING']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actbeauty" />
                                                                        <input type="checkbox" id="ACT_OD_BEAUTY" checked={this.state.fields['ACT_OD_BEAUTY']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actrestaurant" />
                                                                        <input type="checkbox" id="ACT_OD_RESTAURANT" checked={this.state.fields['ACT_OD_RESTAURANT']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actclub" />
                                                                        <input type="checkbox" id="ACT_OD_CLUB" checked={this.state.fields['ACT_OD_CLUB']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actmomchild" />
                                                                        <input type="checkbox" id="ACT_OD_MOMCHILD" checked={this.state.fields['ACT_OD_MOMCHILD']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actrally" />
                                                                        <input type="checkbox" id="ACT_OD_RALLY" checked={this.state.fields['ACT_OD_RALLY']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actadvanture" />
                                                                        <input type="checkbox" id="ACT_OD_ADVANTURE" checked={this.state.fields['ACT_OD_ADVANTURE']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actphoto" />
                                                                        <input type="checkbox" id="ACT_OD_PHOTO" checked={this.state.fields['ACT_OD_PHOTO']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actphilanthropy" />
                                                                        <input type="checkbox" id="ACT_OD_PHILANTHROPY" checked={this.state.fields['ACT_OD_PHILANTHROPY']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="acthoroscope" />
                                                                        <input type="checkbox" id="ACT_OD_HOROSCOPE" checked={this.state.fields['ACT_OD_HOROSCOPE']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actabroad" />
                                                                        <input type="checkbox" id="ACT_OD_ABROAD" checked={this.state.fields['ACT_OD_ABROAD']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actupcountry" />
                                                                        <input type="checkbox" id="ACT_OD_UPCOUNTRY" checked={this.state.fields['ACT_OD_UPCOUNTRY']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actother" />
                                                                        <input type="checkbox" id="ACT_OD_OTHERS" checked={this.state.fields['ACT_OD_OTHERS']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                    <div className="form-group">
                                                                        <input type="text" id="ACT_OD_OTHERS_REMARK" className={`${outside_remark_display} ptmaxcard-regis-input `} placeholder={setLangeditprofilemore.otherremrk} value={this.state.fields['ACT_OD_OTHERS_REMARK']}  onChange={(e) => this.handleGameClick(e)} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <label className="addtional"><FormattedMessage id="act" /></label>
                                                            <div className="row row-no-padding">
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actcooking" />
                                                                        <input type="checkbox"  id="ACT_ID_COOKING" checked={this.state.fields['ACT_ID_COOKING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actbaking" />
                                                                        <input type="checkbox"  id="ACT_ID_BAKING" checked={this.state.fields['ACT_ID_BAKING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actdecorate" />
                                                                        <input type="checkbox"  id="ACT_ID_DECORATE" checked={this.state.fields['ACT_ID_DECORATE']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actgarden" />
                                                                        <input type="checkbox"  id="ACT_ID_GARDENING" checked={this.state.fields['ACT_ID_GARDENING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actreading" />
                                                                        <input type="checkbox"  id="ACT_ID_READING" checked={this.state.fields['ACT_ID_READING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actgame" />
                                                                        <input type="checkbox"  id="ACT_ID_GAMING" checked={this.state.fields['ACT_ID_GAMING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actinternet" />
                                                                        <input type="checkbox"  id="ACT_ID_INTERNET" checked={this.state.fields['ACT_ID_INTERNET']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="selectoneonly"><FormattedMessage id="actother" />
                                                                        <input type="checkbox"  id="ACT_ID_OTHERS" checked={this.state.fields['ACT_ID_OTHERS']} name="inside" value="inside_other" onClick = {(e) => this.handleGameClick(e)} />
                                                                        <span className="checkmark_selectoneonly"></span>
                                                                    </label>
                                                                    <div className="form-group">
                                                                        <input type="text" id="ACT_ID_OTHERS_REMARK" className={`${inside_remark_display} ptmaxcard-regis-input `} placeholder={setLangeditprofilemore.otherremrk} value={this.state.fields['ACT_ID_OTHERS_REMARK']}  onChange={(e) => this.handleGameClick(e)} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <label className="addtional"><FormattedMessage id="activities" /></label>
                                                            <div className="row row-no-padding">
                                                                <div className="col">
                                                                    <label className="check_conditions_select"><FormattedMessage id="activitiesfriend" />
                                                                        <input type="checkbox"  id="ACT_HNGOUT_FRIEND" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_FRIEND']) ? (this.state.fields['ACT_HNGOUT_FRIEND'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false}  />
                                                                            <span className="checkmark_condition_select"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="check_conditions_select"><FormattedMessage id="activitiesfamily" />
                                                                        <input type="checkbox"  id="ACT_HNGOUT_FAMILY" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_FAMILY']) ? (this.state.fields['ACT_HNGOUT_FAMILY'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false} />
                                                                        <span className="checkmark_condition_select"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="w-100"></div>
                                                                <div className="col">
                                                                    <label className="check_conditions_select"><FormattedMessage id="activitieslove" />
                                                                        <input type="checkbox"  id="ACT_HNGOUT_COUPLE" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_COUPLE']) ? (this.state.fields['ACT_HNGOUT_COUPLE'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false} />
                                                                            <span className="checkmark_condition_select"></span>
                                                                    </label>
                                                                </div>
                                                                <div className="col">
                                                                    <label className="check_conditions_select"><FormattedMessage id="noactivities" />
                                                                        <input type="checkbox"  id="ACT_HNGOUT_NO" name="activities"   onChange = {(e) => this.checboxClick(e)} checked={ACT_HNGOUT_NO} />
                                                                        <span className="checkmark_condition_select"></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 text-center py-4">
                                                            <div className="btn_nextstep">
                                                                <button 
                                                                    type="submit" 
                                                                    className="btn btn-block btn-editprofilesumit" 
                                                                >
                                                                    <FormattedMessage id="btnconfirm" />
                                                                </button>
                                                            </div>
                                                        </div>
                                                        {/* <div className="col-6"></div> */}
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.state.modal}
                </div>
            </IntlProvider>
        )
    }
}


