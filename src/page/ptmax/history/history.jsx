import React from 'react';
import '../../ptmax/ptmax.css';
import { getTransections } from '../../../_actions/transectionActions'; 
import { maxcardActions } from '../../../_actions/maxcardActions';
import moment from "moment-timezone";
import { PTsidebar } from '../PTsidebar';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
import 'moment/locale/th';

const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');

export class History extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            showLoadingContent: 'block',
            EarnHistory : {},
            month : moment().startOf('month').format('MM'),
            year : moment().startOf('year').format('YYYY'),
            Profile: "",
        };
    }
    componentDidMount(){
        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
        var ProfileData = localStorage.getItem('Profile')
        var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
        ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        var ProfileArray = JSON.parse(ProfileDecrypt);
        var Profile = ProfileArray[0];

        if(localStorage.getItem('CardItem')){
            var CardItemData = localStorage.getItem('CardItem')
            var CardItemDecrypt = Cipher.decrypt(keyCipher, CardItemData);
            CardItemDecrypt = CardItemDecrypt.replace(/[\u0000-\u0019]+/g,""); 
            var CardItem = JSON.parse(CardItemDecrypt);
            var carD_TYPE_ID = CardItem.carD_TYPE_ID
            this.setState({ 
                tabEarn : "active",
                tabBurn : "",
                tabTotal : "",
                EarnBurnSearch : "",
                userId: Profile.userId,
                customer_id: Profile.customer_id,
                tkvsm: Profile.tkvsm,
                type: "O",
                Profile: Profile
            });
            setTimeout(() => {
                this.getCard(Profile.userId, Profile.customer_id, Profile.tkvsm, carD_TYPE_ID);
            },1500)
            // this.closeNav();
        }else{
            window.location.href = `${process.env.PUBLIC_URL}/PTmaxcard/register/nocard`;
        }
        
    }

    getCard(userId, customer_id, tkvsm, carD_ID){
        setTimeout(() => {
            maxcardActions.getCardList(userId).then(e => {
                var resp = e.data.data
                var carD_TYPE_ID = carD_ID
                var respCard = resp.find(o => o.carD_TYPE_ID === carD_TYPE_ID ? carD_TYPE_ID.toUpperCase() : carD_TYPE_ID);
                var card_member_id = respCard.carD_MEMBER_ID
                this.setState({ card_member_id : card_member_id });
                this.getHistoryList("O", userId, customer_id, card_member_id, tkvsm)
            })
        },500)
    }
    // openNav() {
    //     document.getElementById("mySidenav").style.width = "300px";
    //     document.getElementById("main").style.marginLeft = "300px";
    // }
    // closeNav() {
    //     document.getElementById("mySidenav").style.width = "0";
    //     document.getElementById("main").style.marginLeft= "0";
    // }

    getHistoryList(type, userId, customer_id, card_member_id, tkvsm){
        this.setState({ 
            showLoading: 'none',
            showLoadingContent: 'block',
        })
        const { month, year } = this.state
        var startDate = moment([year, month - 1]).format('YYYY-MM-DD');
        var endDate = moment(startDate).endOf('month').format('YYYY-MM-DD');
        var formData = new FormData();
        formData.append("userId", userId);
        formData.append("TOKEN_ID", tkvsm);
        formData.append("CUSTOMER_ID", customer_id);
        formData.append("CARD_MEMBER_ID", card_member_id);
        formData.append("DATE_START", startDate);
        formData.append("DATE_END", endDate);
        formData.append("TRANS_TYPE", type);
        formData.append("PAGE_NO", "1");
        formData.append("PAGE_SIZE", "1000");
        setTimeout(() => {
            getTransections.getHistoryList(formData).then(e => {
                if(e.data.isSuccess === false){
                    this.setState({ 
                        EarnHistory : '',
                        showLoadingContent: 'none',
                    })
                } else {
                    var resp = e.data.data;
                    if(resp !== ""){
                        resp = resp.replace(/[\u0000-\u0019]+/g,""); 
                        var convertHistory = JSON.parse(resp);
                        this.setState({ 
                            EarnHistory: convertHistory,
                            showLoadingContent: 'none'
                        });
                    }else{
                        this.setState({ 
                            EarnHistory: '',
                            showLoadingContent: 'none'
                        });
                    }
                }
            })
            this.setState({ 
                showLoadingContent: 'none'
            });
        },2000)
    }

    onChangeDate = (event) => {
        const input = event.target;
        const value = input.value;
        this.setState({ [input.name]: value });
    };

    handleChangeTabs(tab){
        const { userId, customer_id, card_member_id, tkvsm } = this.state
        if(tab == 1){
            this.getHistoryList("O", userId, customer_id, card_member_id, tkvsm)
            this.setState({ 
                tabEarn : "active",
                tabBurn : "",
                tabTotal : "",
                EarnBurnSearch : "",
                type : "O",
                month : moment().startOf('month').format('MM'),
                year : moment().startOf('year').format('YYYY'),
                showLoadingContent: 'block',
            });
        } else if(tab == 2){
            this.getHistoryList("R", userId, customer_id, card_member_id, tkvsm)
            this.setState({ 
                tabEarn : "",
                tabBurn : "active",
                tabTotal : "",
                EarnBurnSearch : "",
                type : "R",
                month : moment().startOf('month').format('MM'),
                year : moment().startOf('year').format('YYYY'),
                showLoadingContent: 'block',
            });
        } else if(tab ==3){
            this.getHistoryList("E", userId, customer_id, card_member_id, tkvsm)
            this.setState({ 
                tabEarn : "",
                tabBurn : "",
                tabTotal : "active",
                EarnBurnSearch : "EarnBurnSearchdisplaynone",
                type : "E",
                month : moment().startOf('month').format('MM'),
                year : moment().startOf('year').format('YYYY'),
                showLoadingContent: 'block',
            });
        }
    }

    setupLogout(){
        // localStorage.clear();
        window.location.href=`${process.env.PUBLIC_URL}/home`;
        // var that = this;
        // auth().signOut().then(function() {
        //   that.setState({user: null});ƒ
        // }).catch(function(error) {
        //     console.log(error)
        // });
    }

    groupNormalPoint(data){
        let point = 0
        if(data.length > 0){
          for(let i in data){
            point += data[i].REWARD_POINT
          }
        }
        return point
    }

    groupOtherPoint(data){
        var result = [];
      
        data.reduce(function(res, value) {
          if (!res[value.POINT_TYPE_NAME]) {
            res[value.POINT_TYPE_NAME] = { POINT_TYPE_NAME: value.POINT_TYPE_NAME, REWARD_POINT: 0 };
            result.push(res[value.POINT_TYPE_NAME])
          }
          res[value.POINT_TYPE_NAME].REWARD_POINT += value.REWARD_POINT;
          return res
          
        }, {});
        return result
      }
      

    render(){
        const { EarnHistory } = this.state;
        var EarnHistoryCards = [];
        if(EarnHistory !== ""){
            var order_burn = EarnHistory.TRANS_REDEEM
            if(order_burn){
                EarnHistoryCards = order_burn.map((item) => (
                    <div className="row">
                        <div className="col-9">
                            <div className="name-history text-body-sub text-gray-ptg">{item.TRANS_PRODUCT} ({item.SHOP_NAME})</div>
                            <div className="name-history card-history-detail">{(this.state.language === 'th' ? moment(item.TRANS_DATE).add(543,'y').locale('th').format('LLL') : moment(item.TRANS_DATE).locale('en').format('LLL'))}</div>
                        </div>
                        <div className="col-3">
                            <div className="point-screen text-body-sub text-danger">- {this.groupNormalPoint((item.REDEEM_POINT_DETAIL).filter(e => e.POINT_TYPE_NAME == "Normal Point" || e.POINT_TYPE_NAME == "Special Point"))}</div>
                            <div className="estamp-history text-sub-detail pl-0">
                                {
                                    (this.groupOtherPoint((item.REDEEM_POINT_DETAIL).filter(e => e.POINT_TYPE_NAME != "Normal Point" && e.POINT_TYPE_NAME != "Special Point")).map((point) => {
                                        return (
                                          <div className="sub_date_history text-red"> {point.POINT_TYPE_NAME + " : "} - {point.REWARD_POINT} </div>
                                        )
                                    }))
                                }
                            </div>
                        </div>
                        <div className="col-12 card-history-hr my-3"></div>
                    </div>
                ))
            }


            var order_earn = EarnHistory.TRANS_ORDER
            if(order_earn){
                EarnHistoryCards = order_earn.map((item) => (
                    <div className="row">
                        <div className="col-9">
                            <div className="name-history text-body-sub text-gray-ptg">{item.TRANS_PRODUCT} ({item.SHOP_NAME})</div>
                            <div className="name-history card-history-detail">{(this.state.language === 'th' ? moment(item.TRANS_DATE).add(543,'y').locale('th').format('LLL') : moment(item.TRANS_DATE).locale('en').format('LLL'))}</div>
                        </div>
                        <div className="col-3">
                            <div className="point-screen text-body-sub">+ {this.groupNormalPoint((item.REWARD_DETAIL).filter(e => e.POINT_TYPE_NAME == "Normal Point" || e.POINT_TYPE_NAME == "Special Point"))}</div>
                            <div className="estamp-history text-sub-detail pl-0">
                                {
                                    (this.groupOtherPoint((item.REWARD_DETAIL).filter(e => e.POINT_TYPE_NAME != "Normal Point" && e.POINT_TYPE_NAME != "Special Point")).map((point) => {
                                        return (
                                        <div className="estamp-history text-sub-detail pl-0"> {point.POINT_TYPE_NAME + " : "} {point.REWARD_POINT} </div>
                                        )
                                    }))
                                }
                            </div>
                        </div>
                        <div className="col-12 card-history-hr my-3"></div>
                    </div>
                ))
            }
            
        } else {
            EarnHistoryCards.push(
                <div className="row mx-auto text-center py-2 t-20">
                    <div className="col-12">
                        <FormattedMessage id="nodatasearch" />
                    </div>
                </div>
            );
        }

        var selectYear = []
        var year = (new Date()).getFullYear();
        for (var i = 0; i <= 100; i++) {
            var sumyear = year - i
            selectYear.push(
                <option value={sumyear} selected={(this.state.year == sumyear ? true : false)}>{sumyear}</option>
            );
        }
        var Profile = this.state.Profile
        var setLangHistory = messages[this.state.language].History
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].History}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="bg-ptmaxcard-content">
                            <div className="ptmaxcard-content">
                                <div className="row">
                                    <div className="col-lg-3 col-md-6">
                                        <PTsidebar/>
                                    </div>
                                    <div className="col-lg-9 col-md-6">
                                        <div className="text-head editprofile-title py-3"><FormattedMessage id="history" /></div>
                                        <div className="ptmaxcard-regis-card-header">
                                            <ul className="nav nav-pills nav-justified" role="tablist">
                                                <li className="nav-item ptmaxcard-regis-nav-1">
                                                    <a onClick={() => this.handleChangeTabs(1)} className={`nav-link ${this.state.tabEarn} h-100`}><FormattedMessage id="historyeran" /></a>
                                                </li>
                                                <li className="nav-item ptmaxcard-regis-nav-2">
                                                    <a onClick={() => this.handleChangeTabs(2)} className={`nav-link ${this.state.tabBurn} h-100`}><FormattedMessage id="historyburn" /></a>
                                                </li>
                                                <li className="nav-item ptmaxcard-regis-nav-3">
                                                    <a onClick={() => this.handleChangeTabs(3)} className={`nav-link ${this.state.tabTotal} h-100`}><FormattedMessage id="expirdate" /></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="ptmaxcard-regis-card-body">
                                            <div className="loadingGifLoadmore mx-auto" style={{ display: this.state.showLoadingContent}}>
                                                <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                                            </div>
                                            <div style={{ display: (this.state.showLoadingContent === 'block' ? 'none' : 'block') }}>
                                                <div className="tab-content py-3 px-sm-0">
                                                    <div className={this.state.EarnBurnSearch + " text-center"}>
                                                        <div className="select-date text-body-main"><FormattedMessage id="selectdatatodo" /></div>
                                                        <select 
                                                            className="dropdown-history"
                                                            name="month" 
                                                            value={this.state.month} 
                                                            onChange={this.onChangeDate}
                                                        >
                                                            <option>{setLangHistory.mount}</option>
                                                            <option value="01" selected={(this.state.month == '01' ? true : false)}>{setLangHistory.m1}</option>
                                                            <option value="02" selected={(this.state.month == '02' ? true : false)}>{setLangHistory.m2}</option>
                                                            <option value="03" selected={(this.state.month == '03' ? true : false)}>{setLangHistory.m3}</option>
                                                            <option value="04" selected={(this.state.month == '04' ? true : false)}>{setLangHistory.m4}</option>
                                                            <option value="05" selected={(this.state.month == '05' ? true : false)}>{setLangHistory.m5}</option>
                                                            <option value="06" selected={(this.state.month == '06' ? true : false)}>{setLangHistory.m6}</option>
                                                            <option value="07" selected={(this.state.month == '07' ? true : false)}>{setLangHistory.m7}</option>
                                                            <option value="08" selected={(this.state.month == '08' ? true : false)}>{setLangHistory.m8}</option>
                                                            <option value="09" selected={(this.state.month == '09' ? true : false)}>{setLangHistory.m9}</option>
                                                            <option value="10" selected={(this.state.month == '10' ? true : false)}>{setLangHistory.m10}</option>
                                                            <option value="11" selected={(this.state.month == '11' ? true : false)}>{setLangHistory.m11}</option>
                                                            <option value="12" selected={(this.state.month == '12' ? true : false)}>{setLangHistory.m12}</option>
                                                        </select>
                                                        <select 
                                                            className="dropdown-history"
                                                            name="year" 
                                                            value={this.state.year} 
                                                            onChange={this.onChangeDate}
                                                        >
                                                            <option>{setLangHistory.year}</option>
                                                            {selectYear}
                                                        </select>
                                                        <div className="text-center">
                                                            <button onClick={() => this.getHistoryList(this.state.type, this.state.userId, this.state.customer_id, this.state.card_member_id, this.state.tkvsm)} className="search-text"><FormattedMessage id="btnsearch" /></button>
                                                        </div>
                                                    </div>
                                                    {EarnHistoryCards}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </IntlProvider>
        //     <div>
        //          <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
        //             <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
        //         </div>
        //         <div className="bg-ptmax" style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
        //             <div id="mySidenav" className="sidenav">
        //                 <div className="font-user-profile">
        //                     <img className="icon-width" src={`${process.env.PUBLIC_URL}/images/iconlogin-01.png`} />&nbsp;&nbsp;
        //                     Ptmaxpro09
        //                 </div>
        //                 <button onClick={() => this.closeNav()}>
        //                     <div className="closebtn">&times;</div>
        //                 </button>
        //                 <div className="block-navbar">
        //                     <a href={`${process.env.PUBLIC_URL}/ptmaxcard`}>บัตรพีที แมกซ์ ของฉัน</a>
        //                 </div>
        //                 <div className="block-navbar">
        //                     <a href="#">แก้ไขข้อมูลส่วนตัว</a>
        //                 </div>
        //                 <div className="block-navbar">
        //                     <a href="#">ประวัติการใช้งาน</a>
        //                 </div>
        //                 <div className="block-navbar">
        //                     <a href={`${process.env.PUBLIC_URL}/maxcard/report`}>แจ้งปัญหาการใช้งาน</a>
        //                 </div>
        //                 <div className="block-navbar">
        //                     <a href="#">เงื่อนไขและการบริการ</a>
        //                 </div>
        //                 <div className="block-navbar">
        //                     <a href="#">คำถามที่พบบ่อย</a>
        //                 </div>
        //                 <div className="block-navbar">
        //                     <a className="signout-font" href="#">ออกจากระบบ</a>
        //                 </div>
        //             </div>
        //             <div id="main">
        //                 <div className="headet-ptmaxcard">ประวัติการใช้งาน</div>
        //                 <span className="span-heading" style={{cursor:'pointer'}} onClick={() => this.openNav()}><i className="fas fa-chevron-left"></i>&nbsp;&nbsp; บัตรพีที แมกซ์ ของฉัน</span>
        //                 <div className="mt-3">
        //                 <nav>
        //                     <div className="nav nav-tabs nav-fill" id="nav-tab">
        //                         <a onClick={() => this.handleChangeTabs(1)} className={`nav-item nav-link ${this.state.tabEarn}`} id="nav-home-tab" >ประวัติการสะสมคะแนน</a>
        //                         <a onClick={() => this.handleChangeTabs(2)} className={`nav-item nav-link ${this.state.tabBurn}`} id="nav-profile-tab" data-toggle="tab">ประวัติการแลกคะแนน</a>
        //                         <a onClick={() => this.handleChangeTabs(3)} className={`nav-item nav-link ${this.state.tabTotal}`} id="nav-contact-tab" data-toggle="tab">คะแนนหมดอายุ</a>
        //                     </div>
        //                 </nav>

        //                 <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
        //                     <div id="history-earn" className="tab-pane fade show active box-history" role="tabpanel" aria-labelledby="history-earn">
        //                         <div className={this.state.EarnBurnSearch}>
        //                             <div className="select-date">เลือกวันที่ทำรายการ</div>
        //                             <select 
        //                                 className="dropdown-history"
        //                                 name="month" 
        //                                 value={this.state.month} 
        //                                 onChange={this.onChangeDate}
        //                             >
        //                                 <option>เดือน</option>
        //                                 <option value="01" selected={(this.state.month == '01' ? true : false)}>มกราคม</option>
        //                                 <option value="02" selected={(this.state.month == '02' ? true : false)}>กุมภาพันธ์</option>
        //                                 <option value="03" selected={(this.state.month == '03' ? true : false)}>มีนาคม</option>
        //                                 <option value="04" selected={(this.state.month == '04' ? true : false)}>เมษายน</option>
        //                                 <option value="05" selected={(this.state.month == '05' ? true : false)}>พฤษภาคม</option>
        //                                 <option value="06" selected={(this.state.month == '06' ? true : false)}>มิถุนายน</option>
        //                                 <option value="07" selected={(this.state.month == '07' ? true : false)}>กรกฎาคม</option>
        //                                 <option value="08" selected={(this.state.month == '08' ? true : false)}>สิงหาคม</option>
        //                                 <option value="09" selected={(this.state.month == '09' ? true : false)}>กันยายน</option>
        //                                 <option value="10" selected={(this.state.month == '10' ? true : false)}>ตุลาคม</option>
        //                                 <option value="11" selected={(this.state.month == '11' ? true : false)}>พฤศจิกายน</option>
        //                                 <option value="12" selected={(this.state.month == '12' ? true : false)}>ธันวาคม</option>
        //                             </select>
        //                             <select 
        //                                 className="dropdown-history"
        //                                 name="year" 
        //                                 value={this.state.year} 
        //                                 onChange={this.onChangeDate}
        //                             >
        //                                 <option>ปี</option>
        //                                 {selectYear}
        //                             </select>
        //                             <div className="text-center">
        //                                 <button onClick={() => this.getHistoryList(this.state.type, this.state.userId, this.state.customer_id, this.state.card_member_id, this.state.tkvsm)} className="search-text">ค้นหา</button>
        //                             </div>
        //                         </div>
        //                         {EarnHistoryCards}
        //                         <div className="next-arrow">
        //                             <i className="inextarrow fas fa-chevron-right"></i>
        //                         </div>
        //                     </div>
        //                 </div>
        //             </div>
        //         </div>
        //     </div>
        // </div>
        )
    }
}


