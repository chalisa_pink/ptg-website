import React from 'react';
import '../../../ptmax/ptmax.css';
import { getTransections } from '../../../../_actions/transectionActions'; 
import { maxcardActions } from '../../../../_actions/maxcardActions';
import moment from "moment-timezone";
import { PTsidebar } from '../../PTsidebar';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../../translations/en.json";
import intlMessageTH from "../../../../translations/th.json";
import 'moment/locale/th';

const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');

export class historycardList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            GetTransactionByCard : [],
            Profile: "",
            pageNo: 1,
            isHideLoadmoreList : true,
            showLoadingLoadmore: 'none',
        };
        this.GetTransactionByCard = this.GetTransactionByCard.bind(this);
    }

    componentDidMount(){
        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
        var ProfileData = localStorage.getItem('Profile')
        var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
        ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        var ProfileArray = JSON.parse(ProfileDecrypt);
        var Profile = ProfileArray[0];
        if(localStorage.getItem('CardItem')){
            var CardItemData = localStorage.getItem('CardItem')
            var CardItemDecrypt = Cipher.decrypt(keyCipher, CardItemData);
            CardItemDecrypt = CardItemDecrypt.replace(/[\u0000-\u0019]+/g,""); 
            var CardItem = JSON.parse(CardItemDecrypt);
            var carD_TYPE_ID = CardItem.carD_TYPE_ID
            this.setState({ 
                tabEarn : "active",
                tabBurn : "",
                tabTotal : "",
                EarnBurnSearch : "",
                userId: Profile.userId,
                customer_id: Profile.customer_id,
                tkvsm: Profile.tkvsm,
                type: "O",
                Profile: Profile
            });
            this.getCard(Profile.userId, carD_TYPE_ID);
        }else{
            window.location.href = `${process.env.PUBLIC_URL}/PTmaxcard/register/nocard`;
        }
    }

    getCard(userId, carD_ID){
        setTimeout(() => {
            maxcardActions.getCardList(userId).then(e => {
                var resp = e.data.data
                var carD_TYPE_ID = carD_ID
                var respCard = resp.find(o => o.carD_TYPE_ID === carD_TYPE_ID ? carD_TYPE_ID.toUpperCase() : carD_TYPE_ID);
                var card_member_id = respCard.carD_MEMBER_ID
                this.setState({ card_member_id : card_member_id });
                this.GetTransactionByCard(userId, card_member_id )
            })
        },500)
    }

    GetTransactionByCard(userId, card_member_id){
        var pageNo = this.state.pageNo
        if(pageNo != 1){ this.setState({ showLoadingLoadmore : 'block' }) }
        setTimeout(() => {
            var formData = new FormData();
            formData.append("userId", userId);
            formData.append("cardMemberId", card_member_id);
            formData.append("transeType", "R");
            formData.append("pageNo", pageNo);
            formData.append('udId' , "")
            formData.append('deviceOs' , "")
            maxcardActions.GetTransactionByCard(formData).then(e => {
                if(e.data.isSuccess === true){
                    this.setState({ pageNo: Number(pageNo) + 1 })
                    var dataList = this.state.GetTransactionByCard.concat(e.data.data.tranS_REDEEM);
                    this.setState({ 
                        GetTransactionByCard : dataList,
                        showLoading: 'none',
                        showLoadingLoadmore : 'none'
                    })
                }else{
                    this.setState({ 
                        showLoading: 'none', 
                        showLoadingLoadmore : 'none',
                        isHideLoadmoreList : false,
                    })
                }
            })
        },1000)
    }

    linkToDetail = (ev) => {
        this.setState({ showLoading : 'block' })
        var type = ev.currentTarget.dataset.type
        var id = ev.currentTarget.dataset.id
        var name = ev.currentTarget.dataset.name
        var phoneno = ev.currentTarget.dataset.phoneno
        var address = ev.currentTarget.dataset.address
        var shopid = ev.currentTarget.dataset.shopid
        var trandate = ev.currentTarget.dataset.trandate
        if(type == 1){
            this.props.history.push({
                pathname:`${process.env.PUBLIC_URL}/PTmaxcard/historycard/receiver`,
                state: {
                    redeemCode: id,
                    redeemName: name,
                    redeemPhoneno: phoneno,
                    redeemAddress: address,
                    redeemshopid: shopid

                }
            });
        } else if(type == 2){
            this.props.history.push({
                pathname:`${process.env.PUBLIC_URL}/PTmaxcard/historycard/redeem`,
                state: {
                    redeemCode: id,
                    redeemName: name,
                    redeemPhoneno: phoneno,
                    redeemAddress: address
                }
            });
        } else if(type == 4){
            this.props.history.push({
                pathname:`${process.env.PUBLIC_URL}/PTmaxcard/historycard/qr`,
                state: {
                    redeemCode: id,
                    tranS_DATE: trandate
                }
            });
        }
        
    }

    render(){
        const { GetTransactionByCard } = this.state;
        var GetTransactionByCardCards = []
        if(GetTransactionByCard.length == 0){
            GetTransactionByCardCards.push(
                <div className="row mx-auto text-center py-2 t-20">
                    <div className="col-12">
                        <FormattedMessage id="nodata" />
                    </div>
                </div>
            );
        } else {
            for (var x in GetTransactionByCard) {
                var tranS_DATE = ""
                
                if(this.state.language === 'th'){
                    tranS_DATE = moment(GetTransactionByCard[x].tranS_DATE).add(543,'y').locale('th').format('LLL')
                }else{
                    tranS_DATE = moment(GetTransactionByCard[x].tranS_DATE).locale('en').format('LLL')
                }
                GetTransactionByCardCards.push(
                    <a 
                        className="card-history-list" 
                        onClick={this.linkToDetail}
                        key={GetTransactionByCard[x].redeeM_DETAIL[0].producT_CODE}
                        data-id={GetTransactionByCard[x].redeeM_DETAIL[0].producT_CODE}
                        data-type={GetTransactionByCard[x].shippinG_DETAIL[0].shippinG_TYPE}
                        data-name={GetTransactionByCard[x].shippinG_DETAIL[0].shippinG_NAME}
                        data-phoneno={GetTransactionByCard[x].shippinG_DETAIL[0].shippinG_PHONE_NO}
                        data-address={GetTransactionByCard[x].shippinG_DETAIL[0].shippinG_ADDRESS}
                        data-shopid={GetTransactionByCard[x].shippinG_DETAIL[0].shippinG_SHOP}
                        data-trandate={GetTransactionByCard[x].tranS_DATE}
                    >
                        <div className="row pb-4">
                            <div className="col-8 px-0">
                                <div className="text-body-sub text-gray-ptg">{GetTransactionByCard[x].tranS_PRODUCT}</div>
                                <div className="card-history-detail">{tranS_DATE}</div>
                            </div>
                            <div className="col-4 text-right">
                                <div className="text-body-sub text-danger card-history-point">- {parseInt(GetTransactionByCard[x].totaL_POINT)}</div>
                            </div>
                            <div className="col-12 card-history-hr py-2"></div>
                        </div>
                    </a>
                );
            }
        }
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].historycardList}
            >
            <div>
                <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                    <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                </div>
                <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                    <div className="bg-ptmaxcard-content">
                        <div className="ptmaxcard-content">
                            <div className="row">
                                <div className="col-lg-3 col-md-6">
                                    <PTsidebar/>
                                </div>
                                <div className="col-lg-9 col-md-6">
                                    <div className="text-head editprofile-title py-3"><FormattedMessage id="title" /></div>
                                    <div className="ptmaxcard-history-card-body">
                                        {GetTransactionByCardCards}
                                        <div className="row pb-4" style={{ display : (this.state.isHideLoadmoreList ? 'block' : 'none')}}>
                                            <div className="col-12 text-center">
                                                <div className="loadingGifLoadmore mx-auto" style={{ display: this.state.showLoadingLoadmore}}>
                                                    <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                                                </div>
                                                <div style={{ display: (this.state.showLoadingLoadmore === 'block' ? 'none' : 'block') }}>
                                                    <a onClick={() => this.GetTransactionByCard(this.state.userId, this.state.card_member_id)} className="btn btn-loadmore"><FormattedMessage id="seemore" /></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </IntlProvider>
        )
    }
}


