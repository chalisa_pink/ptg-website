import React from 'react';
import '../../../ptmax/ptmax.css';
import { promotionActions } from '../../../../_actions/promotionActions'; 
import { maxcardActions } from '../../../../_actions/maxcardActions';
import moment from "moment-timezone";
import { PTsidebar } from '../../PTsidebar';
import Barcode  from 'react-barcode';
import QRCode from 'qrcode.react';
import Rating from "react-rating";
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../../translations/en.json";
import intlMessageTH from "../../../../translations/th.json";
import Timer from 'react-compound-timer';
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');

export class historycardDetailQR extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            GetQRbyTransaction: "",
            havedata: "block",
            nodata: "none",
            tranS_DATE: "",
        };
    }

    componentDidMount(){
        var redeemCode = this.props.location.state.redeemCode
        var tranS_DATE = this.props.location.state.tranS_DATE
        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
        var ProfileData = localStorage.getItem('Profile')
        var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
        ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        var ProfileArray = JSON.parse(ProfileDecrypt);
        var Profile = ProfileArray[0];
        var CardItemData = localStorage.getItem('CardItem')
        var CardItemDecrypt = Cipher.decrypt(keyCipher, CardItemData);
        CardItemDecrypt = CardItemDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        var CardItem = JSON.parse(CardItemDecrypt);
        this.setState({ 
            userId: Profile.userId,
            customer_id: Profile.customer_id,
            tkmb: Profile.tkmb,
            Profile: Profile,
            CardItem: CardItem,
            tranS_DATE: tranS_DATE
        });
        this.GetQRbyTransaction(Profile.tkmb, Profile.userId, CardItem.carD_MASTER_ID, redeemCode, tranS_DATE);
        
    }

    GetQRbyTransaction(tkmb, userId, carD_MASTER_ID, redeemCode, tranS_DATE){
        var dateForuse = new Date();
        var lastDayFull  = new Date(dateForuse.getFullYear(), dateForuse.getMonth() + 1, 0);
        var getlastDayFull = moment(lastDayFull, 'YYYY/MM/DD');
        var LastDayOfMonth = getlastDayFull.format('DD');
        var LastMonth = dateForuse.getMonth() + 1;
        var NowMonth = dateForuse.getMonth() + 1;
        var setTrandate =  new Date(tranS_DATE);
        var setNowYear = dateForuse.getFullYear();
        var setTrandateYear = setTrandate.getFullYear();
        if(setTrandateYear !== setNowYear){
            var LastYear = setTrandateYear - 1;
            var NowYear = setTrandateYear + 1;
        } else {
            var LastYear = dateForuse.getFullYear() - 1;
            var NowYear = dateForuse.getFullYear();
        }
        if(LastMonth == 1 || LastMonth == 2 || LastMonth == 3 || LastMonth == 4 || LastMonth == 5 || LastMonth == 6 || LastMonth == 7 || LastMonth == 8 || LastMonth == 9){
            var setLastMonth = `0${LastMonth}`
        } else {
            var setLastMonth = LastMonth
        }
        if(NowMonth == 1 || NowMonth == 2 || NowMonth == 3 || NowMonth == 4 || NowMonth == 5 || NowMonth == 6 || NowMonth == 7 || NowMonth == 8 || NowMonth == 9){
            var setNowMonth = `0${NowMonth}`
        } else {
            var setNowMonth = NowMonth
        }
        var startDate = `${LastYear}-${setLastMonth}-01`
        var endDate = `${NowYear}-${setNowMonth}-${LastDayOfMonth}`
        // var startDate = `2020-01-01`
        // var endDate = `2021-01-31`
        // console.log("dateForuse", dateForuse)
        // console.log("LastMonth", LastMonth)
        // console.log("startDate", startDate)
        // console.log("endDate", endDate)
        // console.log("LastDayOfMonth", LastDayOfMonth)
        // console.log("LastMonth", LastMonth)
        // console.log("LastYear", LastYear)
        // console.log("NowMonth", NowMonth)
        // console.log("NowYear", NowYear)
        // console.log("startDate", startDate)
        // console.log("endDate", endDate)
        setTimeout(() => {
            var formData = new FormData();
            formData.append("tokenMobileApp", tkmb);
            formData.append("userId", userId);
            formData.append("cardMasterId", carD_MASTER_ID);
            formData.append("redeemCode", redeemCode);
            formData.append('startDate' , startDate)
            formData.append('endDate' , endDate)
            formData.append('udId' , "")
            formData.append('deviceOs' , "")
            promotionActions.GetQRbyTransaction(formData).then(e => {
                if(e.data.isSuccess === true){
                    this.setState({ 
                        GetQRbyTransaction : e.data.data.promotion,
                        showLoading: 'none',
                        havedata: "block",
                        nodata: "none",
                    })
                }else{
                    if(e.data.errCode === 99){
                        let { errCode , langMsg } = this.state;
                        errCode = true;
                        if(this.state.language === "th"){
                            langMsg = e.data.errMsg;
                        }else{
                            langMsg = e.data.errMsg_en;
                        }
                        this.setState({ errCode , langMsg })

                    }else{
                        this.setState({ errCode : false })
                    }
                    this.setState({ 
                        GetQRbyTransaction : "",
                        showLoading: 'none', 
                        havedata: "none",
                        nodata: "block"
                    })
                }
            })
        },1500)
    }

    render(){
        const { GetQRbyTransaction } = this.state;
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].historycardDetailQR}
            >
            <div>
                <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                    <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                </div>
                <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                    <div className="bg-ptmaxcard-content">
                        <div className="ptmaxcard-content">
                            <div className="row">
                                <div className="col-lg-3 col-md-6">
                                    <PTsidebar/>
                                </div>
                                <div className="col-lg-9 col-md-6">
                                    <div className="ptmaxcard-historydetail-card-body">
                                        <div style={{ display: this.state.havedata }}>
                                            <div className="row">
                                                <div className="col-lg-5 col-md-6 col-sm-6 col-12 pl-0">
                                                    <img className="img-fluid img-historydetail" src={GetQRbyTransaction.imG_URL_DEFAULT} />
                                                    <span className="newitem-historydetail" style={{ display: (GetQRbyTransaction.isNew ? 'block' : 'none') }}><FormattedMessage id="news" /></span>
                                                </div>
                                                <div className="col-lg-7 col-md-6 col-sm-6 col-12">
                                                    <div className="historydetail-content">
                                                        <div className="text-body-main historydetail-title">{GetQRbyTransaction.redeeM_NAME}</div>
                                                        <Rating
                                                            {...this.props} 
                                                            initialRating={0}
                                                            placeholderRating={GetQRbyTransaction.ratingStar}
                                                            emptySymbol={<img src={process.env.PUBLIC_URL +"/images/stargray.png"} className="icon" width="15"/>}
                                                            placeholderSymbol={<img src={process.env.PUBLIC_URL +"/images/stargreen.png"} className="icon" width="15"/>}
                                                            readonly
                                                        /> 
                                                        <span className="px-2 t-24">({GetQRbyTransaction.ratingStar})</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-12">
                                                    <div className="historydetail-titlebarcode my-3">{GetQRbyTransaction.redeeM_CODE}</div>
                                                    <div className="news-tab tab-content text-center my-4">
                                                        <div className="tab-pane active" id="qrcode">
                                                            <QRCode value={`${GetQRbyTransaction.redeeM_CODE}`} size={200} flat={true} displayValue={false}/>
                                                        </div>
                                                        <div className="tab-pane" id="barcode">
                                                            <Barcode value={`${GetQRbyTransaction.redeeM_CODE}`} width={2} height={100} flat={true} displayValue={false}/>
                                                        </div>
                                                    </div>
                                                    <div className="class-count-time my-3">
                                                        <Timer>
                                                            <Timer.Hours/> : <Timer.Minutes /> : <Timer.Seconds/>
                                                        </Timer>
                                                    </div>
                                                    <div className="historydetail-howto my-2">
                                                        <div><FormattedMessage id="remark1" /></div>
                                                        <div><FormattedMessage id="remark2" /></div>
                                                    </div>
                                                </div>
                                                <ul className="nav nav-promotion-tab nav-justified justify-content-center mt-auto">
                                                    <li className="nav-item m-2">
                                                        <a className="nav-link active" data-toggle="pill" href="#qrcode">QR code</a>
                                                    </li>
                                                    <li className="nav-item m-2">
                                                        <a className="nav-link navestamponly" data-toggle="pill" href="#barcode">Bar code</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="row" style={{ display: this.state.nodata }}>
                                            <div className="col-12 lengthzero mx-auto text-center py-5 t-20">
                                                {this.state.errCode === true ? this.state.langMsg : <FormattedMessage id="nodata" />}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </IntlProvider>
        )
    }
}


