import React from 'react';
import '../../../ptmax/ptmax.css';
import { promotionActions } from '../../../../_actions/promotionActions'; 
import { maxcardActions } from '../../../../_actions/maxcardActions';
import moment from "moment-timezone";
import { PTsidebar } from '../../PTsidebar';
import Barcode  from 'react-barcode';
import QRCode from 'qrcode.react';
import Rating from "react-rating";
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../../translations/en.json";
import intlMessageTH from "../../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');

export class historycardDetailReceiver extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading: 'block',
            GetQRbyTransaction: "",
            havedata: "block",
            nodata: "none",
            GetReceiverbyTransaction: {
                imG_URL_DEFAULT: null,
                isNew: null,
                redeeM_NAME: null,
                ratingStar: null,
            },
            receiverAddress: {},
        };
        // this.GetTransactionByCard = this.GetTransactionByCard.bind(this);
    }

    componentDidMount(){
        var redeemCode = this.props.location.state.redeemCode
        var redeemName = this.props.location.state.redeemName
        var redeemPhoneno = this.props.location.state.redeemPhoneno
        var redeemAddress = this.props.location.state.redeemAddress
        var redeemshopid = this.props.location.state.redeemshopid
        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
        var ProfileData = localStorage.getItem('Profile')
        var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
        ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        var ProfileArray = JSON.parse(ProfileDecrypt);
        var Profile = ProfileArray[0];
        var CardItemData = localStorage.getItem('CardItem')
        var CardItemDecrypt = Cipher.decrypt(keyCipher, CardItemData);
        CardItemDecrypt = CardItemDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        var CardItem = JSON.parse(CardItemDecrypt);
        this.setState({ 
            userId: Profile.userId,
            customer_id: Profile.customer_id,
            tkmb: Profile.tkmb,
            Profile: Profile,
            CardItem: CardItem,
            redeemCode: redeemCode,
            redeemName: redeemName,
            redeemPhoneno: redeemPhoneno,
            redeemAddress: redeemAddress,
            redeemshopid: redeemshopid,
        });
        this.GetReceiverbyTransaction(Profile.tkmb, Profile.userId, redeemCode, redeemshopid);
    }

    GetReceiverbyTransaction(tkmb, userId,redeemCode, redeemshopid){
        setTimeout(() => {
            var formData = new FormData();
            formData.append("tokenMobileApp", tkmb);
            formData.append("userId", userId);
            formData.append("lang", "th");
            formData.append("redeemCode", redeemCode);
            formData.append("ORG_SHOP_ID", redeemshopid);
            promotionActions.GetReceiverbyTransaction(formData).then(e => {
                if(e.data.isSuccess === true){
                    if(e.data.data.promotion == null){
                        this.setState({ 
                            GetReceiverbyTransaction : {
                                imG_URL_DEFAULT: null,
                                isNew: null,
                                redeeM_NAME: null,
                                ratingStar: null,
                            },
                            receiverAddress : e.data.data.receiverAddress,
                            showLoading: 'none',
                            havedata: "block",
                            nodata: "none",
                        })
                    } else {
                        this.setState({ 
                            GetReceiverbyTransaction : e.data.data.promotion,
                            receiverAddress : e.data.data.receiverAddress,
                            showLoading: 'none',
                            havedata: "block",
                            nodata: "none",
                        })
                    }
                }else{
                    this.setState({ 
                        GetReceiverbyTransaction : "",
                        showLoading: 'none', 
                        havedata: "none",
                        nodata: "block",
                    })
                }
            })
        },1500)
    }

    onImageError(e) {
        e.target.setAttribute("src", `${process.env.PUBLIC_URL}/images/logo.jpg`);
    }

    render(){
        const { GetReceiverbyTransaction } = this.state;
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].historycardDetailReceiver}
            >
            <div>
                <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                    <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                </div>
                <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                    <div className="bg-ptmaxcard-content">
                        <div className="ptmaxcard-content">
                            <div className="row">
                                <div className="col-lg-3 col-md-6">
                                    <PTsidebar/>
                                </div>
                                <div className="col-lg-9 col-md-6">
                                    <div className="ptmaxcard-historydetail-card-body">
                                        <div className="row">
                                            <div className="col-lg-5 col-md-12 px-0">
                                                <img className="img-fluid img-historydetail" onError={(e) => this.onImageError(e)} src={GetReceiverbyTransaction.imG_URL_DEFAULT == null ? `${process.env.PUBLIC_URL}/images/logo.jpg` : GetReceiverbyTransaction.imG_URL_DEFAULT} />
                                                <span className="newitem-historydetail" style={{ display: (GetReceiverbyTransaction.isNew == true ? 'block' : 'none') }}><FormattedMessage id="news" /></span>
                                            </div>
                                            <div className="col-lg-7 col-md-12 pl-5">
                                                <div className="historydetail-content">
                                                    <div className="historydetail-title">{GetReceiverbyTransaction.redeeM_NAME == null ? <FormattedMessage id="nodata" /> : GetReceiverbyTransaction.redeeM_NAME}</div>
                                                    <Rating
                                                        {...this.props} 
                                                        initialRating={0}
                                                        placeholderRating={GetReceiverbyTransaction.ratingStar == null ? "0" : GetReceiverbyTransaction.ratingStar}
                                                        emptySymbol={<img src={process.env.PUBLIC_URL +"/images/stargray.png"} className="icon" width="15"/>}
                                                        placeholderSymbol={<img src={process.env.PUBLIC_URL +"/images/stargreen.png"} className="icon" width="15"/>}
                                                        readonly
                                                    /> 
                                                    <span className="px-2 t-24">({GetReceiverbyTransaction.ratingStar == null ? "0" : GetReceiverbyTransaction.ratingStar})</span>
                                                    <hr/>
                                                    <div className="mt-1">
                                                       <div className="historydetail-inside-title"><FormattedMessage id="selectbranch" /></div>
                                                       <div className="historydetail-inside-box my-3 mb-4 t-24">
                                                           <div className="t-green">{this.state.receiverAddress['locatioN_NAME']}</div>
                                                       </div>
                                                       <div className="historydetail-inside-title"><FormattedMessage id="branh" /></div>
                                                       <div className="historydetail-inside-title">{this.state.receiverAddress['address']}</div>
                                                    </div>
                                                    <div className="historydetail-howto mt-4">
                                                        <div><FormattedMessage id="tutorail" /></div>
                                                        <div><FormattedMessage id="tutoraildetail" /></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </IntlProvider>
        )
    }
}



