import React from 'react';
import '../../ptmax/ptmax.css';
import EXIF from "exif-js";
import Img from "react-fix-image-orientation";
import { PTsidebar } from '../PTsidebar';
import { registerAction } from "../../../_actions/registerActions";
import { IntlProvider, FormattedMessage } from "react-intl";
import SweetAlert from "react-bootstrap-sweetalert";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');

var alert;

var disabledimage = true
var disabledimage2 = true

export class Problem extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            file: "",
            imagePreviewUrl: "",
            nexbtn: "button_finish",
            disableAgreement: true,
            disabledBtn: "",
            resultApi: "",
            errorFileSize: "",
            closeBtn: {
              imgPreview1: `none`,
              imgPreview2: `none`,
              imgPreview3: `none`
            },
            classPreview: {
              imgPreview1: `preview-image`,
              imgPreview2: `preview-image`,
              imgPreview3: `preview-image`
            },
            imgPreview: {
              imgPreview1: `${process.env.PUBLIC_URL}/images/mockup.png`,
              imgPreview2: `${process.env.PUBLIC_URL}/images/mockup.png`,
              imgPreview3: `${process.env.PUBLIC_URL}/images/mockup.png`
            },
            fields: {
              CUSTOMER_IMG_INFO: []
            },
            renderPopup: "",
            show: false,
            modal: null,
            Profile: "",
            showLoading: "block",
        };
        this.textArea = this.textArea.bind(this);
    }
    componentDidMount(){
        setTimeout(() => {
            var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
            var ProfileData = localStorage.getItem('Profile')
            var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
            ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
            var ProfileArray = JSON.parse(ProfileDecrypt);
            var Profile = ProfileArray[0];
            this.setState({ Profile , Profile })
            this.setState({ showLoading : 'none'})
        },500)
    }

    handleLoadAvatar(e) {
        let { closeBtn, imgPreview, classPreview, fields } = this.state;
        this.setState({ target_name : e.target.name })
        var system = this;
        EXIF.getData(e.target.files[0], function() {
          var orientation = EXIF.getTag(this, "Orientation");
          var can = document.createElement("canvas");
          var ctx = can.getContext("2d");
    
          var thisImage = new Image();
          thisImage.onload = function() {
            var MAX_WIDTH = 700;
            var MAX_HEIGHT = 700;
            var width = thisImage.width;
            var height = thisImage.height;
    
            if (width > height) {
              if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
              }
            } else {
              if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
              }
            }
            can.width = width;
            can.height = height;
            // can.width  = thisImage.width;
            // can.height = thisImage.height;
            ctx.save();
            var width = can.width;
            var styleWidth = can.style.width;
            var height = can.height;
            var styleHeight = can.style.height;
            if (orientation) {
              if (orientation > 4) {
                can.width = height;
                can.style.width = styleHeight;
                can.height = width;
                can.style.height = styleWidth;
              }
              switch (orientation) {
                case 2:
                  ctx.translate(width, 0);
                  // ctx.scale(-1, 1);
                  break;
                case 3:
                  ctx.translate(width, height);
                  ctx.rotate(Math.PI);
                  break;
                case 4:
                  ctx.translate(0, height);
                  // ctx.scale(1, -1);
                  break;
                case 5:
                  ctx.rotate(0.5 * Math.PI);
                  // ctx.scale(1, -1);
                  break;
                case 6:
                  ctx.rotate(0.5 * Math.PI);
                  ctx.translate(0, -height);
                  break;
                case 7:
                  ctx.rotate(0.5 * Math.PI);
                  ctx.translate(width, -height);
                  // ctx.scale(-1, 1);
                  break;
                case 8:
                  ctx.rotate(-0.5 * Math.PI);
                  ctx.translate(-width, 0);
                  break;
              }
            }
    
            ctx.drawImage(thisImage, 0, 0, width, height);
            ctx.restore();
            var dataURL = can.toDataURL();
            var dataurlLink = can.toDataURL("image/jpeg");
            // var get64 = dataURL
            var get64 = dataurlLink.substr(23)
            // var get64 = dataurlLink
            localStorage.setItem(system.state.target_name,get64)
            // imgPreview[system.state.target_name] = dataURL
            imgPreview[system.state.target_name] = dataurlLink
            classPreview[system.state.target_name] = ''
            var obj = {
              CUSTOMER_IMG: get64
            }
            fields['CUSTOMER_IMG_INFO'].push(obj)
            system.setState({ imgPreview, classPreview })
    
            closeBtn[system.state.target_name] = 'block'
            system.setState({ closeBtn })
            // img.src = e.target.result;
    
    
            imgPreview[system.state.target_name] = dataURL
            // imgPreview[system.state.target_name] = dataurlLink;
            classPreview[system.state.target_name] = ''
            // at this point you can save the image away to your back-end using 'dataURL'
            system.setState({ imgPreview, classPreview });
          };
    
          thisImage.src = URL.createObjectURL(this);
        });
    }
    
    checkImg() {
        // console.log(this.state.imgBase);
    }
    
    handleUploadFile = (e) => {
        if(e === 1) { 
          this.inputElement_1.value = "";
          this.inputElement_1.click();
          disabledimage = false;
        }
        if(e === 2) { 
          this.inputElement_2.value = "";
          this.inputElement_2.click(); 
          disabledimage2 = false;
        }
        if(e === 3) { 
          this.inputElement_3.value = "";
          this.inputElement_3.click(); 
        }
    }
    
    deleteLocalStorage(target) {
        if(target == 'imgPreview1'){
          disabledimage = true;
          disabledimage2 = true;
        }else if(target == 'imgPreview2'){
          disabledimage2 = true;
        }
        let { closeBtn, imgPreview, classPreview } = this.state;
        localStorage.removeItem(target);
        closeBtn[target] = "none";
        classPreview[target] = "preview-image";
        imgPreview[target] = `${process.env.PUBLIC_URL}/images/mockup.png`;
        this.setState({ closeBtn });
    }
    
    handleChange(e) {
        this.setState({ TITLE: e.target.value });
    }

    submitInform(){
        let { buttonDisabled } = this.state;
        var phoneno = this.state.Profile.phoneNo;
        var email = sessionStorage.getItem('_email');
        if(this.state.TITLE == undefined || this.state.TITLE == '0' || this.state.DESCRIPTION == undefined || this.state.DESCRIPTION == ''){
          this.setState({ disableAgreement: false })
          var renderPopup = <FormattedMessage id="select" />;
          var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
          this.modalCheckSubmit(renderPopup,imgPopup) 
        }else{ 
          if(phoneno !== "-"){
            this.setState({ buttonDisabled : true })
            registerAction.insertCaseInsident(this.state.TITLE,this.state.DESCRIPTION,phoneno).then(e => {
              if(e.data.errMsg === "ทำรายการสำเร็จ/บันทึกรายการสำเร็จ"){
                this.setState({ buttonDisabled : true })
                this.setState({ disableAgreement : true })
                this.setState({ messageAlert : <FormattedMessage id="sendsuccess" /> })
                var imgPopup = `${process.env.PUBLIC_URL}/images/checked.png`
                this.modalCheckSubmit(this.state.messageAlert , imgPopup) 
              }else{
                this.setState({ disableAgreement: false })
                this.setState({ resultApi: e.data.errMsg })
                this.setState({ disabledBtn: '' })
                var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`
                this.modalCheckSubmit(this.state.resultApi,imgPopup)
              }
            })
          }else{
            this.setState({ buttonDisabled : true })
            registerAction.insertCaseInsident(this.state.TITLE,this.state.DESCRIPTION,phoneno).then(e => {
              if(e.data.errMsg === "ทำรายการสำเร็จ/บันทึกรายการสำเร็จ"){
                this.setState({ buttonDisabled : true })
                this.setState({ disableAgreement : true })
                this.setState({ messageAlert : <FormattedMessage id="sendsuccess" /> })
                var imgPopup = `${process.env.PUBLIC_URL}/images/checked.png`
                this.modalCheckSubmit(this.state.messageAlert , imgPopup) 
              }else{
                this.setState({ disableAgreement: false })
                this.setState({ resultApi: e.data.errMsg })
                this.setState({ disabledBtn: '' })
                var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`
                this.modalCheckSubmit(this.state.resultApi,imgPopup)
              }
            })
          }
        }
    }

    modalCheckSubmit(res, img) {
        alert = (
          <SweetAlert
            custom
            showCloseButton
            closeOnClickOutside={false}
            focusConfirmBtn={false}
            title=""
            customIcon={img}
            showConfirm={false}
            showCancelButton
            onCancel={() => this.handleChoice(false)}
            onConfirm={() => this.handleChoice(true)}
          >
            <div className="fontSizeCase">{res}</div>
          </SweetAlert>
        );
        this.setState({ show: true, modal: alert });
    }

    handleChoice(choice) {
        if (choice === false) {
          if(this.state.disableAgreement == true){
            this.setState({ show: false , modal: null })
            window.history.back();
          }else{
            this.setState({ show: false , modal: null })
          }
        }
    }

    textArea(e){ 
        let { DESCRIPTION } = this.state;
        DESCRIPTION = e.target.value;
        this.setState({ DESCRIPTION })
    }

    render(){
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].Problem}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="bg-ptmaxcard-content">
                            <div className="ptmaxcard-content">
                                <div className="row">
                                    <div className="col-lg-3 col-md-6">
                                        <PTsidebar/>
                                    </div>
                                    <div className="col-lg-9 col-md-6">
                                        <div className="editprofile-title py-3"><FormattedMessage id="report" /></div>
                                        <div className="editprofile-content py-3">
                                            <div className="form-group">
                                                <select
                                                    className="form-control input_editprofile"
                                                    id="exampleFormControlSelect1"
                                                    name="TITLE"
                                                    onChange={e => this.handleChange(e)}
                                                >
                                                    <option value="0">{messages[this.state.language].Problem.selectinput}</option>
                                                    <option value={messages[this.state.language].Problem.selectvalue1}>{messages[this.state.language].Problem.selectvalue1}</option>
                                                    <option value={messages[this.state.language].Problem.selectvalue2}>{messages[this.state.language].Problem.selectvalue2}</option>
                                                    <option value={messages[this.state.language].Problem.selectvalue3}>{messages[this.state.language].Problem.selectvalue3}</option>
                                                </select>
                                            </div>
                                            <div className="form-group">
                                                <textarea
                                                    className="form-control input_form input_editprofile"
                                                    id="exampleFormControlTextarea1"
                                                    rows="5"
                                                    name="DESCRIPTION"
                                                    onChange={this.textArea}
                                                >
                                                </textarea>
                                            </div>
                                            <div className="errorImageSize">{this.state.errorFileSize}</div>
                                            <div className="">
                                                {/* <form onSubmit={this._handleSubmit}> */}
                                                <div className="row row_image">
                                                    <div className="col-4 col-sm-3 review_image_before">
                                                        <div className="upload_preview">
                                                            <div
                                                            className="close-btn"
                                                            onClick={() => this.deleteLocalStorage("imgPreview1")}
                                                            style={{
                                                                display: this.state.closeBtn["imgPreview1"]
                                                            }}
                                                            >
                                                            <i
                                                                className="fa fa-times-circle"
                                                                aria-hidden="true"
                                                            />
                                                            </div>
                                                            <div onClick={() => this.handleUploadFile(1)} className="div-centers text-center">
                                                            <input
                                                                type="file"
                                                                name="imgPreview1"
                                                                style={{ display: "none" }}
                                                                ref={input => (this.inputElement_1 = input)}
                                                                onChange={e => this.handleLoadAvatar(e)}
                                                            />
                                                            <Img
                                                                src={this.state.imgPreview["imgPreview1"]}
                                                                alt=""
                                                                className={this.state.classPreview["imgPreview1"]}
                                                            />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-4 col-sm-3 review_image_before">
                                                        <div className="upload_preview">
                                                            <div
                                                            className="close-btn"
                                                            onClick={() => this.deleteLocalStorage("imgPreview2")}
                                                            style={{
                                                                display: this.state.closeBtn["imgPreview2"]
                                                            }}
                                                            >
                                                            <i
                                                                className="fa fa-times-circle"
                                                                aria-hidden="true"
                                                            />
                                                            </div>
                                                            <div onClick={() => this.handleUploadFile(2)} className="div-centers text-center">
                                                            {/* <input type="file" onChange={this._handleImageChange} style={{ display: "none" }} /> */}
                                                            <input
                                                                type="file"
                                                                name="imgPreview2"
                                                                style={{ display: "none" }}
                                                                ref={input => (this.inputElement_2 = input)}
                                                                onChange={e => this.handleLoadAvatar(e)}
                                                                disabled={disabledimage}
                                                            />
                                                            <Img
                                                                src={this.state.imgPreview["imgPreview2"]}
                                                                alt=""
                                                                className={this.state.classPreview["imgPreview2"]}
                                                            />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-4 col-sm-3 review_image_before">
                                                        <div className="upload_preview">
                                                            <div
                                                            className="close-btn"
                                                            onClick={() => this.deleteLocalStorage("imgPreview3")}
                                                            style={{
                                                                display: this.state.closeBtn["imgPreview3"]
                                                            }}
                                                            >
                                                            <i
                                                                className="fa fa-times-circle"
                                                                aria-hidden="true"
                                                            />
                                                            </div>
                                                            <div onClick={() => this.handleUploadFile(3)} className="div-centers text-center">
                                                            <input
                                                                type="file"
                                                                name="imgPreview3"
                                                                style={{ display: "none" }}
                                                                ref={input => (this.inputElement_3 = input)}
                                                                onChange={e => this.handleLoadAvatar(e)}
                                                                disabled={disabledimage2}
                                                            />
                                                            <Img
                                                                src={this.state.imgPreview["imgPreview3"]}
                                                                alt=""
                                                                className={this.state.classPreview["imgPreview3"]}
                                                            />
                                                            </div>
                                                        </div>
                                                        </div>
                                                </div>
                                                {/* </form> */}
                                            </div>
                                            <div className="color-incident my-3 ploblem-ft-18"><FormattedMessage id="dataconditonupload" /></div>
                                            <div className="color-incident my-3 ploblem-ft-18"><FormattedMessage id="datainformproblem" /></div>
                                            <div className="text-center py-5">
                                                <button
                                                type="button"
                                                className="btn btn-pt-problemsumit"
                                                onClick={() => this.submitInform()}
                                                disabled={this.state.buttonDisabled}
                                                >
                                                 <FormattedMessage id="btnsenddata" />
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.state.modal}
                </div>
            </IntlProvider>
        )
    }
}


