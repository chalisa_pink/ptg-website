import React from 'react';
import '../../ptmax/ptmax.css';
import { PTsidebar } from '../PTsidebar';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');

export class question extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            Profile: "",
            showLoading: "block",
        };
    }
    componentDidMount(){
        var ProfileData = localStorage.getItem('Profile')
        this.handleChangeTabs(1)
        if(ProfileData){
            setTimeout(() => {
                var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
                var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
                ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
                var ProfileArray = JSON.parse(ProfileDecrypt);
                var Profile = ProfileArray[0];
                this.setState({ Profile , ProfileData})
            },500)
        }
    }

    handleChangeTabs(tab){
        this.setState({ showLoading : 'none'})
        if(tab == 1){
            this.setState({ 
                tab1: "active",
                tab2: "",
                tab3: "",
                tab4: "",
                content1: "block",
                content2: "none",
                content3: "none",
                content4: "none",
            });
        } else if(tab == 2){
            this.setState({ 
                tab1: "",
                tab2: "active",
                tab3: "",
                tab4: "",
                content1: "none",
                content2: "block",
                content3: "none",
                content4: "none",
            });
        } else if(tab == 3){
            this.setState({ 
                tab1: "",
                tab2: "",
                tab3: "active",
                tab4: "",
                content1: "none",
                content2: "none",
                content3: "block",
                content4: "none",
            });
        } else if(tab == 4){
            this.setState({ 
                tab1: "",
                tab2: "",
                tab3: "",
                tab4: "active",
                content1: "none",
                content2: "none",
                content3: "none",
                content4: "block",
            });
        }
    }

    render(){
        var Profile = this.state.Profile
        var ProfileData = this.state.ProfileData
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].question}
            >
            <div>
                <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                    <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                </div>
                <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                    <div className="bg-ptmaxcard-content">
                        <div className="ptmaxcard-content">
                            <div className="row">
                                <div className= {(ProfileData ? "col-lg-3 col-md-6" : "")}>
                                    {(ProfileData ? <PTsidebar/> : '')}
                                </div>
                                <div className={(ProfileData ? "col-lg-9 col-md-6" : "col-12")}>
                                    <div className="editprofile-title py-3"><FormattedMessage id="qanda" /></div>
                                    <div className="ptmaxcard-regis-card-header">
                                        <ul className="nav nav-pills nav-justified ptmaxcard-condi-navall" role="tablist">
                                            <li className="nav-item ptmaxcard-condi-nav-1">
                                                <a onClick={() => this.handleChangeTabs(1)} className={`nav-link ptmaxcard-condi-nav ${this.state.tab1}`}><FormattedMessage id="regis" /></a>
                                            </li>
                                            <li className="nav-item ptmaxcard-condi-nav-2">
                                                <a onClick={() => this.handleChangeTabs(2)} className={`nav-link ptmaxcard-condi-nav ${this.state.tab2}`}><FormattedMessage id="datepoint" /></a>
                                            </li>
                                            <li className="nav-item ptmaxcard-condi-nav-3">
                                                <a onClick={() => this.handleChangeTabs(3)} className={`nav-link ptmaxcard-condi-nav ${this.state.tab3}`}><FormattedMessage id="redeem" /></a>
                                            </li>
                                            <li className="nav-item ptmaxcard-condi-nav-4">
                                                <a onClick={() => this.handleChangeTabs(4)} className={`nav-link ptmaxcard-condi-nav ${this.state.tab4}`}><FormattedMessage id="cardbroke" /></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="ptmaxcard-condi-card-body t-22">
                                        <div style={{ display: this.state.content1 }} >
                                            <div className="mb-3">
                                                <div className="t-24 t-green"><span className="pr-2"><FormattedMessage id="qu" /> : </span> คะแนนใน Max Card ทำอะไรได้บ้าง? </div>
                                                <div className=""><span className="t-24 t-bold pr-2">คำตอบเบื้องต้น : คะแนนใน Max Card สามารถนำมาแลกกับโปรโมชั่นมากมาย ไม่ว่าจะเติมน้ำมัน / กาแฟพันธุ์ไทย / แมกซ์มาร์ท / AUTOBACS หรือโปรร้านอาหาร / ที่พัก / ความสวยความงาม / ประกันภัย ก็มีให้เลือกในแอปฯ Max Rewards</span> </div>
                                                <div className="t-24 t-bold pl-3">IOS: <a href="https://apple.co/2wyWSFy">https://apple.co/2wyWSFy</a></div>
                                                <div className="t-24 t-bold pl-3">Android: <a href="https://bit.ly/39eFDXl">https://bit.ly/39eFDXl</a></div>
                                                <div className="t-24 t-bold pl-3">หรือแลกผ่านไลน์ @PTSTATION ก็สะดวกยิ่งขึ้น</div>
                                                <div className="t-24 t-bold pl-3">คลิกเลยที่: <a href="https://lin.ee/zcj0cK1">https://lin.ee/zcj0cK1</a></div>
                                            </div>
                                            <div className="mb-3">
                                                <div className="t-24 t-green"><span className="pr-2"><FormattedMessage id="qu" /> : </span> สนใจสมัครบัตร Max Card ต้องทำอย่างไร </div>
                                                <div className=""><span className="t-24 t-bold pr-2">คำตอบเบื้องต้น : สมัครฟรี! ไม่มีค่าใช้จ่าย ผ่าน 4 ช่องทาง ดังนี้</span> </div>
                                                <div className="t-24 t-bold pl-3">1. สมัครผ่าน เว็บไซต์ <a href="https://www.ptmaxcard.com">https://www.ptmaxcard.com</a></div>
                                                <div className="t-24 t-bold pl-3">2. สมัครผ่าน แอพพลิเคชั่น PT Max Reward</div>
                                                <div className="t-24 t-bold pl-3">3. สถานีบริการน้ำมัน / LPG พีที ทุกสาขา</div>
                                                <div className="t-24 t-bold pl-3">4. ร้านค้าที่ร่วมรายการ  ดังนี้ ร้านกาแฟพันธุ์ไทย, Coffee World,  Autobacs, Max Mart, ศูนย์ตัวแทนจำหน่ายฟิล์มลามิน่าที่ร่วมรายการ</div>
                                            </div>
                                            <div className="mb-3">
                                                <div className="t-24 t-green"><span className="pr-2"><FormattedMessage id="qu" /> : </span> แลกของรางวัลต้องทำอย่างไร </div>
                                                <div className=""><span className="t-24 t-bold pr-2">คำตอบเบื้องต้น : </span> *แลกส่วนลดน้ำมัน เพียงติดต่อที่สถานีบริการน้ำมัน พร้อมแสดงบัตร Max Card คู่กับบัตรประชาชน เพื่อยืนยันการแลกคะแนน</div>
                                                <div className="t-24 t-bold"> *แลกของรางวัลอื่นๆ แลกด้วยตัวเองผ่าน Max Rewards App หรือ ติดต่อ Call Center 1614 เมื่อติดต่อรับสินค้า เพียงแสดงบัตรประชาชน พร้อมบัตรสมาชิก Max Card ยืนยันความเป็นเจ้าของบัตร สินค้าจัดส่งภายใน 30 วัน</div>
                                            </div>
                                            <div className="mb-3">
                                                <div className="t-24 t-green"><span className="pr-2"><FormattedMessage id="qu" /> : </span> สะสมแต้มแบบไม่แสดงบัตร Max Card ได้หรือไม่ หากต้องการแก้เบอร์โทรศัพท์ ทำอย่างไร</div>
                                                <div className=""><span className="t-24 t-bold pr-2">คำตอบเบื้องต้น : กรณีลืมบัตร Max Card สามารถแจ้งหมายเลขบัตรประชาชน หรือ เบอร์โทรศัพท์มือถือของท่าน เพื่อทำการสะสมคะแนนได้</span> </div>
                                                <div className="t-24 t-bold">หากสมาชิกต้องการอัพเดทเบอร์โทรศัพท์ในระบบ และ ไม่ได้ใช้งาน Mobile Application Max Rewards สามารถติดต่อแก้ไขผ่าน Call Center 1614 ได้เลย แต่หากใช้งาน Max Rewards App เพื่อป้องกันการแอบอ้างสิทธิ์แลกของรางวัลจาก App กรุณาส่งสำเนาบัตร ปชช ระบุข้อความ ขอแก้ไขเบอร์โทรศัพท์ พร้อมลายเซ็นต์ จากนั้นส่งเข้า Inbox Fanpage PT Station หรือ ยื่นเอกสารที่สถานีบริการน้ำมัน ได้เลย</div>
                                                <div className="t-24 t-bold"> สาเหตุที่ต้องมีเอกสารเพราะ เป็นการรักษาสิทธิ์และเป็นหลักฐานยืนยันในการขอเปลี่ยนแปลงเพราะมีผลต่อการแลกของรางวัลด้วยตนเองได้</div>
                                            </div>
                                            <div className="mb-3">
                                                <div className="t-24 t-green"><span className="pr-2"><FormattedMessage id="qu" /> : </span> อายุเท่าไรถึงสมัครบัตร​ Max Card ​ได้ </div>
                                                <div className=""><span className="t-24 t-bold pr-2">คำตอบเบื้องต้น : สมัครได้ตั้งแต่อายุครบ 7 ปีบริบูรณ์ แต่หากอายุน้อย 10 ปี ต้องได้รับความยินยอมจากผู้ปกครอง โดยแนะนำกรอกเอกสารให้ความยินยอมในใบสมัคร ด้วย</span> </div>
                                            </div>
                                            <div className="mb-3">
                                                <div className="t-24 t-green"><span className="pr-2"><FormattedMessage id="qu" /> : สมัครบัตร Max Card ผ่านทางเว็บไซต์ จะได้รับเมื่อไร</span> </div>
                                                <div className=""><span className="t-24 t-bold pr-2">คำตอบเบื้องต้น : </span> การสมัครบัตรสมาชิกผ่านช่องทางเว็บไซต์ จะได้รับบัตรภายใน 15 วันทำการ ระยะเวลาอาจช้าเร็วในการจัดส่งขึ้นกับทางไปรษณีย์ หากเกิน 15 วันแล้วยังไม่ได้รับบัตร กรุณาติดต่อ Call Center 1614</div>
                                            </div>
                                            <div className="mb-3">
                                                <div className="t-24 t-green"><span className="pr-2"><FormattedMessage id="qu" /> : </span> ทำบัตรหาย ทำอย่างไร</div>
                                                <div className=""><span className="t-24 t-bold pr-2">คำตอบเบื้องต้น : </span> กรณีบัตรสมาชิกหาย กรุณาติดต่อ Call Center 1614 (เปิดบริการทุกวัน เวลาทำการ 8.00 น. - 20.00 น. ) แจ้งออกบัตรสมาชิกใบใหม่ มีค่าธรรมเนียมออกบัตรใหม่ จำนวน 100 แต้ม โดยจะหักจากแต้มคงเหลือปัจจุบัน หากจำนวนแต้มคงเหลือมีน้อยกว่า 100 แต้ม หักยอดตามจริงที่คงเหลืออยู่</div>
                                            </div>
                                            <div className="mb-3">
                                                <div className="t-24 t-green"><span className="pr-2"><FormattedMessage id="qu" /> : </span> เติมน้ำมัน แถมน้ำไหม ได้สะสมคะแนนไหม</div>
                                                <div className=""><span className="t-24 t-bold pr-2">คำตอบเบื้องต้น : </span> เมื่อเติมน้ำมันครบตามจำนวนที่กำหนด จะได้รับน้ำดื่ม (ขึ้นอยู่กับโปรโมชั่นในช่วงนั้นๆ กรุณาตรวจสอบทีสาขาอีกครั้ง ) และ แต้มสะสม ตามจำนวนลิตรที่เติม หากไม่ต้องการรับน้ำดื่ม สามารถแจ้งเปลี่ยนเป็นแต้มสะสมได้ ( น้ำดื่ม 1 ขวด = 35 แต้ม) เป็นอีกทางเลือกหนึ่งสำหรับผู้ที่ไม่ต้องการน้ำ และ ต้องการได้แต้มสะสม ไว้ขึ้น เพื่อนำไปแลกรางวัล</div>
                                            </div>
                                            <div className="mb-3">
                                                <div className="t-24 t-green"><span className="pr-2"><FormattedMessage id="qu" /> : </span> แต้มแลกน้ำคืออะไร ใช้กี่คะแนน</div>
                                                <div className=""><span className="t-24 t-bold pr-2">คำตอบเบื้องต้น : </span> "แต้มแลกน้ำ" เป็นอีกทางเลือกหนึ่ง สำหรับผู้ที่มีคะแนนสะสมเพียงพอ และไม่ได้มาเติมน้ำมัน สามารถนำแต้มสะสมที่มี มาแลกน้ำดื่มแพคได้ ใช้เพียง 490 คะแนน แลกน้ำแพคขนาด 1500 มล.ได้ 1 แพค (มี 6 ขวด) เพียงแค่ใช้บริการบริษัทในเครือ สะสมคะแนนให้ครบตามเงื่อนไข</div>
                                            </div>
                                            <div className="mb-3">
                                                <div className="t-24 t-green"><span className="pr-2"><FormattedMessage id="qu" /> : </span> ใช้กี่คะแนน แลกส่วนลดน้ำมัน</div>
                                                <div className=""><span className="t-24 t-bold pr-2">คำตอบเบื้องต้น : </span> ลูกค้าสามารถนำแต้มสะสมที่มี แลกเป็นส่วนลดน้ำมัน ได้ดังนี้ </div>
                                                <div className="t-24 t-bold pl-3"> ใช้ 50 แต้ม แลกส่วนลดน้ำมันได้ 5</div>
                                                <div className="t-24 t-bold pl-3"> ใช้ 100 แต้ม แลกส่วนลดน้ำมันได้ 10</div>
                                                <div className="t-24 t-bold pl-3"> ใช้ 150 แต้ม แลกส่วนลดน้ำมันได้ 15</div>
                                                <div className="t-24 t-bold pl-3"> ใช้ 500 แต้ม แลกส่วนลดน้ำมันได้ 50</div>
                                                <div className="t-24 t-bold pl-3"> ใช้ 1,000 แต้ม แลกส่วนลดน้ำมันได้ 100</div>
                                                <div className="t-24 t-bold pl-3"> ใช้ 5,000 แต้ม แลกส่วนลดน้ำมันได้ 500</div>
                                                <div className="t-24 t-bold"> สามารถแลกส่วนลดได้ที่พนักงาน พร้อมแสดงบัตร Max Card และ บัตร ปชช ด้วย เพื่อตรวจสอบความเป็นสมาชิกว่าถูกต้องผม</div>
                                            </div>
                                            <div className="mb-3">
                                                <div className="t-24 t-green"><span className="pr-2"><FormattedMessage id="qu" /> : </span> เช็คแต้ม ทำอย่างไร</div>
                                                <div className=""><span className="t-24 t-bold pr-2">คำตอบเบื้องต้น : </span> เช็คคะแนนกันพลาด จะได้ฟาดจัดเต็มทุกดีล!! ทำได้ผ่าน 5 ช่องทาง</div>
                                                <div className="t-24 t-bold"> วิธีที่ 1: ดูคะแนนท้ายสลิปของบัตร พีที แมกซ์ การ์ด</div>
                                                <div className="t-24 t-bold"> วิธีที่ 2: เช็คแอปฯ ผ่าน PT Max Rewards ในหน้า Max Card</div>
                                                <div className="t-24 t-bold"> วิธีที่ 3: เช็คใน LINE @PTStation ผ่านปุ่ม “My Max Card” ไปที่ “ตรวจสอบคะแนน”</div>
                                                <div className="t-24 t-bold"> วิธีที่ 4: พิมพ์ *793*02* ตามด้วยเลขบัตร พีที แมกซ์ การ์ด 10 หลัก กด # แล้วโทรออก (ฟรี)</div>
                                                <div className="t-24 t-bold"> วิธีที่ 5: โทรหา 1614 กด 1 พีที คอล เซ็นเตอร์</div>
                                            </div>
                                            <div className="mb-3">
                                                <div className="t-24 t-green"><span className="pr-2"><FormattedMessage id="qu" /> : </span> แต้มสะสมของบัตร Max Card มีหมดอายุหรือไม่</div>
                                                <div className=""><span className="t-24 t-bold pr-2">คำตอบเบื้องต้น : </span> คะแนนสะสมมีอายุ 3 ปีนับจากปีที่สมัคร โดยคะแนนสะสมที่ไม่ได้นำมาแลกรางวัลจะหมดอายุในวันที่ 31 ธันวาคมของปีปฏิทินที่ 3 และจะถูกหักออกจากคะแนนสะสมรวมของสมาชิกโดยอัตโนมัติ เช่น คะแนนสะสมในช่วงวันที่ 1 มกราคม – 31 ธันวาคม 2560 จะหมดอายุในวันที่ 31 ธันวาคม 2563</div>
                                                <div className="t-24 t-bold"> เพื่อเป็นการรักษาสิทธิ์ของคุณลูกค้า แนะนำให้ตรวจสอบคะแนนสะสมคงเหลือของท่าน เพื่อทำการแลกของรางวัลภายในวันที่กำหนด</div>
                                                <div className="t-24 t-bold"> หมายเหตุ : หากข้อมูลส่วนตัวของบัตรสมาชิกไม่ถูกต้องตรง ขอสงวนสิทธิ์ไม่สามารถแลกส่วนลดน้ำมันหรือ ของรางวัลได้ จนกว่าจะมีการแจ้งแก้ไขข้อมูลจากคุณลูกค้า บัตรสมาชิกนั้นๆ</div>
                                            </div>
                                            <div className="mb-3">
                                                <div className="t-24 t-green"><span className="pr-2"><FormattedMessage id="qu" /> : </span> E-Stamp คืออะไร</div>
                                                <div className=""><span className="t-24 t-bold pr-2">คำตอบเบื้องต้น : </span> E-Stamp สามารถนำมาแลกของรางวัลใน Max Rewards App ได้ สะสม E-Stamp จากการเข้าใช้บริการบริษัทในเครือ พีที จะได้รับ E-Stamp จำนวนสูงสุด 1 ดวง ต่อ วัน และ มีอายุ 3 ปี เช่นเดียวกับแต้มสะสม</div>
                                            </div>
                                      
                                            {/* <div className="t-green"><span className="t-bold t-24 pr-2"><FormattedMessage id="qu" /> : </span> <FormattedMessage id="qu1" /></div>
                                            <div className=""><span className="t-24 t-bold pr-2"><FormattedMessage id="an" /> : </span> <FormattedMessage id="an1" /></div> */}
                                        </div>
                                        <div style={{ display: this.state.content2 }} >
                                            <div className="t-green"><span className="t-24 pr-2"><FormattedMessage id="qu" /> : </span> <FormattedMessage id="qu2" /> </div>
                                            <div className=""><span className="t-24 t-bold pr-2"><FormattedMessage id="an" /> : </span> 
                                            <FormattedMessage id="an21" /> <br/>
                                            <FormattedMessage id="an22" /> <br/>
                                            <FormattedMessage id="an23" /><br/>
                                            </div>
                                        </div>
                                        <div style={{ display: this.state.content3 }} >
                                            <div className="t-green"><span className="t-24 pr-2"><FormattedMessage id="qu" /> : </span> <FormattedMessage id="qu3" />ข</div>
                                            <div className="pb-3"><span className="t-24 t-bold pr-2"><FormattedMessage id="an" /> : </span> <FormattedMessage id="an3" /></div>
                                            <div className="t-green"><span className="t-24 pr-2"><FormattedMessage id="qu" /> : </span> <FormattedMessage id="qu4" /></div>
                                            <div className="pb-3"><span className="t-24 t-bold pr-2"><FormattedMessage id="an" /> : </span> <FormattedMessage id="an4" /></div>
                                            <div className="t-green"><span className="t-24 pr-2"><FormattedMessage id="qu" /> : </span> <FormattedMessage id="qu5" /></div>
                                            <div className=""><span className="t-24 t-bold pr-2"><FormattedMessage id="an" /> : </span> <FormattedMessage id="an5" /></div>
                                        </div>
                                        <div style={{ display: this.state.content4 }} >
                                            <div className="t-green"><span className="t-24 pr-2"><FormattedMessage id="qu" /> : </span> <FormattedMessage id="qu6" /></div>
                                            <div className="pb-3"><span className="t-24 t-bold pr-2"><FormattedMessage id="an" /> : </span> <FormattedMessage id="an6" /></div>
                                            <div className="t-green"><span className="t-24 pr-2"><FormattedMessage id="qu" /> : </span><FormattedMessage id="qu7" /></div>
                                            <div className=""><span className="t-24 t-bold pr-2"><FormattedMessage id="an" /> : </span><FormattedMessage id="an7" /></div>
                                        </div>
                                    </div>    
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </IntlProvider>
        )
    }
}


