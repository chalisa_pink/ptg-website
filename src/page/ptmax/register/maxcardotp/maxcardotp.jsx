import React from "react";
import '../../../register/register.css';
import { registerAction } from '../../../../_actions/registerActions'; 
import { maxcardActions } from '../../../../_actions/maxcardActions';
import SweetAlert from "react-bootstrap-sweetalert";
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../../translations/en.json";
import intlMessageTH from "../../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');

var OTP_All = ""

export class regisotp extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            fields: {},
            resp: [],
            show: false,
            modal: null,
            secondsElapsed: 0,
            setOTP: '', 
            setOTPAll: '',
            Profile: "",
            getCustomerId: "",
        } 
    }

    componentDidMount(){
        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
        var ProfileData = localStorage.getItem('Profile');
        var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
        ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        var ProfileArray = JSON.parse(ProfileDecrypt);
        var Profile = ProfileArray[0];
        this.setState({ Profile , Profile })
        if(this.props.location.state.data) {
            let newObject = Object.assign(this.props.location.state.data , this.state.fields)
            this.setState({ fields: newObject })
        }
        // this.sentOTP(this.props.location.state.data)
        this.startTime();
        document.addEventListener("keydown", this.deleteBtnFunction, false);
    }

    startTime() {
        var _this = this;
        this.countdown = setInterval(function() {
          if(_this.state.secondsElapsed > 0){
            _this.setState({ 
              secondsElapsed: _this.state.secondsElapsed - 1 
            });
          }         
        }, 1000);
    }

    resetTime() {
        this.sentOTP(this.props.location.state.data);
        this.reset = this.setState({
          secondsElapsed: (this.state.secondsElapsed = 60)
        });
    }

    getTime() {
        if(this.state.secondsElapsed % 60 > 0){
            var textOTP = this.showCountdown();
        } else {
            var textOTP = <button className="text-green" onClick={() => this.resetTime()}>คลิกเพื่อรับ OTP</button>
        }
        return (textOTP);
    }

    showCountdown= (e) => {
        return (`${messages[this.state.language].regisotp.sendotpin}`  + (this.state.secondsElapsed % 60) + `${messages[this.state.language].regisotp.secion}`)
    }

    sentOTP(data){
        var type = 'Register';
        registerAction.sendOTP(data.PHONE_NO,type).then( e => {
            if(e.data.isSuccess === true){
                this.setState({ resp : e.data.data })
            }else{
                this.setState({ resp: e.data.data })
            }
        })
    }

    verifyOTPClick(setOTP,otpkey){
        var Profile = this.state.Profile;
        var OTP_All = setOTP;
        var formData = new FormData();
        var userid = Profile.userId;
        var typeotp = 'Register';
        formData.append('otp_Key_No',otpkey);
        formData.append('eventType','Register');
        formData.append('eventNo',this.props.location.state.data.PHONE_NO);
        formData.append('value', OTP_All);
        registerAction.verifyOTP(formData).then(e => {
            if(e.data.isSuccess === true){
                this.setState({ resp : e.data.data })
                this.onConfirmAfterRegis(userid, typeotp)
            }else{
                var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
                var msg = e.data.errMsg
                this.modalCheckSubmit(msg,imgPopup)
            }
        })
    } 

    onConfirmAfterRegis(userid, typeotp){
        registerAction.confirmAfterRegis(userid,typeotp).then(e => {
            if(e.data.isSuccess === true){
                this.getCardList(userid);
            }else{
                var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
                var msg = e.data.errMsg;
                this.modalCheckSubmit(msg,imgPopup);
            }
        })
    }

    getCardList(userid){
        maxcardActions.getCustomerId(userid).then(e => {
            if(e.data.isSuccess === true){
                this.setState({ getCustomerId: e.data.data })
                var getCustomerId = e.data.data
                this.handleVsmartcall(e.data.data.customerId, userid, getCustomerId);
            }else{
                var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
                var msg = e.data.errMsg;
                this.modalCheckSubmit(msg,imgPopup)
            }
        })
    }

    handleVsmartcall(customeR_ID, userid, getCustomerId){
        var fields = this.state.fields;
        var reqData = `{
            "CUSTOMER_ID": "${customeR_ID}",
            "UPDATE_BY": "CNY2022_PDPA",
            "CONSENT_INFO": [
                {
                  "CONSENT_MASTER_ID": ${10},
                  "CONSENT_NAME": "เก็บรวบรวมและใช้ข้อมูลส่วนบุคคลของท่านเพื่อวัตถุประสงค์ทางการตลาด และเพื่อให้ท่านได้รับประโยชน์จากการส่งข้อมูลข่าวสาร การโฆษณา การจัดทำแคมเปญ รายการส่งเสริมการขาย การเสนอโปรโมชั่น สิทธิประโยชน์ และเพื่อเชิญชวนให้ท่านเข้าร่วมกิจกรรมต่างๆ ที่ท่านได้ระบุความสนใจไว้แก่บริษัทฯ",
                  "CONSENT_VERSION": ${3},
                  "IS_CHECK": ${fields.pdpacon3}
                },
                {
                  "CONSENT_MASTER_ID": ${11},
                  "CONSENT_NAME": "วิเคราะห์การใช้สินค้าหรือบริการของสมาชิก เพื่อจัดทำโปรโมชั่น หรือส่วนลดในสินค้าหรือบริการให้ตรงใจ",
                  "CONSENT_VERSION": ${3},
                  "IS_CHECK": ${fields.pdpacon4}
                },
                {
                    "CONSENT_MASTER_ID": ${12},
                    "CONSENT_NAME": "เปิดเผยข้อมูลส่วนบุคคลและข้อมูลใดๆ ของท่าน ให้แก่ บริษัท พีทีจี เอ็นเนอยี จำกัด (มหาชน) บริษัท แมกซ์ โซลูชัน เซอร์วิส จำกัด รวมถึงบริษัทในเครือ บริษัทย่อย บริษัทร่วมทุน คู่ค้า พันธมิตรและผู้ให้บริการของบริษัทดังกล่าว เพื่อวัตถุประสงค์ทางการตลาด การนำเสนอข่าวสาร รายการส่งเสริมการขาย การจัดสิทธิประโยชน์และโปรโมชั่น การจัดทำแคมเปญต่างๆ การนำเสนอขายผลิตภัณฑ์ และ การวิเคราะห์การใช้สินค้าหรือบริการของสมาชิก",
                    "CONSENT_VERSION": ${3},
                    "IS_CHECK": ${fields.pdpacon5}
                }
            ]
        }`
        var formData = new FormData();
        formData.append("TOKEN_ID", "");
        formData.append("reqData", reqData);
        formData.append("action", "UpdateConsent");
        registerAction.vsmartcall(formData).then(e => {
            var errMsg_en = JSON.parse(e.data.errMsg_en);
            var rescode = errMsg_en.RESPONSE_INFO.RESCODE;
            var errMsg = errMsg_en.RESPONSE_INFO.RESMSG;
            if(rescode === "000") {
                this.getCard(userid, getCustomerId)
            } else {
                var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
                this.modalCheckSubmit(errMsg,imgPopup)
            }
        });
    }

    
    getCard(userId, getCustomerId){
        maxcardActions.getCardList(userId).then(e => {
            if(e.data.isSuccess === true){
                if(e.data.data.length === 0){
                    localStorage.setItem("CardItem", "");
                    window.location.href = `${process.env.PUBLIC_URL}/PTmaxcard`;
                }else{
                    var CardItem = e.data.data[0]
                    var CardItemstringify = JSON.stringify(CardItem)
                    var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
                    var CardItemEncrypt = Cipher.encrypt(keyCipher, CardItemstringify);
                    localStorage.setItem("CardItem", CardItemEncrypt);
                    var resp = e.data.data[0].carD_TYPE_ID;
                    var carD_NO = e.data.data[0].carD_NO;
                    this.getProfiel(getCustomerId, carD_NO)
                }
            }else{
                localStorage.setItem("CardItem",false);
                window.location.href = `${process.env.PUBLIC_URL}/PTmaxcard`;
            }
        })
    }


    getProfiel(getCustomerId, carD_NO){
        const { Profile } = this.state
        var newProfile = [];
        newProfile.push({
            "userId" : Profile.userId,
            "firstName" : Profile.firstName,
            "lastName" : Profile.lastName,
            "phoneNo" : Profile.phoneNo,
            "ptCardNo" : carD_NO,
            "email" : Profile.email,
            "birthDay" : Profile.birthDay,
            "tkmb" : Profile.tkmb,
            "tkvsm" : getCustomerId.tokenId,
            "customer_id":getCustomerId.customerId,
            "isMember":Profile.isMemberPTG,
            "full_address":Profile.fulL_ADDRESS,
            "titlE_NAME_ID" :Profile.titlE_NAME_ID,
            "titlE_NAME_REMARK":Profile.titlE_NAME_REMARK,
            "citizeN_TYPE_ID":Profile.citizeN_TYPE_ID,
            "citizeN_ID":Profile.citizeN_ID,
            "idCard":Profile.idCard,
            "customeR_STATUS":Profile.customeR_STATUS,
            "isActive":Profile.isActive,
            "gender" : Profile.gender
        });
        var newProfilestringify = JSON.stringify(newProfile)
        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
        var newProfileEncrypt = Cipher.encrypt(keyCipher, newProfilestringify);
        localStorage.setItem("Profile", newProfileEncrypt);
        window.location.href = `${process.env.PUBLIC_URL}/PTmaxcard`;
    }

    modalCheckSubmit(msg,img){
        alert = (
            <SweetAlert
              custom
              showCloseButton
              closeOnClickOutside={false}
              focusConfirmBtn={false}
              title=""
              customIcon={img}
              showConfirm={false}
              showCancelButton
              onCancel={() => this.handleChoice(false)}
              onConfirm={() => this.handleChoice(true)}
            >
              <div className="iconClose" onClick={() => this.handleChoice(false)}></div>
              <div className="fontSizeCase">{msg}</div>
            </SweetAlert>
          );
          this.setState({ show: true, modal: alert });
    }

    handleChoice(){
        this.setState({ modal : null , show : false })
    }

    maxLengthCheck = (object) => {
        if (object.target.value.length > object.target.maxLength) {
            object.target.value = object.target.value.slice(0, object.target.maxLength)
        }
    }

    handleChange(e){
        this.setState({ 
            setOTP : e.target.value.replace(/[^0-9]/g, ''),
        })
    }

    render(){
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].regisotp}
            >
                <div>
                    <div className="bg-body py-5">
                        <div className="content-otp">
                            <div className="container-fluid">
                                <div className="bottom-hr text-gray p-3">
                                    <div className="row">
                                        <div className="col-7 col-xs-7 col-sm-7 col-md-7"><FormattedMessage id="sendotpto" /></div>
                                        <div className="col-5 col-xs-5 col-sm-5 col-md-5 text-black text-right">
                                            {this.props.location.state.data.PHONE_NO ? this.props.location.state.data.PHONE_NO : this.props.location.state.data}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="text-center p-3 my-5">
                                <input 
                                    type="text"
                                    maxLength="6"
                                    onInput={this.maxLengthCheck}
                                    className="inputTel"
                                    onChange={(e) => this.handleChange(e)}
                                />
                            </div>
                            <div className="mt-4 text-center text-gray"><FormattedMessage id="remarkotp" /> : {this.state.resp.ref_No}</div>
                            <div className="mt-5 text-center text-green">
                                <div>{this.getTime()}</div>  
                            </div>
                            <div className="btn_nextstep p-3 text-center">
                                <div className="otp-btn"><button ref="7" id="7" type="button" onClick={() => this.verifyOTPClick(this.state.setOTP,this.state.resp.otP_Key_No)} className="btn btn-secondary btn-block">ยืนยัน OTP</button></div>
                                <div className="text-center text-gray mt-3"><FormattedMessage id="otpsix" /></div>
                            </div>
                            {this.state.modal}
                        </div>
                    </div>
                </div>
            </IntlProvider>
        )
    }
} 
