import React from "react";
import '../../../register/register.css';
import '../../../ptmax/ptmax.css';
import { maxcardActions } from '../../../../_actions/maxcardActions';
import SweetAlert from "react-bootstrap-sweetalert";
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../../translations/en.json";
import intlMessageTH from "../../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

var Cipher = require('aes-ecb');

export class regisnocard extends React.Component {
    constructor(props){
        super(props);
        this.state = {
          language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            show:false,
            modal:null,
            fields: {},
            errors: {},
            showLoading: 'block',
            disableAgreement: false,
            errorsFocus: {
              CARD_ID: "",
              PT_MAX_CARD_NO: "",
              PHONE_NO: "",
            },
            cardIdNumber: '',
            Maxcard:''
        } 
    }

    componentDidMount(){
        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
        var ProfileData = localStorage.getItem('Profile')
        var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
        ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        var ProfileArray = JSON.parse(ProfileDecrypt);
        var Profile = ProfileArray[0];
        // console.log("Profile", Profile)
        this.setState({ 
          Profile , Profile,
          showLoading: 'none'
        })
    }

    handleChange(e) {
      var re = /^[0-9\b]+$/;
        let { errors,errorsFocus, fields, GENDER_ID, TITLE_NAME_REMARK , date } = this.state;
        if (e.target.name === "CARD_ID") {
          if (e.target.value === "" || re.test(e.target.value)) {
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
          } else {
            e.preventDefault();
            // return false;
          }
        } 
        if (e.target.name === "PT_MAX_CARD_NO") {
            const Maxcard = (e.target.validity.valid) ? e.target.value : this.state.Maxcard;
            this.setState({ Maxcard })
            if (e.target.value === "" || re.test(e.target.value)) {
              fields[e.target.name] = e.target.value;
              errors[e.target.name] = null;
              errorsFocus[e.target.name] = '';
            } else {
              errors[e.target.name] = <FormattedMessage id="inputptmaxcard1" />;
              errorsFocus[e.target.name] = '';
              e.preventDefault();
              // return false;
            }
        }else{
          fields[e.target.name] = e.target.value;
          errors[e.target.name] = null;
          errorsFocus[e.target.name] = ''
        }
        if (e.target.name === "PHONE_NO") {
          const cardIdNumber = (e.target.validity.valid) ? e.target.value : this.state.cardIdNumber;
          this.setState({ cardIdNumber });
          if (e.target.value === "" || re.test(e.target.value)) {
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
          } else {
            errors[e.target.name] = <FormattedMessage id="inputphonenotop" />;
            errorsFocus[e.target.name] = ''
            e.preventDefault();
          }
        } else {
          fields[e.target.name] = e.target.value;
          errors[e.target.name] = null;
          errorsFocus[e.target.name] = ''
        }
        this.setState({ errors, fields, GENDER_ID, TITLE_NAME_REMARK , [e.target.id]: e.target.value , date});
    }

    validatePhone(phonenumber) {
      var re = /^[0]{1}[689]/;
      return re.test(String(phonenumber));
    }

    async validateForm() {
        let fields = this.state.fields;
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;

        if (!fields["CARD_ID"]) {
            formIsValid = false;
            errorsFocus["CARD_ID"] = 'errorFocus'
            errors["CARD_ID"] = <FormattedMessage id="plsinputcardid" /> 
        }

        if (!fields["PT_MAX_CARD_NO"]) {
            formIsValid = false;
            errorsFocus["PT_MAX_CARD_NO"] = 'errorFocus'
            errors["PT_MAX_CARD_NO"] = <FormattedMessage id="plsinputptmax" /> 
        }
          
    
        if (!fields["PHONE_NO"]) {
          formIsValid = false;
          errorsFocus["PHONE_NO"] = 'errorFocus'
          errors["PHONE_NO"] = <FormattedMessage id="plsinputphoneno" /> 
        } else {
          if(fields["PHONE_NO"].toString().length ) {}
          var validPhone = this.validatePhone(fields["PHONE_NO"])
          if(validPhone === false) {
            formIsValid = false;
            errorsFocus["PHONE_NO"] = 'errorFocus'
            errors["PHONE_NO"] = <FormattedMessage id="plsinputphonenonine" /> 
          }
        } 
        this.setState({
          errors: errors,
          errorsFocus: errorsFocus
        });
    
        return formIsValid;
    }

    async submitFormMb(){
      if(await this.validateForm()){
        this.submitFinalForm();
      }else{
        //  console.log(false);
      }
    }

    handleChoice(choice) {
      if (choice === false) {
      if(this.state.disableAgreement == true){
          this.setState({ show: false , modal: null })
      }else{
          this.setState({ show: false , modal: null })
        }
      }
    }

    modalCheckSubmit(res, img) {
    alert = (
      <SweetAlert
        custom
        showCloseButton
        closeOnClickOutside={false}
        focusConfirmBtn={false}
        title=""
        customIcon={img}
        showConfirm={false}
        showCancelButton
        onCancel={() => this.handleChoice(false)}
        onConfirm={() => this.handleChoice(true)}
      >
        <div className="iconClose" onClick={() => this.handleChoice(false)}></div>
        <div className="fontSizeCase">{res}</div>
      </SweetAlert>
    );
    this.setState({ show: true, modal: alert });
    }

    submitFinalForm(){
      var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
      var ProfileData = localStorage.getItem('Profile')
      var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
      ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
      var ProfileArray = JSON.parse(ProfileDecrypt);
      var Profile = ProfileArray[0];
      var userid = Profile.userId;
      let formData = new FormData();
      formData.append("userId", userid);
      formData.append("ptMaxCardNo", this.state.fields.PT_MAX_CARD_NO);
      formData.append("idCard", this.state.fields.CARD_ID);
      formData.append("phoneNo", this.state.fields.PHONE_NO);
      maxcardActions.CheckPhoneBeforeSyncPTCard(formData).then(e => {
        console.log("CheckPhoneBeforeSyncPTCard", e)
        if(e.data.isSuccess === false) {
          this.setState({ disableAgreement: false })
          this.setState({ resultApi: e.data.errMsg })
      //     this.setState({ disabledBtn: '' })
          var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
          this.modalCheckSubmit(this.state.resultApi,imgPopup)
        } else {
          this.setState({ resultApi: e.data.errMsg })
          this.props.history.push({
              pathname: `${process.env.PUBLIC_URL}/PTmaxcard/sync/otp`,
              state: {
              data: this.state.fields
              }
          });
        }
      })
  }

  openRegiaterPage(){
    window.location.href=`${process.env.PUBLIC_URL}/PTmaxcard/register`;
  }

  openReportPage(){
    window.location.href = `${process.env.PUBLIC_URL}/PTmaxcard/report`;
  }

    render(){
      var setLangRegisterSuccess = messages[this.state.language].regisnocard
        return(
          <IntlProvider
              locale={this.state.language}
              messages={messages[this.state.language].regisnocard}
          >
            <div>
                <div id="loading" style={{ display : this.state.showLoading }}>
                    <div className="content_class">
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                </div>

                <div className="bg-ptmax" style={{ display: this.state.content_page}}>
                    <div className="container p-0">
                        <form className="form-horizontal">
                        <div className="row">
                                <div className="col-sm-6 col-md-12">
                                    <div className="text-white text-sub-head "><FormattedMessage id="getptmaxcard" /></div>
                                </div>
                            </div>
                            <div className="row justify-content-md-center">
                                <div className="col-sm-6 col-md-8 box-content">
                                   
                                    <div className="form-group">
                                        <label>
                                            <span className="label-validate">* </span>
                                            <span className="font-header-label pl-2"><FormattedMessage id="numberptmaxcard" /></span>
                                        </label>
                                        <div>
                                            <input
                                                type="text"
                                                maxLength="10"
                                                pattern="[0-9]*"
                                                value={this.state.Maxcard}
                                                className={`form-control ${this.state.errorsFocus['PT_MAX_CARD_NO']}`}
                                                name="PT_MAX_CARD_NO"
                                                onChange={e => this.handleChange(e)}
                                                // value={this.state.fields['PT_MAX_CARD_NO']}
                                                placeholder={setLangRegisterSuccess.inputptmaxcard}
                                            />
                                        </div>
                                        <div className="errorMsg PT_MAX_CARD_NO">{this.state.errors["PT_MAX_CARD_NO"]}</div>
                                    </div>
                                    <div className="form-group">
                                        <label>
                                            <span className="label-validate">* </span>
                                            <span className="font-header-label pl-2"><FormattedMessage id="cardid" /></span>
                                        </label>
                                        <div>
                                            <input
                                                type="text"
                                                className={`form-control ${this.state.errorsFocus['CARD_ID']}`}
                                                name="CARD_ID"
                                                maxlength="13"
                                                onChange={e => this.handleChange(e)}
                                                value={this.state.fields['CARD_ID']}
                                                placeholder={setLangRegisterSuccess.inputidcard}
                                            />
                                        </div>
                                        <div className="errorMsg CARD_ID">{this.state.errors["CARD_ID"]}</div>
                                    </div>
                                    <div className="form-group">
                                        <label>
                                            <span className="label-validate">* </span>
                                            <span className="font-header-label pl-2"><FormattedMessage id="phoneno" /></span>
                                        </label>
                                        <div>
                                            <input
                                                type="text"
                                                className={`form-control ${this.state.errorsFocus['PHONE_NO']}`}
                                                maxLength="10"
                                                pattern="[0-9]*"
                                                name="PHONE_NO"
                                                value={this.state.cardIdNumber}
                                                onChange={e => this.handleChange(e)}
                                                // value={this.state.fields['PHONE_NO']}
                                                placeholder={setLangRegisterSuccess.inputphoneno}
                                            />
                                        </div>
                                        <div className="errorMsg PHONE_NO">{this.state.errors["PHONE_NO"]}</div>
                                    </div>
                                    <div className="form-group mt-5">
                                        <div className="">
                                          <button 
                                            type="button" 
                                            className={`btn btn-gray btn-block btn_agreement ${this.state.disabledBtn}`} 
                                            onClick={() => this.submitFormMb()}>
                                            <FormattedMessage id="btnnext" />
                                          </button>
                                        </div>
                                    </div>

                                    <div className="form-group mt-2">
                                        <div className="">
                                            <button
                                            type="button"
                                            className="btn btn-secondary btn-block btn_agreement"
                                            onClick={() => this.openRegiaterPage()}
                                            >
                                            <FormattedMessage id="btnregis" />
                                            </button>
                                        </div>
                                    </div>
                                    <div className="form-group" style={{marginTop:'50px'}}>
                                      <div className="">
                                          <button
                                          type="button"
                                          className="btn btn-gray btn-block btn_agreement"
                                          onClick={() => this.openReportPage()}
                                          >
                                          <FormattedMessage id="sendproblem" /> 
                                          </button>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        
                        {this.state.modal}
                    </div>
                
                        
                </div>
            </div>
            </IntlProvider>

        )
    }
} 
