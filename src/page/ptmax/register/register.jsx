import React , { useEffect, useState } from 'react';
import '../../ptmax/ptmax.css';
import { registerAction } from '../../../_actions/registerActions';
import moment from "moment-timezone";
import 'moment/locale/th';
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from 'react-datepicker';
import EXIF from "exif-js"
import SweetAlert from "react-bootstrap-sweetalert";
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');
var crypto = require('crypto');

sessionStorage.setItem('inputBody','{}')
let SessionData = JSON.parse(sessionStorage.getItem('inputBody'))
var genderClass = "form-control";
let alert
var disabledimage = true
var disabledimage2 = true
let amphureListData = [];
let tumbonListData = [];
let postcodeListData = [];
let postcodeListDataNew = [];
var ERROR_USER_EMAIL = false;
var disabled_block = 'form-control input_form disabled';
var entertain_remark_display = 'form-control input_form disabled';
var sport_remark_display = 'form-control input_form disabled';
var outside_remark_display = 'form-control input_form disabled';
var inside_remark_display = 'form-control input_form disabled';

export class registemaxcard extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
      date : new Date(),
      isForeigner: (JSON.parse(sessionStorage.getItem('inputBody')).CARD_TYPE_ID != null && JSON.parse(sessionStorage.getItem('inputBody')).CARD_TYPE_ID === 2 ? "block" : "none" ),
      inputcard: (JSON.parse(sessionStorage.getItem('inputBody')).CARD_TYPE_ID != null && JSON.parse(sessionStorage.getItem('inputBody')).CARD_TYPE_ID === 2 ? false : true ),
      errors: {},
      errorsFocus: {
        CARD_ID: "",
        CARD_TYPE_ID: "",
        TITLE_NAME_ID: "",
        GENDER_ID: "",
        PHONE_NO: "",
        MARITAL_STATUS: "",
        FNAME_TH:"",
        LNAME_TH:""
      },
      TITLE_NAME_ID: 1,
      TITLE_NAME_REMARK: "none",
      GENDER_ID:1,
      file: '',
      agreement: false,
      imagePreviewUrl: '',
      nexbtn: 'button_finish',
      disableAgreement: false,
      disabledBtn: '',
      resultApi: '',
      errorFileSize: '',
      closeBtn: {
        imgPreview1: `none`,
        imgPreview2: `none`,
        imgPreview3: `none`,
      },
      classPreview: {
        imgPreview1: `preview-image`,
        imgPreview2: `preview-image`,
        imgPreview3: `preview-image`,
      },
      imgPreview: {
        imgPreview1: `${process.env.PUBLIC_URL}/images/mockup.png`,
        imgPreview2: `${process.env.PUBLIC_URL}/images/mockup.png`,
        imgPreview3: `${process.env.PUBLIC_URL}/images/mockup.png`,
      },
      fields: {
        CARD_TYPE_ID: (JSON.parse(sessionStorage.getItem('inputBody')).CARD_TYPE_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).CARD_TYPE_ID : 1 ),
        CARD_ID: (JSON.parse(sessionStorage.getItem('inputBody')).CARD_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).CARD_ID : "" ),
        Laser: (JSON.parse(sessionStorage.getItem('inputBody')).Laser != null ? JSON.parse(sessionStorage.getItem('inputBody')).Laser : "" ),
        TITLE_NAME_ID: (JSON.parse(sessionStorage.getItem('inputBody')).TITLE_NAME_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).TITLE_NAME_ID : 0 ),
        TITLE_NAME_REMARK: (JSON.parse(sessionStorage.getItem('inputBody')).TITLE_NAME_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).TITLE_NAME_REMARK : "" ),
        GENDER_ID: (JSON.parse(sessionStorage.getItem('inputBody')).GENDER_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).GENDER_ID : 0 ),
        FNAME_TH: (JSON.parse(sessionStorage.getItem('inputBody')).FNAME_TH != null ? JSON.parse(sessionStorage.getItem('inputBody')).FNAME_TH : sessionStorage.getItem("_firstname") ),
        LNAME_TH: (JSON.parse(sessionStorage.getItem('inputBody')).LNAME_TH != null ? JSON.parse(sessionStorage.getItem('inputBody')).LNAME_TH : sessionStorage.getItem("_lastname") ),
        BIRTH_DATE: (JSON.parse(sessionStorage.getItem('inputBody')).BIRTH_DATE != null ? JSON.parse(sessionStorage.getItem('inputBody')).BIRTH_DATE : sessionStorage.getItem("_birthdate") ),
        MARITAL_STATUS: (JSON.parse(sessionStorage.getItem('inputBody')).MARITAL_STATUS != null ? JSON.parse(sessionStorage.getItem('inputBody')).MARITAL_STATUS : 0 ),
        ADDR_ADDRESS: "",
        ADDR_MOO: "",
        ADDR_VILLEGE: "",
        ADDR_SOI: "",
        ADDR_STREET: "",
        ADDR_ORG_SUB_DISTRICT_ID: "",
        ADDR_ORG_AMPHURE_ID: "",
        ADDR_ORG_PROVINCE_ID: "",
        ADDR_ORG_POSTALCODE_ID: "",
        ADDR_OTHER: "",
        OCCUPATION_ID: "",
        OCCAPATION_REMARK: "",
        EDUCATION_ID: "",
        INCOME_ID: "",
        USER_EMAIL: (JSON.parse(sessionStorage.getItem('inputBody')).USER_EMAIL != null ? JSON.parse(sessionStorage.getItem('inputBody')).USER_EMAIL : sessionStorage.getItem("_emid") ),
        PHONE_NO: (JSON.parse(sessionStorage.getItem('inputBody')).PHONE_NO != null ? JSON.parse(sessionStorage.getItem('inputBody')).PHONE_NO : sessionStorage.getItem("_phid") ),
        PHONE_HOME: "",
        ABOUT_SOURCE: "",
        REF_CARD_NO: "",
        PMT_FOOD: "",
        PMT_TRAVEL: "",
        PMT_SHOPPING: "",
        PMT_BEAUTY: "",
        PMT_SPORT_AND_CAR: "",
        PMT_OTHERS: "",
        PMT_OTHERS_REMARK: "",
        ACT_EN_MUSIC: "",
        ACT_EN_CONCERT: "",
        ACT_EN_COUNTRY_CONCERT: "",
        ACT_EN_MOVIES: "",
        ACT_EN_THEATER: "",
        ACT_EN_OTHERS: "",
        ACT_EN_OTHERS_REMARK: "",
        ACT_SPT_FITNESS: "",
        ACT_SPT_RUN: "",
        ACT_SPT_CYCLING: "",
        ACT_SPT_FOOTBALL: "",
        ACT_SPT_OTHERS: "",
        ACT_SPT_OTHERS_REMARK: "",
        ACT_OD_SHOPPING: "",
        ACT_OD_BEAUTY: "",
        ACT_OD_RESTAURANT: "",
        ACT_OD_CLUB: "",
        ACT_OD_MOMCHILD: "",
        ACT_OD_RALLY: "",
        ACT_OD_ADVANTURE: "",
        ACT_OD_PHOTO: "",
        ACT_OD_PHILANTHROPY: "",
        ACT_OD_HOROSCOPE: "",
        ACT_OD_ABROAD: "",
        ACT_OD_UPCOUNTRY: "",
        ACT_OD_OTHERS: "",
        ACT_OD_OTHERS_REMARK: "",
        ACT_ID_COOKING: "",
        ACT_ID_BAKING: "",
        ACT_ID_DECORATE: "",
        ACT_ID_GARDENING: "",
        ACT_ID_READING: "",
        ACT_ID_GAMING: "",
        ACT_ID_INTERNET: "",
        ACT_ID_OTHERS: "",
        ACT_ID_OTHERS_REMARK: "",
        ACT_HNGOUT_FRIEND: "",
        ACT_HNGOUT_COUPLE: "",
        ACT_HNGOUT_FAMILY: "",
        ACT_HNGOUT_NO: "",
        CARD_CAR_TYPE_ID: "",
        CAR_BRAND: "",
        CAR_BRAND_REMARK: "",
        CUSTOMER_IMG_INFO: [],
        CARD_ID_ENG: (JSON.parse(sessionStorage.getItem('inputBody')).CARD_ID_ENG != null ? JSON.parse(sessionStorage.getItem('inputBody')).CARD_ID_ENG : "" )
      },
      show: false,
      modal: null,
      activeColor2: "fade",
      activeColor3: "fade",
      activeColor1: "active",
      active1:"active",
      active2:"",
      active3:"",
      additionnal:'none',
      submitFormNoadditional:'block',
      OCCAPATION_REMARK:'none',
      CAR_BRAND_REMARK:'none',
      disabled: true,
      disabled_entertain:true,
      disabled_sport: true,
      disabled_outside: true,
      disabled_inside: true,
      disabledBtn: '',
      showLoading: 'block',
      content_page: 'none',
      chkDisplayAdd:'none',
      chkDisplayAddress:'block',
      showLoadingContent: 'none',
      chkPDPA : 'none',
      showAddition:'none',



      DopaBirthDay: "DMY",
      DopaBirthDayValue: "",
      DopaBirthDayShow: "",
      DopaBirthDayText: "วัน/เดือน/ปีเกิด",
      isOpenDOPASelectDate: false,

      DopaDay: "00",
      DopaMonth: "00",
      DopaYear: "00",
      DopaBirthDayType: "DMY"
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount(){
    let { fields ,TITLE_NAME_REMARK , checkType , CAR_BRAND_REMARK, OCCAPATION_REMARK } = this.state;
    const params = this.props.match.params;

    this.getDropdownlist_type("TITLE").then(result => {
      if(result === undefined){
        var showLoading = '';
        var content_page = '';
          showLoading = 'block';
          content_page = 'none';
        var th = this;
        setTimeout(function(){
            th.setState({ showLoading : showLoading})
            th.setState({ content_page: content_page })
        },1000)
      }
      else if(result.status === 200){
        this.setState({ name_titles: result.data.dropdowN_INFO });
        var showLoading = '';
        var content_page = '';
          showLoading = 'none';
          content_page = 'block';
        var th = this;
        setTimeout(function(){
            th.setState({ showLoading : showLoading})
            th.setState({ content_page: content_page })
        },1000)
      }
    });

    this.getDropdownlist_type("GENDER").then(result => {
      if(result === undefined){
        var showLoading = '';
        var content_page = '';
          showLoading = 'block';
          content_page = 'none';
        var th = this;
        setTimeout(function(){
            th.setState({ showLoading : showLoading})
            th.setState({ content_page: content_page })
        },1000)
      }
      else if(result.status === 200){
        this.setState({ gender: result.data.dropdowN_INFO });
        var showLoading = '';
        var content_page = '';
          showLoading = 'none';
          content_page = 'block';
        var th = this;
        setTimeout(function(){
            th.setState({ showLoading : showLoading})
            th.setState({ content_page: content_page })
        },1000)
      }
    });

    this.getDropdownlist_type("MARITAL_STATUS").then(result => {
      if(result === undefined){
        var showLoading = '';
        var content_page = '';
          showLoading = 'block';
          content_page = 'none';
        var th = this;
        setTimeout(function(){
            th.setState({ showLoading : showLoading})
            th.setState({ content_page: content_page })
        },1000)
      }
      else if(result.status === 200){
        this.setState({ marital_status: result.data.dropdowN_INFO });
        var showLoading = '';
        var content_page = '';
          showLoading = 'none';
          content_page = 'block';
        var th = this;
        setTimeout(function(){
            th.setState({ showLoading : showLoading})
            th.setState({ content_page: content_page })
        },1000)
      }
    });

    this.getProvinceNewApi().then(result => { 
      if(result === undefined){
        var showLoading = '';
        var content_page = '';
          showLoading = 'block';
          content_page = 'none';
        var th = this;
        setTimeout(function(){
            th.setState({ showLoading : showLoading})
            th.setState({ content_page: content_page })
        },1000)
      }
      else if(result.status === 200){
        this.setState({ province: result.data.dropdowN_INFO });
        var showLoading = '';
        var content_page = '';
          showLoading = 'none';
          content_page = 'block';
        var th = this;
        setTimeout(function(){
            th.setState({ showLoading : showLoading})
            th.setState({ content_page: content_page })
        },1000)
      }
    })
    this.getAmphure(this.state.fields['ADDR_ORG_PROVINCE_ID'])
    this.getDropdownlist_type("OCCUPATION").then(result => {this.setState({ occupation: result.data.dropdowN_INFO })})
    this.getDropdownlist_type("EDUCATION").then(result => {this.setState({ education: result.data.dropdowN_INFO })})
    this.getDropdownlist_type("INCOME").then(result => {this.setState({ income: result.data.dropdowN_INFO })})
    this.getDropdownlist_type("CAR_TYPE").then(result => {this.setState({ cartype: result.data.dropdowN_INFO })})
    this.getDropdownlist_type("CAR_BRAND").then(result => {this.setState({ car_brand: result.data.dropdowN_INFO })})
          
    fields['OCCUPATION_ID'] = (JSON.parse(sessionStorage.getItem('inputBody')).OCCUPATION_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).OCCUPATION_ID : 0)
    fields['CAR_TYPE'] = (JSON.parse(sessionStorage.getItem('inputBody')).CAR_TYPE != null ? JSON.parse(sessionStorage.getItem('inputBody')).CAR_TYPE : 0)
    fields['CAR_BRAND'] = (JSON.parse(sessionStorage.getItem('inputBody')).CAR_BRAND != null ? JSON.parse(sessionStorage.getItem('inputBody')).CAR_BRAND : 0)
    fields['EDUCATION_ID'] = (JSON.parse(sessionStorage.getItem('inputBody')).EDUCATION_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).EDUCATION_ID : 0)
    fields['INCOME_ID'] = (JSON.parse(sessionStorage.getItem('inputBody')).INCOME_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).INCOME_ID : 0)
    
    if(SessionData != null) {
      let newObject = Object.assign(SessionData , this.state.fields)
      fields = newObject
      if (fields.TITLE_NAME_ID === 4) {
        TITLE_NAME_REMARK = "block";
      }
      if (fields.CAR_BRAND === 9) {
        CAR_BRAND_REMARK = "block";
      }
      if (fields.OCCUPATION_ID === 5) {
        OCCAPATION_REMARK = "block";
      }
      this.setState({ fields, TITLE_NAME_REMARK , checkType ,CAR_BRAND_REMARK, OCCAPATION_REMARK})
    }

    if(localStorage.getItem('Profile') !== '' && localStorage.getItem('Profile') !== null){
        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
        var ProfileData = localStorage.getItem('Profile')
        var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
        ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        var ProfileArray = JSON.parse(ProfileDecrypt);
        var Profile = ProfileArray[0];
        fields['FNAME_TH'] = Profile.firstName;
        fields['LNAME_TH'] = Profile.lastName;
        fields['BIRTH_DATE'] = Profile.birthDay;
        fields['USER_EMAIL'] = Profile.email;
        fields['PHONE_NO'] = Profile.phoneNo;
        this.setState({ Profile , Profile })
    }
  }

  renderAdditional(type) {
      const { occupation, education, income, cartype, car_brand} = this.state;
      var option = []
      var name_title;
      if (type === 'type_cartype') {
        name_title = cartype 
      } else if (type === 'type_brand') {
          name_title = car_brand 
      } else if (type === 'type_occupation') {
            name_title = occupation 
      } else if (type === 'type_education'){
        name_title = education 
      } else {
        name_title = income 
      }
      for(var i in name_title) {
        option.push(<option key={i} value={name_title[i].code}>{name_title[i].values}</option>)
      }
      return option
  }

  dropdownlistCheck(e) {
    let { errors, errorsFocus ,fields, CAR_BRAND_REMARK, OCCAPATION_REMARK } = this.state;
    if(e.target.name === "CAR_BRAND") {
      if(e.target.value === '9'){
        CAR_BRAND_REMARK = 'block'
      } else {
        CAR_BRAND_REMARK = 'none'
        fields["CAR_BRAND_REMARK"] = '';
        errors["CAR_BRAND_REMARK"]  = null;
        errorsFocus["CAR_BRAND_REMARK"]  = ''
      }
      errors[e.target.name] = null;
      fields[e.target.name] = parseInt(e.target.value)
      errorsFocus[e.target.name] = ''
    }else if(e.target.name === "OCCUPATION_ID") {
      if(e.target.value === '5'){
        OCCAPATION_REMARK = 'block'
      } else {
        OCCAPATION_REMARK = 'none'
        fields["OCCAPATION_REMARK"] = '';
        errors["OCCAPATION_REMARK"]  = null;
        errorsFocus["OCCAPATION_REMARK"]  = ''
      }
      errors[e.target.name] = null;
      fields[e.target.name] = parseInt(e.target.value)
      errorsFocus[e.target.name] = ''
    } else {
      fields[e.target.name] = e.target.value;
      errors[e.target.name] = null;
      errorsFocus[e.target.name] = ''
    }
    this.setState({ errors, errorsFocus, fields, CAR_BRAND_REMARK, OCCAPATION_REMARK})
  }
  
    checboxClick(ev) {
      let { fields } = this.state;
      if(ev.target.name === "news") {
        fields[ev.target.id] = ev.target.checked
        if (ev.target.id === "SUBSCRIBE_NONE") {
          if(ev.target.checked === true) {
            fields['SUBSCRIBE_EMAIL'] = false
            fields['SUBSCRIBE_LETTER'] = false
            fields['SUBSCRIBE_SMS'] = false
          }
        } else {
          fields['SUBSCRIBE_NONE'] = false
        }
      }else if(ev.target.name === "activities"){
        fields[ev.target.id] = ev.target.checked
        if (ev.target.id === "ACT_HNGOUT_NO") {
          if(ev.target.checked === true) {
            fields['ACT_HNGOUT_FRIEND'] = false
            fields['ACT_HNGOUT_COUPLE'] = false
            fields['ACT_HNGOUT_FAMILY'] = false
          }
        } else {
          fields['ACT_HNGOUT_NO'] = false
        }
      }else {
        fields[ev.target.id] = ev.target.value
      }
      this.setState({ fields })
    }

    handleGameClick(ev){
      let { fields } = this.state;
      if(ev.target.name === "entertain") {
        if (ev.target.id === "ACT_EN_OTHERS") {
          if(ev.target.checked === true) {
            entertain_remark_display = 'form-control input_form'
            fields[ev.target.id] = ev.target.checked
          } else {
            entertain_remark_display = 'form-control input_form disabled'
            fields['ACT_EN_OTHERS_REMARK'] = ''
            fields[ev.target.id] = ev.target.checked
          }
        } else {
          if(fields['ACT_EN_OTHERS'] === true) {
            entertain_remark_display = 'form-control input_form'
          } else {
            entertain_remark_display = 'form-control input_form disabled'
            fields['ACT_EN_OTHERS_REMARK'] = ''
          }
          fields[ev.target.id] = ev.target.checked
        }
        this.setState( {disabled_entertain: (ev.target.checked === "" ? false : true)} )
      } else if(ev.target.name === "sport") {
        if (ev.target.id === "ACT_SPT_OTHERS") {
          if(ev.target.checked === true) {
            sport_remark_display = 'form-control input_form'
            fields[ev.target.id] = ev.target.checked
          } else {
            sport_remark_display = 'form-control input_form disabled'
            fields['ACT_SPT_OTHERS_REMARK'] = ''
            fields[ev.target.id] = ev.target.checked
          }
        } else {
          if (fields['ACT_SPT_OTHERS'] === true) {
            sport_remark_display = 'form-control input_form'
          } else {
            sport_remark_display = 'form-control input_form disabled'
            fields['ACT_SPT_OTHERS_REMARK'] = ''
          }
          fields[ev.target.id] = ev.target.checked
        }
        this.setState( {disabled_sport: (ev.target.checked === "" ? false : true)} )
      } else if(ev.target.name === "outside") {
        if (ev.target.id === "ACT_OD_OTHERS") {
          if(ev.target.checked === true) {
            outside_remark_display = 'form-control input_form'
            fields[ev.target.id] = ev.target.checked
          } else {
            outside_remark_display = 'form-control input_form disabled'
            fields['ACT_OD_OTHERS_REMARK'] = ''
            fields[ev.target.id] = ev.target.checked
          }
        } else {
          if (fields['ACT_OD_OTHERS'] === true){
            outside_remark_display = 'form-control input_form'
          } else {
            outside_remark_display = 'form-control input_form disabled'
            fields['ACT_OD_OTHERS_REMARK'] = ''
          }
          fields[ev.target.id] = ev.target.checked
        }  
        this.setState( {disabled_outside: (ev.target.checked === "" ? false : true)} )
      } else if(ev.target.name === "inside") {
        if (ev.target.id === "ACT_ID_OTHERS") {
          if(ev.target.checked === true) {
            inside_remark_display = 'form-control input_form'
            fields[ev.target.id] = ev.target.checked
          } else {
            inside_remark_display = 'form-control input_form disabled'
            fields['ACT_ID_OTHERS_REMARK'] = ''
            fields[ev.target.id] = ev.target.checked
          }
        } else {
          if ( fields['ACT_ID_OTHERS'] === true) {
            inside_remark_display = 'form-control input_form'
          } else {
            inside_remark_display = 'form-control input_form disabled'
            fields['ACT_ID_OTHERS_REMARK'] = ''
          }
          fields[ev.target.id] = ev.target.checked
        }  
        this.setState( {disabled_inside: (ev.target.checked === "" ? false : true)} )
      } else if(ev.target.name === "inside") { 
        fields['ACT_HNGOUT_FRIEND'] = false
        fields['ACT_HNGOUT_COUPLE'] = false
        fields['ACT_HNGOUT_FAMILY'] = false
        fields['ACT_HNGOUT_NO'] = false
        fields[ev.target.id] = ev.target.checked
      } else {
        fields[ev.target.id] = ev.target.value
      }
      this.setState({ fields })
    }

    handleGameClik(ev) {
      let { fields } = this.state;
      if(ev.target.name === "promotion") {
        if (ev.target.id === "PMT_OTHERS") {
          if(ev.target.checked === true) {
            disabled_block = 'form-control input_form'
            fields[ev.target.id] = ev.target.checked
          } else {
            disabled_block = 'form-control input_form disabled'
            fields['PMT_OTHERS_REMARK'] = ''
            fields[ev.target.id] = ev.target.checked
          }
        } else {
          if(fields['PMT_OTHERS'] === true) {
            disabled_block = 'form-control input_form'
          } else {
            disabled_block = 'form-control input_form disabled'
            fields['PMT_OTHERS_REMARK'] = ''
          }
          fields[ev.target.id] = ev.target.checked
        }
        this.setState( {disabled: (ev.target.checked === "" ? false : true)} )
      } else {
        fields[ev.target.id] = ev.target.value
      }
        this.setState({ fields })
    } 

    getProvinceNewApi(){
        return registerAction.getProvinceNew()
    }
    
    getDropdownlist_type(title) {
      return registerAction.getDropdownlist(title);
    }
    
    getAmphure(val, type) {
        let { fields } = this.state;
        if(type) {
          fields['ADDR_ORG_AMPHURE_ID'] = ''
          fields['ADDR_ORG_SUB_DISTRICT_ID'] = ''
          fields['ADDR_ORG_POSTALCODE_ID'] = ''
        }
        this.setState({ amphure: '' })
        var value = val;
        fields['ADDR_ORG_PROVINCE_ID'] = value
        this.setState({ fields })
        registerAction.getAmphure(value).then(e => {
          this.setState({
            amphure: e.data.data
          })
          this.getTumbon(this.state.fields['ADDR_ORG_AMPHURE_ID'])
        })
    }
    
    getTumbon(val, type) {
        let { fields } = this.state;
        if(type) {
          fields['ADDR_ORG_SUB_DISTRICT_ID'] = ''
          fields['ADDR_ORG_POSTALCODE_ID'] = ''
        }
        fields['ADDR_ORG_AMPHURE_ID'] = val
        this.setState({ fields })
        var tumbon = val
        registerAction.getTumbon(tumbon).then(e => {
          this.setState({
            tumbon: e.data.data
          })
          this.getPostcode(this.state.fields['ADDR_ORG_SUB_DISTRICT_ID'])
        })
    }
    
    getPostcode(val, type) {
        let { fields } = this.state;
        if(type) {
          fields['ADDR_ORG_POSTALCODE_ID'] = ''
        }
        fields['ADDR_ORG_SUB_DISTRICT_ID'] = val
        this.setState({ fields })
        var postcode = val
        registerAction.getPostcode(postcode).then(e => {
          if(e.data.data.length === 1){
              fields['ADDR_ORG_POSTALCODE_ID'] = e.data.data[0].codE_GUID
              this.setState({ fields })
          }
            this.setState({
              postcode: e.data.data,
            })
        })
    }

    changeHandle(e) {
      var { errorsFocus,errors, fields , additionnal , submitFormNoadditional , chkDisplayAdd , chkDisplayAddress , nexbtn } = this.state;
      var checkedForeign = (e.target.checked === true) ? 2 : 1;
      fields["CARD_TYPE_ID"] = checkedForeign;
      if(checkedForeign === 1 && e.target.name === 'aggreement') {
        errorsFocus['CARD_ID_ENG'] = '';
        errors['CARD_ID_ENG'] = '';
        fields['CARD_ID_ENG'] = '';
        chkDisplayAdd = 'none';
        chkDisplayAddress = 'block';
        fields['ADDR_NO'] = '';
        fields['ADDR_STREET'] = '';
        fields['ADDR_VILLAGE'] = '';
        fields['ADDR_ORG_PROVINCE_ID'] = '';
        fields['ADDR_ORG_AMPHURE_ID'] = '';
        fields['ADDR_ORG_SUB_DISTRICT_ID'] = '';
        fields['ADDR_ORG_POSTALCODE_ID'] = '';
      } else if(checkedForeign === 2 && e.target.name === 'aggreement') {
        errorsFocus['CARD_ID'] = ''
        errors['CARD_ID'] = ''
        fields['CARD_ID'] = '';
        fields['Laser'] = '';
        chkDisplayAdd = 'block';
        chkDisplayAddress = 'none';
        fields['ADDR_NO'] = '90';
        fields['ADDR_STREET'] = 'ถนนรัชดาภิเษก';
        fields['ADDR_VILLAGE'] = 'CW TOWER A (33rd Floor)';
        fields['ADDR_ORG_PROVINCE_ID'] = 'F22C6DB0-D492-E311-9402-0050568975FF';
        fields['ADDR_ORG_AMPHURE_ID'] = '531A266E-C692-E311-9402-0050568975FF';
        fields['ADDR_ORG_SUB_DISTRICT_ID'] = 'EF57AE60-4992-E311-9402-0050568975FF';
        fields['ADDR_ORG_POSTALCODE_ID'] = 'D8987141-D592-E311-9402-0050568975FF';
      }

      if(e.target.checked === true) {
        nexbtn = 'button_nextstep'
        additionnal = 'block';
        submitFormNoadditional = 'none';
      } else {
        nexbtn = 'button_finish'
        additionnal = 'none';
        submitFormNoadditional = 'block';
      }

      this.setState({ 
        nexbtn , 
        additionnal , 
        submitFormNoadditional , 
        agreement: e.target.checked , 
        errorsFocus, 
        errors, 
        fields,
        isForeigner: e.target.checked === true ? "block" : "none", 
        inputcard: e.target.checked === true ? false : true ,
        chkDisplayAdd ,
        chkDisplayAddress ,
        nexbtn 
      })
    }

    changeHandlePDPA(e){
      let { chkPDPA , btnBlock , fields } = this.state;
      if(e.target.name === "pdpacondition"){
        if(e.target.checked === true){
          sessionStorage.setItem('chkPDPA',true);
          fields['chkPDPA'] = e.target.checked;
          chkPDPA = 'block'; 
          btnBlock = 'block';
        }else{
          sessionStorage.setItem('chkPDPA',false);
          fields[e.target.name] = e.target.checked;
          chkPDPA = 'none'; 
          btnBlock = 'none';
        }
      }
      // else if(e.target.name === "pdpacon1"){
      //   if(e.target.checked === true){
      //     sessionStorage.setItem('pdpacon1', JSON.parse(true));
      //     fields[e.target.name] = e.target.checked;
      //     fields['pdpacon1'] = true;
      //   }else{
      //     sessionStorage.setItem('pdpacon1', JSON.parse(false));
      //     fields[e.target.name] = e.target.checked;
      //     fields['pdpacon1'] = false;
      //   }
      // }else if(e.target.name === "pdpacon2"){
      //   if(e.target.checked === true){
      //     sessionStorage.setItem('pdpacon2', JSON.parse(true));
      //     fields[e.target.name] = e.target.checked;
      //     fields['pdpacon2'] = true;
      //   }else{
      //     sessionStorage.setItem('pdpacon2', JSON.parse(false));
      //     fields[e.target.name] = e.target.checked;
      //     fields['pdpacon2'] = false;
      //   }
      // }else if(e.target.name === "pdpacon3"){
      //   if(e.target.checked === true){
      //     sessionStorage.setItem('pdpacon3', JSON.parse(true));
      //     fields[e.target.name] = e.target.checked;
      //     fields['pdpacon3'] = true;
      //   }else{
      //     sessionStorage.setItem('pdpacon3', JSON.parse(false));
      //     fields[e.target.name] = e.target.checked;
      //     fields['pdpacon3'] = false;
      //   }
      // }else if(e.target.name === "pdpacon4"){
      //   if(e.target.checked === true){
      //     sessionStorage.setItem('pdpacon4', JSON.parse(true));
      //     fields[e.target.name] = e.target.checked;
      //     fields['pdpacon4'] = true;
      //   }else{
      //     sessionStorage.setItem('pdpacon4', JSON.parse(false));
      //     fields[e.target.name] = e.target.checked;
      //     fields['pdpacon4'] = false;
      //   }
      // }else if(e.target.name === "pdpacon5"){
      //   if(e.target.checked === true){
      //     sessionStorage.setItem('pdpacon5', JSON.parse(true));
      //     fields[e.target.name] = e.target.checked;
      //     fields['pdpacon5'] = true;
      //   }else{
      //     sessionStorage.setItem('pdpacon5', JSON.parse(false));
      //     fields[e.target.name] = e.target.checked;
      //     fields['pdpacon5'] = false;
      //   }
      // }
      
      this.setState({ chkPDPA , btnBlock , fields })
    }

    renderContent(type) {
        const { name_titles, gender, marital_status } = this.state;
        var option = [];
        var name_title;
        if (type === "type_title_name") {
          name_title = name_titles;
        } else if (type === "type_gender") {
          name_title = gender;
        } else {
          name_title = marital_status;
        }
        for (var i in name_title) {
          option.push(
            <option key={i} value={name_title[i].code}>
              {name_title[i].values}
            </option>
          );
        }
        return option;
    }

    handleUploadFile = (e) => {
        if(e === 1) { 
          this.inputElement_1.value = "";
          this.inputElement_1.click(); 
          disabledimage = false;
        }
        if(e === 2) { 
          this.inputElement_2.value = "";
          this.inputElement_2.click(); 
          disabledimage2 = false;
        }
        if(e === 3) { 
          this.inputElement_3.value = "";
          this.inputElement_3.click(); 
        }
    }
    
    deleteLocalStorage(target) {
        if(target == 'imgPreview1'){
          disabledimage = true;
          disabledimage2 = true;
        }else if(target == 'imgPreview2'){
          disabledimage2 = true;
        }
        let { closeBtn, imgPreview, classPreview } = this.state;
        localStorage.removeItem(target);
        closeBtn[target] = 'none';
        classPreview[target] = 'preview-image';
        imgPreview[target] = `${process.env.PUBLIC_URL}/images/mockup.png`;
        this.setState({ closeBtn })
    }

    handleLoadAvatar(e) {
        let { closeBtn, imgPreview, classPreview, fields } = this.state;
        this.setState({ target_name : e.target.name })
        var system = this;
        EXIF.getData(e.target.files[0], function() {
          var orientation = EXIF.getTag(this, "Orientation");
          var can = document.createElement("canvas");
          var ctx = can.getContext("2d");
    
          var thisImage = new Image();
          thisImage.onload = function() {
            var MAX_WIDTH = 700;
            var MAX_HEIGHT = 700;
            var width = thisImage.width;
            var height = thisImage.height;
    
            if (width > height) {
              if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
              }
            } else {
              if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
              }
            }
            can.width = width;
            can.height = height;
            ctx.save();
            var width = can.width;
            var styleWidth = can.style.width;
            var height = can.height;
            var styleHeight = can.style.height;
            if (orientation) {
              if (orientation > 4) {
                can.width = height;
                can.style.width = styleHeight;
                can.height = width;
                can.style.height = styleWidth;
              }
              switch (orientation) {
                case 2:
                  ctx.translate(width, 0);
                  break;
                case 3:
                  ctx.translate(width, height);
                  ctx.rotate(Math.PI);
                  break;
                case 4:
                  ctx.translate(0, height);
                  break;
                case 5:
                  ctx.rotate(0.5 * Math.PI);
                  break;
                case 6:
                  ctx.rotate(0.5 * Math.PI);
                  ctx.translate(0, -height);
                  break;
                case 7:
                  ctx.rotate(0.5 * Math.PI);
                  ctx.translate(width, -height);
                  break;
                case 8:
                  ctx.rotate(-0.5 * Math.PI);
                  ctx.translate(-width, 0);
                  break;
              }
            }
            ctx.drawImage(thisImage, 0, 0, width, height);
            ctx.restore();
            var dataURL = can.toDataURL();
            var dataurlLink = can.toDataURL("image/jpeg");
            // var get64 = dataURL;
            var get64 = dataurlLink.substr(23)
            localStorage.setItem(system.state.target_name,get64)
            imgPreview[system.state.target_name] = dataURL
            classPreview[system.state.target_name] = ''
            var obj = {
              CUSTOMER_IMG: get64
            }
            fields['CUSTOMER_IMG_INFO'].push(obj)
            system.setState({ imgPreview, classPreview })
            closeBtn[system.state.target_name] = 'block'
            system.setState({ closeBtn })
            imgPreview[system.state.target_name] = dataURL;
            classPreview[system.state.target_name] = ''
            system.setState({ imgPreview, classPreview });
          };
          thisImage.src = URL.createObjectURL(this);
        });
    }

    renderContentProvince(type) {
      const { province } = this.state;
      var option = []
      var name_title;
      if (type === 'type_province') {
        name_title = province
      }
  
      for (var i in name_title) {
        option.push(<option key={i} value={name_title[i].codE_GUID}>{name_title[i].values}</option>)
      }
  
      return <select
        className={`form-control input_form ptmaxcard-regis-input ${this.state.errorsFocus['ADDR_ORG_PROVINCE_ID']}`}
        id='exampleFormControlSelect1'
        name="ADDR_ORG_PROVINCE_ID"
        value={this.state.fields['ADDR_ORG_PROVINCE_ID']}
        onChange={(e) => this.getAmphure(e.target.value, 'onchange')}
      >
        <option name="ADDR_ORG_PROVINCE_ID">กรุณาเลือกจังหวัด</option>
        {option}
      </select>
    }

    handleSubmit(event) {
        var { fields } = this.state;
        event.preventDefault();
        if (this.validateForm()) {
          if(fields['CARD_TYPE_ID'] === 2) {
            fields['CARD_ID'] = fields['CARD_ID_ENG']
            this.setState({ fields })
          }
          sessionStorage.setItem('inputBody', JSON.stringify(this.state.fields))  
        } else {
          // console.log('formsubmit ' + false);
        }
    }

    checkAddress(e){
      if((e.target.value.match(/-/g) || []).length > 1){
        return false
      }else{
        return true
      }
    }
  
    checkCharacter(e){
      var check = /^\d*[a-zA-Zก-ฮ][a-zA-Zก-ฮ\d\s]{1,10}$/;
      if(check.test(e.target.value) != false){
        return true
      }else{
        return false
      }
    }
    
    async validateForm() {
        this.setState({ showLoadingContent : 'none' })
        let fields = this.state.fields;
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;
        // var firstChar = fields.ADDR_NO.charAt(0)
        var check = /^[a-zA-Zก-ฮ\b]+$/;
        var getTrue = false;
        var getSymbol = false;
        var getReplaceChar = false;
        var getReplace = /^[^0][0-9]{1,3}(\/[^0][0-9]{1,3})(\-[^0][0-9]{1,3})?$/;
        var getcheckreplace = /^[0-9]{1,5}(\-[0-9]{1,5})?$/;
        var getChecklastChar = /^[0-9]{1,4}(\/[0-9]{1,4})([a-zA-Zก-ฮ]{1,})?$/;
        var getcheck = false;
        if (!fields["pdpacon3"] || !fields["pdpacon4"] || !fields["pdpacon5"]) {
          formIsValid = false;
          errorsFocus["ck_agree_conditions"] = 'errorFocus'
          errors["ck_agree_conditions"] = <FormattedMessage id="CK_AGREE_CONDITIONS" />;
        }

        if (!fields["ADDR_NO"] || fields['ADDR_NO'] === undefined) {
          formIsValid = false;
          errors["ADDR_NO"] = 'กรุณากรอกบ้านเลขที่'
          errorsFocus["ADDR_NO"] = 'errorFocus'
        }else{
          for(var i in fields.ADDR_NO){
            if(check.test(fields.ADDR_NO[i]) == true){
              getTrue = true
              if(getChecklastChar.test(fields.ADDR_NO) == false){
                getcheck = true
              }
            }else{
              getTrue = false
            }
            if(fields.ADDR_NO[i] == '-'){
              getSymbol = true
              if(getReplace.test(fields.ADDR_NO) == false ){
                getReplaceChar = true
              }
              if(getcheckreplace.test(fields.ADDR_NO) == true){
                getReplaceChar = false
              }
            }
          }

          if(getcheck == true){
            formIsValid = false;
            errors["ADDR_NO"] = 'รูปแบบบ้านเลขที่ไม่ถูกต้อง'
            errorsFocus["ADDR_NO"] = 'errorFocus'
          } 
          if(getReplaceChar == true){
            formIsValid = false;
            errors["ADDR_NO"] = 'รูปแบบบ้านเลขที่ไม่ถูกต้อง'
            errorsFocus["ADDR_NO"] = 'errorFocus'
          }
          if(getSymbol == true && getTrue == true){
            formIsValid = false;
            errors["ADDR_NO"] = 'ไม่สามารถมีทั้งตัวอักษรและอักขระ '-' อยู่ในบ้านเลขที่ได้'
            errorsFocus["ADDR_NO"] = 'errorFocus'
          }
          // if(firstChar == 0){
          //   formIsValid = false;
          //   errors["ADDR_NO"] = 'ไม่สามารถเริ่มต้นด้วย 0'
          //   errorsFocus["ADDR_NO"] = 'errorFocus'
          // }
          var count = (fields.ADDR_NO.match(/[a-zA-Zก-ฮ]/g) || []).length;
          if(count > 1){
            formIsValid = false;
            errors["ADDR_NO"] = 'มีตัวอักษร ประกอบได้แค่ 1 ตัวอักษรและต้องเป็นตัวสุดท้ายเท่านั้น'
            errorsFocus["ADDR_NO"] = 'errorFocus'
          }
        }
    
        if (!fields["ADDR_ORG_PROVINCE_ID"]) {
          formIsValid = false;
          errors["ADDR_ORG_PROVINCE_ID"] = 'กรุณาเลือกจังหวัด'
          errorsFocus["ADDR_ORG_PROVINCE_ID"] = 'errorFocus'
        }
    
        if (!fields["ADDR_ORG_AMPHURE_ID"]) {
          formIsValid = false;
          errors["ADDR_ORG_AMPHURE_ID"] = 'กรุณาเลือกอำเภอ'
          errorsFocus["ADDR_ORG_AMPHURE_ID"] = 'errorFocus'
        }
    
        if (!fields["ADDR_ORG_SUB_DISTRICT_ID"]) {
          formIsValid = false;
          errors["ADDR_ORG_SUB_DISTRICT_ID"] = 'กรุณาเลือกตำบล'
          errorsFocus["ADDR_ORG_SUB_DISTRICT_ID"] = 'errorFocus'
        }
    
        if (!fields["ADDR_ORG_POSTALCODE_ID"] || fields["ADDR_ORG_POSTALCODE_ID"] == '') {
          formIsValid = false;
          errors["ADDR_ORG_POSTALCODE_ID"] = 'กรุณาเลือกรหัสไปรษณีย์'
          errorsFocus["ADDR_ORG_POSTALCODE_ID"] = 'errorFocus'
        }
    
        if(this.state.fields.CARD_TYPE_ID === 2){
          if(!fields["ADDR_OTHER"]){
            formIsValid = false;
            errors["ADDR_OTHER"] = 'กรุณากรอกข้อมูลที่อยู่ (เพิ่มเติม)'
            errorsFocus["ALERT_ADDR_OTHER"] = 'errorFocus'
          }
        }

        
        if(fields['CARD_TYPE_ID'] === 1) {
          if (!fields["CARD_ID"]) {
              formIsValid = false;
              errorsFocus["CARD_ID"] = 'errorFocus'
              errors["CARD_ID"] = 'กรุณาระบุเลขที่บัตรประชาชน'
          } else {
              var chkValidCard = await this.checkCard(this.state.fields["CARD_TYPE_ID"],this.state.fields["CARD_ID"]);
              if (chkValidCard.status === true) {
                formIsValid = false;
                errorsFocus["CARD_ID"] = 'errorFocus'
                errors["CARD_ID"] = (chkValidCard.msg === "" ? 'เลขที่บัตรประชาชนนี้ มีการลงทะเบียนเรียบร้อยแล้ว' : chkValidCard.msg)
              }
          }
        } else {
          if (!fields["CARD_ID_ENG"]) {
              formIsValid = false;
              errorsFocus["CARD_ID_ENG"] = 'errorFocus';
              errors["CARD_ID_ENG"] = 'กรุณาระบุเลขที่บัตรประชาชน';
          }else{
            var chkValidCard = await this.checkCard(this.state.fields["CARD_TYPE_ID"],this.state.fields["CARD_ID_ENG"]);
            if(chkValidCard.status === true){
              formIsValid = false;
              errorsFocus["CARD_ID_ENG"] = 'errorFocus'
              errors["CARD_ID_ENG"] = (chkValidCard.msg === "" ? 'เลขที่หนังสือเดินทางนี้ มีการลงทะเบียนเรียบร้อยแล้ว' : chkValidCard.msg)
            }
          }
        }
    
        if (!fields["TITLE_NAME_ID"]) {
          formIsValid = false;
          errorsFocus["TITLE_NAME_ID"] = 'errorFocus'
          errors["TITLE_NAME_ID"] = 'กรุณาระบุคำนำหน้าชื่อ';
        }
        if(fields["TITLE_NAME_ID"] === 4) {
          if (!fields["TITLE_NAME_REMARK"]) {
            formIsValid = false;
            errorsFocus["TITLE_NAME_REMARK"] = 'errorFocus'
            errors["TITLE_NAME_REMARK"] = (
              'กรุณาระบุคำนำหน้าชื่อ'
            );
          }
        }
        if (!fields["FNAME_TH"]) {
          formIsValid = false;
          errorsFocus["FNAME_TH"] = 'errorFocus'
          errors["FNAME_TH"] = 'กรุณากรอกชื่อจริง';
        }
        if (!fields["LNAME_TH"]) {
          formIsValid = false;
          errorsFocus["LNAME_TH"] = 'errorFocus'
          errors["LNAME_TH"] = 'กรุณากรอกนามสกุล';
        }
        if (!fields["GENDER_ID"]) {
          formIsValid = false;
          errorsFocus["GENDER_ID"] = 'errorFocus'
          errors["GENDER_ID"] = 'กรุณาเลือกเพศ';
        } else {
          if (fields["GENDER_ID"] === 4) {
            if (fields["GENDER_ID"]) {
              formIsValid = false;
              errorsFocus["GENDER_ID"] = 'errorFocus'
              errors["GENDER_ID"] = (
                'กรุณาเลือกเพศ'
              );
            }
          }
        }
        if (!fields["BIRTH_DATE"]) {
          formIsValid = false;
          errorsFocus["BIRTH_DATE"] = 'errorFocus'
          errors["BIRTH_DATE"] = 'กรุณาระบุ วัน/เดือน/ปี เกิด';
        } else {
          var dateBeforDiff = moment(fields["BIRTH_DATE"]).format("YYYY-MM-DD");
          var diff = moment().diff(moment(dateBeforDiff, "YYYY-MM-DD"), "years");
          var diffDay = moment().diff(moment(dateBeforDiff, "YYYY-MM-DD"), "days");
          if (diff < 7 || diffDay <= 2556) {
            formIsValid = false;
            errorsFocus["BIRTH_DATE"] = 'errorFocus'
            errors["BIRTH_DATE"] = 'อายุผู้สมัครยังไม่ถึง 7 ปีบริบูรณ์';
          }
        }
        if(!fields["MARITAL_STATUS"] || fields["MARITAL_STATUS"] === '0'){
          formIsValid = false;
          errorsFocus["MARITAL_STATUS"] = 'errorFocus'
          errors["MARITAL_STATUS"] = 'กรุณาระบุสถานภาพ';
        }
        if (fields["USER_EMAIL"]) {
        // (!fields["USER_EMAIL"]) {
        //   formIsValid = false;
        //   errorsFocus["USER_EMAIL"] = 'errorFocus'
        //   errors["USER_EMAIL"] = 'กรุณากรอกอีเมล';
        // } else {
          var validEmail = this.validateEmail(fields["USER_EMAIL"]);
          if (validEmail === false) {
            formIsValid = false;
            errorsFocus["USER_EMAIL"] = 'errorFocus'
            errors["USER_EMAIL"] = 'รูปแบบอีเมล์ไม่ถูกต้อง';
          } else {
            var chkValidEmail = await this.checkEmail();
            if (chkValidEmail === true) {
              formIsValid = false;
              errorsFocus["USER_EMAIL"] = 'errorFocus'
              errors["USER_EMAIL"] = 'อีเมล์นี้ มีการลงทะเบียนเรียบร้อยแล้ว';
            }
          }
        }
        if (!fields["PHONE_NO"]) {
          formIsValid = false;
          errorsFocus["PHONE_NO"] = 'errorFocus'
          errors["PHONE_NO"] = 'กรุณาระบุหมายเลขโทรศัพท์'
        } else {
          if(fields["PHONE_NO"].toString().length ) {}
          var validPhone = this.validatePhone(fields["PHONE_NO"])
          if(validPhone === false) {
            formIsValid = false;
            errorsFocus["PHONE_NO"] = 'errorFocus'
            errors["PHONE_NO"] = 'เบอร์โทรศัพท์มือถือควรขึ้นต้นด้วย 06 08 หรือ 09'
          } else {
            var chkValidPhone = await this.checkPhoneExist();
            if (chkValidPhone.status === true) {
              formIsValid = false;
              errorsFocus["PHONE_NO"] = 'errorFocus'
              errors["PHONE_NO"] = (chkValidPhone.msg === "" ? 'หมายเลขโทรศัพท์นี้ มีการลงทะเบียนเรียบร้อยแล้ว' : chkValidPhone.msg)
            }
          }
        }
        if(this.state.fields["CARD_TYPE_ID"] !== 1){
          if(!this.state.fields.ADDR_OTHER){
            formIsValid = false;
            errorsFocus["ADDR_OTHER"] = 'errorFocus'
            errors["ADDR_OTHER"] = 'กรุณากรอกที่อยู่'
          }
        }
        if (!fields["Laser"]) {
            formIsValid = false;
            errorsFocus["Laser"] = 'errorFocus'
            errors["Laser"] = "กรุณาระบุหมายเลข Laser Code";
          } else if(fields["Laser"].length < 12) {
            formIsValid = false;
            errorsFocus["Laser"] = 'errorFocus'
            errors["Laser"] = "กรุณาระบุหมายเลข Laser Code 12 หลัก";
          }

          // if(this.state.DopaBirthDayType === "DMY"){
          //   if(this.state.DopaDay === "00"){
          //     formIsValid = false;
          //     errorsFocus["DopaDay"] = 'errorFocus'
          //     errors["DopaDay"] = "ระบุวันเกิด";
          //   }
          //   if(this.state.DopaMonth === "00"){
          //     formIsValid = false;
          //     errorsFocus["DopaMonth"] = 'errorFocus'
          //     errors["DopaMonth"] = "ระบุเดือนเกิด";
          //   }
          //   if(this.state.DopaYear === "00"){
          //     formIsValid = false;
          //     errorsFocus["DopaYear"] = 'errorFocus'
          //     errors["DopaYear"] = "ระบุปีเกิด";
          //   }
          // } else if(this.state.DopaBirthDayType === "DY"){
          //   if(this.state.DopaDay === "00"){
          //     formIsValid = false;
          //     errorsFocus["DopaDay"] = 'errorFocus'
          //     errors["DopaDay"] = "ระบุวันเกิด";
          //   }
          //   if(this.state.DopaYear === "00"){
          //     formIsValid = false;
          //     errorsFocus["DopaYear"] = 'errorFocus'
          //     errors["DopaYear"] = "ระบุปีเกิด";
          //   }
          // } else if(this.state.DopaBirthDayType === "MY"){
          //   if(this.state.DopaMonth === "00"){
          //     formIsValid = false;
          //     errorsFocus["DopaMonth"] = 'errorFocus'
          //     errors["DopaMonth"] = "ระบุเดือนเกิด";
          //   }
          //   if(this.state.DopaYear === "00"){
          //     formIsValid = false;
          //     errorsFocus["DopaYear"] = 'errorFocus'
          //     errors["DopaYear"] = "ระบุปีเกิด";
          //   }
          // } else if(this.state.DopaBirthDayType === "Y"){
          //   if(this.state.DopaYear === "00"){
          //     formIsValid = false;
          //     errorsFocus["DopaYear"] = 'errorFocus'
          //     errors["DopaYear"] = "ระบุปีเกิด";
          //   }
          // }



        this.setState({
          errors: errors,
          errorsFocus: errorsFocus
        });
        return formIsValid;
    }
    
    handleChange(e) {
        let { errors,errorsFocus, fields, GENDER_ID, TITLE_NAME_REMARK , date , name } = this.state;
        if(e.target.name == 'CARD_ID_ENG'){
          fields['CARD_ID'] = e.target.value;
        }
        if(e.target.name == 'ADDR_NO'){
          var chkAddr = this.checkAddress(e)
          var chkChar = this.checkCharacter(e)
          if(chkAddr === true) {
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
            var firstChar = e.target.value.charAt(0)
            var lastChar = e.target.value[e.target.value.length - 1]
            if(firstChar == '-'){
              errors[e.target.name] = '- ต้องไม่เป็นอักขระตัวแรก'
              errorsFocus[e.target.name] = 'errorFocus'
            }else if(lastChar == '-'){
              errors[e.target.name] = '- ต้องไม่เป็นอักขระตัวสุดท้าย'
              errorsFocus[e.target.name] = 'errorFocus'
            }else       
              if(chkChar == true){
                errors[e.target.name] = "มีตัวอักษร ประกอบได้แค่ 1 ตัวอักษรและต้องเป็นตัวสุดท้ายเท่านั้น"
                errorsFocus[e.target.name] = 'errorFocus'
                e.preventDefault();
              }else if(chkChar == false){
                fields[e.target.name] = e.target.value;
                errors[e.target.name] = null;
                errorsFocus[e.target.name] = ''
              }
          } else {
            errors[e.target.name] = 'มี - ประกอบได้แค่ 1 ตัวเท่านั้น'
            errorsFocus[e.target.name] = 'errorFocus'
          }
        }
        else if(e.target.name === 'ADDR_MOO') {
            const re = /^[0-9\b]+$/;
            if (re.test(e.target.value) !== false) {
              fields[e.target.name] = e.target.value;
              errors[e.target.name] = null;
              errorsFocus[e.target.name] = ''
            } else {
              if(e.target.value === '') { fields[e.target.name] = ''; }
                errors[e.target.name] = 'กรุณากรอกเฉพาะตัวเลข'
                errorsFocus[e.target.name] = ''
                errorsFocus["ADDR_MOO"] = 'errorFocus'
                e.preventDefault();
            }
          } 
          else {
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
          } 
        if (e.target.name === "TITLE_NAME_ID") {
          if (
            e.target.value === "1" ||
            e.target.value === "2" ||
            e.target.value === "3"
          ) {
            GENDER_ID = e.target.value === "1" ? 1 : 2;
            fields["GENDER_ID"] = GENDER_ID;
            genderClass = "form-control disabled";
            TITLE_NAME_REMARK = "none";
            fields["TITLE_NAME_REMARK"] = '';
            errors["GENDER_ID"]  = null;
            errorsFocus["GENDER_ID"]  = ''
    
          } else {
            fields["GENDER_ID"] = 4;
            genderClass = "form-control";
            TITLE_NAME_REMARK = "block";
          }
          errors[e.target.name] = null;
          errorsFocus[e.target.name] = ''
          fields[e.target.name] = parseInt(e.target.value);
        } else if (e.target.name === "CARD_ID") {
          const re = /^[0-9\b]+$/;
          if (e.target.value === "" || re.test(e.target.value)) {
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
          } else {
            e.preventDefault();
            return false;
          }
        } else if (e.target.name === "PHONE_NO") {
          const re = /^[0-9\b]+$/;
          if (e.target.value === "" || re.test(e.target.value)) {
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
          } else {
            errors[e.target.name] = 'กรุณากรอกเบอร์โทรศัพท์';
            errorsFocus[e.target.name] = ''
            e.preventDefault();
          }
        } else if (e.target.name === "Laser") {
            fields[e.target.name] = e.target.value.replace(/[^A-Za-z][^0-9]/g, '').toUpperCase()
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
        } else {
          fields[e.target.name] = e.target.value;
          errors[e.target.name] = null;
          errorsFocus[e.target.name] = ''
        }
        this.setState({ errors, fields, GENDER_ID, TITLE_NAME_REMARK , [e.target.id]: e.target.value , date});
  }

  maxLengthCheck = (object) => {
    if (object.target.value.length > object.target.maxLength) {
        object.target.value = object.target.value.slice(0, object.target.maxLength)
    }
  }
    
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
    
    async checkEmail() {
      return registerAction.checkEmailExist(this.state.fields["USER_EMAIL"]).then(e => {
        console.log("checkEmailExist", e)
        var resp = e.data.responsE_INFO;
        if(resp != null){
          if (resp.rescode !== "000") {
            ERROR_USER_EMAIL = false
          } else {
            ERROR_USER_EMAIL = true
          }
        }else{
          // console.log('null')
        }
        return ERROR_USER_EMAIL
      });
    }
    
  async checkPhoneExist() {
    return registerAction.checkPhoneExist(this.state.fields["PHONE_NO"]).then(e => {
      var resp = e.data.responsE_INFO;
      var response = {}
      if (resp.rescode !== "041")   {
        if(resp.rescode === "042") {
          response = {
            status: true,
            msg:'042'
          }
        } else {
          response = {
            status: true,
            msg: ""
          }
        }
      } else {
        response = {
          status: false,
          msg: ""
        }
      }
      return response;
    });
  }
    
  validatePhone(phonenumber) {
    var re = /^[0]{1}[689]/;
    return re.test(String(phonenumber));
  }
    
  async checkCard(type, card) {
    return registerAction.checkexistingIdCard(type, card).then(e => {
      var resp = e.data.responsE_INFO;
      var response = {}
      if(resp != null){
        if (resp.rescode !== "031")   {
          if(resp.rescode === "032") {
            response = {
              status: true,
              msg: '032'
            }
          } else {
            response = {
              status: true,
              msg: ""
            }
          }
        } else {
          response = {
            status: false,
            msg: ""
          }
        }
      }else{
        response = {
          status: false,
          msg: ""
        }
      }
      return response
    });
  }

    modalCheckSubmit(res){
      alert = (
        <SweetAlert
          custom
          confirmBtnBsStyle="success"
          closeOnClickOutside={true}
          cancelBtnBsStyle="default"
          focusConfirmBtn={false}
          title=""
          customIcon="../images/cancel.png"
          showConfirm={false}
          showCancelButton
          onCancel={() => this.handleChoice(false)}
          onConfirm={() => this.handleChoice(true)}
          onOutsideClick={() => {
            this.wasOutsideClick = true;
            this.setState({ showConfirm: false })
          }}
        >
          <div className="iconClose" onClick={() => this.handleChoice(false)}><i className="fas fa-times sizeCloseBtn"></i></div>
          {res}
        </SweetAlert>
      );
      this.setState({ show: true , modal: alert })
    }

    handleChoice(){
      this.setState({ show : false , modal:null})
    }

    async submitFormMb(){
      if(await this.validateForm()){
        if(this.state.inputcard === true){
          this.onCheckDopa()
        } else {
          this.submitFinalForm();
        }
      }else{
        var msg = "กรุณากรอกข้อมูลให้ครบถ้วน";
        this.modalCheckSubmit(msg);
      }
    }

    onCheckDopa(){
        const { fields } = this.state
        // var setDopaYear = parseInt(DopaYear) + 543

        const checkDopaBirthDayValue = (fields['BIRTH_DATE']).split('-');
        var DopaDay = checkDopaBirthDayValue[2]
        var DopaMonth = checkDopaBirthDayValue[1]
        var setDopaYear = parseInt(checkDopaBirthDayValue[0]) + 543
        // var check = moment(fields["BIRTH_DATE"], 'YYYY-MM-DD');
        // var month = check.format('M');
        // var day   = check.format('D');
        // var year  = check.add(543,'y').format('YYYY');
        var dataForEncrypt = `{
            "PID": "${fields["CARD_ID"]}",
            "FirstName": "${fields["FNAME_TH"]}",
            "LastName": "${fields["LNAME_TH"]}",
            "DateOfBirth": "${DopaDay}",
            "MonthOfBirth": "${DopaMonth}",
            "YearOfBirth": "${setDopaYear}",
            "Laser": "${fields["Laser"]}"
        }`
        const ENC_KEY = "S6ZKJTl3yqzsNmckMhE4Pi0p1Br7GnbX";
        const IV = "k4Iaf1LKsRUZ2Evi";
        let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENC_KEY), IV);
        let encrypted = cipher.update(dataForEncrypt);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        var dataEncrypt = encrypted.toString('base64');
        var iat = new Date().getTime();
        var exp = new Date(iat * 1000).getTime();
        var tokenForEncrypt = `{
            "iat": ${iat},
            "exp": ${exp},
            "data": "${dataEncrypt}"
        }`
        var tokenEncryptBase64 = Buffer.from(tokenForEncrypt).toString('base64');
        var dataToken = {
            "token": `${tokenEncryptBase64}`
        }
        registerAction.CheckCardByLaser(dataToken).then(e => {
            var response = e.response
            if(response.code === 0){
                this.submitFinalForm();
                // this.onCheckDopa()
            } else {
                var msg = response.message;
                var img = `${process.env.PUBLIC_URL}/images/cancel.png`;
                this.onOpenAlert(msg,img);
            }
        })
     
    }

    onOpenAlert(msg,img){
        alert = (
            <SweetAlert
                custom
                confirmBtnBsStyle="success"
                closeOnClickOutside={true}
                cancelBtnBsStyle="default"
                focusConfirmBtn={false}
                title=""
                customIcon={img}
                showConfirm={false}
                showCancelButton
                onCancel={() => this.onCloseAlert(false)}
                onOutsideClick={() => {
                    this.setState({ onOpenAlert: null })
                }}
            >
                <div className="">
                    <div className="iconClose" onClick={() => this.onCloseAlert(false)}><i className="fas fa-times sizeCloseBtn"></i></div>
                    {msg}
                </div>
            </SweetAlert>
        );
        this.setState({ 
            modal: alert,
            content_loading: "none"
        })
    }

    onCloseAlert(){
        this.setState({ 
            modal: null,
            content_loading: "none"
        })
    }

  submitFinalForm(){
    var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
    var ProfileData = localStorage.getItem('Profile');
    var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
    ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
    var ProfileArray = JSON.parse(ProfileDecrypt);
    var Profile = ProfileArray[0];
    var userid = Profile.userId;
    // console.log("this.state.fields",this.state.fields)
    registerAction.activateAndRegister(this.state.fields,userid).then(e => {
      // console.log("activateAndRegister", e)
      if(e.data.isSuccess === false) {
        this.setState({ disableAgreement: false })
        this.setState({ resultApi: e.data.errMsg })
        this.setState({ disabledBtn: '' })
        var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
        this.modalCheckSubmit(this.state.resultApi,imgPopup)
      } else {
        this.setState({ resultApi: e.data.errMsg })
        this.props.history.push({
          pathname: `${process.env.PUBLIC_URL}/PTmaxcard/register/otp`,
          state: {data: this.state.fields}
        });
      }
    })
  }

  checkPDPAcon(e){
    let { fields } = this.state;
    if(e.target.name === "pdpacon1" || e.target.name === "pdpacon2" || e.target.name === "pdpacon3" || e.target.name === "pdpacon4" || e.target.name === "pdpacon5"){
      fields[e.target.name] = e.target.value;
      sessionStorage.setItem(e.target.name, e.target.value);
    }
    
    // if(e.target.name === "pdpacon1"){
    //   if(e.target.checked === true){
    //     fields[e.target.name] = e.target.checked;
    //     fields['pdpacon1'] = true;
    //   }else{
    //     fields[e.target.name] = e.target.checked;
    //     fields['pdpacon1'] = false;
    //   }
    // }else if(e.target.name === "pdpacon2"){
    //   if(e.target.checked === true){
    //     fields[e.target.name] = e.target.checked;
    //     fields['pdpacon2'] = true;
    //   }else{
    //     fields[e.target.name] = e.target.checked;
    //     fields['pdpacon2'] = false;
    //   }
    // }else if(e.target.name === "pdpacon3"){
    //   if(e.target.checked === true){
    //     fields[e.target.name] = e.target.checked;
    //     fields['pdpacon3'] = true;
    //   }else{
    //     fields[e.target.name] = e.target.checked;
    //     fields['pdpacon3'] = false;
    //   }
    // }else if(e.target.name === "pdpacon4"){
    //   if(e.target.checked === true){
    //     fields[e.target.name] = e.target.checked;
    //     fields['pdpacon4'] = true;
    //   }else{
    //     fields[e.target.name] = e.target.checked;
    //     fields['pdpacon4'] = false;
    //   }
    // }else if(e.target.name === "pdpacon5"){
    //   if(e.target.checked === true){
    //     fields[e.target.name] = e.target.checked;
    //     fields['pdpacon5'] = true;
    //   }else{
    //     fields[e.target.name] = e.target.checked;
    //     fields['pdpacon5'] = false;
    //   }
    // }
  }

  cutLink(){
    window.location.href = `https://www.ptmaxcard.com/MaxCardPoint/`;
  }
  cutLink1(){
      window.location.href = `https://www.ptmaxcard.com/privacypolicy`;
  }


  onChangeDopaBirthDay(type){
    this.setState({
      DopaBirthDayType: type,
      DopaDay: "00",
      DopaMonth: "00",
      DopaYear: "00",
    })
  }

  onChangeDopaDate(e, type){
    var value = e.target.value;
    const { errors, errorsFocus } = this.state
    if(type === "DopaDay"){
      errors["DopaDay"] = null;
      errorsFocus["DopaDay"] = ''
      this.setState({
        DopaDay: value,
        errors,
        errorsFocus
      })
    } else if(type === "DopaMonth"){
      errors["DopaMonth"] = null;
      errorsFocus["DopaMonth"] = ''
      this.setState({
        DopaMonth: value,
        errors,
        errorsFocus
      })
    } else if(type === "DopaYear"){
      errors["DopaYear"] = null;
      errorsFocus["DopaYear"] = ''
      this.setState({
        DopaYear: value,
        errors,
        errorsFocus
      })
    }
  }

  render(){
    var { fields, amphure, tumbon, postcode } = this.state;
    amphureListData = [];
    for (var i in amphure) {
      amphureListData.push(<option name="ADDR_ORG_AMPHURE_ID" key={i} value={amphure[i].codE_GUID}>{amphure[i].values}</option>)
    }
    tumbonListData = [];
    for (var i in tumbon) {
      tumbonListData.push(<option name="ADDR_ORG_SUB_DISTRICT_ID" key={i} value={tumbon[i].codE_GUID}>{tumbon[i].values}</option>)
    }
    postcodeListData = [];
    for (var i in postcode) {
      postcodeListData.push(<option name="ADDR_ORG_POSTALCODE_ID" key={i} value={postcode[i].codE_GUID}>{postcode[i].values}</option>)
    }
    var setLangmyMaxCard = messages[this.state.language].registemaxcard

    var setDopaDay = []
    for (var i = 1; i <= 31; i++) {
      setDopaDay.push(
        <option value={i}>{i}</option>
      );
    };
    var start_year = new Date().getFullYear();
    var setDopaYear = []
    for (var i = start_year; i > start_year - 100; i--) {
      setDopaYear.push(
        <option value={i}>{i}</option>
      );
    };
    
    return(
      <IntlProvider
          locale={this.state.language}
          messages={messages[this.state.language].registemaxcard}
      >
        <div>
          <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
            <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
          </div>
          <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
          <div className="container-fluid bg-ptmaxcard-content">
            <div>
              <div className="ptmaxcard-regis-content">
                <div className="ptmaxcard-regis-title pb-4 pt-4"><FormattedMessage id="regisptmaxcard" /></div>
                  {/* <div className="ptmaxcard-regis-card-header">
                    <ul className="nav nav-pills nav-justified" role="tablist">
                      <li className="nav-item ptmaxcard-regis-nav-1">
                        <a className={`nav-link ${this.state.active1}`}>STEP 1</a>
                      </li>
                      <li className="nav-item ptmaxcard-regis-nav-2">
                        <a className={`nav-link ${this.state.active2}`}>STEP 2</a>
                      </li>
                      <li className="nav-item ptmaxcard-regis-nav-3">
                        <a className={`nav-link ${this.state.active3}`}>STEP 3</a>
                      </li>
                    </ul>
                  </div> */}
                  <div className="ptmaxcard-regis-card-body">
                    {/* <div className="tab-content py-3 px-sm-0">
                      <div id="step-one" className={`tab-pane ${this.state.activeColor1}`}> */}
                        <div className="loadingGifLoadmore mx-auto" style={{ display: this.state.showLoadingContent}}>
                          <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                        </div>
                        <div style={{ display: (this.state.showLoadingContent === 'block' ? 'none' : 'block') }}>
                          <div className="row mx-3">
                            <div className="col-xs-12 col-sm-12 col-lg-6 col-md-12 col-xl-6">
                              <div className="header-profile"><FormattedMessage id="infoprofile" /></div>
                              <div className="form-group">
                                <label>
                                  <span className="label-validate">* </span>
                                  <span className="font-header-label pl-2">{messages[localStorage.getItem('language')].registemaxcard.cardid}</span>
                                </label>
                              <div>
                                <input 
                                  disabled={!this.state.inputcard}
                                  type="text"
                                  className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['CARD_ID']}`}
                                  maxLength="13"
                                  name="CARD_ID"
                                  onChange={e => this.handleChange(e)}
                                  id="exampleFormControlInput1"
                                  placeholder={setLangmyMaxCard.inputcardid}
                                  value={this.state.fields["CARD_TYPE_ID"] == 1 ? this.state.fields["CARD_ID"] : ''}
                                />
                              </div>
                              <div className="errorMsg">{this.state.errors["CARD_ID"]}</div>
                            </div>

                            <div className="form-group">
                                <label>
                                  <span className="label-validate">* </span>
                                  <span className="font-header-label pl-2">หมายเลข Laser code</span>
                                </label>
                              <div>
                                <input 
                                  disabled={!this.state.inputcard}
                                  type="text"
                                  className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['Laser']}`}
                                  maxLength="12"
                                  name="Laser"
                                  onChange={e => this.handleChange(e)}
                                  id="exampleFormControlInput1"
                                  placeholder="ตัวอย่างการกรอก Laser Code : JT99999999999"
                                  value={this.state.fields["CARD_TYPE_ID"] == 1 ? this.state.fields["Laser"] : ''}
                                />
                              </div>
                              <div className="errorMsg">{this.state.errors["Laser"]}</div>
                            </div>



                {this.state.inputcard === true ? 
                <div className="pb-2 pt-1">
                  <div className="row">
                    <a className="pl-0 col-12 d-flex mb-1" onClick={() => this.onChangeDopaBirthDay('DMY')}>
                      {this.state.DopaBirthDayType === "DMY" ?
                      <img src={`${process.env.PUBLIC_URL}/images/checked.png`} className="dopa-birthdayimage"/>
                      :
                      <div className="dopa-birthdaycheck"></div>
                      }
                      <span className="dopa-birthdaytext">มีวัน/เดือน/ปีเกิด</span>
                    </a>
                    <a className="pl-0 col-12 d-flex mb-1" onClick={() => this.onChangeDopaBirthDay('DY')}>
                      {this.state.DopaBirthDayType === "DY" ?
                      <img src={`${process.env.PUBLIC_URL}/images/checked.png`} className="dopa-birthdayimage"/>
                      :
                      <div className="dopa-birthdaycheck"></div>
                      }
                      <span className="dopa-birthdaytext">มีเฉพาะวันและปีเกิด</span>
                    </a>
                    <a className="pl-0 col-12 d-flex mb-1" onClick={() => this.onChangeDopaBirthDay('MY')}>
                      {this.state.DopaBirthDayType === "MY" ?
                      <img src={`${process.env.PUBLIC_URL}/images/checked.png`} className="dopa-birthdayimage"/>
                      :
                      <div className="dopa-birthdaycheck"></div>
                      }
                      <span className="dopa-birthdaytext">มีเฉพาะเดือนและปีเกิด</span>
                    </a>
                    <a className="pl-0 col-12 d-flex mb-1" onClick={() => this.onChangeDopaBirthDay('Y')}>
                      {this.state.DopaBirthDayType === "Y" ?
                      <img src={`${process.env.PUBLIC_URL}/images/checked.png`} className="dopa-birthdayimage"/>
                      :
                      <div className="dopa-birthdaycheck"></div>
                      }
                      <span className="dopa-birthdaytext">มีเฉพาะปีเกิด</span>
                    </a>
                  </div>
                  {/* <div className="mt-1">
                    {this.state.DopaBirthDayType === "DMY" || this.state.DopaBirthDayType === "DY" ?
                    <div className="form-group">
                      <select
                        className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['DopaDay']}`}
                        name="DopaDay"
                        onChange={(e) => this.onChangeDopaDate(e, 'DopaDay')}
                        value={this.state.DopaDay}
                      >
                        <option value="00">เลือกวัน</option>
                        {setDopaDay}
                      </select>
                    </div>
                    :
                    null
                    }

                    {this.state.DopaBirthDayType === "DMY" || this.state.DopaBirthDayType === "MY" ?
                    <div className="form-group">
                      <select
                        className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['DopaMonth']}`}
                        name="DopaMonth"
                        onChange={(e) => this.onChangeDopaDate(e, 'DopaMonth')}
                        value={this.state.DopaMonth}
                      >
                        <option value="00">เลือกเดือน</option>
                        <option value="01">มกราคม</option>
                        <option value="02">กุมภาพันธ์</option>
                        <option value="03">มีนาคม</option>
                        <option value="04">เมษายน</option>
                        <option value="05">พฤษภาคม</option>
                        <option value="05">มิถุนายน</option>
                        <option value="07">กรกฎาคม</option>
                        <option value="08">สิงหาคม</option>
                        <option value="09">กันยายน</option>
                        <option value="10">ตุลาคม</option>
                        <option value="11">พฤศจิกายน</option>
                        <option value="12">ธันวาคม</option>
                      </select>
                    </div>
                    :
                    null
                    }

                    <div className="form-group">
                      <select
                        className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['DopaYear']}`}
                        name="DopaYear"
                        onChange={(e) => this.onChangeDopaDate(e, 'DopaYear')}
                        value={this.state.DopaYear}
                      >
                        <option value="00">เลือกปี</option>
                        {setDopaYear}
                      </select>
                    </div>
                  </div> */}
                  <div className="form-group">
                    <div>
                    <input
                      className={`inputWidth form-control ptmaxcard-regis-input ${this.state.errorsFocus['BIRTH_DATE']}`}
                      placeholder="กรุณาเลือกวันเกิด"
                      type="date" 
                      id="date"
                      name="BIRTH_DATE"
                      value={this.state.fields['BIRTH_DATE']}
                      // selected={this.state.startDate}
                      onChange={(e) => this.handleChange(e)}
                    />
                  </div>
                  <div className="errorMsg BIRTH_DATE">{this.state.errors["BIRTH_DATE"]}</div>
                </div>
                </div>
                :
                null
                }
                {/* <DatePicker 
                    selected={new Date()} 
                    onChange={(date) => this.setStartDate(date)} 
                  />
                  <DatePicker 
                    selected={new Date()} 
                    onChange={(date) => this.setStartDate(date)} 
                    dateFormat="MM/yyyy"
                    showMonthYearPicker
                    showFullMonthYearPicker
                  />
                  <DatePicker 
                    showYearPicker 
                    dateFormat="yyyy"
                    yearItemNumber={9}
                    selected={new Date()} 
                    onChange={(date) => this.setStartDate(date)} 
                  /> */}

                  
                  {/* <input
                                className={`inputWidth form-control ptmaxcard-regis-input ${this.state.errorsFocus['BIRTH_DATE']}`}
                                placeholder="กรุณาเลือกวันเกิด"
                                type="date" 
                                id="date"
                                name="BIRTH_DATE"
                                value={this.state.fields['BIRTH_DATE']}
                                // selected={this.state.startDate}
                                onChange={(e) => this.handleChange(e)}
                              />
                    <div className="errorMsg DopaBirthDayValue">{this.state.errors["DopaBirthDayValue"]}</div> */}


                            {/* <label className="check_conditions_form"><FormattedMessage id="ifforeigner" />
                              <input
                                type="checkbox"
                                name="aggreement"
                                checked={(this.state.fields["CARD_TYPE_ID"] === 1 ? false : true)}
                                onChange={e => this.changeHandle(e)}
                              />
                              <span className="checkmark_condition" />
                            </label> */}
                            <div className="form-group" style={{ display: this.state.isForeigner }}>
                              <input
                                type="text"
                                className={`form-control input_form ptmaxcard-regis-input ${this.state.errorsFocus['CARD_ID_ENG']}`}
                                name="CARD_ID_ENG"
                                maxLength="10"
                                onChange={(e) => this.handleChange(e)}
                                id="exampleFormControlInput1"
                                placeholder={this.state.errors["CARD_ID_ENG"] == null ? setLangmyMaxCard.inputcardforeigner : this.state.errors["CARD_ID_ENG"]}
                                value={this.state.fields['CARD_ID_ENG']}
                              />
                            </div>
                            <div className="form-group">
                              <label>
                                <span className="label-validate">* </span>
                                <span className="font-header-label pl-2"><FormattedMessage id="titlename" /></span>
                              </label>
                              <div>
                                <select
                                  className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['TITLE_NAME_ID']}`}
                                  name="TITLE_NAME_ID"
                                  id="exampleFormControlSelect1"
                                  onChange={e => this.handleChange(e)}
                                  value={this.state.fields.TITLE_NAME_ID == 0 ? this.state.fields.TITLE_NAME_ID = '' : this.state.fields.TITLE_NAME_ID}
                                >
                                  <option value="0">{this.state.errors["TITLE_NAME_ID"] == null ? setLangmyMaxCard.selecttitlename : this.state.errors["TITLE_NAME_ID"]}</option>
                                  {this.renderContent("type_title_name")}
                                </select>
                              </div>
                              <div className="form-group" style={{ display: this.state.TITLE_NAME_REMARK }}>
                                <input
                                  type="text"
                                  className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['TITLE_NAME_REMARK']} input_form`}
                                  name="TITLE_NAME_REMARK"
                                  onChange={e => this.handleChange(e)}
                                  id="exampleFormControlInput1"
                                  placeholder={this.state.errors["TITLE_NAME_REMARK"] == null ? setLangmyMaxCard.inputtitlename : this.state.errors["TITLE_NAME_REMARK"]}
                                  value={this.state.fields['TITLE_NAME_REMARK']}
                                />
                              </div>
                            </div>
                            <div className="form-group">
                              <label>
                                <span className="label-validate">* </span>
                                <span className="font-header-label pl-2"><FormattedMessage id="firstname" /></span>
                              </label>
                              <input
                                type="text"
                                className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['FNAME_TH']} input_form`}
                                name="FNAME_TH"
                                onChange={e => this.handleChange(e)}
                                id="exampleFormControlInput1"
                                placeholder={this.state.errors["FNAME_TH"] == null ? setLangmyMaxCard.inputfirstname : this.state.errors["FNAME_TH"]}
                                value={this.state.fields['FNAME_TH'] || ''}
                              />
                            </div>
                            <div className="form-group">
                              <label>
                                <span className="label-validate">* </span>
                                <span className="font-header-label pl-2"><FormattedMessage id="lastname" /></span>
                              </label>
                              <input
                                type="text"
                                className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['LNAME_TH']} input_form`}
                                name="LNAME_TH"
                                onChange={e => this.handleChange(e)}
                                id="exampleFormControlInput1"
                                placeholder={this.state.errors["LNAME_TH"] == null ? setLangmyMaxCard.inputlastname : this.state.errors["LNAME_TH"]}
                                value={this.state.fields['LNAME_TH'] || ''}
                              />
                            </div>
                            <div className="form-group">
                              <label>
                                <span className="label-validate">* </span>
                                <span className="font-header-label pl-2"><FormattedMessage id="gender" /></span>
                              </label>
                              <select
                                className={`${genderClass} form-control ptmaxcard-regis-input ${this.state.errorsFocus['GENDER_ID']} input_form`}
                                name="GENDER_ID"
                                id="exampleFormControlSelect1"
                                onChange={e => this.handleChange(e)}
                                value={this.state.fields["GENDER_ID"]}
                              >
                                <option value="0"> {this.state.errors["GENDER_ID"] == null ? setLangmyMaxCard.selectgenfer : this.state.errors["GENDER_ID"]}</option> 
                                {this.renderContent("type_gender")}
                              </select>
                            </div>
                            {/* <div className="form-group">
                              <label>
                                <span className="label-validate">* </span>
                                <span className="font-header-label pl-2"><FormattedMessage id="birthday" /></span>
                              </label>
                              <div>
                              <input
                                className={`inputWidth form-control ptmaxcard-regis-input ${this.state.errorsFocus['BIRTH_DATE']}`}
                                placeholder="กรุณาเลือกวันเกิด"
                                type="date" 
                                id="date"
                                name="BIRTH_DATE"
                                value={this.state.fields['BIRTH_DATE']}
                                // selected={this.state.startDate}
                                onChange={(e) => this.handleChange(e)}
                              />
                            </div>
                            <div className="errorMsg BIRTH_DATE">{this.state.errors["BIRTH_DATE"]}</div>
                          </div> */}
                          <div className="form-group">
                            <label>
                              <span className="label-validate">* </span>
                              <span className="font-header-label pl-2"><FormattedMessage id="status" /></span>
                            </label>
                            <select
                              className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['MARITAL_STATUS']} input_form`}
                              name="MARITAL_STATUS"
                              id="exampleFormControlSelect1"
                              onChange={e => this.handleChange(e)}
                              value={this.state.fields['MARITAL_STATUS']}
                            >
                              <option value="0"> {this.state.errors["MARITAL_STATUS"] == null ? setLangmyMaxCard.inputstatus : this.state.errors["MARITAL_STATUS"]}</option>
                              {this.renderContent("type_status")}
                            </select>
                          </div>
                          <div className="header-profile mt-5"><FormattedMessage id="infocontactus" /></div>
                          <div className="form-group">
                            <label>
                              <span className="label-validate"></span>
                              <span className="font-header-label pl-2"><FormattedMessage id="email" /></span>
                            </label>
                            <input
                              type="text"
                              className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['USER_EMAIL']} input_form`}
                              name="USER_EMAIL"
                              onChange={e => this.handleChange(e)}
                              id="exampleFormControlInput1"
                              placeholder={this.state.errors["USER_EMAIL"] == null ? setLangmyMaxCard.inputemail : this.state.errors["USER_EMAIL"]}
                              value={this.state.fields['USER_EMAIL'] || ''}
                            />
                          </div>
                          <div className="errorMsg">{this.state.errors['USER_EMAIL']}</div>
                          <div className="form-group">
                            <label>
                              <span className="label-validate">* </span>
                              <span className="font-header-label pl-2"><FormattedMessage id="phoneno" /></span>
                            </label>
                            <input
                              type="tel"
                              maxLength="10"
                              className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['PHONE_NO']} input_form ${(this.state.fields["PHONE_NO"] ? `` : '')}`}
                              name="PHONE_NO"
                              onInput={this.maxLengthCheck}
                              onChange={e => this.handleChange(e)}
                              id="exampleFormControlInput1"
                              placeholder={this.state.errors["PHONE_NO"] == null ? setLangmyMaxCard.inputphoneno : this.state.errors["PHONE_NO"]}
                              value={this.state.fields["PHONE_NO"] || ''}
                            />
                          </div>
                          <div className="errorMsg">{this.state.errors['PHONE_NO']}</div>
                        </div>          
                      <div className="col-xs-12 col-sm-12 col-lg-6 col-md-12 col-xl-6">
                        <div className="header-profile"><FormattedMessage id="addressinfo" /></div>
                        <div style={{display:this.state.chkDisplayAddress}}>        
                          <div className="form-group">
                            <span className="label-validate">* </span>
                            <label className="label font-header-label pl-2"><FormattedMessage id="addressno" /></label>
                            <input 
                              type="text" 
                              className={`form-control input_form ptmaxcard-regis-input ${this.state.errorsFocus['ADDR_NO']}`} 
                              id="exampleFormControlInput1" 
                              maxLength="10" 
                              name="ADDR_NO" 
                              onChange={(e) => this.handleChange(e)} 
                              value={this.state.fields['ADDR_NO'] || ''}
                              placeholder={this.state.errors["ADDR_NO"] == null ? setLangmyMaxCard.inputaddressno : this.state.errors["ADDR_NO"]}
                            />
                          </div>
                          <div className="form-group">
                            <label><FormattedMessage id="moo" /></label>
                            <input
                              type="text"
                              className={`form-control input_form ptmaxcard-regis-input ${this.state.errorsFocus['ADDR_MOO']}`}
                              maxLength="3"
                              id="exampleFormControlInput1"
                              name="ADDR_MOO"
                              onChange={(e) => this.handleChange(e)}
                              value={this.state.fields['ADDR_MOO']}
                              placeholder={this.state.errors["ADDR_MOO"] == null ? setLangmyMaxCard.inputmoo : this.state.errors["ADDR_MOO"]}
                            />
                          </div>
                          <div className="form-group">
                            <label><FormattedMessage id="villege" /></label>
                            <input 
                              type="text" 
                              className="form-control input_form ptmaxcard-regis-input" 
                              id="exampleFormControlInput1" 
                              name="ADDR_VILLEGE" 
                              onChange={(e) => this.handleChange(e)} 
                              value={this.state.fields['ADDR_VILLEGE']} 
                              placeholder={this.state.errors["ADDR_VILLEGE"] == null ? setLangmyMaxCard.inputvillege : this.state.errors["ADDR_VILLEGE"]}
                            />
                          </div>
                          <div className="form-group">
                            <label><FormattedMessage id="soi" /></label>
                            <input 
                              type="text" 
                              className="form-control input_form ptmaxcard-regis-input" 
                              name="ADDR_SOI" 
                              onChange={(e) => this.handleChange(e)} 
                              value={this.state.fields.ADDR_SOI} 
                              id="exampleFormControlInput1" 
                              placeholder={setLangmyMaxCard.inputsoi}
                            />
                          </div>
                          <div className="form-group">
                            <label><FormattedMessage id="street" /></label>
                            <input 
                              type="text" 
                              className="form-control input_form ptmaxcard-regis-input" 
                              name="ADDR_STREET" 
                              onChange={(e) => this.handleChange(e)} 
                              value={this.state.fields.ADDR_STREET} 
                              id="exampleFormControlInput1" 
                              placeholder={setLangmyMaxCard.inputstreet}
                            />
                          </div>
                          <div className="form-group">
                            <label>
                              <span className="label-validate">* </span>
                              <span className="font-header-label pl-2"><FormattedMessage id="province" /></span>
                            </label>
                            {this.renderContentProvince('type_province')}
                          </div>
                          <div className="form-group">
                            <label>
                              <span className="label-validate">* </span>
                              <span className="font-header-label pl-2"><FormattedMessage id="ampure" /></span>
                            </label>
                            <select
                              className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['ADDR_ORG_AMPHURE_ID']}`}
                              id='exampleFormControlSelect1'
                              name="ADDR_ORG_AMPHURE_ID"
                              onChange={(e) => this.getTumbon(e.target.value, 'onchange')}
                              value={this.state.fields['ADDR_ORG_AMPHURE_ID'] == 0 ? this.state.fields['ADDR_ORG_AMPHURE_ID'] = '' : this.state.fields['ADDR_ORG_AMPHURE_ID']}
                            >
                              <option value="0">{this.state.errors["ADDR_ORG_AMPHURE_ID"] == null ? setLangmyMaxCard.selectampure : this.state.errors["ADDR_ORG_AMPHURE_ID"]}</option>
                              {amphureListData}
                            </select>
                          </div>
                          <div className="form-group">
                            <label>
                              <span className="label-validate">* </span>
                              <span className="font-header-label pl-2"><FormattedMessage id="district" /></span>
                            </label>
                            <select
                              className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['ADDR_ORG_SUB_DISTRICT_ID']}`}
                              id='exampleFormControlSelect1'
                              name="ADDR_ORG_SUB_DISTRICT_ID"
                              onChange={(e) => this.getPostcode(e.target.value, 'onchange')}
                              value={this.state.fields['ADDR_ORG_SUB_DISTRICT_ID'] == 0 ? this.state.fields['ADDR_ORG_SUB_DISTRICT_ID'] = '' : this.state.fields['ADDR_ORG_SUB_DISTRICT_ID']}
                            >
                              <option value="0">{this.state.errors["ADDR_ORG_SUB_DISTRICT_ID"] == null ? setLangmyMaxCard.selectdistrict : this.state.errors["ADDR_ORG_SUB_DISTRICT_ID"]}</option>
                              {tumbonListData}
                            </select>
                          </div>
                          <div className="form-group">
                            <label>
                              <span className="label-validate">* </span>
                              <span className="font-header-label pl-2"><FormattedMessage id="postcode" /></span>
                            </label>
                            <select 
                              className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['ADDR_ORG_POSTALCODE_ID']}`}
                              id='exampleFormControlSelect1' 
                              name="ADDR_ORG_POSTALCODE_ID" 
                              onChange={(e) => this.handleChange(e)}
                              value={this.state.fields['ADDR_ORG_POSTALCODE_ID'] == 0 ? this.state.fields['ADDR_ORG_POSTALCODE_ID'] = '' : (this.state.fields['ADDR_ORG_POSTALCODE_ID'] !== '' ? this.state.fields['ADDR_ORG_POSTALCODE_ID'] : '')}
                            >
                              <option value="0">{this.state.errors["ADDR_ORG_POSTALCODE_ID"] == null ? setLangmyMaxCard.inputpostcode : this.state.errors["ADDR_ORG_POSTALCODE_ID"]}</option>
                              {postcodeListData}
                            </select>
                          </div>
                          <div className="form-group">
                            <label><FormattedMessage id="infoaddress" /></label>
                            <textarea 
                              className="form-control input_form ptmaxcard-regis-input" 
                              id="exampleFormControlTextarea1" 
                              rows="3" 
                              name="ADDR_OTHER" 
                              value={this.state.fields.ADDR_OTHER} 
                              onChange={(e) => this.handleChange(e)} 
                              placeholder={setLangmyMaxCard.inputmoredata}
                            >
                            </textarea>
                          </div>
                        </div>

                        <div style={{display:this.state.chkDisplayAdd}}>
                          <form className="form-horizontal">
                            <div className="bg_full">
                              <div className="container-fluid">
                                <div className="content_page">
                                  <div className="head_title_agree"></div>
                                  <div className="form-group">
                                    <label>
                                      <span className="label-validate">* </span>
                                      <span className="font-header-label pl-2"><FormattedMessage id="infoaddress" /></span>
                                    </label>
                                    <textarea 
                                      className={`form-control input_form ptmaxcard-regis-input ${this.state.errorsFocus['ADDR_OTHER']}`}
                                      id="exampleFormControlTextarea1" 
                                      rows="3" 
                                      name="ADDR_OTHER" 
                                      value={this.state.fields.ADDR_OTHER} 
                                      onChange={(e) => this.handleChange(e)} 
                                      placeholder={this.state.errors["ADDR_OTHER"] == null ? setLangmyMaxCard.inputmoredata : this.state.errors["ADDR_OTHER"]}
                                    >
                                    </textarea>
                                  </div>
                                  <div className="errorMsg ADDR_OTHER">{this.state.errors["ADDR_OTHER"]}</div>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                        {/* <div className="btn_nextstep nav nav-tabs nav-fill my-3" id="nav-tab" role="tablist">
                          <a className="btn btn-secondary btn-block" onClick={() => this.setUpActive(2)}><FormattedMessage id="step1nextstep" /></a>
                        </div> */}
                      </div>
                    {/* </div>
                  </div> */}
                </div>         

                {/* <div id="step-two" className={`tab-pane ${this.state.activeColor2}`}> */}
                  <div className="bg_fullall">
                    <div className="container-fluid">
                      <div className="content_page">
                        <div className="header-profile">ข้อมูลเพิ่มเติม (รูปภาพ)</div>
                          <div className="errorImageSize">{this.state.errorFileSize}</div>
                          <div className="">
                            <div className="row row_image">
                              <div className="col-4 col-sm-3 review_image_before">
                                <div className="upload_preview" disabled={false}>
                                  <div className="close-btn" onClick={() => this.deleteLocalStorage('imgPreview1')} style={{ display: this.state.closeBtn['imgPreview1'] }}>
                                    <i className="fa fa-times-circle" aria-hidden="true"></i>
                                  </div>
                                  <div onClick={() => this.handleUploadFile(1)} className="control-img">
                                    <input type="file" name="imgPreview1" style={{ display: "none" }} ref={input => this.inputElement_1 = input} onChange={(e) => this.handleLoadAvatar(e)} />
                                    <img src={this.state.imgPreview['imgPreview1']} alt="" className={this.state.classPreview['imgPreview1']} />
                                  </div>
                                </div>
                              </div>
                              <div className="col-4 col-sm-3 review_image_before">
                                <div className="upload_preview">
                                  <div className="close-btn" onClick={() => this.deleteLocalStorage('imgPreview2')} style={{ display: this.state.closeBtn['imgPreview2'] }}><i className="fa fa-times-circle" aria-hidden="true"></i></div>
                                  <div onClick={() => this.handleUploadFile(2)} className="control-img">
                                    <input type="file" name="imgPreview2" style={{ display: "none" }} ref={input => this.inputElement_2 = input} onChange={(e) => this.handleLoadAvatar(e)} disabled={disabledimage}/>
                                    <img src={this.state.imgPreview['imgPreview2']} alt="" className={this.state.classPreview['imgPreview2']} />
                                  </div>
                                </div>
                              </div>
                              <div className="col-4 col-sm-3 review_image_before">
                                <div className="upload_preview">
                                  <div className="close-btn" onClick={() => this.deleteLocalStorage('imgPreview3')} style={{ display: this.state.closeBtn['imgPreview3'] }}><i className="fa fa-times-circle" aria-hidden="true"></i></div>
                                    <div onClick={() => this.handleUploadFile(3)} className="control-img">
                                      <input type="file" name="imgPreview3" style={{ display: "none" }} ref={input => this.inputElement_3 = input} onChange={(e) => this.handleLoadAvatar(e)} disabled={disabledimage2}/>
                                      <img src={this.state.imgPreview['imgPreview3']} alt="" className={this.state.classPreview['imgPreview3']} />
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            {this.state.modal}
                          </div>
                        </div>
                      </div>
                    </div>
                        

                      <div className="bg_full">
                        <div className="row container-fluid">
                          <div className="col-xs-12 col-sm-12 col-lg-6 col-xl-6 col-md-12 mt-3 content_page">
                            <div className="form-group">
                              <label className="addtional"><FormattedMessage id="cartype" /></label>
                              <select className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['CAR_TYPE']}`}
                                name="CAR_TYPE" 
                                id='exampleFormControlSelect1'
                                value={this.state.fields['CAR_TYPE']}
                                onChange={(e) => this.dropdownlistCheck(e)}
                              >
                                <option value="0">{setLangmyMaxCard.selectcartype}</option>{this.renderAdditional('type_cartype')}
                              </select>
                              <div className="errorMsg CAR_TYPE">{this.state.errors["CAR_TYPE"]}</div>
                            </div>
                            <div className="form-group">
                              <label className="addtional"><FormattedMessage id="carbrand" /></label>
                              <select 
                                className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['CAR_BRAND']}`} 
                                name="CAR_BRAND" 
                                value={this.state.fields['CAR_BRAND']}
                                id='exampleFormControlSelect1' 
                                onChange={(e) => this.dropdownlistCheck(e)}
                              >
                                <option value="0">{setLangmyMaxCard.selectcarbrand}</option>{this.renderAdditional('type_brand')}
                              </select>
                              <div className="errorMsg CAR_BRAND">{this.state.errors['CAR_BRAND']}</div>
                            </div>
                            <div className="form-group" style={{ display: this.state.CAR_BRAND_REMARK}}>
                              <input
                                type="text"
                                className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['CAR_BRAND_REMARK']} input_form`}
                                name="CAR_BRAND_REMARK"
                                value={this.state.fields['CAR_BRAND_REMARK']}
                                onChange={e => this.dropdownlistCheck(e)}
                                id="exampleFormControlInput1"
                                placeholder={setLangmyMaxCard.remarkcarbrand}
                              />
                              <div className="errorMsg CAR_BRAND_REMARK">{this.state.errors['CAR_BRAND_REMARK']}</div>
                            </div>
                            <div className="form-group">
                              <label className="addtional"><FormattedMessage id="carree" /></label>
                              <select 
                                className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['OCCUPATION_ID']}`}
                                name="OCCUPATION_ID" 
                                value={this.state.fields['OCCUPATION_ID']}
                                id='exampleFormControlSelect1' 
                                onChange={(e) => this.dropdownlistCheck(e)}
                              >
                                <option value="0" >{setLangmyMaxCard.selectcarree}</option>{this.renderAdditional('type_occupation')}
                              </select>
                              <div className="errorMsg OCCUPATION_ID">{this.state.errors['OCCUPATION_ID']} </div>
                            </div>
                            <div className="form-group" style={{ display: this.state.OCCAPATION_REMARK}}>
                              <input
                                type="text"
                                className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['OCCAPATION_REMARK']} input_form`}
                                name="OCCAPATION_REMARK"
                                value={this.state.fields['OCCAPATION_REMARK']}
                                onChange={e => this.dropdownlistCheck(e)}
                                id="exampleFormControlInput1"
                                placeholder={setLangmyMaxCard.remarkmorecarree}
                              />
                              <div className="errorMsg OCCAPATION_REMARK">{this.state.errors['OCCAPATION_REMARK']}</div> 
                            </div>
                            <div className="form-group">
                              <label className="addtional"><FormattedMessage id="occapation" /></label>
                              <select 
                                className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['EDUCATION_ID']}`}
                                name="EDUCATION_ID" 
                                value={this.state.fields['EDUCATION_ID']}
                                id='exampleFormControlSelect1' 
                                onChange={(e) => this.dropdownlistCheck(e)}
                              >
                                <option value="0">{setLangmyMaxCard.selectoccapation}</option>{this.renderAdditional('type_education')}
                              </select>
                              <div className="errorMsg EDUCATION_ID">{this.state.errors["EDUCATION_ID"]} </div>
                            </div>
                            <div className="form-group">
                              <label className="addtional"><FormattedMessage id="income" /></label>
                                <select  
                                  className={`form-control ptmaxcard-regis-input ${this.state.errorsFocus['INCOME_ID']}`}
                                  name="INCOME_ID" 
                                  value={this.state.fields['INCOME_ID']}
                                  id='exampleFormControlSelect1' 
                                  onChange={(e) => this.dropdownlistCheck(e)}
                                >
                                  <option value="0">{setLangmyMaxCard.selectincome}</option>{this.renderAdditional('type_income')}
                                </select>
                                <div className="errorMsg INCOME_ID">{this.state.errors["INCOME_ID"]}</div>
                            </div>
                    <label className="recommend"><FormattedMessage id="youacceppromotion" /></label>
                    <div className="row">
                      <div className="col">
                        <label className="check_conditions_select"><FormattedMessage id="pmtnews" />
                          <input type="checkbox" id="SUBSCRIBE_LETTER" name="news"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_LETTER']) ? (this.state.fields['SUBSCRIBE_LETTER'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false} />
                          <span className="checkmark_condition_select"></span>
                        </label>
                      </div>
                      <div className="col">
                        <label className="check_conditions_select"><FormattedMessage id="pmtemail" />
                          <input type="checkbox" id="SUBSCRIBE_EMAIL" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_EMAIL']) ? (this.state.fields['SUBSCRIBE_EMAIL'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false} />
                          <span className="checkmark_condition_select"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                      <div className="col">
                        <label className="check_conditions_select"><FormattedMessage id="pmtsms" />
                          <input type="checkbox" id="SUBSCRIBE_SMS" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_SMS']) ? (this.state.fields['SUBSCRIBE_EMAIL'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false} />
                          <span className="checkmark_condition_select"></span>
                        </label>
                      </div>
                      <div className="col">
                        <label className="check_conditions_select"><FormattedMessage id="pmtnonews" />
                          <input type="checkbox" id="SUBSCRIBE_NONE" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_NONE']) ? (this.state.fields['SUBSCRIBE_NONE'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false } />
                          <span className="checkmark_condition_select"></span>
                        </label>
                      </div>
                    </div>
                    <label className="addtional"><FormattedMessage id="pmtpromotion" /></label>
                    <div className="row">
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="pmtfood" />
                          <input type="checkbox" id="PMT_FOOD" checked={this.state.fields['PMT_FOOD']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="pmttravel" />
                          <input type="checkbox" id="PMT_TRAVEL" checked={this.state.fields['PMT_TRAVEL']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="pmtshopping" />
                          <input type="checkbox" id="PMT_SHOPPING" checked={this.state.fields['PMT_SHOPPING']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="pmtbeauty" />
                          <input type="checkbox" id="PMT_BEAUTY" checked={this.state.fields['PMT_BEAUTY']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="pmtsport" />
                          <input type="checkbox" id="PMT_SPORT_AND_CAR" checked={this.state.fields['PMT_SPORT_AND_CAR']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="pmtother" />
                          <input type="checkbox" id="PMT_OTHERS" checked={this.state.fields['PMT_OTHERS']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                        <div className="form-group">
                          <input 
                            type="text" 
                            id="PMT_OTHERS_REMARK" 
                            value={this.state.fields['PMT_OTHERS_REMARK']} 
                            className={`${disabled_block} ptmaxcard-regis-input `} 
                            placeholder={setLangmyMaxCard.pmtothers}
                            value={this.state.fields['PMT_OTHERS_REMARK']}  
                            onChange={(e) => this.handleGameClik(e)} 
                          />
                        </div>
                      </div>
                    </div>
                    <label className="addtional"><FormattedMessage id="addtional" /></label>
                    <div><label><FormattedMessage id="entertrainment" /></label></div>
                    <div className="row">
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actmusic" />
                          <input type="checkbox" id="ACT_EN_MUSIC" checked={this.state.fields['ACT_EN_MUSIC']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actconcert" />
                          <input type="checkbox" id="ACT_EN_CONCERT" checked={this.state.fields['ACT_EN_CONCERT']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actconutry" />
                          <input type="checkbox" id="ACT_EN_COUNTRY_CONCERT" checked={this.state.fields['ACT_EN_COUNTRY_CONCERT']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actmovies" />
                          <input type="checkbox" id="ACT_EN_MOVIES" checked={this.state.fields['ACT_EN_MOVIES']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="acttheater" />
                          <input type="checkbox" id="ACT_EN_THEATER" checked={this.state.fields['ACT_EN_THEATER']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actothers" />
                          <input type="checkbox" id="ACT_EN_OTHERS" checked={this.state.fields['ACT_EN_OTHERS']} name="entertain" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                        <div className="form-group">
                          <input 
                            type="text" 
                            id="ACT_EN_OTHERS_REMARK" 
                            className={`${entertain_remark_display} ptmaxcard-regis-input `} 
                            placeholder={setLangmyMaxCard.pmtothers}
                            value={this.state.fields['ACT_EN_OTHERS_REMARK']}  
                            onChange={(e) => this.handleGameClick(e)} 
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-xs-12 col-sm-12 col-lg-6 col-xl-6 col-md-12 mt-3 mt-5 content_page">
                    <label><FormattedMessage id="actsport" /></label>
                    <div className="row">
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actfitness" />
                          <input type="checkbox" id="ACT_SPT_FITNESS" checked={this.state.fields['ACT_SPT_FITNESS']} name="sport" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actrun" />
                          <input type="checkbox" id="ACT_SPT_RUN" checked={this.state.fields['ACT_SPT_RUN']} name="sport" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actcycling" />
                          <input type="checkbox" id="ACT_SPT_CYCLING" checked={this.state.fields['ACT_SPT_CYCLING']} name="sport" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actfootball" />
                          <input type="checkbox" id="ACT_SPT_FOOTBALL" checked={this.state.fields['ACT_SPT_FOOTBALL']} name="sport" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actothers" />
                          <input type="checkbox" id="ACT_SPT_OTHERS" checked={this.state.fields['ACT_SPT_OTHERS']} name="sport"  onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                        <div className="form-group">
                          <input 
                            type="text" 
                            id="ACT_SPT_OTHERS_REMARK" 
                            className={`${sport_remark_display} ptmaxcard-regis-input `} 
                            placeholder={setLangmyMaxCard.pmtothers}
                            value={this.state.fields['ACT_SPT_OTHERS_REMARK']}  
                            onChange={(e) => this.handleGameClick(e)} 
                          />
                        </div>
                      </div>
                    </div>
                    <label className="addtional"><FormattedMessage id="actaddtional" /></label>
                    <div className="row">
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actshopping" />
                          <input type="checkbox" id="ACT_OD_SHOPPING" checked={this.state.fields['ACT_OD_SHOPPING']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actbeauty" />
                          <input type="checkbox" id="ACT_OD_BEAUTY" checked={this.state.fields['ACT_OD_BEAUTY']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actrestaurant" />
                          <input type="checkbox" id="ACT_OD_RESTAURANT" checked={this.state.fields['ACT_OD_RESTAURANT']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actclub" />
                          <input type="checkbox" id="ACT_OD_CLUB" checked={this.state.fields['ACT_OD_CLUB']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actmomchild" />
                          <input type="checkbox" id="ACT_OD_MOMCHILD" checked={this.state.fields['ACT_OD_MOMCHILD']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actrally" />
                          <input type="checkbox" id="ACT_OD_RALLY" checked={this.state.fields['ACT_OD_RALLY']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actadvanture" />
                          <input type="checkbox" id="ACT_OD_ADVANTURE" checked={this.state.fields['ACT_OD_ADVANTURE']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actphoto" />
                          <input type="checkbox" id="ACT_OD_PHOTO" checked={this.state.fields['ACT_OD_PHOTO']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actphilanthropy" />
                          <input type="checkbox" id="ACT_OD_PHILANTHROPY" checked={this.state.fields['ACT_OD_PHILANTHROPY']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="acthoroscope" />
                          <input type="checkbox" id="ACT_OD_HOROSCOPE" checked={this.state.fields['ACT_OD_HOROSCOPE']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actabroad" />
                          <input type="checkbox" id="ACT_OD_ABROAD" checked={this.state.fields['ACT_OD_ABROAD']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actupcountry" />
                          <input type="checkbox" id="ACT_OD_UPCOUNTRY" checked={this.state.fields['ACT_OD_UPCOUNTRY']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actothers" />
                          <input type="checkbox" id="ACT_OD_OTHERS" checked={this.state.fields['ACT_OD_OTHERS']} name="outside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                        <div className="form-group">
                          <input type="text" id="ACT_OD_OTHERS_REMARK" className={`${outside_remark_display} ptmaxcard-regis-input `} placeholder="อื่นๆโปรดระบุข้อมูล" value={this.state.fields['ACT_OD_OTHERS_REMARK']}  onChange={(e) => this.handleGameClick(e)} />
                        </div>
                      </div>
                    </div>
                    <label className="addtional"><FormattedMessage id="actadtional" /></label>
                    <div className="row">
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actcooking" />
                          <input type="checkbox"  id="ACT_ID_COOKING" checked={this.state.fields['ACT_ID_COOKING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actbaking" />
                          <input type="checkbox"  id="ACT_ID_BAKING" checked={this.state.fields['ACT_ID_BAKING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actdecorate" />
                          <input type="checkbox"  id="ACT_ID_DECORATE" checked={this.state.fields['ACT_ID_DECORATE']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actgardening" />
                          <input type="checkbox"  id="ACT_ID_GARDENING" checked={this.state.fields['ACT_ID_GARDENING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actreading" />
                          <input type="checkbox"  id="ACT_ID_READING" checked={this.state.fields['ACT_ID_READING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="col">
                        <label className="selectoneonly"><FormattedMessage id="actgame" />
                          <input type="checkbox"  id="ACT_ID_GAMING" checked={this.state.fields['ACT_ID_GAMING']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                          <span className="checkmark_selectoneonly"></span>
                        </label>
                      </div>
                      <div className="w-100"></div>
                        <div className="col">
                          <label className="selectoneonly"><FormattedMessage id="actinternet" />
                            <input type="checkbox"  id="ACT_ID_INTERNET" checked={this.state.fields['ACT_ID_INTERNET']} name="inside" onClick = {(e) => this.handleGameClick(e)} />
                            <span className="checkmark_selectoneonly"></span>
                          </label>
                        </div>
                        <div className="w-100"></div>
                          <div className="col">
                            <label className="selectoneonly"><FormattedMessage id="actothers" />
                              <input type="checkbox"  id="ACT_ID_OTHERS" checked={this.state.fields['ACT_ID_OTHERS']} name="inside" value="inside_other" onClick = {(e) => this.handleGameClick(e)} />
                              <span className="checkmark_selectoneonly"></span>
                            </label>
                            <div className="form-group">
                              <input type="text" id="ACT_ID_OTHERS_REMARK" className={`${inside_remark_display} ptmaxcard-regis-input `} placeholder="อื่นๆโปรดระบุข้อมูล" value={this.state.fields['ACT_ID_OTHERS_REMARK']}  onChange={(e) => this.handleGameClick(e)} />
                            </div>
                          </div>
                        </div>
                        <label className="addtional"><FormattedMessage id="actactivityyouwant" /></label>
                        <div className="row">
                          <div className="col">
                            <label className="check_conditions_select"><FormattedMessage id="actfriend" />
                              <input type="checkbox"  id="ACT_HNGOUT_FRIEND" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_FRIEND']) ? (this.state.fields['ACT_HNGOUT_FRIEND'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false}  />
                              <span className="checkmark_condition_select"></span>
                            </label>
                          </div>
                          <div className="col">
                            <label className="check_conditions_select"><FormattedMessage id="actlove" />
                              <input type="checkbox"  id="ACT_HNGOUT_COUPLE" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_COUPLE']) ? (this.state.fields['ACT_HNGOUT_COUPLE'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false} />
                              <span className="checkmark_condition_select"></span>
                            </label>
                          </div>
                          <div className="w-100"></div>
                          <div className="col">
                            <label className="check_conditions_select"><FormattedMessage id="actfamily" />
                              <input type="checkbox"  id="ACT_HNGOUT_FAMILY" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_FAMILY']) ? (this.state.fields['ACT_HNGOUT_FAMILY'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false} />
                              <span className="checkmark_condition_select"></span>
                            </label>
                          </div>
                          <div className="col">
                            <label className="check_conditions_select"><FormattedMessage id="actnoout" />
                              <input type="checkbox"  id="ACT_HNGOUT_NO" name="activities"   onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_NO'] === true ? true : false)} />
                              <span className="checkmark_condition_select"></span>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className=" container-fluid d-flex">
                      <div className="pl-2">
                        <label className="check_conditions">
                          <input type="checkbox" onChange={(e) => this.changeHandlePDPA(e)} value={this.state.fields['chkPDPA']} onClick={(e) => this.changeHandlePDPA(e)} name="pdpacondition"/>
                          <span className="checkmark_condition"></span>
                        </label>
                      </div>
                      <div className="check_conditions pdpa-condition-main">
                        <FormattedMessage id="iaccept" />  <a style={{color:'#03ac4f'}} href="https://www.ptmaxcard.com/MaxCardPoint/" target='_blank'><FormattedMessage id="condition" /></a>
                        <FormattedMessage id="step2and" /> <a style={{color:'#03ac4f'}} href="https://www.ptmaxcard.com/privacypolicy" target='_blank'><FormattedMessage id="policy" /></a>
                      </div>
                    </div>
                    <div className="container-fluid" style={{display:this.state.chkPDPA}}>
                      <div className="check_conditions pdpa-condition-main"><FormattedMessage id="pdpacon1line1" /></div>
                      <div className="row mt-3 head_check_conditions" style={{display:"inline-flex"}}>
                        <div className="col-2">
                          <FormattedMessage id="agree" />
                        </div>
                        <div className="col-2">
                          <FormattedMessage id="notagree" />
                        </div>
                        <div className="col-8 p-0">
                          <FormattedMessage id="activity" />
                        </div>
                      </div>

                      {/* <div className="row">
                        <div className="col-2 check_conditions">
                          <label>
                            <input type="radio" 
                              name="pdpacon1" 
                              id="pdpacon1_1"
                              onChange={(e) => this.checkPDPAcon(e)}
                              onClick={(e) => this.checkPDPAcon(e)} 
                              defaultChecked={this.state.fields['pdpacon1'] == true ? true : false} 
                              value={true} 
                            />
                            <span className="radio_checkmark_condition"></span>
                          </label>
                        </div>
                        <div className="col-2 check_conditions">
                          <label>
                            <input type="radio" 
                              name="pdpacon1" 
                              id="pdpacon1_2"
                              onChange={(e) => this.checkPDPAcon(e)}
                              onClick={(e) => this.checkPDPAcon(e)} 
                              defaultChecked={this.state.fields['pdpacon1'] == false ? true : false} 
                              value={false} 
                            />
                            <span className="radio_checkmark_condition"></span>
                          </label>
                        </div>
                        <div className="col-8 check_conditions">
                          <FormattedMessage id="pdpacon1line2" />
                        </div>
                      </div> */}

                      {/* <div className="row">
                        <div className="col-2 check_conditions">
                          <label>
                            <input type="radio" 
                              name="pdpacon2" 
                              id="pdpacon2_1"
                              onChange={(e) => this.checkPDPAcon(e)}
                              onClick={(e) => this.checkPDPAcon(e)} 
                              defaultChecked={this.state.fields['pdpacon2'] == true ? true : false} 
                              value={true} 
                            />
                            <span className="radio_checkmark_condition"></span>
                          </label>
                        </div>
                        <div className="col-2 check_conditions ff">
                          <label>
                            <input type="radio" 
                              name="pdpacon2" 
                              id="pdpacon2_2"
                              onChange={(e) => this.checkPDPAcon(e)}
                              onClick={(e) => this.checkPDPAcon(e)} 
                              defaultChecked={this.state.fields['pdpacon2'] == false ? true : false} 
                              value={false} 
                            />
                            <span className="radio_checkmark_condition"></span>
                          </label>
                        </div>
                        <div className="col-8 check_conditions">
                          <FormattedMessage id="pdpacon2" />
                        </div>
                      </div> */}

                      <div className="row">
                        <div className="col-2 check_conditions">
                          <label>
                            <input type="radio" 
                              name="pdpacon3" 
                              id="pdpacon3_1"
                              onChange={(e) => this.checkPDPAcon(e)}
                              onClick={(e) => this.checkPDPAcon(e)} 
                              defaultChecked={this.state.fields['pdpacon3'] == true ? true : false} 
                              value={true} 
                            />
                            <span className="radio_checkmark_condition"></span>
                          </label>
                        </div>
                        <div className="col-2 check_conditions">
                          <label>
                            <input type="radio" 
                              name="pdpacon3" 
                              id="pdpacon3_2"
                              onChange={(e) => this.checkPDPAcon(e)}
                              onClick={(e) => this.checkPDPAcon(e)} 
                              defaultChecked={this.state.fields['pdpacon3'] == false ? true : false} 
                              value={false} 
                            />
                            <span className="radio_checkmark_condition"></span>
                          </label>
                        </div>
                        <div className="col-8 check_conditions">
                          <FormattedMessage id="pdpacon3" />
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-2 check_conditions">
                          <label>
                            <input type="radio" 
                              name="pdpacon4" 
                              id="pdpacon4_1"
                              onChange={(e) => this.checkPDPAcon(e)}
                              onClick={(e) => this.checkPDPAcon(e)} 
                              defaultChecked={this.state.fields['pdpacon4'] == true ? true : false} 
                              value={true} 
                            />
                            <span className="radio_checkmark_condition"></span>
                          </label>
                        </div>
                        <div className="col-2 check_conditions">
                          <label>
                            <input type="radio" 
                              name="pdpacon4" 
                              id="pdpacon4_2"
                              onChange={(e) => this.checkPDPAcon(e)}
                              onClick={(e) => this.checkPDPAcon(e)} 
                              defaultChecked={this.state.fields['pdpacon4'] == false ? true : false} 
                              value={false} 
                            />
                            <span className="radio_checkmark_condition"></span>
                          </label>
                        </div>
                        <div className="col-8 check_conditions">
                          <FormattedMessage id="pdpacon4" />
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-2 check_conditions">
                          <label>
                            <input type="radio" 
                              name="pdpacon5" 
                              id="pdpacon5_1"
                              onChange={(e) => this.checkPDPAcon(e)}
                              onClick={(e) => this.checkPDPAcon(e)} 
                              defaultChecked={this.state.fields['pdpacon5'] == true ? true : false} 
                              value={true} 
                            />
                            <span className="radio_checkmark_condition"></span>
                          </label>
                        </div>
                        <div className="col-2 check_conditions">
                          <label>
                            <input type="radio" 
                              name="pdpacon5" 
                              id="pdpacon5_2"
                              onChange={(e) => this.checkPDPAcon(e)}
                              onClick={(e) => this.checkPDPAcon(e)} 
                              defaultChecked={this.state.fields['pdpacon5'] == false ? true : false} 
                              value={false} 
                            />
                            <span className="radio_checkmark_condition"></span>
                          </label>
                        </div>
                        <div className="col-8 check_conditions">
                          <FormattedMessage id="pdpacon5" />
                        </div>
                      </div>
                      <div className="errorMsg">{this.state.errors["ck_agree_conditions"]}</div>
                      
                      
                      {/* <label className="check_conditions check_conditions-margin">
                        <FormattedMessage id="pdpacon1line2" />
                        <input type="checkbox" onChange={(e) => this.checkPDPAcon(e)} defaultChecked={this.state.fields['pdpacon1']} value={this.state.fields['pdpacon1']} onClick={(e) => this.changeHandlePDPA(e)} name="pdpacon1"/>
                        <span className="checkmark_condition"></span>
                      </label>
                      <label className="check_conditions check_conditions-margin">
                        <FormattedMessage id="pdpacon2" />
                        <input type="checkbox" onChange={(e) => this.checkPDPAcon(e)} defaultChecked={this.state.fields['pdpacon2']} value={this.state.fields['pdpacon2']}  onClick={(e) => this.changeHandlePDPA(e)}name="pdpacon2"/>
                        <span className="checkmark_condition"></span>
                      </label>
                      <label className="check_conditions check_conditions-margin">
                        <FormattedMessage id="pdpacon3" />
                        <input type="checkbox" onChange={(e) => this.checkPDPAcon(e)} defaultChecked={this.state.fields['pdpacon3']} value={this.state.fields['pdpacon3']} onClick={(e) => this.changeHandlePDPA(e)} name="pdpacon3"/>
                        <span className="checkmark_condition"></span>
                      </label>
                      <label className="check_conditions check_conditions-margin">
                        <FormattedMessage id="pdpacon4" />
                        <input type="checkbox" onChange={(e) => this.checkPDPAcon(e)} defaultChecked={this.state.fields['pdpacon4']} value={this.state.fields['pdpacon4']} onClick={(e) => this.changeHandlePDPA(e)} name="pdpacon4"/>
                        <span className="checkmark_condition"></span>
                      </label>
                      <label className="check_conditions check_conditions-margin">
                        <FormattedMessage id="pdpacon5" />
                        <input type="checkbox" onChange={(e) => this.checkPDPAcon(e)} defaultChecked={this.state.fields['pdpacon5']} value={this.state.fields['pdpacon5']} onClick={(e) => this.changeHandlePDPA(e)} name="pdpacon5"/>
                        <span className="checkmark_condition"></span>
                      </label> */}
                      <div className="container-fluid btn btn-secondary btn-block btn_agreement">
                        <button type="button" className={`btn-register-ptmax ${this.state.disabledBtn}`} onClick={() => this.submitFormMb()}><FormattedMessage id="step3senddata" /></button>
                      </div>
                    </div>
                  </div>
                {/* </div> */}
              </div>
          </div>
        </div>
          </div>
        </div>
        {this.state.modal}
      </div>
      </IntlProvider>
    )
  }
}