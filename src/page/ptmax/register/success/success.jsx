import React from 'react';
import '../../../ptmax/ptmax.css';
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../../translations/en.json";
import intlMessageTH from "../../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

export class Success extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
        };
    }


    componentDidMount(){

    }


    render(){

        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].RegisterSuccess}
            >
                <div>
                    <div className="bg-ptmax">
                        <div id="main">
                            <div className="box-content">
                                <div className="text-center my-4">
                                    <img style={{width:'20%'}} src={`${process.env.PUBLIC_URL}/images/01.png`} />
                                </div>
                                <div className="text-center my-4">
                                    <img style={{width:'5%'}} src={`${process.env.PUBLIC_URL}/images/cancel.png`} />
                                </div>
                                <div className="text-center sent-data-success my-3">
                                    <FormattedMessage id="senddata" />
                                </div>
                                <div className="text-center">
                                    <button className="btn-secondary"><FormattedMessage id="btnback" /></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </IntlProvider>
        )
    }
}


