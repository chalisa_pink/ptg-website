import React from 'react';
import '../../register/register.css';
import moment from "moment-timezone";
import 'moment/locale/th';
import DatePicker from 'react-datepicker';
import { registerAction } from '../../../_actions/registerActions'; 
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
import SweetAlert from "react-bootstrap-sweetalert";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');

const monthMap = {
    '1': '01',
    '2': '02',
    '3': '03',
    '4': '04',
    '5': '05',
    '6': '06',
    '7': '07',
    '8': '08',
    '9': '09',
    '10': '10',
    '11': '11',
    '12': '12',
  };
  
  const dateConfig = {
    'date':{
      format: 'DD',
    },
    'month':{
      format: value => monthMap[value.getMonth() + 1]
    },
    'year':{
      format: 'YYYY'
    },
  }
  
  const dateConfigTH = {
    'date':{
      format: 'DD',
    },
    'month':{
      format: value => monthMap[value.getMonth() + 1]
    },
    'year':{
      format: value => moment(value).add(543,'y').format('YYYY')
    },
  }

export class Edit extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            showLoading:"none",
            errors: {},
            errorsFocus:{},
            setActive: "active",
            fields : {
                PHONE_NO:'',
                EMAIL:'',
                PASSWORD:'',
                CHECKPASSWORD:'',
                FIRSTNAME:'',
                LASTNAME:'',
                BIRTHDATE:'',
                GENDER:''
            },
            show:false,
            modal:null,
            fields:{},
            time: new Date(),
            isOpenDatepickerMobile: false,
            errors: {},
            errorsFocus: {},
            email:'',
            dp:false,
            checkHandChange: false,
            Profile: "",
        };
        // this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount(){
        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
        var ProfileData = localStorage.getItem('Profile')
        var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
        ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        var ProfileArray = JSON.parse(ProfileDecrypt);
        var Profile = ProfileArray[0];
        var userId = Profile.userId;
        var tkmb = Profile.tkmb;
        this.setState({ Profile: Profile })
        this.getCustomerprofile(userId, tkmb);
    }
    getCustomerprofile(userId, tkmb){
        let { fields } = this.state;
        var userid = userId;
        var tkmb = tkmb;
        registerAction.getUserById(userid,tkmb).then(e => {
            // console.log("getUserById", e.data.data)
            if(e.data.isSuccess === true){
                fields = e.data.data
                this.setState({ fields })
            }else{

            }
        })
    }

    maxLengthCheck = (object) => {
        if (object.target.value.length > object.target.maxLength) {
            object.target.value = object.target.value.slice(0, object.target.maxLength)
        }
    }

    handChange(e){
        let { errors,errorsFocus, fields } = this.state;
        if (e.target.name === "PHONE_NO") {
          const re = /^[0-9\b]+$/;
          if (e.target.value === "" || re.test(e.target.value)) {
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
          } else {
            errors[e.target.name] = <FormattedMessage id="plsinputonlynumber" />
            errorsFocus[e.target.name] = ''
            e.preventDefault();
          }
        } else {
          fields[e.target.name] = e.target.value;
          errors[e.target.name] = null;
          errorsFocus[e.target.name] = ''
        }
        this.setState({ errors, fields , errorsFocus, checkHandChange: true });
    }

    async handleSubmit(event) {
        var { fields } = this.state;
        event.preventDefault();
        if (await this.validateForm()) {
            // console.log("fields", fields)
            if(fields.phoneNo == "-"){
                this.onUpdateUser()
            } else {
                this.props.history.push({
                    pathname: `${process.env.PUBLIC_URL}/edit/otp`,
                    state: {
                        data: this.state.fields['phoneNo'],
                        firstname : this.state.fields['firstName'],
                        lastname : this.state.fields['lastName'],
                        birthday : this.state.fields['birthDay'],
                        gender : this.state.fields['gender']
                    }
                })
            }
        } else {
        //   console.log('formsubmit ' + false);
        }
        this.setState({ fields })
    }
    
      async validateForm() {
        let fields = this.state.fields;
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;

        if(!fields["phoneNo"]){
            formIsValid = false;
            errorsFocus["phoneNo"] = 'errorFocus'
            errors["phoneNo"] = <FormattedMessage id="plsinputphoneno" />
        }
        if(!fields["email"]){
            formIsValid = false;
            errorsFocus["email"] = 'errorFocus'
            errors["email"] = <FormattedMessage id="plsinputemail" />
        }
        if(!fields["firstName"]){
            formIsValid = false;
            errorsFocus["firstName"] = 'errorFocus'
            errors["firstName"] = <FormattedMessage id="plsinputname" />
        }
        if(!fields["lastName"]){
            formIsValid = false;
            errorsFocus["lastName"] = 'errorFocus'
            errors["lastName"] = <FormattedMessage id="plsinputlastname" />
        }
        if(!fields["birthDay"]){
            formIsValid = false;
            errorsFocus["birthDay"] = 'errorFocus'
            errors["birthDay"] = <FormattedMessage id="plsinputbirthday" />
        }else{
            var dateBeforDiff = moment(fields["birthDay"]).format("YYYY-MM-DD");
            var diff = moment().diff(moment(dateBeforDiff, "YYYY-MM-DD"), "years");
            var diffDay = moment().diff(moment(dateBeforDiff, "YYYY-MM-DD"), "days");
            if (diff < 7 || diffDay <= 2556) {
                formIsValid = false;
                errorsFocus["birthDay"] = 'errorFocus'
                errors["birthDay"] = <FormattedMessage id="oldnosevenyears" />
            }
        }
        if(!fields["gender"]){
            formIsValid = false;
            errorsFocus["gender"] = 'errorFocus'
            errors["gender"] = <FormattedMessage id="plsselectgender" />
        }
        this.setState({
            errors: errors,
            errorsFocus: errorsFocus
        });

        return formIsValid;
      }


      onUpdateUser(){
        const { Profile, fields } = this.state
        var userid = Profile.userId;
        var token = Profile.tkmb;
        var formData = new FormData()
        formData.append('tokenMobileApp' , token)
        formData.append('userId' , userid)
        formData.append('firstName' , fields.firstName)
        formData.append('lastName' , fields.lastName)
        formData.append('birthDay' , fields.birthDay)
        formData.append('gender' , fields.gender)
        registerAction.updateUser(formData).then(e => {
            // console.log("updateUser", e)
            if(e.data.isSuccess === true){
                window.location.href = `${process.env.PUBLIC_URL}/home`
            }else{
                var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
                var msg = e.data.errMsg
                this.modalCheckSubmit(msg,imgPopup)
            }
        })
    }

    modalCheckSubmit(msg,img){
        alert = (
            <SweetAlert
                custom
                showCloseButton
                closeOnClickOutside={false}
                focusConfirmBtn={false}
                title=""
                customIcon={img}
                showConfirm={false}
                showCancelButton
                onCancel={() => this.handleChoice(false)}
                onConfirm={() => this.handleChoice(true)}
            >
              <div className="iconClose" onClick={() => this.handleChoice(false)}></div>
              <div className="fontSizeCase">{msg}</div>
            </SweetAlert>
          );
          this.setState({ show: true, modal: alert });
    }

    handleChoice(){
        this.setState({ modal : null , show : false })
    }

    render(){
        var yearnow = new Date().getFullYear()
        var start = yearnow - parseInt(100);
        var setYear = [];
        var selectTime = new Date()
        var minDate = new Date();
        minDate.setFullYear(minDate.getFullYear() - 100);
        var date_Config = dateConfigTH;
        var Editprofile = messages[this.state.language].Edit
        var setGender = this.state.fields['gender']
        if(setGender == 0){
            var genderValue = 1
        } else if(setGender == 1){
            var genderValue = 2
        } else if(setGender == 2){
            var genderValue = 3
        } else {
            var genderValue = 4
        }
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].Edit}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="bg-ptmaxcard-content">
                            <div className="ptmaxcard-content">
                                <div className="row">
                                    <div className="col-lg-7 col-md-12 mx-auto">
                                        <div className="web-register-card-body">
                                            <form onSubmit={e => this.handleSubmit(e)}>
                                                <div>
                                                    <label>
                                                        <span className="label-validate">*</span> 
                                                        <span className="font-header-label pl-2"><FormattedMessage id="phoneno" /></span>
                                                    </label>
                                                    <div>
                                                        <div className="form-control disabled input_editregister" disabled>
                                                            {this.state.fields['phoneNo']}
                                                        </div>
                                                        <input 
                                                            maxLength="10"
                                                            className={`inputWidth form-control input_editregister ${this.state.errorsFocus['phoneNo']}`}
                                                            onInput={this.maxLengthCheck}
                                                            placeholder={Editprofile.inputphoneno}
                                                            type="hidden"
                                                            name="phoneNo"
                                                            value={this.state.fields['phoneNo']}
                                                            onChange={(e) => this.handChange(e)}
                                                        />
                                                    </div>
                                                    <div className="errorMsg phoneNo">{this.state.errors["phoneNo"]}</div>
                                                </div>

                                                <div className="label-mt">
                                                    <label>
                                                        <span className="label-validate">*</span> 
                                                        <span className="font-header-label pl-2"><FormattedMessage id="email" /></span>
                                                    </label>
                                                    <div>
                                                        <div className="form-control disabled input_editregister" disabled>
                                                            {(this.state.fields['email'])}
                                                        </div>
                                                        <input 
                                                            className={`inputWidth form-control input_form input_editregister ${this.state.errorsFocus['email']}`}
                                                            placeholder={Editprofile.inputemail}
                                                            name="email"
                                                            type="hidden"
                                                            disabled={this.state.dp}
                                                            value={(this.state.fields['email'])}
                                                            onChange={(e) => this.handChange(e)}
                                                        />
                                                    </div>
                                                    <div className="errorMsg EMAIL">{this.state.errors["EMAIL"]}</div>
                                                </div>
                                                <div className="header-label t-30 mt-5 mb-3"><FormattedMessage id="profiledata" /></div>
                                                <div className="label-mt">
                                                    <label className="font-header-label"><span className="label-validate">*</span> <FormattedMessage id="firstname" /></label>
                                                    <div>
                                                        <input 
                                                            className={`inputWidth form-control input_form input_editregister ${this.state.errorsFocus['firstName']}`}
                                                            placeholder={Editprofile.inputfirstname}
                                                            name="firstName"
                                                            value={this.state.fields['firstName']}
                                                            onChange={(e) => this.handChange(e)}
                                                        />
                                                    </div>
                                                    <div className="errorMsg firstName">{this.state.errors["firstName"]}</div>
                                                </div>
                                                <div className="label-mt">
                                                    <label>
                                                        <span className="label-validate">*</span> 
                                                        <span className="font-header-label pl-2"><FormattedMessage id="lastname" /></span>
                                                    </label>
                                                    <div>
                                                        <input 
                                                            className={`inputWidth form-control input_form input_editregister ${this.state.errorsFocus['lastName']}`}
                                                            placeholder={Editprofile.inputlastname}
                                                            name="lastName"
                                                            value={this.state.fields['lastName']}
                                                            onChange={(e) => this.handChange(e)}
                                                        />
                                                    </div>
                                                    <div className="errorMsg LASTNAME">{this.state.errors["lastName"]}</div>
                                                </div>
                                                <div className="label-mt">
                                                    <label>
                                                        <span className="label-validate">*</span> 
                                                        <span className="font-header-label pl-2"><FormattedMessage id="birthday" /></span>
                                                    </label>
                                                    <div>
                                                        <input
                                                            className={`inputWidth form-control input_form input_editregister ${this.state.errorsFocus['birthDay']}`}
                                                            placeholder={Editprofile.inputbirthday}
                                                            type="date" 
                                                            id="date"
                                                            name="birthDay"
                                                            value={this.state.fields['birthDay']}
                                                            selected={this.state.startDate}
                                                            onChange={(e) => this.handChange(e)}
                                                        />
                                                    </div>
                                                    <div className="errorMsg birthDay">{this.state.errors["birthDay"]}</div>
                                                </div>
                                                <div className="label-mt">
                                                    <label>
                                                        <span className="label-validate">*</span> 
                                                        <span className="font-header-label pl-2"><FormattedMessage id="gender" /></span>
                                                    </label>
                                                    <div>
                                                        <select 
                                                            className={`inputWidth form-control input_form input_editregister ${this.state.errorsFocus['gender']}`}
                                                            placeholder={Editprofile.inputgender}
                                                            name="gender"
                                                            // value={this.state.checkHandChange ? this.state.fields['gender'] : genderValue }
                                                            value={this.state.fields['gender']}
                                                            onChange={(e) => this.handChange(e)}
                                                        >
                                                            <option value="4">{Editprofile.selectgender}</option>
                                                            <option value="1">{Editprofile.genderboy}</option>
                                                            <option value="2">{Editprofile.gendergirl}</option>
                                                            <option value="3">{Editprofile.genderother}</option>
                                                        </select>
                                                    </div>
                                                    <div className="errorMsg gender">{this.state.errors["gender"]}</div>
                                                </div>
                                                <div className="btn_nextstep mt-5">
                                                    <button 
                                                        type="submit"
                                                        className="btn btn-block btn-web-register"
                                                    >
                                                        <FormattedMessage id="btnconfirm" />
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.state.modal}
                </div>
            </IntlProvider>
        )
    }
}

