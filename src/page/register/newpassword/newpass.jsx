import React from 'react';
import '../../register/register.css';
import SweetAlert from 'react-bootstrap-sweetalert';
import { registerAction } from '../../../_actions/registerActions';  
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
var Cipher = require('aes-ecb');

export class newpass extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            errors: {},
            errorsFocus:{},
            fields:{},
            show: false, 
            modal: null,
            showLoading:"none",
        };
    }

    componentDidMount(){
    }

    modalAlert(msg,img){
        alert = (
            <SweetAlert
              custom
              showCloseButton
              closeOnClickOutside={false}
              focusConfirmBtn={false}
              title=""
              customIcon={img}
              showConfirm={false}
              showCancelButton
              onCancel={() => this.handleChoice(false)}
              onConfirm={() => this.handleChoice(true)}
            >
              <div className="iconClose" onClick={() => this.handleChoice(false)}></div>
              <div className="fontSizeCase">{msg}</div>
            </SweetAlert>
          );
          this.setState({ show: true, modal: alert });
    }

    handleChoice(){
        this.setState({ modal : null , show : false })
    }

    handelSubmit(passold,passfirst,passtwo){
        var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
        var ProfileData = localStorage.getItem('Profile')
        var ProfileDecrypt = Cipher.decrypt(keyCipher, ProfileData);
        ProfileDecrypt = ProfileDecrypt.replace(/[\u0000-\u0019]+/g,""); 
        var ProfileArray = JSON.parse(ProfileDecrypt);
        var Profile = ProfileArray[0];
        let { fields } = this.state;
        if(this.validateForm()){
            let formData = new FormData();
            formData.append('userId' , Profile.userId)
            formData.append('password' , passold)
            registerAction.chkPassword(formData).then(e => {
                if(e.data.isSuccess === true){
                    this.props.history.push({
                        pathname: `${process.env.PUBLIC_URL}/newpassotp`,
                        state: {
                            data: Profile.phoneNo,
                            password : passfirst,
                            passold : passold
                        }
                    })
                }else{
                    var msgEn = e.data.errMsg_en;
                    var msgTH = e.data.errMsg;
                    var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
                    this.modalAlert(msgTH,imgPopup);
                }
            })
        }else{
            // console.log('formsubmit ' + false);
        }
        this.setState({ fields })
    }

    validateForm() {
        let fields = this.state.fields;
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;
        if(!fields['PASSOLD']){
            formIsValid = false;
            errorsFocus["PASSOLD"] = 'errorFocus';
            errors["PASSOLD"] = <FormattedMessage id="plsinputpassword" />
        }else{
            var lengthpass = fields['PASSOLD'].length;
            if(lengthpass < 6){
                formIsValid = false;
                errorsFocus["PASSOLD"] = 'errorFocus';
                errors["PASSOLD"] = <FormattedMessage id="validatepasswordsixchar" />;
            }
        }
        if(!fields["PASSONE"]){
            formIsValid = false;
            errorsFocus["PASSONE"] = 'errorFocus'
            errors["PASSONE"] = <FormattedMessage id="plsinputpassword" />
        }else{
            var lengthpass = fields['PASSONE'].length;
            if(lengthpass < 6){
                formIsValid = false;
                errorsFocus["PASSONE"] = 'errorFocus';
                errors["PASSONE"] = <FormattedMessage id="validatepasswordsixchar" />;
            }
        }
        if(!fields["PASSTWO"]){
            formIsValid = false;
            errorsFocus["PASSTWO"] = 'errorFocus'
            errors["PASSTWO"] = <FormattedMessage id="plsinputpasswordagain" />
        }else{
            var lengthpass = fields['PASSTWO'].length;
            if(lengthpass < 6){
                formIsValid = false;
                errorsFocus["PASSTWO"] = 'errorFocus';
                errors["PASSTWO"] = <FormattedMessage id="validatepasswordsixchar" />;
            }
        }
        if(fields["PASSONE"] !== fields["PASSTWO"]){
            formIsValid = false;
            errorsFocus["PASSTWO"] = 'errorFocus'
            errors["PASSTWO"] = <FormattedMessage id="passwordnotmacth" />
        }

        this.setState({
            errors: errors,
            errorsFocus: errorsFocus
        });

        return formIsValid;
    }

    handleChange(e){
        let { errors,errorsFocus, fields } = this.state;
        fields[e.target.name] = e.target.value;
        errors[e.target.name] = null;
        errorsFocus[e.target.name] = ''
        this.setState({ errors, fields , errorsFocus });
    }

    render(){
        var setlangnewpass = messages[this.state.language].newpass
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].newpass}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="bg-ptmaxcard-content">
                            <div className="ptmaxcard-content">
                                <div className="row">
                                    <div className="col-lg-7 col-md-12 mx-auto">
                                        <div className="web-register-card-body">
                                            <form>
                                                <div className="label-mt">
                                                    <label>
                                                        <span className="label-validate">*</span>
                                                        <span className="font-header-label pl-2"><FormattedMessage id="oldpassword" /></span>
                                                    </label>
                                                    <div>
                                                        <input
                                                            className={`inputWidth form-control input_form input_editregister ${this.state.errorsFocus['PASSOLD']}`}
                                                            name="PASSOLD"
                                                            type="password"
                                                            minLength="6"
                                                            placeholder={setlangnewpass.inputpassword}
                                                            onChange={(e) => this.handleChange(e)}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="errorMsg PASSONE">{this.state.errors["PASSOLD"]}</div>
                                                <div className="label-mt">
                                                    <label>
                                                        <span className="label-validate">*</span>
                                                        <span className="font-header-label pl-2"><FormattedMessage id="password" /></span>
                                                    </label>
                                                    <div>
                                                        <input
                                                            className={`inputWidth form-control input_form input_editregister ${this.state.errorsFocus['PASSONE']}`}
                                                            name="PASSONE"
                                                            minLength="6"
                                                            type="password"
                                                            placeholder={setlangnewpass.inputpassword}
                                                            onChange={(e) => this.handleChange(e)}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="errorMsg PASSONE">{this.state.errors["PASSONE"]}</div>
                                                <div className="label-mt">
                                                    <label>
                                                        <span className="label-validate">*</span>
                                                        <span className="font-header-label pl-2"><FormattedMessage id="inputpassagain" /></span>
                                                    </label>
                                                    <div>
                                                        <input
                                                            className={`inputWidth form-control input_form input_editregister ${this.state.errorsFocus['PASSTWO']}`}
                                                            placeholder={setlangnewpass.plsinputpasswordagain}
                                                            type="password"
                                                            name="PASSTWO"
                                                            minLength="6"
                                                            onChange={(e) => this.handleChange(e)}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="errorMsg PASSTWO">{this.state.errors["PASSTWO"]}</div>
                                                <div  className="btn_nextstep">
                                                    <button 
                                                        type="button"
                                                        className="btn btn-block btn-web-register"
                                                        onClick={() => this.handelSubmit(this.state.fields['PASSOLD'],this.state.fields['PASSONE'] , this.state.fields['PASSTWO'])}
                                                    >
                                                        <FormattedMessage id="btnconfirm" />
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.state.modal}
                </div>
            </IntlProvider>
        )
    }
}

