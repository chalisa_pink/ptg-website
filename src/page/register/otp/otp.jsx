import React from "react";
import '../../register/register.css';
import { registerAction } from '../../../_actions/registerActions'; 
import SweetAlert from "react-bootstrap-sweetalert";
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

var OTP_All = ""

export class otp extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            fields: {},
            resp: [],
            show: false,
            modal: null,
            secondsElapsed: 0,
            setOTP: '', 
            setOTPAll: '',
            firstClickSendOTP: 0
        } 
    }

    componentDidMount(){
        if(this.props.location.state.data) {
            let newObject = Object.assign(this.props.location.state.data , this.state.fields)
            this.setState({ fields: newObject })
        }
        // this.sentOTP(this.props.location.state.data)
        this.startTime();
        document.addEventListener("keydown", this.deleteBtnFunction, false);
    }

    startTime() {
        var _this = this;
        this.countdown = setInterval(function() {
            if(_this.state.secondsElapsed > 0){
                _this.setState({ 
                secondsElapsed: _this.state.secondsElapsed - 1 
                });
            }         
        }, 1000);
    }

    resetTime() {
        this.sentOTP(this.props.location.state.data);
        this.reset = this.setState({
            secondsElapsed: (this.state.secondsElapsed = 60)
        });
    }

    getTime() {
        var textOTP
        if(this.state.secondsElapsed % 60 > 0){
            var textOTP = this.showCountdown();
        }else{
            var textOTP = <button className="text-green" onClick={() => this.resetTime()}><FormattedMessage id="sendotp" /></button>
        }
        return (textOTP);
    }

    showCountdown= (e) => {
        return (`${messages[this.state.language].registerotp.otpwillsendin} ` + (this.state.secondsElapsed % 60) + ` ${messages[this.state.language].registerotp.section}`);
    }

    sentOTP(phone_no){
        var type = 'Register';
        registerAction.sendOTP(phone_no,type).then( e => {
            if(e.data.isSuccess === true){
                this.setState({ resp : e.data.data })
            }else{
                this.setState({ resp: e.data.data })
            }
        })
    }

    verifyOTPClick(setOTP,otpkey){
        var OTP_All = setOTP
        var formData = new FormData();
        formData.append('otp_Key_No',otpkey)
        formData.append('eventType','Register')
        formData.append('eventNo',this.props.location.state.data)
        formData.append('value', OTP_All)

        registerAction.verifyOTP(formData).then(e => {
            if(e.data.isSuccess === true){
                this.setState({ resp : e.data.data })
                this.props.history.push({
                    pathname: `${process.env.PUBLIC_URL}/register/web`,
                    state: {
                        type:1, 
                        phone:this.props.location.state.data,
                    }
                  });
            }else{
                var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
                var msg = e.data.errMsg
                this.modalCheckSubmit(msg,imgPopup)
            }
        })
    } 

    modalCheckSubmit(msg,img){
        alert = (
            <SweetAlert
              custom
              showCloseButton
              closeOnClickOutside={false}
              focusConfirmBtn={false}
              title=""
              customIcon={img}
              showConfirm={false}
              showCancelButton
              onCancel={() => this.handleChoice(false)}
              onConfirm={() => this.handleChoice(true)}
            >
              <div className="iconClose" onClick={() => this.handleChoice(false)}></div>
              <div className="fontSizeCase">{msg}</div>
            </SweetAlert>
          );
          this.setState({ show: true, modal: alert });
    }

    handleChoice(){
        this.setState({ modal : null , show : false })
    }


    maxLengthCheck = (object) => {
        if (object.target.value.length > object.target.maxLength) {
            object.target.value = object.target.value.slice(0, object.target.maxLength)
        }
    }

    handleChange(e){
        this.setState({ 
            setOTP : e.target.value.replace(/[^0-9]/g, ''),            
        })
    }

    render(){
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].registerotp}
            >
                <div>
                    <div className="bg-body py-5">
                        <div className="content-otp">
                            <div className="container-fluid">
                                <div className="bottom-hr text-gray p-3">
                                    <div className="row">
                                        <div className="col-7 col-xs-7 col-sm-7 col-md-7"><FormattedMessage id="otpsendtophoneno" /></div>
                                        <div className="col-5 col-xs-5 col-sm-5 col-md-5 text-black text-right">
                                            {this.props.location.state.data}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="text-center p-3 my-5">
                                <input 
                                    type="text"
                                    maxLength="6"
                                    onInput={this.maxLengthCheck}
                                    className="inputTel"
                                    onChange={(e) => this.handleChange(e)}
                                />
                            </div>
                            <div className="mt-4 text-center text-gray"><FormattedMessage id="remarkotp" /> : {this.state.resp.ref_No}</div>
                            <div className="mt-5 text-center text-green">
                                <div>{this.getTime()}</div>  
                            </div>
                            <div className="btn_nextstep p-3 text-center">
                                <div className="otp-btn"><button ref="7" id="7" type="button" onClick={() => this.verifyOTPClick(this.state.setOTP,this.state.resp.otP_Key_No)} className="btn btn-secondary btn-block"><FormattedMessage id="confrimotp" /></button></div>
                                <div className="text-center text-gray mt-3"><FormattedMessage id="remark" /></div>
                            </div>
                            {this.state.modal}
                        </div>
                    </div>
                </div>
            </IntlProvider>
        )
    }
} 
