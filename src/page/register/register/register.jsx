import React from 'react';
import '../../register/register.css';
import moment from "moment-timezone";
import 'moment/locale/th';
import DatePicker from 'react-datepicker';
import { registerAction } from '../../../_actions/registerActions'; 
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
import SweetAlert from 'react-bootstrap-sweetalert';
var Cipher = require('aes-ecb');

const publicIp = require('public-ip');

const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};

const monthMap = {
    '1': '01',
    '2': '02',
    '3': '03',
    '4': '04',
    '5': '05',
    '6': '06',
    '7': '07',
    '8': '08',
    '9': '09',
    '10': '10',
    '11': '11',
    '12': '12',
  };
  
  const dateConfig = {
    'date':{
      format: 'DD',
    },
    'month':{
      format: value => monthMap[value.getMonth() + 1]
    },
    'year':{
      format: 'YYYY'
    },
  }
  
  const dateConfigTH = {
    'date':{
      format: 'DD',
    },
    'month':{
      format: value => monthMap[value.getMonth() + 1]
    },
    'year':{
      format: value => moment(value).add(543,'y').format('YYYY')
    },
  }

export class Register extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            errors: {},
            errorsFocus:{},
            setActive: "active",
            fields : {
                PHONE_NO:'',
                EMAIL:'',
                PASSWORD:'',
                CHECKPASSWORD:'',
                FIRSTNAME:'',
                LASTNAME:'',
                BIRTHDATE:'',
                GENDER:'',
                // udId:this.state.ipaddress,
                playerId : 'WEBSITE',
            },
            show:false,
            modal:null,
            time: new Date(),
            isOpenDatepickerMobile: false,
            errors: {},
            errorsFocus: {},
            email:'',
            dpemail:false,
            dpphone:false
        };
        // this.handleChange = this.handleChange.bind(this);
    }

    async componentDidMount(){
        let { fields , dpemail , dpphone } = this.state;
        if(this.props.location.state) {
            let newObject = Object.assign(this.props.location.state , this.state.fields)
            fields = newObject
            this.setState({ fields })
        }

        if(this.props.history.location.state !== undefined && this.props.history.location.state.type === 0){
            fields['EMAIL'] = this.props.history.location.state.data;
            dpemail = true;
            dpphone = false;
            this.setState({ email :  this.props.history.location.state.data , fields , dpemail , dpphone})
        }else if(this.props.history.location.state !== undefined && this.props.history.location.state.type === 1){
            fields['PHONE_NO'] = this.props.history.location.state.data;
            dpphone = true;
            dpemail = false;
            this.setState({ tel : this.props.history.location.state.data , fields , dpemail , dpphone})
        }else{
            if(this.props.history.location.state.socialType === "gmail" || this.props.history.location.state.socialType === "facebook"){
                fields['EMAIL'] = this.props.history.location.state.email;
            }else{
                this.props.history.location.state = '';
            }
        }
        if(this.props.location.state.phone !== ""){ 
            fields['PHONE_NO'] = this.props.history.location.state.phone;
            dpphone = false;
            dpemail = true;
        }
        if(this.props.location.state.email !== ""){
            fields['EMAIL'] = this.props.history.location.state.email;
            dpemail = false;
            dpphone = true;
        }
        this.setState({ fields , dpemail , dpphone })

        await registerAction.getLocationIP().then(e => {
            fields['udId'] = e.data.ip;
            this.setState({fields})
        })

    }

    maxLengthCheck = (object) => {
        if (object.target.value.length > object.target.maxLength) {
            object.target.value = object.target.value.slice(0, object.target.maxLength)
        }
    }
    
    modalCheckSubmit(msg,img,err){
        alert = (
            <SweetAlert
              custom
              showCloseButton
              closeOnClickOutside={false}
              focusConfirmBtn={false}
              title=""
              customIcon={img}
              showConfirm={false}
              showCancelButton
              onCancel={() => this.handleChoice(false,err)}
              onConfirm={() => this.handleChoice(true,err)}
            >
              <div className="iconClose" onClick={() => this.handleChoice(false,err)}></div>
              <div className="fontSizeCase">{msg}</div>
            </SweetAlert>
          );
          this.setState({ show: true, modal: alert });
    }

    handleChoice(status,err){
        this.setState({ modal : null , show : false })
    }

    handChange(e){
        let { errors,errorsFocus, fields } = this.state;
        if (e.target.name === "PHONE_NO") {
          const re = /^[0-9\b]+$/;
          if (e.target.value === "" || re.test(e.target.value)) {
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
          } else {
            errors[e.target.name] = <FormattedMessage id="plsinputonlynumber" />
            errorsFocus[e.target.name] = ''
            e.preventDefault();
          }
        } else {
          fields[e.target.name] = e.target.value;
          errors[e.target.name] = null;
          errorsFocus[e.target.name] = ''
        }
        this.setState({ errors, fields , errorsFocus });
    }

    async handleSubmit(event) {
        var { fields } = this.state;
        event.preventDefault();
        if (await this.validateForm()) {
            sessionStorage.setItem('inputBody', JSON.stringify(this.state.fields))
            var formData = new FormData();
            formData.append('socialId' , '')
            formData.append('socialType' , '')
            formData.append('phoneNo' , this.state.fields['PHONE_NO'])
            formData.append('email' , this.state.fields['EMAIL'])
            formData.append('password' , this.state.fields['PASSWORD'])
            formData.append('preName' , '0')
            formData.append('firstName' , this.state.fields['FIRSTNAME'])
            formData.append('lastName' , this.state.fields['LASTNAME'])
            formData.append('birthDay' , this.state.fields['BIRTH_DATE'])
            formData.append('gender' , this.state.fields['GENDER'])
            formData.append('udId' , this.state.fields.udId)
            formData.append('tokenId' , '')
            formData.append('playerId' , this.state.fields.playerId)
            registerAction.registerWebSite(formData).then(e => {
                if(e.data.isSuccess === true){
                    this.setState({ 
                        userid : e.data.data.userId ,
                        phone : e.data.data.phoneNo , 
                        mail : e.data.data.email,
                        password:e.data.data.password ,
                        cardno : e.data.data.ptCardNo ,
                        prename : e.data.data.preName ,
                        firstname : e.data.data.firstName ,
                        lastname : e.data.data.lastName , 
                        birthdate : e.data.data.birthDay,
                        idcard : e.data.data.idCard ,
                        pinCode: e.data.data.pinCode ,
                        gerder:e.data.data.gender ,
                        facebookId : e.data.data.facebookId , 
                        gmailId : e.data.data.gmailId,
                        isActive : e.data.data.isActive,
                        tokeN_ID : e.data.data.tokeN_ID,
                        customeR_ID : e.data.data.customeR_ID,
                        citizeN_TYPE_ID : e.data.data.citizeN_TYPE_ID,
                        citizeN_ID : e.data.data.citizeN_ID,
                        titlE_NAME_ID : e.data.data.titlE_NAME_ID ,
                        titlE_NAME_REMARK : e.data.data.titlE_NAME_REMARK ,
                        fulL_ADDRESS : e.data.data.full_ADDRESS ,
                        customeR_STATUS : e.data.data.customeR_STATUS ,
                        isMemberPTG : e.data.data.isMemberPTG ,
                        tokenMobileApp : e.data.data.tokenMobileApp ,
                    })

                    let formData = new FormData();
                    if(this.props.history.location.state.socialType === "gmail" || this.props.history.location.state.socialType === "facebook"){
                        formData.append('userName' , this.state.fields['PHONE_NO']);
                        formData.append('password' , this.state.fields['PASSWORD']);
                        formData.append('udId',this.state.fields.udId);
                        formData.append('tokenId','');
                        formData.append('playerId',this.state.fields.playerId);
                    }else{
                        formData.append('userName' , this.state.fields['PHONE_NO']);
                        formData.append('password' , this.state.fields['PASSWORD']);
                        formData.append('udId',this.state.fields.udId);
                        formData.append('tokenId','');
                        formData.append('playerId',this.state.fields.playerId);
                    }
                    registerAction.loginWebsite(formData).then(e => {
                        if(e.data.isSuccess === true){
                            localStorage.setItem("user", e.data.data.firstName)
                            localStorage.setItem("2qMtf2VrI3aNUcxMQ9KHpciWgxAUV2Uu", "true")
                            localStorage.setItem('uid',e.data.data.userId)
                            localStorage.setItem('phone' , e.data.data.phoneNo)
                            localStorage.setItem('tkmb' , e.data.data.tokenMobileApp)
                            localStorage.setItem("lastname", e.data.data.lastName)
                            localStorage.setItem('email',e.data.data.email)
                            localStorage.setItem('birthdate',e.data.data.birthDay)


                            var Profile = [];
                            Profile.push({
                                "userId" : e.data.data.userId,
                                "firstName" : e.data.data.firstName,
                                "lastName" : e.data.data.lastName,
                                "phoneNo" : e.data.data.phoneNo,
                                "ptCardNo" : e.data.data.ptCardNo,
                                "email" : e.data.data.email,
                                "birthDay" : e.data.data.birthDay,
                                "tkmb" : e.data.data.tokenMobileApp,
                                "tkvsm" : e.data.data.tokeN_ID,
                                "customer_id":e.data.data.customeR_ID,
                                "isMember":e.data.data.isMemberPTG,
                                "full_address":e.data.data.fulL_ADDRESS,
                                "titlE_NAME_ID" :e.data.data.titlE_NAME_ID,
                                "titlE_NAME_REMARK":e.data.data.titlE_NAME_REMARK,
                                "citizeN_TYPE_ID":e.data.data.citizeN_TYPE_ID,
                                "citizeN_ID":e.data.data.citizeN_ID,
                                "idCard":e.data.data.idCard,
                                "customeR_STATUS":e.data.data.customeR_STATUS,
                                "isActive":e.data.data.isActive,
                                "gender" : e.data.data.gender
                            });
                            var Profilestringify = JSON.stringify(Profile);
                            var keyCipher = 'pPTGWebsiteMaxCardEncryptDecrypt';
                            var ProfileEncrypt = Cipher.encrypt(keyCipher, Profilestringify);
                            localStorage.setItem("Profile", ProfileEncrypt);

                            window.location.href = `${process.env.PUBLIC_URL}/home`;
                        }
                    })
                }else{
                    var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
                    this.modalCheckSubmit((this.state.language == 'en' ? e.data.errMsg_en : e.data.errMsg),imgPopup,e.errCode)

                }
          })
        } else {
            // console.log('formsubmit ' + false);
        }
        this.setState({ fields })
      }
    
      async validateForm() {
        let fields = this.state.fields;
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;
        // if(this.props.history.location.state.socialType === "gmail"){
        //     if(!fields["PHONE_NO"]){
        //         formIsValid = true;
        //         errorsFocus["PHONE_NO"] = undefined;
        //         errors["PHONE_NO"] = "";
        //     }
        // }else if(this.props.history.location.state.socialType === "facebook"){
        //     if(!fields["PHONE_NO"]){
        //         formIsValid = true;
        //         errorsFocus["PHONE_NO"] = undefined;
        //         errors["PHONE_NO"] = "";
        //     }
        // }else{
            if(!fields["PHONE_NO"]){
                formIsValid = false;
                errorsFocus["PHONE_NO"] = 'errorFocus'
                errors["PHONE_NO"] = <FormattedMessage id="plsinputphoneno" />
            }
        // }
        if(!fields["EMAIL"]){
            formIsValid = false;
            errorsFocus["EMAIL"] = 'errorFocus'
            errors["EMAIL"] = <FormattedMessage id="plsinputemail" />
        }
        if(!fields["PASSWORD"]){
            formIsValid = false;
            errorsFocus["PASSWORD"] = 'errorFocus'
            errors["PASSWORD"] = <FormattedMessage id="plsinputpassword" />
        }
        if(!fields["CHECKPASSWORD"]){
            formIsValid = false;
            errorsFocus["CHECKPASSWORD"] = 'errorFocus'
            errors["CHECKPASSWORD"] = <FormattedMessage id="plsonputpasswordaagain" />
        }
        if(fields["PASSWORD"] !== fields["CHECKPASSWORD"]){
            formIsValid = false;
            errorsFocus["CHECKPASSWORD"] = 'errorFocus'
            errors["CHECKPASSWORD"] = <FormattedMessage id="passwordnotmatch" />
        }
        if(!fields["FIRSTNAME"]){
            formIsValid = false;
            errorsFocus["FIRSTNAME"] = 'errorFocus'
            errors["FIRSTNAME"] = <FormattedMessage id="plsinputfirstname" />
        }
        if(!fields["LASTNAME"]){
            formIsValid = false;
            errorsFocus["LASTNAME"] = 'errorFocus'
            errors["LASTNAME"] = <FormattedMessage id="plsinputlastname" />
        }
        if(!fields["BIRTH_DATE"]){
            formIsValid = false;
            errorsFocus["BIRTH_DATE"] = 'errorFocus'
            errors["BIRTH_DATE"] = <FormattedMessage id="plsinputbirthday" />
        }else{
            var dateBeforDiff = moment(fields["BIRTH_DATE"]).format("YYYY-MM-DD");
            var diff = moment().diff(moment(dateBeforDiff, "YYYY-MM-DD"), "years");
            var diffDay = moment().diff(moment(dateBeforDiff, "YYYY-MM-DD"), "days");
            if (diff < 7 || diffDay <= 2556) {
                formIsValid = false;
                errorsFocus["BIRTH_DATE"] = 'errorFocus'
                errors["BIRTH_DATE"] = <FormattedMessage id="oldregisnoyetsevenyears" />
            }
        }
        if(!fields["GENDER"]){
            formIsValid = false;
            errorsFocus["GENDER"] = 'errorFocus'
            errors["GENDER"] = <FormattedMessage id="plsselectgender" />
        }
        this.setState({
            errors: errors,
            errorsFocus: errorsFocus
        });

        return formIsValid;
      }

    render(){
        var yearnow = new Date().getFullYear()
        var start = yearnow - parseInt(100);
        var setYear = [];
        var selectTime = new Date()
        var minDate = new Date();
        minDate.setFullYear(minDate.getFullYear() - 100);
        var date_Config = dateConfigTH;
        var Register = messages[this.state.language].Register
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].Register}
            >
                <div>
                    <div className="bg-body-regis py-3">
                        <div className="">
                            <div className="container-fluid">
                                <form onSubmit={e => this.handleSubmit(e)}>
                                    <div>
                                        <label>
                                            <span className="label-validate">*</span> 
                                            <span className="font-header-label pl-2"><FormattedMessage id="phoneno" /></span>
                                        </label>
                                        <div>
                                            <input 
                                                maxLength="10"
                                                className={`inputWidth form-control input_form ${this.state.errorsFocus['PHONE_NO']}`}
                                                onInput={this.maxLengthCheck}
                                                placeholder={Register.inputphoneno}
                                                type="tel"
                                                name="PHONE_NO"
                                                value={this.state.fields['PHONE_NO']}
                                                onChange={(e) => this.handChange(e)}
                                            />
                                        </div>
                                        <div className="errorMsg PHONE_NO">{this.state.errors["PHONE_NO"]}</div>
                                    </div>

                                    <div className="label-mt">
                                        <label>
                                            <span className="label-validate">*</span> 
                                            <span className="font-header-label pl-2"><FormattedMessage id="email" /></span>
                                        </label>
                                        <div>
                                            <input 
                                                className={`inputWidth form-control input_form ${this.state.errorsFocus['EMAIL']}`}
                                                placeholder={Register.inputemail}
                                                name="EMAIL"
                                                disabled={this.state.dpemail}
                                                value={(this.state.fields['EMAIL'])}
                                                onChange={(e) => this.handChange(e)}
                                            />
                                        </div>
                                        <div className="errorMsg EMAIL">{this.state.errors["EMAIL"]}</div>
                                    </div>
                                    <div className="label-mt">
                                        <label>
                                            <span className="label-validate">*</span> 
                                            <span className="font-header-label pl-2"><FormattedMessage id="password" /></span>
                                        </label>
                                        <div>
                                            <input 
                                                type="password"
                                                className={`inputWidth form-control input_form ${this.state.errorsFocus['PASSWORD']}`}
                                                placeholder={Register.inputpassword}
                                                name="PASSWORD"
                                                value={this.state.fields['PASSWORD']}
                                                onChange={(e) => this.handChange(e)}
                                            />
                                        </div>
                                        <div className="errorMsg PASSWORD">{this.state.errors["PASSWORD"]}</div>
                                    </div>
                                    <div className="label-mt">
                                        <label>
                                            <span className="label-validate">*</span> 
                                            <span className="font-header-label pl-2"><FormattedMessage id="inputpasswordagain" /></span>
                                        </label>
                                        <div>
                                            <input
                                                type="password"
                                                className={`inputWidth form-control input_form ${this.state.errorsFocus['CHECKPASSWORD']}`}
                                                placeholder={Register.inputpasswordagain}
                                                name="CHECKPASSWORD"
                                                value={this.state.fields['CHECKPASSWORD']}
                                                onChange={(e) => this.handChange(e)}
                                            />
                                        </div>
                                        <div className="errorMsg CHECKPASSWORD">{this.state.errors["CHECKPASSWORD"]}</div>
                                    </div>
                                    <div className="header-label"><FormattedMessage id="infodata" /></div>
                                    <div className="label-mt">
                                        <label className="font-header-label"><span className="label-validate">*</span><FormattedMessage id="fisrtname" /> </label>
                                        <div>
                                            <input 
                                                className={`inputWidth form-control input_form ${this.state.errorsFocus['FIRSTNAME']}`}
                                                placeholder={Register.inputfirstname}
                                                name="FIRSTNAME"
                                                value={this.state.fields['FIRSTNAME']}
                                                onChange={(e) => this.handChange(e)}
                                            />
                                        </div>
                                        <div className="errorMsg FIRSTNAME">{this.state.errors["FIRSTNAME"]}</div>
                                    </div>
                                    <div className="label-mt">
                                        <label>
                                            <span className="label-validate">*</span> 
                                            <span className="font-header-label pl-2"><FormattedMessage id="lastname" /></span>
                                        </label>
                                        <div>
                                            <input 
                                                className={`inputWidth form-control input_form ${this.state.errorsFocus['LASTNAME']}`}
                                                placeholder={Register.inputlastname}
                                                name="LASTNAME"
                                                value={this.state.fields['LASTNAME']}
                                                onChange={(e) => this.handChange(e)}
                                            />
                                        </div>
                                        <div className="errorMsg LASTNAME">{this.state.errors["LASTNAME"]}</div>
                                    </div>
                                    <div className="label-mt">
                                        <label>
                                            <span className="label-validate">*</span> 
                                            <span className="font-header-label pl-2"><FormattedMessage id="birthday" /></span>
                                        </label>
                                        <div>
                                            <input
                                                className={`inputWidth form-control input_form ${this.state.errorsFocus['BIRTH_DATE']}`}
                                                placeholder={Register.inputbirthday}
                                                type="date" 
                                                id="date"
                                                name="BIRTH_DATE"
                                                value={this.state.fields['BIRTH_DATE']}
                                                // selected={this.state.startDate}
                                                onChange={(e) => this.handChange(e)}
                                            />
                                        </div>
                                        <div className="errorMsg BIRTH_DATE">{this.state.errors["BIRTH_DATE"]}</div>
                                    </div>
                                    <div className="label-mt">
                                        <label>
                                            <span className="label-validate">*</span> 
                                            <span className="font-header-label pl-2"><FormattedMessage id="gender" /></span>
                                        </label>
                                        <div>
                                            <select 
                                                className={`inputWidth form-control input_form ${this.state.errorsFocus['GENDER']}`}
                                                placeholder={Register.selectgender}
                                                name="GENDER"
                                                value={this.state.fields['GENDER']}
                                                onChange={(e) => this.handChange(e)}
                                            >
                                                <option value="4">{Register.selectgender}</option>
                                                <option value="1">{Register.selectgenderboy}</option>
                                                <option value="2">{Register.selectgendergirl}</option>
                                                <option value="3">{Register.selectgenderother}</option>
                                            </select>
                                        </div>
                                        <div className="errorMsg GENDER">{this.state.errors["GENDER"]}</div>
                                    </div>
                                    <div className="btn_nextstep">
                                        <button 
                                            type="submit"
                                            className="btn btn-secondary btn-block btn-check"
                                        >
                                            <FormattedMessage id="btnconfirm" />
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    {this.state.modal}
                </div>
            </IntlProvider>
        )
    }
}

