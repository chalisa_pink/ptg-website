import React from 'react';
import '../../register/register.css';
import { registerAction } from '../../../_actions/registerActions'; 
import SweetAlert from "react-bootstrap-sweetalert";
import { IntlProvider, FormattedMessage } from "react-intl";
import intlMessageEN from "../../../translations/en.json";
import intlMessageTH from "../../../translations/th.json";
const messages = {
    th: intlMessageTH,
    en: intlMessageEN
};
const publicIp = require('public-ip');
export class Regisweb extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            language: localStorage.getItem('language') == null ? "th" : localStorage.getItem('language'),
            errors: {},
            errorsFocus:{},
            setActive: "active",
            fields : {},
            show:false,
            modal:null,
            showLoading:"none",
        };
    }

    componentDidMount(){
        (async () => {
            this.setState({ ipaddress : await publicIp.v4() })
            // console.log(await publicIp.v4());
        })();
    }

    maxLengthCheck = (object) => {
        if (object.target.value.length > object.target.maxLength) {
            object.target.value = object.target.value.slice(0, object.target.maxLength)
        }
    }

    handleChange(e,type){
        let { fields , errors , errorsFocus } = this.state;
        if(e.target.name == "MAIL"){
            fields['MAIL'] = e.target.value
        }
        if(e.target.name == "TEL"){
            const re = /^[0-9\b]+$/;
            if(e.target.value == "" || re.test(e.target.value)){
                fields[e.target.name] = e.target.value;
                errors[e.target.name] = null;
            }else{
                errors[e.target.name] = <FormattedMessage id="plsinputnumber" />
                errorsFocus[e.target.name] = ''
                e.preventDefault();
            }
        }else{
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
        }
        this.setState({ 
            setValue : e.target.value,
            setType : type ,
            errors, 
            fields
        })
    }

    btnActive(){
        this.setState({ setActive: ""})
    }

    submitData(value,type,lang){
        if(this.validateForm(type)){
            this.checkPhoneorEmail(value,type,lang);
        }else{
            // console.log(false)
        }
    }

    validateForm(type){
        let fields = this.state.fields;
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;

        if(type === 0){
            if(!fields['EMAIL']){
                formIsValid = false;
                errors["EMAIL"] = <FormattedMessage id="plsinputdata" />
                errorsFocus["EMAIL"] = 'errorFocus'
            }else{
                var validEmail = this.validateEmail(fields["EMAIL"]);
                if (validEmail === false) {
                  formIsValid = false;
                  errorsFocus["EMAIL"] = 'errorFocus'
                  errors["EMAIL"] = <FormattedMessage id="formatemailfaild" />
                }
            }
        }else{
            if(!fields['TEL']){
                formIsValid = false;
                errors["TEL"] = <FormattedMessage id="plsinputdata" />
                errorsFocus["TEL"] = 'errorFocus'
            }
        }
        this.setState({
            errors: errors,
            errorsFocus: errorsFocus
          });
      
          return formIsValid;
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    checkPhoneorEmail(value,type,lang){
        registerAction.checkEmailorPhone(type,value).then(e => {
            if(e.data.isSuccess === true && type === 1){
                this.props.history.push({
                    pathname: `${process.env.PUBLIC_URL}/register/otp`,
                    state: {
                        data: value,
                        socialid : '',
                        socialType: '',
                        udId:this.state.ipaddress,
                        playerId : 'WEBSITE',
                        email:'',
                        phone:'',
                    }
                })
            }else if(e.data.isSuccess === true && type === 0){
                this.props.history.push({
                    pathname:`${process.env.PUBLIC_URL}/register/web`,
                    state:{
                        type:0,
                        data:value,
                        socialid : '',
                        socialType: '',
                        udId:this.state.ipaddress,
                        playerId : 'WEBSITE',
                        email:'',
                        phone:'',
                    }
                })
            }else{
                if(lang === 'th'){
                    this.modalAlert(e.data.errMsg);
                }else{
                    this.modalAlert(e.data.errMsg_en);
                }
            }
        })
    }

    modalAlert(msg){
        var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
        alert = (
            <SweetAlert
              custom
              showCloseButton
              closeOnClickOutside={false}
              focusConfirmBtn={false}
              title=""
              customIcon={imgPopup}
              showConfirm={false}
              showCancelButton
              onCancel={() => this.handleChoice(false)}
              onConfirm={() => this.handleChoice(true)}
            >
              <div className="iconClose" onClick={() => this.handleChoice(false)}></div>
              <div className="fontSizeCase">{msg}</div>
            </SweetAlert>
            // <SweetAlert
            //     custom
            //     showCloseButton
            //     closeOnClickOutside={false}
            //     focusConfirmBtn={false}
            //     title=""
            //     showConfirm={false}
            //     showCancelButton
            //     onCancel={() => this.handleChoice(false)}
            //     onConfirm={() => this.handleChoice(true)}

            // >
            //     <div className="iconClose" onClick={() => this.handleChoice(false)}>
            //         {/* <i className="fas fa-times sizeCloseBtn"></i> */}
            //         <img style={{width:"25%"}} src={`${process.env.PUBLIC_URL}/images/cancel.png`} />
            //         <div className="my-3 alert-dismiss">
            //             {msg}
            //         </div>
            //     </div>
            // </SweetAlert>
        )
        this.setState({ show : true , modal : alert })
    }

    handleChoice(choice) {
        if(choice === false){
            this.setState({ show: false , modal: null })
        }else{
            this.setState({ show:true , modal:alert })
        }
    }

    render(){
        return(
            <IntlProvider
                locale={this.state.language}
                messages={messages[this.state.language].Regisweb}
            >
                <div>
                    <div className="loadingGif mx-auto" style={{ display: this.state.showLoading}}>
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                    <div style={{ display: (this.state.showLoading === 'block' ? 'none' : 'block') }}>
                        <div className="bg-ptmaxcard-content">
                            <div className="ptmaxcard-content">
                                <div className="row py-5">
                                    <div className="col-lg-7 col-md-12 mx-auto">
                                        <div className="web-register-card-body">
                                            {/* <ul className="nav nav-box text-center">
                                                <li className={this.state.setActive}>
                                                    <a 
                                                        data-toggle="tab" 
                                                        href="#tel"
                                                    >
                                                        เบอร์โทรศัพท์
                                                    </a>
                                                </li>
                                                <li className={this.state.mailActive}>
                                                    <a 
                                                        data-toggle="tab" 
                                                        href="#mail"
                                                        onClick={() => this.btnActive()}
                                                    >
                                                        อีเมล
                                                    </a>
                                                </li>
                                            </ul> */}
                                            <ul className="nav nav-web-register">
                                                <li className="nav-item">
                                                    <a className="nav-link web-register-tab active" data-toggle="pill" href="#tel"><FormattedMessage id="phoneno" /></a>
                                                </li>
                                                <li className="nav-item ">
                                                    <a className="nav-link web-register-tab" data-toggle="pill" href="#mail"><FormattedMessage id="email" /></a>
                                                </li>
                                            </ul>
                                            <div className="tab-content text-center mt-4">
                                                <div id="tel" className="tab-pane active">
                                                    <div className="header-title"><FormattedMessage id="phoneno" /></div>
                                                    <input 
                                                        maxLength="10"
                                                        type="tel"
                                                        name="TEL"
                                                        id="phoneno"
                                                        onInput={this.maxLengthCheck}
                                                        onChange={(e) => this.handleChange(e,1)}
                                                        className={`form-control input_editprofile ${this.state.errorsFocus['TEL']}`}
                                                        value={this.state.fields['TEL']}
                                                    />
                                                    <div className="errorMsg TEL">{this.state.errors["TEL"]}</div>
                                                </div>
                                                <div id="mail" className="tab-pane">
                                                    <div className="header-title"><FormattedMessage id="email" /></div>
                                                    <input 
                                                        type="email"
                                                        name="EMAIL"
                                                        onChange={(e) => this.handleChange(e,0)}
                                                        pattern=".+@globex.com" 
                                                        size="30" 
                                                        required
                                                        className={`form-control input_editprofile ${this.state.errorsFocus['EMAIL']}`}
                                                        value={this.state.fields['EMAIL']}
                                                    />
                                                    <div className="errorMsg EMAIL">{this.state.errors["EMAIL"]}</div>
                                                </div>
                                                <div className="mt-5 text-center">
                                                    <button className="btn btn-block btn-web-register" onClick={() => this.submitData(this.state.setValue,this.state.setType,this.state.language)}><FormattedMessage id="btnconfirm" /></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                {this.state.modal}
                    
            </div>
            </IntlProvider>
        )
    }
}

