import React from 'react';
import { Switch , Route } from 'react-router-dom';
import { home , adsdetail, BannerDetail } from '../page/home';
import { Loginweb } from '../page/loginweb'; 
import { News, NewsDetail } from '../page/news';
import { Privileges , Detail, PrivilegesQR, PrivilegesBranch, PrivilegesBranchSearch, PrivilegesBranchSuccess, PrivilegesAddress, PrivilegesAddressSuccess  } from '../page/privileges';
import { Location2, Location , DetailMap , listShop , Mainlocation, SearchStation, listPromotion , listPartner , PartnerList , partnerbyid , GoogleMapContainer, ShopPartner, PromotionList, ListStation, ListStationNear} from '../page/location'; 

import {Regisweb , Regishome , otp , Register , Edit , forgot , changepass , forgotp ,newpass,newpassotp ,otpedit } from '../page/register';
import NotFoundPages from '../page/404';
import Header from "../_pagebuilder/Header";
import Footer from "../_pagebuilder/Footer";
import { Problem , myMaxCard , History , editprofile , otpeditprfilecard , registemaxcard , Success , regisotp , regisnocard , syncotp, editprofilemore, PTsidebar, condition, question, historycardList, historycardDetailRedeem, historycardDetailQR, historycardDetailReceiver } from '../page/ptmax';
import { PrivacyPolicy } from '../page/privacypolicy';
import { CookiePolicy } from '../page/cookiepolicy'
import { Base_API } from "../_constants/matcher";

var language = localStorage.getItem('lang');
export default () => (
    <Route>
        <div>
        <Header />
                <Switch>
                    <Route exact path={`${Base_API.pathWeb}/`} component={home}/>
                    <Route exact path={`${Base_API.pathWeb}/website`} component={home}/>
                    <Route exact path={`${Base_API.pathWeb}/home`} component={home}/>
                    <Route exact path={`${Base_API.pathWeb}/bannerdetail`} component={BannerDetail}/>
                    <Route exact path={`${Base_API.pathWeb}/adsdetail`} component={adsdetail} />

                    <Route exact path={`${Base_API.pathWeb}/news`} component={News} />
                    <Route exact path={`${Base_API.pathWeb}/news/detail/:type/:id`} component={NewsDetail} />

                    <Route exact path={`${Base_API.pathWeb}/register`} component={Regisweb} />
                    <Route exact path={`${Base_API.pathWeb}/register/home`} component={Regishome} />
                    <Route exact path={`${Base_API.pathWeb}/register/otp`} component={otp} />
                    <Route exact path={`${Base_API.pathWeb}/register/web`} component={Register} />
                    <Route exact path={`${Base_API.pathWeb}/editprofile`} component={Edit} />
                    <Route exact path={`${Base_API.pathWeb}/forgotpassword`} component={forgot} />
                    <Route exact path={`${Base_API.pathWeb}/forgotpassword/changpassword`} component={changepass} />
                    <Route exact path={`${Base_API.pathWeb}/forgotpassword/otp`} component={forgotp} />
                    <Route exact path={`${Base_API.pathWeb}/newpassword`} component={newpass} />
                    <Route exact path={`${Base_API.pathWeb}/newpassotp`} component={newpassotp}/>
                    <Route exact path={`${Base_API.pathWeb}/edit/otp`} component={otpedit} />

                    <Route exact path={`${Base_API.pathWeb}/privileges`} component={Privileges} />
                    <Route exact path={`${Base_API.pathWeb}/privileges/detail/:id`} component={Detail} />
                    <Route exact path={`${Base_API.pathWeb}/privileges/qr`} component={PrivilegesQR} />
                    <Route exact path={`${Base_API.pathWeb}/privileges/branch`} component={PrivilegesBranch} />
                    <Route exact path={`${Base_API.pathWeb}/privileges/branch/search`} component={PrivilegesBranchSearch} />
                    <Route exact path={`${Base_API.pathWeb}/privileges/branch/success`} component={PrivilegesBranchSuccess} />
                    <Route exact path={`${Base_API.pathWeb}/privileges/address`} component={PrivilegesAddress} />
                    <Route exact path={`${Base_API.pathWeb}/privileges/address/success`} component={PrivilegesAddressSuccess} />

                    <Route exact path={`${Base_API.pathWeb}/location`} component={Location} />
                    <Route exact path={`${Base_API.pathWeb}/location/detail/:shopId/:lat/:lng`} component={DetailMap} />
                    <Route exact path={`${Base_API.pathWeb}/location/listPartner`} component={listPartner} />
                    <Route exact path={`${Base_API.pathWeb}/location/listShop`} component={listShop} />
                    {/* <Route exact path={`${Base_API.pathWeb}/Mainlocation`} component={Mainlocation} /> */}
                    <Route exact path={`${Base_API.pathWeb}/location/partnerlist`} component={PartnerList} />
                    <Route exact path={`${Base_API.pathWeb}/location/partner/:id/:lat/:lng`} component={partnerbyid} />
                    <Route exact path={`${Base_API.pathWeb}/mappartner`} component={GoogleMapContainer}/>
                    <Route exact path={`${Base_API.pathWeb}/shoppartner/promotion/:id`} component={PromotionList}/>
                    <Route exact path={`${Base_API.pathWeb}/shoppartner`} component={ShopPartner}/>
                    
                    <Route exact path={`${Base_API.pathWeb}/Mainlocation`} component={SearchStation} />
                    <Route exact path={`${Base_API.pathWeb}/ListPromotion/:type`} component={listPromotion} />
                    <Route exact path={`${Base_API.pathWeb}/ListStationNear`} component={ListStationNear} />
                    <Route exact path={`${Base_API.pathWeb}/ListStation/:id`} component={ListStation} />

                    {/* <Route exact path={`${Base_API.pathWeb}/location2`} component={Location2} /> */}

                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard/report`} component={Problem} /> 
                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard`} component={myMaxCard} />
                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard/history`} component={History} />
                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard/editprofile`} component={editprofile} />
                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard/editprofilemore`} component={editprofilemore} />
                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard/otpeditprfile`} component={otpeditprfilecard} />
                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard/register`} component={registemaxcard} />
                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard/success`} component={Success} />
                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard/register/otp`} component={regisotp} />
                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard/register/nocard`} component={regisnocard} />
                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard/sync/otp`} component={syncotp} />
                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard/PTsidebar`} component={PTsidebar} /> 
                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard/condition`} component={condition} /> 
                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard/question`} component={question} /> 
                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard/historycard`} component={historycardList} />
                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard/historycard/receiver`} component={historycardDetailReceiver} />
                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard/historycard/redeem`} component={historycardDetailRedeem} />
                    <Route exact path={`${Base_API.pathWeb}/PTmaxcard/historycard/qr`} component={historycardDetailQR} />

                    <Route exact path={`${Base_API.pathWeb}/PrivacyPolicy`} component={PrivacyPolicy} /> 
                    <Route exact path={`${Base_API.pathWeb}/CookiePolicy`} component={CookiePolicy} /> 

                    {/* <Route exact path={`${Base_API.pathWeb}/login`} component={Loginweb}/> */}
                    <Route component={NotFoundPages} />
                </Switch>
            <Footer />
        </div>
    </Route>
)


